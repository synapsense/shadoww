#!/usr/bin/env bash

DEPS_DIR=deps
# find the erlang lib dir to index all files there
ERL_LIB_DIR=`erl -noshell -eval 'io:fwrite("~s~n", [code:lib_dir()]).' -s init stop`

if [[ -x `which exctags` ]]; then
	CTAGS=`which exctags`
elif [[ -x `which ctags-exuberant` ]]; then
	CTAGS=`which ctags-exuberant`
fi

rm tags
find $ERL_LIB_DIR -name '*.*rl' > cscope.files
find apps -name '*.*rl' >> cscope.files
find $DEPS_DIR -name '*.*rl' >> cscope.files
$CTAGS --file-scope=no --languages=erlang -L cscope.files &
./erlcscope . &
wait
rm cscope.files

