%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(es_ac_sync_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

%% common test callbacks
suite() ->
    TestCfg = filename:join([code:lib_dir(es_ac_sync), "test", "app_env.config"]),
    ct:add_config(ct_config_plain, TestCfg),
    [
     {timetrap, {minutes, 1}},
     {require, env}
    ].

init_per_suite(Config) ->
    {ok, Started} = start_app(),
    room_manager:isolated(false),
    process_cache:isolated(false),
    [{started, Started} | Config].

end_per_suite(Config) ->
    stop_app(?config(started, Config)),
    TestCfg = filename:join([code:lib_dir(es_ac_sync), "test", "app_env.config"]),
    ct:remove_config(ct_config_plain, TestCfg).

init_per_group(_G, Config) ->
    Config.

end_per_group(_G, _Config) ->
    ok.

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

groups() -> [].

all() ->
    [
     sync_saved,
     room_created,
     crahs_created,
     resync1,
     crah_deleted,
     resync2,
     crah_restored
    ].

%% test cases

sync_saved(_C) ->
    LibDir = code:lib_dir(es_ac_sync),
    ModelDump = filename:join([LibDir, "test", "es_model1.dump"]),
    es_sync_proc:sync_to_dumpfile(ModelDump),
    timer:sleep(1000).

room_created(_C) ->
    ?assertEqual([room_id()], ac_api:list_rooms()).

crahs_created(_C) ->
    ?assertEqual(lists:sort(crah_ids()), lists:sort(ac_api:list_crahs(room_id()))).

resync1(_C) ->
    LibDir = code:lib_dir(es_ac_sync),
    ModelDump = filename:join([LibDir, "test", "es_model2.dump"]),
    es_sync_proc:sync_to_dumpfile(ModelDump),
    timer:sleep(1000).

crah_deleted(_C) ->
    ?assertEqual([room_id()], ac_api:list_rooms()),
    ?assertEqual(lists:sort(crah_ids() -- [<<"CRAC:302">>]), lists:sort(ac_api:list_crahs(room_id()))).

resync2(_C) ->
    LibDir = code:lib_dir(es_ac_sync),
    ModelDump = filename:join([LibDir, "test", "es_model1.dump"]),
    es_sync_proc:sync_to_dumpfile(ModelDump),
    timer:sleep(1000).

crah_restored(_C) ->
    ?assertEqual([room_id()], ac_api:list_rooms()),
    ?assertEqual(lists:sort(crah_ids()), lists:sort(ac_api:list_crahs(room_id()))).

%% utility functions

room_id() ->
    <<"ROOM:205">>.

crah_ids() ->
    [
     <<"CRAC:218">>,
     <<"CRAC:239">>,
     <<"CRAC:260">>,
     <<"CRAC:281">>,
     <<"CRAC:302">>,
     <<"CRAC:323">>,
     <<"CRAC:344">>,
     <<"CRAC:365">>,
     <<"CRAC:386">>
    ].

start_app() ->
    {ok, _} = net_kernel:start([test, longnames]),

    Env = ct:get_config(env),

    ct:pal("app loading results: ~p", [[{AppName, application:load(AppName)} || {AppName, _} <- Env]]),
    [[application:set_env(AppName, K, V) || {K,V} <- Tuples] || {AppName, Tuples} <- Env],

    DummyNode = list_to_atom("dummy@" ++ net_adm:localhost()),

    application:set_env(active_control, loadbalance_nodes, [node(), DummyNode]),

    LibDir = code:lib_dir(es_ac_sync),
    Defaults = filename:join([LibDir, "test", "cluster_conf.def"]),
    Over = filename:join([LibDir, "test", "cluster_conf.over"]),

    application:set_env(cluster_config, nodes, [node(), DummyNode]),
    application:set_env(cluster_config, defaults_file, Defaults),
    application:set_env(cluster_config, overrides_file, Over),

    YawsConf = filename:join([LibDir, "test", "yaws.conf"]),
    application:set_env(yaws, conf, YawsConf),

    [ct:pal("~p env: ~p", [AppName, application:get_all_env(AppName)]) || {AppName, _} <- Env],

    application:ensure_all_started(es_ac_sync).

stop_app(Started) ->
    [application:stop(App) || App <- lists:reverse(Started)],
    net_kernel:stop().

