
-define(ALERT_TYPE_CRAC_HIGH_HEAD_PRESSURE,  <<"High Head Pressure">>).
-define(ALERT_TYPE_CRAC_LOW_SUCTION_PRESSURE, <<"Low Suction Pressure">>).
-define(ALERT_TYPE_CRAC_COMPRESSOR_OVERLOAD, <<"Compressor Overload">>).
-define(ALERT_TYPE_CRAC_SHORT_CYCLE, <<"Short Cycle">>).
-define(ALERT_TYPE_CRAC_GENERAL, <<"CRAC General Alert">>).

-define(ALERT_TYPE_CRAH_GENERAL, <<"CRAH General Alert">>).

-define(ALERT_TYPE_CHANGE_FILTERS, <<"Change Filters">>).

-define(ALERT_TYPE_VFD_OVER_CURRENT, <<"Over Current">>).
-define(ALERT_TYPE_VFD_OVER_VOLTAGE, <<"Over Voltage">>).
-define(ALERT_TYPE_VFD_UNDER_VOLTAGE, <<"Under Voltage">>).
-define(ALERT_TYPE_VFD_OVER_TEMPERATURE, <<"Over Temperature">>).
-define(ALERT_TYPE_VFD_GENERAL, <<"VFD General Alert">>).

-define(ALERT_TYPE_CRITICAL, <<"Active Control Critical Alert">>).
-define(ALERT_TYPE_COMMUNICATION, <<"Active Control Communication Alert">>).

% For completeness so that all user-visible strings are outside of code modules
-define(ALERT_NAME_CANNOT_CONNECT_GATEWAY, <<"Cannot connect to gateway">>).
-define(ALERT_NAME_GATEWAY_TIMEOUT, <<"Modbus gateway timing out">>).
-define(ALERT_NAME_DEVICE_NOT_RESPONDING, <<"Device not responding">>).
-define(ALERT_NAME_DEVICE_ERROR, <<"Device responding with error">>).
-define(ALERT_NAME_DATA_CORRUPTION, <<"Modbus data corruption">>).
-define(ALERT_NAME_DEVICE_OFFLINE, <<"Device offline">>).
-define(ALERT_NAME_INTERNAL_FAILURE, <<"ActiveControl internal failure">>).

-define(ALERT_NAME_IO_FAILURE, <<"Internal I/O failure">>).
-define(ALERT_NAME_CRAC_HIGH_HEAD_PRESSURE,  <<"High Head Pressure">>).
-define(ALERT_NAME_CRAC_LOW_SUCTION_PRESSURE, <<"Low Suction Pressure">>).
-define(ALERT_NAME_CRAC_COMPRESSOR_OVERLOAD, <<"Compressor Overload">>).
-define(ALERT_NAME_CRAC_SHORT_CYCLE, <<"Short Cycle">>).
-define(ALERT_NAME_CHANGE_FILTERS, <<"Change Filters">>).
-define(ALERT_NAME_VFD_OVER_CURRENT, <<"Over Current">>).
-define(ALERT_NAME_VFD_OVER_VOLTAGE, <<"Over Voltage">>).
-define(ALERT_NAME_VFD_UNDER_VOLTAGE, <<"Under Voltage">>).
-define(ALERT_NAME_VFD_OVER_TEMPERATURE, <<"Over Temperature">>).
-define(ALERT_NAME_CRAH_GENERAL, <<"CRAH General Alert">>).
-define(ALERT_NAME_CRAC_GENERAL, <<"CRAC General Alert">>).
-define(ALERT_NAME_VFD_GENERAL, <<"VFD General Alert">>).
-define(ALERT_NAME_BAD_GEOM, <<"Bad Room Geometry">>).

% All message defines end with the number of parameters in the message
%'$name$' - Unable to reach Modbus gateway [%1$s]
-define(ALERT_MSG_GW_UNREACHABLE_1, <<"$localize(alert_msg_gw_unreachable_1,\"~s\")$">>).
%'$name$' - Modbus gateway is not responding [%1$s]
-define(ALERT_MSG_GW_NOT_RESPONDING_1, <<"$localize(alert_msg_gw_not_responding_1,\"~s\")$">>).
%'$name$' - Device %1$s on gateway '%2$s' is not responding
-define(ALERT_MSG_DEVICE_NOT_RESPONDING_2, <<"$localize(alert_msg_device_not_responding_2,\"~w\",\"~s\")$">>).
%'$name$' - Error occurred on device %1$s on gateway [%2$s]: %3$s
-define(ALERT_MSG_MODBUS_ERROR_CODE_3, <<"$localize(alert_msg_modbus_error_code_3,\"~w\",\"~s\",\"~s\")$">>).
%'$name$' - Error occurred on device %1$s on gateway [%2$s]: Corrupted packet(s) received
-define(ALERT_MSG_MODBUS_DATA_CORRUPTION_2, <<"$localize(alert_msg_modbus_data_corruption_2,\"~w\",\"~s\")$">>).
%'$name$' - Device is disconnected due to end-to-end communication failure
-define(ALERT_MSG_DEVICE_COMM_INTERRUPTED_0, <<"$localize(alert_msg_device_comm_interrupted_0)$">>).
%'$name$' - CRAH is offline
-define(ALERT_MSG_CRAH_OFFLINE_0, <<"$localize(alert_msg_crah_offline_0)$">>).
%'$name$' - Device is not online. Reverting to fallback setting [%1$s] in manual mode.
-define(ALERT_MSG_DEVICE_OFFLINE_2, <<"$localize(alert_msg_device_offline_2,\"~ts\",\"$conv(~.2f,percent)$\")$">>).
%'$name$' - Critical I/O failure.  Check controller log. %1$s
-define(ALERT_MSG_IO_FAILURE_1, <<"$localize(alert_msg_io_failure_1,\"~s\")$">>).
%High Head Pressure on [$name$] compressor %1$s.
-define(ALERT_MSG_HIGH_HEAD_PRESSURE_1, <<"$localize(alert_msg_high_head_pressure_1,\"~s\")$">>).
%Low Suction Pressure on [$name$] compressor %1$s.
-define(ALERT_MSG_LOW_SUCTION_PRESSURE_1, <<"$localize(alert_msg_low_suction_pressure_1,\"~s\")$">>).
%Compressor Overload on [$name$] compressor %1$s.
-define(ALERT_MSG_COMPRESSOR_OVERLOAD_1, <<"$localize(alert_msg_compressor_overload_1,\"~s\")$">>).
%Short Cycle on [$name$] compressor %1$s.
-define(ALERT_MSG_SHORT_CYCLE_1, <<"$localize(alert_msg_short_cycle_1,\"~s\")$">>).
%Change Filters on [$name$].
-define(ALERT_MSG_CHANGE_FILTERS_0, <<"$localize(alert_msg_change_filters_0)$">>).
%Over Current on [$name$].
-define(ALERT_MSG_OVER_CURRENT_0, <<"$localize(alert_mgs_over_current_0)$">>).
%Over Voltage on [$name$].
-define(ALERT_MSG_OVER_VOLTAGE_0, <<"$localize(alert_msg_over_voltage_0)$">>).
%Under Voltage on [$name$].
-define(ALERT_MSG_UNDER_VOLTAGE_0, <<"$localize(alert_msg_under_voltage_0)$">>).
%Over Temperature on [$name$].
-define(ALERT_MSG_OVER_TEMPERATURE_0, <<"$localize(alert_msg_over_temperature_0)$">>).
%General Alert on [$name$].
-define(ALERT_MSG_GENERAL_0, <<"$localize(alert_msg_general_0)$">>).
-define(ALERT_MSG_EVAP_SENSORS_NOT_REPORTING_3, <<"$localize(alert_msg_evap_sensors_not_reporting_2,\"~b\",\"$conv(~.2f,~s)$\")$">>).
-define(ALERT_MSG_NEED_FAN_RAISE_REACHED_MAX_2, <<"$localize(alert_msg_need_fan_raise_reached_max_1,\"$conv(~.2f,~s)$\")$">>).
-define(ALERT_MSG_NEED_FAN_RAISE_MANUAL_0, <<"$localize(alert_msg_need_fan_raise_manual_0)$">>).
-define(ALERT_MSG_REACHED_MAX_LOW_TEMP_6, <<"$localize(alert_msg_reached_max_low_temp_3,\"$conv(~.2f,~s)$\",\"$conv(~.2f,~s)$\",\"$conv(~.2f,~s)$\")$">>).

-define(ALERT_MSG_BAD_ROOM_GEOMETRY_0, <<"$localize(alert_msg_bad_room_geometry_0)$">>).

