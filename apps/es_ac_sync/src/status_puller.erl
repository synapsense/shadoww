
-module(status_puller).

-behaviour(gen_server).

%% API
-export([
         start_link/0,
         collect_status/0
        ]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-record(state, {
          frequency :: integer(),
          collector_ref :: reference()
         }).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
    {ok, Frequency} = cluster_config:get_value([es_ac_sync], update_freq),
    erlang:send_after(Frequency, self(), collect_status),
    {ok, #state{frequency=Frequency}}.

handle_call(Request, _From, State) ->
    lager:warning("unexpected handle_call(~p)", [Request]),
    {reply, huh, State}.

handle_cast(Msg, State) ->
    lager:warning("unexpected handle_cast(~p)", [Msg]),
    {noreply, State}.

handle_info(collect_status, State) ->
    lager:debug("Kicking off a status collector"),
    {_Pid, Mref} = spawn_monitor(?MODULE, collect_status, []),
    {noreply, State#state{collector_ref = Mref}};
handle_info({'DOWN', Mref, process, _, Info},
            State = #state{
                       frequency=Freq,
                       collector_ref=Cref
                      }) when Mref =:= Cref ->
    lager:debug("Status collector finished with: ~p", [Info]),
    erlang:send_after(Freq, self(), collect_status),
    {noreply, State#state{collector_ref=undefined}};
handle_info(Info, State) ->
    lager:warning("unexpected handle_info(~p)", [Info]),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

collect_status() ->
    Ids = room_manager:rooms_on_node(node()),
    [[driver_status(CrahId) || CrahId <- crah_ids(RoomId)] || RoomId <- Ids].

crah_ids(RoomId) ->
    case room_manager:room_type(RoomId) of
        {ok, remote_control_room} ->
            remote_control_room:list_crahs(RoomId);
        {ok, raised_floor_room} ->
            raised_floor_room:list_crahs(RoomId)
    end.

driver_status(CrahId) ->
    lager:debug("writing status for CRAH: ~p", [CrahId]),
    pressure_driver_status(CrahId),
    temp_driver_status(CrahId).

pressure_driver_status(CrahId) ->
    #vfd_status{setpoint=Setpoint,kw=Kw} = driver_lib:call(driver_lib:proc_key(CrahId, pressure, driver), get_status, []),
    [DriverObject] = esapi:get_children(esapi:to(CrahId), <<"CONTROLOUT_VFD">>),
    ok = esapi:set_all_properties_values(DriverObject, [{<<"setpoint">>, Setpoint}, {<<"kw">>, Kw}]).

temp_driver_status(CrahId) ->
    TStatus = driver_lib:call(driver_lib:proc_key(CrahId, temperature, driver), get_status, []),
    case TStatus of
        #crah_status{setpoint=Setpoint,capacity=Capacity,rat=Rat,sat=Sat} ->
            [DriverObject] = esapi:get_children(esapi:to(CrahId), <<"CONTROLOUT_CRAH">>),
            esapi:set_all_properties_values(DriverObject,
                                            [
                                             {<<"setpoint">>, Setpoint},
                                             {<<"capacity">>, Capacity},
                                             {<<"deviceRat">>, Rat},
                                             {<<"deviceSat">>, Sat}
                                            ]);
        #crac_status{setpoint=Setpoint,capacity=Capacity,stages=Stages,rat=Rat,sat=Sat} ->
            [DriverObject] = esapi:get_children(esapi:to(CrahId), <<"CONTROLOUT_CRAC">>),
            esapi:set_all_properties_values(DriverObject,
                                            [
                                             {<<"setpoint">>, Setpoint},
                                             {<<"capacity">>, Capacity},
                                             {<<"compressorStages">>, Stages},
                                             {<<"deviceRat">>, Rat},
                                             {<<"deviceSat">>, Sat}
                                            ])
    end.

