%% coding: utf-8
%% vim: set et ff=unix ts=4 sw=4 tw=80 cc=80 :

%% @doc
%% Pull configuration from ES, massage, push to the AC API.
%% Listens for configuration change events from ES that trigger a
%% reconfiguration.
%% Maintains the logical "ownership" of the ES connection, so that if
%% connection is lost/regained it can trigger a change in the cluster state.
%% This process can also be controlled externally, perhaps by the cluster
%% administration tool, for administrative tasks like firmware upgrades.
%% @end

-module(config_loader).

-behaviour(locks_leader).

%% API
-export([
         start_link/0,
         get_cluster_nodes/0,
         set_cluster_nodes/1,
         add_cluster_node/1,
         remove_cluster_node/1,
         halt_sync/0,
         resume_sync/0,
         resync/0,
         cluster_state/0
        ]).

%% locks_leader callbacks
-export([
         init/1,
         handle_cast/3,
         handle_call/4,
         handle_info/3,
         handle_leader_call/4,
         handle_leader_cast/3,
         handle_DOWN/3,
         elected/3,
         surrendered/3,
         from_leader/3,
         code_change/4,
         terminate/2
        ]).

%% internal functions
-export([
        ]).


-define(SERVER, ?MODULE).
-define(RETRY_TIME, 30 * 1000).  % 30 seconds
-define(STATE, ?MODULE).
-define(REGISTER, register).

% This info should be the same across all candidates
-record(cluster_state, {
          % is the cluster isolated from both ES and sibling?
          isolated = true :: boolean(),
          % should the config loader try to sync with ES,
          % if not, loss of connection with ES does not imply isolation
          sync_active = true :: boolean()
         }).

-record(?STATE, {
           last_synced = {0, 0, 0} :: erlang:timestamp(),
           sync_timer = undefined :: reference() | undefined,
           sync_pid = undefined :: pid() | undefined,
           cluster_state = #cluster_state{} :: #cluster_state{},
           es_node = undefined :: node(),
           am_leader = false :: boolean(),
           cluster_nodes = [] :: [node()]
          }).


%%%===================================================================
%%% API
%%%===================================================================

% Start a config_loader candidate
%
% The leader will handle model change events and kick off a sync
% process when required.
%
% The config_loader is also responsible for notifying other processes when
% this controller is isolated and should perform a partial shutdown.
start_link() ->
    locks_leader:start_link(?SERVER, ?MODULE, [], []).

% Update the list of nodes that represents the "whole" cluster (not including
% ES or self) for determining what constitutes isolation.  Only updates the
% list on this candidate, not the cluster.  If the process restarts, the
% list will revert to the list in the sys.config file for the release.
get_cluster_nodes() ->
    locks_leader:call(?SERVER, get_cluster_node).
set_cluster_nodes(Nodes) ->
    locks_leader:call(?SERVER, {set_cluster_nodes, Nodes}).
-spec add_cluster_node(node()) -> [node()] | {error, [node()]}.
add_cluster_node(Node) when is_atom(Node) ->
    locks_leader:call(?SERVER, {add_cluster_node, Node}).
-spec remove_cluster_node(node()) -> [node()] | {error, [node()]}.
remove_cluster_node(Node) when is_atom(Node) ->
    locks_leader:call(?SERVER, {remove_cluster_node, Node}).

% Halt synchronization with ES.  Also implies that this node cannot be isolated.
% This affects the entire cluster, not just this node.
halt_sync() ->
    locks_leader:leader_call(?SERVER, halt_sync).

% Resume synchronization with ES.  Also implies that this node could be
% isolated, and will be if this node is not already connected to ES and/or its
% sibling.
% This affects the entire cluster, not just this node.  If a re-election (or
% coup) occurs, the newly elected leader's setting will win.
resume_sync() ->
    locks_leader:leader_call(?SERVER, resume_sync).

% Initiate an immediate ES resync
resync() ->
    locks_leader:leader_cast(?SERVER, resync).

cluster_state() ->
    locks_leader:call(?SERVER, cluster_state).

%%%===================================================================
%%% locks_leader callbacks
%%%===================================================================

init([]) ->
    ok = net_kernel:monitor_nodes(true, [{node_type, all}]),
    EsNode = esapi:node_name(),
    ClusterNodes = cluster_nodes(),
    SyncActive = sync_active(),
    lager:info("starting with EsNode: ~p, SyncActive: ~p, ClusterNodes: ~p", [EsNode, SyncActive, ClusterNodes]),
    S = #?STATE{
            cluster_state = #cluster_state{
                               isolated = true,
                               sync_active = SyncActive
                              },
            es_node = EsNode,
            cluster_nodes = ClusterNodes
           },
    net_kernel:connect_node(EsNode),
    spawn(create_alert_types, create_all, []), % fire 'n forget
    {ok, S}.

%% @spec elected(State::state(), I::info(), Cand::pid() | undefined) ->
%%   {ok, Broadcast, NState}
%% | {reply, Msg, NState}
%% | {ok, AmLeaderMsg, FromLeaderMsg, NState}
%% | {error, term()}
%%
%%     Broadcast = broadcast()
%%     NState    = state()
%%
%% @doc Called by the leader when it is elected leader, and each time a
%% candidate recognizes the leader.
%%
%% This function is only called in the leader instance, and `Broadcast'
%% will be sent to all candidates (when the leader is first elected),
%% or to the new candidate that has appeared.
%%
%% `Broadcast' might be the same as `NState', but doesn't have to be.
%% This is up to the application.
%%
%% If `Cand == undefined', it is possible to obtain a list of all new
%% candidates that we haven't synced with (in the normal case, this will be
%% all known candidates, but if our instance is re-elected after a netsplit,
%% the 'new' candidates will be the ones that haven't yet recognized us as
%% leaders). This gives us a chance to talk to them before crafting our
%% broadcast message.
%%
%% We can also choose a different message for the new candidates and for
%% the ones that already see us as master. This would be accomplished by
%% returning `{ok, AmLeaderMsg, FromLeaderMsg, NewState}', where
%% `AmLeaderMsg' is sent to the new candidates (and processed in
%% {@link surrendered/3}, and `FromLeaderMsg' is sent to the old
%% (and processed in {@link from_leader/3}).
%%
%% If `Cand == Pid', a new candidate has connected. If this affects our state
%% such that all candidates need to be informed, we can return `{ok, Msg, NSt}'.
%% If, on the other hand, we only need to get the one candidate up to speed,
%% we can return `{reply, Msg, NSt}', and only the candidate will get the
%% message. In either case, the candidate (`Cand') will receive the message
%% in {@link surrendered/3}. In the former case, the other candidates will
%% receive the message in {@link from_leader/3}.
%%
%% @end
%%
elected(State = #?STATE{}, Election, undefined) ->
    lager:info("Elected leader of ~p",
               [[node(P) || P <- locks_leader:candidates(Election)]]),
    NewState = check_action(State#?STATE{am_leader = true}, Election),
    {ok, {cluster_state, NewState#?STATE.cluster_state}, NewState};
elected(State = #?STATE{}, Election, Pid) ->
    lager:info("Process ~p on ~p joined cluster", [Pid, node(Pid)]),
    NewState = check_action(State#?STATE{am_leader=true}, Election),
    NewCS = NewState#?STATE.cluster_state,
    broadcast_cluster_state([node(Pid)], NewCS#cluster_state.isolated),
    {reply, {cluster_state, NewCS}, NewState}.

%% @spec surrendered(State::state(), Synch::broadcast(), I::info()) ->
%%          {ok, NState}
%%
%%    NState = state()
%%
%% @doc Called by each candidate when it recognizes another instance as
%% leader.
%%
%% Strictly speaking, this function is called when the candidate
%% acknowledges a leader and receives a Synch message in return.
%%
%% @end
surrendered(State = #?STATE{}, {cluster_state, ClusterState}, _Election) ->
    % TODO: need a take_action here?
    lager:info("Surrendered. recv'd cluster state: ~p", [ClusterState]),
    {ok, State#?STATE{am_leader = false, cluster_state = ClusterState}}.

%% @spec handle_leader_call(Msg::term(), From::callerRef(), State::state(),
%%                          I::info()) ->
%%    {reply, Reply, NState} |
%%    {reply, Reply, Broadcast, NState} |
%%    {noreply, state()} |
%%    {stop, Reason, Reply, NState} |
%%    commonReply()
%%
%%   Broadcast = broadcast()
%%   NState    = state()
%%
%% @doc Called by the leader in response to a
%% {@link locks_leader:leader_call/2. leader_call()}.
%%
%% If the return value includes a `Broadcast' object, it will be sent to all
%% candidates, and they will receive it in the function {@link from_leader/3}.
%%
%% In this particular example, `leader_lookup' is not actually supported
%% from the {@link gdict. gdict} module, but would be useful during
%% complex operations, involving a series of updates and lookups. Using
%% `leader_lookup', all dictionary operations are serialized through the
%% leader; normally, lookups are served locally and updates by the leader,
%% which can lead to race conditions.
%% @end
handle_leader_call(halt_sync, _From, State = #?STATE{cluster_state=CS}, _Election) ->
    lager:warning("Halting ES sync"),
    NCS = CS#cluster_state{sync_active=false},
    {reply, ok, {cluster_state, NCS}, State#?STATE{cluster_state = NCS}};
handle_leader_call(resume_sync, _From, State = #?STATE{cluster_state=CS}, _Election) ->
    lager:warning("Resuming ES sync"),
    NCS = CS#cluster_state{sync_active=true},
    {reply, ok, {cluster_state, NCS}, State#?STATE{cluster_state = NCS}};
handle_leader_call(cluster_state, _From, State = #?STATE{cluster_state=CS}, Election) ->
    R = [
         {isolated, CS#cluster_state.isolated},
         {sync_active, CS#cluster_state.sync_active},
         {am_leader, State#?STATE.am_leader},
         {cluster_nodes, State#?STATE.cluster_nodes},
         {candidates, locks_leader:candidates(Election)}
        ],
    {reply, R, State};
handle_leader_call(Request, _From, State = #?STATE{}, _Election) ->
    lager:warning("unexpected leader call: ~p", [Request]),
    {reply, huh, State}.

%% @spec handle_leader_cast(Msg::term(), State::term(), I::info()) ->
%%   commonReply()
%%
%% @doc Called by the leader in response to a {@link locks_leader:leader_cast/2.
%% leader_cast()}.
%% @end
handle_leader_cast(resync, State = #?STATE{}, _Election) ->
    lager:info("scheduling user-initiated resync"),
    self() ! configuration_complete_event,
    {ok, State};
handle_leader_cast(Request, State = #?STATE{}, _Election) ->
    lager:warning("unexpected leader cast: ~p", [Request]),
    {ok, State}.

%% @spec from_leader(Msg::term(), State::state(), I::info()) ->
%%    {ok, NState}
%%
%%   NState = state()
%%
%% @doc Called by each candidate in response to a message from the leader.
%%
%% In this particular module, the leader passes an update function to be
%% applied to the candidate's state.
%% @end
from_leader({cluster_state, NewCS}, State, _Election) ->
    lager:info("New cluster state from leader: ~p", [NewCS]),
    {ok, State#?STATE{am_leader = false, cluster_state = NewCS}};
from_leader(Sync, State, _Election) ->
    lager:warning("unexpected from_leader(~p, ~p)", [Sync, State]),
    {ok, State}.

%% @spec handle_DOWN(Candidate::pid(), State::state(), I::info()) ->
%%    {ok, NState} | {ok, Broadcast, NState}
%%
%%   Broadcast = broadcast()
%%   NState    = state()
%%
%% @doc Called by the leader when it detects loss of a candidate.
%%
%% If the function returns a `Broadcast' object, this will be sent to all
%% candidates, and they will receive it in the function {@link from_leader/3}.
%% @end
handle_DOWN(Pid, State, El) ->
    lager:warning("DOWN message from: ~p@~p", [Pid,node(Pid)]),
    NewState = check_action(State, El),
    {ok, NewState}.

%% @spec handle_call(Request::term(), From::callerRef(), State::state(),
%%                   I::info()) ->
%%    {reply, Reply, NState}
%%  | {noreply, NState}
%%  | {stop, Reason, Reply, NState}
%%  | commonReply()
%%
%% @doc Equivalent to `Mod:handle_call/3' in a gen_server.
%%
%% Note the difference in allowed return values. `{ok,NState}' and
%% `{noreply,NState}' are synonymous.
%%
%% `{noreply,NState}' is allowed as a return value from `handle_call/3',
%% since it could arguably add some clarity, but mainly because people are
%% used to it from gen_server.
%% @end
%%
handle_call(get_cluster_nodes, _From,
            State = #?STATE{cluster_nodes=Cluster}, _Election) ->
    {reply, Cluster, State};
handle_call({set_cluster_nodes, Nodes}, _From,
            State = #?STATE{cluster_nodes=OldCluster}, _Election) ->
    lager:info("Setting nodes in cluster from ~p to: ~p", [OldCluster, Nodes]),
    {reply, ok, State#?STATE{cluster_nodes = Nodes}};
handle_call({add_cluster_node, Node}, _From,
            State = #?STATE{cluster_nodes=OldCluster}, _Election) ->
    case lists:member(Node, OldCluster) of
        false ->
            lager:info("Adding node ~p to cluster ~p", [Node, OldCluster]),
            NewCluster = [Node | OldCluster],
            {reply, NewCluster, State#?STATE{cluster_nodes = NewCluster}};
        true ->
            lager:warning("Adding node ~p already in cluster ~p", [Node, OldCluster]),
            {reply, {error, OldCluster}, State}
    end;
handle_call({remove_cluster_node, Node}, _From,
            State = #?STATE{cluster_nodes=OldCluster}, _Election) ->
    case lists:member(Node, OldCluster) of
        true ->
            lager:info("Removing node ~p from cluster ~p", [Node, OldCluster]),
            NewCluster = lists:delete(Node, OldCluster),
            {reply, NewCluster, State#?STATE{cluster_nodes = NewCluster}};
        false ->
            lager:warning("Node ~p not in cluster ~p", [Node, OldCluster]),
            {reply, {error, OldCluster}, State}
    end;
handle_call(Request, _From, State = #?STATE{}, _Election) ->
    lager:warning("Unexpected handle_call(message: ~p, state: ~p)", [Request, State]),
    {reply, huh, State}.

%% @spec handle_cast(Msg::term(), State::state(), I::info()) ->
%%    {noreply, NState}
%%  | commonReply()
%%
%% @doc Equivalent to `Mod:handle_call/3' in a gen_server, except
%% (<b>NOTE</b>) for the possible return values.
%%
handle_cast(Msg, State = #?STATE{}, _Election) ->
    lager:warning("Unexpected handle_cast(message: ~p, state: ~p)", [Msg, State]),
    {ok, State}.

%% @spec handle_info(Msg::term(), State::state(), I::info()) ->
%%     {noreply, NState}
%%   | commonReply()
%%
%% @doc Equivalent to `Mod:handle_info/3' in a gen_server,
%% except (<b>NOTE</b>) for the possible return values.
%%
%% This function will be called in response to any incoming message
%% not recognized as a call, cast, leader_call, leader_cast, from_leader
%% message, internal leader negotiation message or system message.
%% @end
handle_info(?REGISTER, State, _E) ->
    %TODO: ignore if sync_active is false
    try
        lager:info("Attempting ES event registration"),
        Filter = esapi:event_type_filter([configuration_complete_event]),
        Self = self(),
        ok = esapi:register_processor(Self, Filter),
        lager:info("Registration successful"),
        resync()
    catch
        E:X ->
            lager:error("Exception registering for ES events: {~p, ~p}", [E, X]),
            erlang:send_after(?RETRY_TIME, self(), ?REGISTER)
    end,
    {ok, State};
handle_info({nodeup, Node, _},
            State = #?STATE{es_node=EsNode, am_leader=Leader}, El)
  when Node =:= EsNode, Leader ->
    lager:info("ES Node came up"),
    NewState = check_action(State, El),
    {ok, NewState};
handle_info({nodeup, Node, _}, State, _El) ->
    lager:info("Node ~p came up.  whatever", [Node]),
    {ok, State};
handle_info({nodedown, Node, _},
            State = #?STATE{am_leader=Leader, es_node=EsNode}, El)
  when Node =:= EsNode, Leader ->
    lager:info("ES Node went down"),
    NewState = check_action(State, El),
    {ok, NewState};
handle_info({nodedown, Node, _}, State, _El) ->
    lager:info("Node ~p went down.  you look like I should care", [Node]),
    {ok, State};
handle_info(configuration_complete_event,
            State = #?STATE{sync_pid=SyncPid,
                            sync_timer=SyncTimer,
                            am_leader=Leader}, _E)
  when Leader ->
    lager:info("very mapsense, much data, leader action!"),
    NewState = if
                   % no sync in progress or scheduled, so schedule one for now
                   SyncPid =:= undefined, SyncTimer =:= undefined ->
                       State#?STATE{sync_timer = erlang:send_after(0, self(), start_sync)}
                       ;
                   % a sync is already scheduled
                   SyncPid =:= undefined, is_reference(SyncTimer) ->
                       State
                       ;
                   % a sync is currently in progress, but we need another when this one's done
                   is_pid(SyncPid), SyncTimer =:= undefined ->
                       State#?STATE{sync_timer = erlang:send_after(?RETRY_TIME, self(), start_sync)}
                       ;
                   % a sync is currently in progress, and another is already scheduled
                   is_pid(SyncPid), is_reference(SyncTimer) ->
                       State
               end,
    {ok, NewState};
handle_info(configuration_complete_event, State, _El) ->
    lager:info("some configuration, somewhere, completed.  hope someone does something about it"),
    {ok, State};
handle_info(start_sync, State = #?STATE{sync_pid = SyncPid, am_leader = Leader}, _E) when Leader ->
    NewState = if
                   % A sync is currently in progress, so try again in a bit
                   is_pid(SyncPid) ->
                       State#?STATE{sync_timer = erlang:send_after(?RETRY_TIME, self(), start_sync)}
                       ;
                   SyncPid == undefined ->
                       State#?STATE{sync_pid = start_sync(), sync_timer = undefined}
               end,
    {ok, NewState};
handle_info({'DOWN', _Mref, process, Pid, Exit}, State = #?STATE{sync_pid = SyncPid}, _E) when Pid == SyncPid ->
    NewState = case Exit of
                   normal ->
                       lager:info("Sync complete"),
                       State#?STATE{sync_pid = undefined}
                       ;
                   Exit ->
                       lager:alert("Sync process failed with ~p", [Exit]),
                       spawn(fun() -> timer:sleep(30), resync() end),
                       State#?STATE{sync_pid = undefined}
               end,
    {ok, NewState};

handle_info(Info, State = #?STATE{}, _E) ->
    lager:warning("Unexpected handle_info(info: ~p, state: ~p", [Info, State]),
    {ok, State}.

%% @spec terminate(Reason::term(), State::state()) -> Void
%%
%% @doc Equivalent to `terminate/2' in a gen_server callback
%% module.
%% @end
terminate(_Reason, _State) ->
    ok.

%% @spec code_change(FromVsn::string(), OldState::term(),
%%                   I::info(), Extra::term()) ->
%%       {ok, NState}
%%
%%    NState = state()
%%
%% @doc Similar to `code_change/3' in a gen_server callback module, with
%% the exception of the added argument.
%% @end
code_change(_OldVsn, State, _Election, _Extra) ->
    {ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================


start_sync() ->
    Pid = es_sync_proc:start(),
    monitor(process, Pid),
    Pid
    .

%TODO: modules / processes should register for notification...
notify_modules() ->
    [room_manager, process_cache]
    .

cluster_quorum(ClusterNodes, Election) ->
    Candidates = locks_leader:candidates(Election),
    lager:info("cluster_quorum(cluster_nodes:~p,candidates:~p",
               [ClusterNodes,[node(C) || C <- Candidates]]),
    length(Candidates)+1 > length(ClusterNodes) / 2.

broadcast_cluster_state(Nodes, Isolated) ->
    lager:info("Broadcasting state isolated=~p to ~p", [Isolated, Nodes]),
    [rpc:call(Node, Mod, isolated, [Isolated])
     || Mod <- notify_modules(), Node <- Nodes].

check_action(State = #?STATE{
                         cluster_state = ClusterState,
                         cluster_nodes = ClusterNodes
                        }, El) ->
    take_action(State,
                next_action(ClusterState,
                            cluster_quorum(ClusterNodes, El),
                            esapi:connected()
                           )
               ).

take_action(S = #?STATE{cluster_state=CS,cluster_nodes=CN}, Action) ->
    lager:info("take_action(~p, ~p)", [CS, Action]),
    case Action of
        nothing -> S;
        sync ->
            broadcast_cluster_state([node()|CN], false),
            erlang:send_after(0, self(), ?REGISTER),
            S#?STATE{cluster_state = CS#cluster_state{isolated = false}};
        pause ->
            broadcast_cluster_state([node()|CN], true),
            S#?STATE{cluster_state = CS#cluster_state{isolated = true}}
    end.

next_action(#cluster_state{isolated=WasIsolated,sync_active=SyncActive},
            Quorum, EsConnected) ->
    Isolated = isolated(SyncActive, Quorum, EsConnected),
    A = if
            WasIsolated =:= Isolated -> nothing;
            WasIsolated =/= Isolated andalso not Isolated -> sync;
            WasIsolated =/= Isolated andalso Isolated -> pause
        end,
    lager:info("next_action(wasiso=~p,iso=~p,sync=~p,majority=~p,es_connected=~p) -> ~p",
               [WasIsolated, Isolated, SyncActive, Quorum, EsConnected, A]),
    A.

isolated(_SyncActive = true, Majority, EsConnected) ->
    (Majority =:= false) andalso (EsConnected =:= false);
isolated(_SyncActive = false, Majority, _EsConnected) ->
    Majority =:= false.

cluster_nodes() ->
    {ok, ClusterNodes} = application:get_env(loadbalance_nodes),
    ClusterNodes.

sync_active() ->
    {ok, SyncActive} = application:get_env(sync_active),
    if
        SyncActive -> true;
        not SyncActive -> false
    end.

