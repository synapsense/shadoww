
-module(es_ac_sync_app).

-behaviour(application).

-export([start/2, stop/1, config_change/3]).

start(normal, StartArgs) ->
	lager:info("ES/AC sync application starting. start args: ~p", [StartArgs]),
	es_ac_sync_sup:start_link().

stop(State) ->
	lager:info("ES/AC sync application stopping. state: ~p", [State]).

config_change(Changed, New, Removed) ->
	lager:info("ES/AC sync config change. changed: ~p, new: ~p, removed: ~p", [Changed, New, Removed]),
	ok.

