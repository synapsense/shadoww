%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(create_alert_types).

-export([create_all/0]).

create_all() ->
    FilePath = filename:join(code:priv_dir(es_ac_sync), "alert_types.cfg"),
    {ok, AlertTypes} = file:consult(FilePath),
    [create_if_not_exist(apply(esapi, alert_type, T)) || T <- AlertTypes].

%% for new types of alarms
create_if_not_exist(AlertType) ->
    try
	esapi:get_alert_type(esapi:alert_type_name(AlertType))
    catch
	throw:_E ->
	    esapi:create_alert_type(AlertType)
    end.

