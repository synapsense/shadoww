-module(es_model_info).

-export([
    input_types/0,
    input_types/1,
    output_types/0,
    output_cfg_types/0,
    strategy_types/0,
    crah_strategy_types/0,
    crah_strategy_types/1,
    raised_floor_crah_strategy_types/1,
    remote_control_crah_strategy_types/1,
    room_strategy_types/0,
    room_strategy_types/1,
    raised_floor_room_strategy_types/1,
    remote_control_room_strategy_types/1,
    rack_types/0,
    crac_types/0,
    pressure_types/0,
    rf_types/0,
    heartbeat_types/0
        ]).

-export([fetch_children/0,
         fetch_types/0,
         osi_type/1
        ]).

rf_types() ->
    [<<"ROOM">>,
     <<"WSNNETWORK">>,
     <<"WSNNODE">>,
     <<"WSNSENSOR">>].


crah_strategy_types() ->
    lists:usort(crah_strategy_types(temperature)
                ++ crah_strategy_types(pressure)).

crah_strategy_types(Resource) ->
    lists:usort(raised_floor_crah_strategy_types(Resource)
                ++ remote_control_crah_strategy_types(Resource)).

raised_floor_crah_strategy_types(temperature) ->
    [<<"CONTROLALG_RF_CRAH">>];
raised_floor_crah_strategy_types(pressure) ->
    [<<"CONTROLALG_RF_VFD">>].

remote_control_crah_strategy_types(temperature) ->
    [<<"CONTROLALG_REMOTE_CONTROL">>];
remote_control_crah_strategy_types(pressure) ->
    [].

room_strategy_types() ->
    lists:usort(room_strategy_types(temperature)
                ++ room_strategy_types(pressure)).

room_strategy_types(Resource) ->
    lists:usort(raised_floor_room_strategy_types(Resource)
                ++ remote_control_room_strategy_types(Resource)).

raised_floor_room_strategy_types(temperature) ->
    [<<"CONTROLALG_RF_ROOM_TEMP">>];
raised_floor_room_strategy_types(pressure) ->
    [<<"CONTROLALG_RF_ROOM_PRESSURE">>].

remote_control_room_strategy_types(temperature) ->
    [];
remote_control_room_strategy_types(pressure) ->
    [].

strategy_types() ->
    crah_strategy_types()
    ++ room_strategy_types().

rack_types()->
    [<<"RACK">>].

pressure_types()->
    [<<"PRESSURE">>].

crac_types()->
    [<<"CRAC">>].

input_types() ->
    lists:usort(input_types(temperature)
                ++ input_types(pressure)).

input_types(temperature) ->
    [<<"CONTROLPOINT_SINGLECOMPARE">>];
input_types(pressure) ->
    [<<"CONTROLPOINT_SINGLECOMPARE">>].

output_types() ->
    [<<"CONTROLOUT_CRAH">>,
     <<"CONTROLOUT_CRAC">>,
     <<"CONTROLOUT_VFD">>].

output_cfg_types() ->
    [<<"CONTROLCFG_LIEBERT_AHU">>,
     <<"CONTROLCFG_GENERIC_CRAC">>,
     <<"CONTROLCFG_GENERIC_CRAH">>,
     <<"CONTROLCFG_ACH550_VFD">>,
     <<"CONTROLCFG_GENERIC_VFD">>].

protocol_types() ->
    [<<"CONTROLPROTO_MODBUS">>,
     <<"CONTROLPROTO_BACNET">>].

heartbeat_types() ->
    [<<"HEARTBEAT_MANAGER">>].

fetch_types() ->
    input_types() ++
    output_types() ++
    output_cfg_types() ++
    protocol_types() ++
    strategy_types() ++
    rack_types() ++
    crac_types() ++
    pressure_types() ++
    rf_types() ++
    heartbeat_types()
    .

fetch_children() ->
    [<<"ROOM">>,
     <<"RACK">>,
     <<"CRAC">>,
     <<"PRESSURE">>,
     <<"CONTROLOUT_VFD">>].

%% Determine the type of "thing" this TO represents, an input, output or strategy.
%% Return 'undefined' if it isn't any of them.
osi_type(To) ->
    OType = esapi:object_type(To),
    Input = lists:member(OType, input_types()),
    Output = lists:member(OType, output_types()),
    Strategy = lists:member(OType, strategy_types()),
    if
        Input -> input;
        Output -> output;
        Strategy -> strategy;
        true -> undefined
    end
    .

