
-module(es_sync_proc).

-export([
         start/0,
         start_link/0
        ]).

-export([
         sync/0,
         sync_to_es/0,
         sync_to_dumpfile/1,
         sync_ac/2,
         bulk_load_from_es/0,
         objects_by_type/2,
         dump_es_model/2,
         model_dump_file/0
        ]).

-export([parse_float_pairlist/1, parse_pairlist/1]).

-define(PROPERTY_GETTER(SimpleObject), fun(PName) -> object_property(SimpleObject, PName) end).

-include_lib("active_control/include/ac_constants.hrl").

%% Configuration functions
model_dump_file() ->
    {ok, CacheDir} = application:get_env(es_ac_sync, cache_dir),
    filename:join(CacheDir, "es_model.dump").

%% Entry point(s)
start() ->
    case whereis(?MODULE) of
        undefined ->
            P = spawn(?MODULE, sync, []),
            register(?MODULE, P),
            P
            ;
        _ ->
            error(already_running)
    end.

start_link() ->
    case whereis(?MODULE) of
        undefined ->
            P = spawn_link(?MODULE, sync, []),
            register(?MODULE, P),
            P
            ;
        _ ->
            error(already_running)
    end.

%% main sync function, sync to ES
sync() ->
    try
        lager:info("Starting ES sync..."),

    %dbg:start(),
    %dbg:tracer(port, dbg:trace_port(file, "od.trace")),
    %dbg:tpl({?MODULE, '_', '_'}, []),
    %dbg:tpl({esapi, '_', '_'}, []),
    %dbg:p(self(), all),

        sync_to_es(),
        lager:info("ES sync complete!")
    catch
        error:{badmatch, {error, noreply}} ->
            lager:error("Timeout during ES sync!"),
            exit(sync_timeout)
            ;
        E:R ->
            lager:error("Exception during ES sync: {~p,~p} ~p", [E, R, erlang:get_stacktrace()]),
            exit(sync_failed)
    end.

% Sync AC to the object model in ES.  Also, since the ES model is in memory,
% take advantage of the situation to dump the model to disk.
sync_to_es() ->
    {Objects, Relationships} = bulk_load_from_es(),
    sync_ac(Objects, Relationships),
    dump_es_model(Objects, Relationships),
    ok.

% Write the ES model out to disk for later syncing/analysis
dump_es_model(Objects, Relationships) ->
    File = model_dump_file(),
    Os = [{Type, gb_trees:to_list(Instances)} || {Type, Instances} <- Objects],
    Text = io_lib:format("~p.", [{Os, Relationships}]),
    lager:info("Writing ES model dump file to ~s", [File]),
    file:write_file(File, Text).

% Load a previously saved config and sync AC to it
sync_to_dumpfile(FileName) ->
    {ok, [{ObjectsL, Relationships}]} = file:consult(FileName),
    Objects = [{Type, gb_trees:from_orddict(orddict:from_list(Instances))} || {Type, Instances} <- ObjectsL],
    sync_ac(Objects, Relationships),
    ok.

% Use the given Objects and Relationships to build mechanical models, then sync AC internals
-spec sync_ac(object_cache(), relationship_cache()) -> ok.
sync_ac(Objects, Relationships) ->
    {StopRC, UpdateRC, StartRC} = remote_control_rooms(Objects, Relationships),
    {StopRF, UpdateRF, StartRF} = raised_floor_rooms(Objects, Relationships),

    % Sync actions must be processed in the right order in case an ID is moved
    % between rooms.
    % Stop everything that needs stopping
    % Update/restart everything that needs updating/restarting
    % Start everything that needs starting
    lager:info("RC Sync actions: stop: ~p, update: ~p, start: ~p",
               [StopRC, UpdateRC, StartRC]),
    lager:info("RF Sync actions: stop: ~p, update: ~p, start: ~p",
               [StopRF, UpdateRF, StartRF]),

    [remote_control_room:stop(RoomId) || RoomId <- StopRC],
    [raised_floor_room:stop(RoomId) || RoomId <- StopRF],

    [remote_control_room:update_config(NewConfig) || NewConfig <- UpdateRC],
    [raised_floor_room:update_config(NewConfig) || NewConfig <- UpdateRF],

    [remote_control_room:start(NewRoom) || NewRoom <- StartRC],
    [raised_floor_room:start(NewRoom) || NewRoom <- StartRF],

    lager:info("sync complete!"),
    ok.

remote_control_rooms(Objects, Relationships) ->
    RoomObjects = objects_by_type(<<"ROOM">>, Objects),
    AllRooms = [{bin_id(Room), remote_control_room_model(Room, Objects, Relationships)}
                || Room <- RoomObjects,
                   object_property(Room, <<"controlType">>) =:= <<"remote">>],
    {AllRoomIds, _} = lists:unzip(AllRooms),

    RunningRooms = remote_control_room:list_rooms(),
    StopIds = RunningRooms -- AllRoomIds,
    StartIds = AllRoomIds -- RunningRooms,
    MaybeRestartIds = RunningRooms -- StopIds,
    RestartIds = [RoomId
               || {RoomId, Model} <- AllRooms,
                  lists:member(RoomId, MaybeRestartIds),
                  remote_control_room:requires_restart(Model)],
    UpdateIds = MaybeRestartIds -- RestartIds,
    {_, Update} = lists:unzip(shadoww_lib:keyfilter(UpdateIds, 1, AllRooms)),
    {_, Start} = lists:unzip(shadoww_lib:keyfilter(StartIds ++ RestartIds, 1, AllRooms)),
    {StopIds ++ RestartIds, Update, Start}.

raised_floor_rooms(Objects, Relationships) ->
    RoomObjects = objects_by_type(<<"ROOM">>, Objects),
    AllRooms = [{bin_id(Room), raised_floor_room_model(Room, Objects, Relationships)}
                  || Room <- RoomObjects,
                     lists:member(
                       object_property(Room, <<"controlType">>),
                       [<<"pressure">>, <<"pressuretemperature">>])],
    {AllRoomIds, _} = lists:unzip(AllRooms),

    RunningRooms = raised_floor_room:list_rooms(),
    StopIds = RunningRooms -- AllRoomIds,
    StartIds = AllRoomIds -- RunningRooms,
    MaybeRestartIds = RunningRooms -- StopIds,
    RestartIds = [RoomId
               || {RoomId, Model} <- AllRooms,
                  lists:member(RoomId, MaybeRestartIds),
                  raised_floor_room:requires_restart(Model)],
    UpdateIds = MaybeRestartIds -- RestartIds,
    {_, Update} = lists:unzip(shadoww_lib:keyfilter(UpdateIds, 1, AllRooms)),
    {_, Start} = lists:unzip(shadoww_lib:keyfilter(StartIds ++ RestartIds, 1, AllRooms)),
    {StopIds ++ RestartIds, Update, Start}.

remote_control_room_model(Room, Os, Rels) ->
    CandidateCrahs = children_by_type(Room, es_model_info:crac_types(), Os, Rels),
    ControlledCrahs = lists:flatten([remote_control_crah_model(Crah, Os, Rels) || Crah <- CandidateCrahs]),
    Heartbeat = heartbeat_manager(Room, Os, Rels),
    lager:debug("Creating remote_control_room with \n\tRoomId = ~p, \n\tControlledCrahs = ~p",
        [bin_id(Room), ControlledCrahs]),
    room_builder:remote_control_room(
      bin_id(Room),
      Heartbeat,
      ControlledCrahs
     ).

remote_control_crah_model(Crah, Os, Rels) ->
    try
        TempDriver = crah_driver(Crah, Os, Rels),
        TempStrategy = remote_control_crah_strategy(Crah, Os, Rels),
        VfdDriver = vfd_driver(Crah, Os, Rels),
        VfdStrategy = remote_control_vfd_strategy(Crah, Os, Rels),
        if
            TempDriver =:= none, TempStrategy =:= none, VfdDriver =:= none, VfdStrategy =:= none ->
                %FIXME should this be a runtime error?
                [];
            true ->
                room_builder:remote_control_crah(
                    bin_id(Crah),
                    TempDriver,
                    TempStrategy,
                    VfdDriver,
                    VfdStrategy
                )
        end
    catch
        E:R ->
            lager:info("Bad/no control config for remote control CRAH ~p~n~p:~p  -  ~p", [object_id(Crah),E,R,erlang:get_stacktrace()]),
            []
    end.

crah_driver(Crah, Os, Rels) ->
    CandidateDriver = [O || O <- children_by_type(Crah, [<<"CONTROLOUT_CRAH">>, <<"CONTROLOUT_CRAC">>], Os, Rels),
        object_property(O, <<"resource">>) =:= <<"temperature">>],
    case CandidateDriver of
        [] -> none;
        [{ObjectId, _Obj}=CrahDriverObj] ->
            P = ?PROPERTY_GETTER(CrahDriverObj),
            OutputType = esapi:object_type(ObjectId),
            case OutputType of
                <<"CONTROLOUT_CRAH">> ->
                    room_builder:crah_driver(
                      bin_id(Crah),
                      temperature,
                      P(<<"pullTime">>),
                      P(<<"pushTime">>),
                      coolingcontrolprop(P(<<"coolingControl">>)),
                      boolprop(P(<<"cascadeStandby">>)),
                      protocol_record(object(P(<<"protocol">>), Os)),
                      crah_driver_config_record(object(P(<<"config">>), Os))
                     );
                <<"CONTROLOUT_CRAC">> ->
                    room_builder:crac_driver(
                        bin_id(Crah),
                        temperature,
                        P(<<"pullTime">>),
                        P(<<"pushTime">>),
                        coolingcontrolprop(P(<<"coolingControl">>)),
                        %P(<<"maxStages">>), %FIXME: Is this necessary?
                        4,
                        boolprop(P(<<"cascadeStandby">>)),
                        protocol_record(object(P(<<"protocol">>), Os)),
                        crac_driver_config_record(object(P(<<"config">>), Os))
                    );
                _ ->
                    lager:info("Unsupported temperature output driver", [OutputType]),
                    []
            end
    end.

crah_driver_config_record(CrahConfigObject) ->
    P = ?PROPERTY_GETTER(CrahConfigObject),
    case object_type(CrahConfigObject) of
        <<"CONTROLCFG_LIEBERT_AHU">> ->
            room_builder:liebert_ahu(
              P(<<"port">>)
              );
        <<"CONTROLCFG_GENERIC_CRAH">> ->
            room_builder:generic_crah(
              strprop(P(<<"setpointWrite">>)),
              strprop(P(<<"setpointRead">>)),
              strprop(P(<<"valveRead">>)),
              strprop(P(<<"ratRead">>)),
              strprop(P(<<"satRead">>)),
              strprop(P(<<"standbyRead">>)),
              strprop(P(<<"commRead">>)),
              strprop(P(<<"changeFiltersAlarmRead">>)),
              strprop(P(<<"generalAlarmRead">>)),
              strprop(P(<<"clearAlarmWrite">>))
             )
    end.

crac_driver_config_record(CracConfigObject) ->
    P = ?PROPERTY_GETTER(CracConfigObject),
    case object_type(CracConfigObject) of
        <<"CONTROLCFG_LIEBERT_AHU">> ->
            room_builder:liebert_ahu(
              P(<<"port">>)
              );
        <<"CONTROLCFG_GENERIC_CRAC">> ->
            room_builder:generic_crac(
              strprop(P(<<"setpointWrite">>)),
              strprop(P(<<"setpointRead">>)),
              strprop(P(<<"ratRead">>)),
              strprop(P(<<"satRead">>)),
              strprop(P(<<"compressorRead">>)),
              strprop(P(<<"capacityRead">>)),
              strprop(P(<<"standbyRead">>)),
              strprop(P(<<"commRead">>)),
              strprop(P(<<"highHeadPressureAlarmRead">>)),
              strprop(P(<<"lowSuctionPressureAlarmRead">>)),
              strprop(P(<<"compressorOverloadAlarmRead">>)),
              strprop(P(<<"shortCycleAlarmRead">>)),
              strprop(P(<<"changeFiltersAlarmRead">>)),
              strprop(P(<<"generalAlarmRead">>)),
              strprop(P(<<"clearAlarmWrite">>))
             )
    end.

remote_control_crah_strategy(Crah, Os, Rels) ->
    CandidateStrategy = [O || O <- children_by_type(Crah, [<<"CONTROLALG_REMOTE_CONTROL">>], Os, Rels),
                              object_property(O, <<"resource">>) =:= <<"temperature">>],
    case CandidateStrategy of
        [] -> none;
        [CrahStrategyObject] ->
            P = ?PROPERTY_GETTER(CrahStrategyObject),
            room_builder:remote_control_strategy(
              bin_id(Crah),
              temperature,
              P(<<"manualOutput">>),
              modeprop(P(<<"mode">>))
             )
    end.

remote_control_vfd_strategy(Crah, Os, Rels) ->
    CandidateStrategy = [O || O <- children_by_type(Crah, [<<"CONTROLALG_REMOTE_CONTROL">>], Os, Rels),
                              object_property(O, <<"resource">>) =:= <<"pressure">>],
    case CandidateStrategy of
        [] -> none;
        [VfdStrategyObject] ->
            P = ?PROPERTY_GETTER(VfdStrategyObject),
            room_builder:remote_control_strategy(
              bin_id(Crah),
              pressure,
              P(<<"manualOutput">>),
              modeprop(P(<<"mode">>))
             )
    end.

heartbeat_manager(Room, Os, Rels) ->
    case children_by_type(Room, es_model_info:heartbeat_types(), Os, Rels) of
        [] -> none;
        [HeartbeatObject] ->
            P = ?PROPERTY_GETTER(HeartbeatObject),
            room_builder:heartbeat_manager(
              P(<<"interval">>),
              strprop(P(<<"heartbeatWrite">>)),
              protocol_record(object(P(<<"protocol">>), Os))
             )
    end.

raised_floor_crah_model(Crah, Os, Rels, ControlType) when ControlType =:= <<"pressure">> ->
    try
        VfdDriver = vfd_driver(Crah, Os, Rels),
        VfdStrategy = raised_floor_vfd_strategy(Crah, Os, Rels),
        if
            VfdDriver =:= none, VfdStrategy =:= none ->
                %FIXME should this be a runtime error?
                [];
            true ->
                room_builder:raised_floor_crah(
                    bin_id(Crah),
                    object_position(Crah),
                    none,
                    none,
                    VfdDriver,
                    VfdStrategy
                )
        end
    catch
        E:R ->
            lager:info("Bad/no control config for remote control CRAH ~p~n~p:~p  -  ~p", [object_id(Crah),E,R,erlang:get_stacktrace()]),
            []
    end;
raised_floor_crah_model(Crah, Os, Rels, _ControlType) ->
    try
        TempDriver = crah_driver(Crah, Os, Rels),
        TempStrategy = raised_floor_crah_strategy(Crah, Os, Rels),
        VfdDriver = vfd_driver(Crah, Os, Rels),
        VfdStrategy = raised_floor_vfd_strategy(Crah, Os, Rels),
        if
            TempDriver =:= none, TempStrategy =:= none, VfdDriver =:= none, VfdStrategy =:= none ->
                %FIXME should this be a runtime error?
                [];
            true ->
                room_builder:raised_floor_crah(
                    bin_id(Crah),
                    object_position(Crah),
                    TempDriver,
                    TempStrategy,
                    VfdDriver,
                    VfdStrategy
                )
        end
    catch
        E:R ->
            lager:info("Bad/no control config for remote control CRAH ~p~n~p:~p  -  ~p", [object_id(Crah),E,R,erlang:get_stacktrace()]),
            []
    end.

raised_floor_crah_strategy(Crah, Os, Rels) ->
    CandidateStrategy = [O || O <- children_by_type(Crah, [<<"CONTROLALG_RF_CRAH">>], Os, Rels),
                              object_property(O, <<"resource">>) =:= <<"temperature">>],
    case CandidateStrategy of
        [] -> none;
        [CrahStrategyObject] ->
            P = ?PROPERTY_GETTER(CrahStrategyObject),
            C = ?PROPERTY_GETTER(Crah),
            room_builder:raised_floor_crah_strategy(
              bin_id(Crah),
              temperature,
              P(<<"manualOutput">>),
              modeprop(P(<<"mode">>)),
              bin_to(C(<<"supplyT">>)),
              bin_to(C(<<"returnT">>))
             )
    end.

raised_floor_vfd_strategy(Crah, Os, Rels) ->
    CandidateStrategy = [O || O <- children_by_type(Crah, [<<"CONTROLALG_RF_VFD">>], Os, Rels),
                              object_property(O, <<"resource">>) =:= <<"pressure">>],
    case CandidateStrategy of
        [] -> none;
        [VfdStrategyObject] ->
            P = ?PROPERTY_GETTER(VfdStrategyObject),
            room_builder:raised_floor_vfd_strategy(
              bin_id(Crah),
              pressure,
              P(<<"manualOutput">>),
              modeprop(P(<<"mode">>))
             )
    end.

vfd_driver(Crah, Os, Rels) ->
    CandidateDriver = [Drv || Drv <- children_by_type(Crah, [<<"CONTROLOUT_VFD">>], Os, Rels),
                              object_property(Drv, <<"resource">>) =:= <<"pressure">>],

    case CandidateDriver of
        [] -> none;
        [VfdDriverObj] ->
            P = ?PROPERTY_GETTER(VfdDriverObj),
            CfmTable = parse_float_pairlist(object_property(Crah, <<"cfm_table">>)),

            room_builder:vfd_driver(
              bin_id(Crah),
              pressure,
              P(<<"pullTime">>),
              P(<<"pushTime">>),
              CfmTable,
              protocol_record(object(P(<<"protocol">>), Os)),
              vfd_driver_config_record(object(P(<<"config">>), Os))
             )
    end.

protocol_record(ProtocolObject) ->
    P = ?PROPERTY_GETTER(ProtocolObject),
    case object_type(ProtocolObject) of
        <<"CONTROLPROTO_BACNET">> ->
            protocol_bindings:bacnet_bindings(
              strprop(P(<<"ip">>)),
              P(<<"network">>),
              P(<<"devid">>)
             );
        <<"CONTROLPROTO_MODBUS">> ->
            protocol_bindings:modbus_bindings(
              strprop(P(<<"ip">>)),
              P(<<"devid">>)
             )
    end.

vfd_driver_config_record(DriverObject) ->
    P = ?PROPERTY_GETTER(DriverObject),
    case object_type(DriverObject) of
        <<"CONTROLCFG_ACH550_VFD">> -> room_builder:ach550();
        <<"CONTROLCFG_GENERIC_VFD">> ->
            room_builder:generic_vfd(
              strprop(P(<<"setpointWrite">>)),
              strprop(P(<<"setpointRead">>)),
              strprop(P(<<"powerRead">>)),
              strprop(P(<<"localOverrideRead">>)),
              strprop(P(<<"commRead">>)),
              strprop(P(<<"overCurrentAlarmRead">>)),
              strprop(P(<<"overVoltageAlarmRead">>)),
              strprop(P(<<"underVoltageAlarmRead">>)),
              strprop(P(<<"overTemperatureAlarmRead">>)),
              strprop(P(<<"generalAlarmRead">>)),
              strprop(P(<<"clearAlarmWrite">>))
             )
    end.

raised_floor_room_model(Room, Os, Rels) ->
    ControlType = object_property(Room, <<"controlType">>),
    ContainedRooms = [room_bounds(object_property(R, <<"points">>)) || R <- children_by_type(Room, [<<"ROOM">>], Os, Rels)],
    Heartbeat = heartbeat_manager(Room, Os, Rels),
    TempInputs = temp_inputs(Room, Os, Rels),
    PressureInputs = pressure_inputs(Room, Os, Rels),
    CandidateCrahs = children_by_type(Room, es_model_info:crac_types(), Os, Rels),
    ControlledCrahs = lists:flatten([raised_floor_crah_model(Crah, Os, Rels, ControlType) || Crah <- CandidateCrahs]),
    TempBalancer = case binary:match(ControlType, <<"temperature">>) of
                       nomatch -> none;
                       _ -> raised_floor_temp_balancer_strategy(Room, Os, Rels)
                   end,
    PressureBalancer = raised_floor_pressure_balancer_strategy(Room, Os, Rels),
    lager:debug("Creating raised_floor_room with \n\tRoomId = ~p, \n\tContainedRooms = ~p, \n\tTempInputs = ~p, \n\tPressureInputs = ~p, \n\tControlledCrahs = ~p, \n\tTempBalancer = ~p, \n\tPressureBalancer = ~p",
        [bin_id(Room), ContainedRooms, TempInputs, PressureInputs, ControlledCrahs, TempBalancer, PressureBalancer]),
    room_builder:raised_floor_room(
      bin_id(Room),
      room_bounds(object_property(Room, <<"points">>)),
      ContainedRooms,
      Heartbeat,
      TempInputs,
      PressureInputs,
      ControlledCrahs,
      TempBalancer,
      PressureBalancer
     ).

temp_inputs(Room, Os, Rels) ->
    Racks = children_by_type(Room, es_model_info:rack_types(), Os, Rels),
    [single_input(R, Os, Rels) || R <- Racks].

pressure_inputs(Room, Os, Rels) ->
    Pressures = children_by_type(Room, es_model_info:pressure_types(), Os, Rels),
    [single_input(P, Os, Rels) || P <- Pressures].

raised_floor_temp_balancer_strategy(Room, Os, Rels) ->
    [Strategy] = children_by_type(Room, [<<"CONTROLALG_RF_ROOM_TEMP">>], Os, Rels),
    P = ?PROPERTY_GETTER(Strategy),
    room_builder:raised_floor_balancer_strategy(
      bin_id(Room),
      temperature,
      aggressivenessprop(P(<<"aggressiveness">>))
     ).
raised_floor_pressure_balancer_strategy(Room, Os, Rels) ->
    [Strategy] = children_by_type(Room, [<<"CONTROLALG_RF_ROOM_PRESSURE">>], Os, Rels),
    P = ?PROPERTY_GETTER(Strategy),
    room_builder:raised_floor_balancer_strategy(
      bin_id(Room),
      pressure,
      aggressivenessprop(P(<<"aggressiveness">>))
     ).

single_input(EnvObj, Os, Rels) ->
    [InputObject] = children_by_type(EnvObj, [<<"CONTROLPOINT_SINGLECOMPARE">>], Os, Rels),
    P = ?PROPERTY_GETTER(InputObject),
    room_builder:single_input(
      bin_id(EnvObj),
      bin_to(P(<<"sensor">>)),
      object_position(object(P(<<"sensor">>), Os)),
      P(<<"target">>),
      enabledprop(P(<<"status">>)),
      resourceprop(P(<<"resource">>))
     ).

room_bounds(Points) ->
    geom:poly([geom:point(X,Y) || {X,Y} <- parse_float_pairlist(Points)]).

parse_float_pairlist(TableText) ->
    [{bin_to_float(A), bin_to_float(B)} || {A,B} <- parse_pairlist(TableText), A =/= <<>>, B =/= <<>>].

bin_to_float(B) ->
    try
        list_to_float(binary_to_list(B))
    catch
        _:_ -> float(list_to_integer(binary_to_list(B)))
    end.

parse_pairlist(Pairlist) ->
    [begin [A,B] = re:split(P, ","), {A,B} end || P <- re:split(Pairlist, ";", [trim])].

% assume that object O has an 'x' and 'y' property.
% extract them and turn into a geom:point()
object_position(O) ->
    X = object_property(O, <<"x">>),
    Y = object_property(O, <<"y">>),
    geom:point(X,Y).

-type object_cache() :: [{binary(), gb_trees:tree()}].
-type relationship_cache() :: [{binary(), [binary()]}].

-spec child_ids(esapi:to(), relationship_cache()) -> [esapi:to()].
child_ids(ObjectId, Relationships) ->
    proplists:get_value(ObjectId, Relationships, []).

-spec child_ids_of_type(esapi:to(), binary(), relationship_cache()) -> [esapi:to()].
child_ids_of_type(ObjectId, Type, Relationships) ->
    [ChildId || ChildId <- child_ids(ObjectId, Relationships), esapi:object_type(ChildId) == Type].

-spec objects_by_type(binary(), object_cache()) -> [esapi:simple_object()].
objects_by_type(Type, Objects) ->
    case proplists:get_value(Type, Objects) of
        undefined -> [];
        O -> gb_trees:to_list(O)
    end.

-spec children_by_type(
        ObjectOrId :: esapi:simple_object(),
        TypeList :: [binary()],
        Objects :: object_cache(),
        Relationships :: relationship_cache()
       ) -> [Children :: esapi:simple_object()].
children_by_type(Object,Types,Objs,Rels) ->
    ChildIds = lists:flatten([child_ids_of_type(object_id(Object), Type, Rels) || Type <-Types]),
    [object(To, Objs) || To <- ChildIds].

object(ObjectId, Objects) ->
    {ObjectId, gb_trees:get(ObjectId, proplists:get_value(esapi:object_type(ObjectId), Objects))}.

-spec bulk_load_from_es() -> { Objects :: object_cache(), Children :: relationship_cache()}.
%% where Instances is a map of esapi:to() -> [proplists:property()]
bulk_load_from_es() ->
    lager:info("starting ES bulk load"),
    FetchTypes = es_model_info:fetch_types(), %% names of all object types to fetch
    Objects = [{Type, instance_dict(Type)} || Type <- FetchTypes], % build a proplist of gb_trees, [{TypeName, InstanceTree(Id, Instance)}]
    Children = lists:flatten(
                 [esapi:get_children(gb_trees:keys(proplists:get_value(Type, Objects)))
                  || Type <- es_model_info:fetch_children()]),
    lager:info("ES bulk load finished, loaded ~B of stuff", [erlang:external_size({Objects, Children})]),
    {Objects, Children}
    .

-spec instance_dict(binary()) -> gb_trees:tree().
instance_dict(Type) ->
    Instances = esapi:get_all_properties_values(esapi:get_objects_by_type(Type)),
    gb_trees:from_orddict(orddict:from_list(Instances))
    .

-spec object_id(esapi:simple_object()) -> esapi:to().
object_id({Id, _Plist}) -> Id.
object_type({Id, _Plist}) -> esapi:object_type(Id).
object_property({_Id, Proplist}, PName) ->
    {PName, Value} = lists:keyfind(PName, 1, Proplist),
    Value.

bin_to(To) -> esapi:to_id(To).

%% Convert ES property values to values expected by API
bin_id(O) -> bin_to(object_id(O)).

coolingcontrolprop(1) -> return;
coolingcontrolprop(2) -> supply.

boolprop(1) -> true;
boolprop(0) -> false.

enabledprop(?STATUS_DISABLED) -> false;
enabledprop(_) -> true.

aggressivenessprop(<<"high">>) -> high;
aggressivenessprop(<<"med">>) -> med;
aggressivenessprop(<<"low">>) -> low.

modeprop(<<"manual">>) -> manual;
modeprop(<<"automatic">>) -> automatic;
modeprop(<<"disengaged">>) -> disengaged;
modeprop(<<"standby">>) -> standby.

resourceprop(<<"temperature">>) -> temperature;
resourceprop(<<"pressure">>) -> pressure.

strprop(null) -> "";
strprop(B) when is_binary(B) -> binary_to_list(B).

