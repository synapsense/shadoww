
-module(es_ac_sync_sup).

-behaviour(supervisor).

-export([
	 start_link/0,
	 init/1
	]).

-define(SERVER, ?MODULE).


start_link() ->
	lager:info("ES/AC Sync Supervisor starting"),
	Return = supervisor:start_link({local, ?SERVER}, ?MODULE, []),
	lager:info("ES/AC Sync Supervisor started: ~p", [Return]),
	Return.

init(_Args) ->
	RestartStrategy		= rest_for_one,	% each process that dies, resart it and all after it
	MaxRestarts		= 0,		% number of restarts allowed in
	MaxTimeBetRestarts	= 3600,		% this number of seconds

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpecs = [{
		config_loader,
		{config_loader, start_link, []},
		permanent,
		1000,
		worker,
		[config_loader]
	}, {
		alarm_eh_mon,
		{alarm_eh_mon, start_link, []},
		permanent,
		1000,
		worker,
		[alarm_eh_mon]
	}, {
		status_puller,
		{status_puller, start_link, []},
		permanent,
		1000,
		worker,
		[status_puller]
	}],

	{ok, {SupFlags, ChildSpecs}}.

