-module(alarm_eh_mon).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-define(ALARM_MODULE, control_alarm_es_eh).

start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

init([]) ->
    install(),
    {ok, nostate}.

handle_call(_Request, _From, State) -> {reply, ok, State}.

handle_cast(_Msg, State) -> {noreply, State}.

handle_info({gen_event_EXIT, ?ALARM_MODULE, Reason}, S) ->
    case Reason of
        normal -> ok;
        shutdown -> ok;
        T ->
            lager:alert("control alarm event handler crashed, reinstalling: ~p", [T]),
            install()
    end,
    {noreply, S};
handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, _State) ->
    control_alarm:delete_handler(?ALARM_MODULE, []).

code_change(_OldVsn, State, _Extra) -> {ok, State}.

%%

install() ->
    control_alarm:add_sup_handler(?ALARM_MODULE, []).

