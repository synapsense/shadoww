
-module(control_alarm_es_eh).

-behaviour(gen_event).

%% gen_event callbacks
-export([init/1,
         handle_event/2,
         handle_call/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-include("alert_types.hrl").

%%%===================================================================
%%% gen_event callbacks
%%%===================================================================

init([]) -> {ok, nostate}.

handle_event({Ev, Details, Id}, State) -> dispatch(Ev, Details, Id), {ok, State}.

handle_call(Request, State) -> lager:error("unexpected handle_call(~p)", [Request]), {ok, huh, State}.

handle_info(_Info, State) -> {ok, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================


dispatch(gateway_unreachable, Ip, Id) ->
    raise_alert(?ALERT_NAME_CANNOT_CONNECT_GATEWAY, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_GW_UNREACHABLE_1, [Ip], Id);
dispatch(gateway_not_responding, Ip, Id) ->
    raise_alert(?ALERT_NAME_GATEWAY_TIMEOUT, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_GW_NOT_RESPONDING_1, [Ip], Id);
dispatch(modbus_gw_error, {Ip, Device, 11}, Id) ->
    raise_alert(?ALERT_NAME_DEVICE_NOT_RESPONDING, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_DEVICE_NOT_RESPONDING_2, [Device, Ip], Id);
dispatch(modbus_gw_error, {Ip, Device, ErrorCode}, Id) ->
    Details = mbtcp_pipeline:get_error_string(ErrorCode),
    raise_alert(?ALERT_NAME_DEVICE_ERROR, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_MODBUS_ERROR_CODE_3, [Device, Ip, Details], Id);
dispatch(modbus_data_corruption, {Ip, Device, _Req, _Resp}, Id) ->
    raise_alert(?ALERT_NAME_DATA_CORRUPTION, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_MODBUS_DATA_CORRUPTION_2, [Device, Ip], Id);
dispatch(device_not_responding, {_Point, _Expression, Ip, Devid}, Id) ->
    raise_alert(?ALERT_NAME_DEVICE_NOT_RESPONDING, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_DEVICE_NOT_RESPONDING_2, [Devid, Ip], Id);
dispatch(comm_broken, _, Id) ->
    raise_alert(?ALERT_NAME_DEVICE_OFFLINE, ?ALERT_TYPE_COMMUNICATION, ?ALERT_MSG_DEVICE_COMM_INTERRUPTED_0, [], Id);

dispatch(room_crashed, Info, Id) ->
    raise_alert(?ALERT_NAME_INTERNAL_FAILURE, ?ALERT_TYPE_CRITICAL, "Room ~p crashed~n~p", [Id, Info], Id);
dispatch(io_expression_failed, {_Point, _Expr, AbortMsg}, Id) ->
    raise_alert(?ALERT_NAME_IO_FAILURE, ?ALERT_TYPE_CRITICAL, ?ALERT_MSG_IO_FAILURE_1, [format_quotes(AbortMsg)], Id);
dispatch(device_in_standby, _, Id) ->
    raise_alert(?ALERT_NAME_DEVICE_OFFLINE, ?ALERT_TYPE_CRITICAL, ?ALERT_MSG_CRAH_OFFLINE_0, [], Id);

dispatch(high_head_pressure, Compressor, Id) ->
    raise_alert(?ALERT_NAME_CRAC_HIGH_HEAD_PRESSURE, ?ALERT_TYPE_CRAC_HIGH_HEAD_PRESSURE, ?ALERT_MSG_HIGH_HEAD_PRESSURE_1, [Compressor], Id);
dispatch(low_suction_pressure, Compressor, Id) ->
    raise_alert(?ALERT_NAME_CRAC_LOW_SUCTION_PRESSURE, ?ALERT_TYPE_CRAC_LOW_SUCTION_PRESSURE, ?ALERT_MSG_LOW_SUCTION_PRESSURE_1, [Compressor], Id);
dispatch(compressor_overload, Compressor, Id) ->
    raise_alert(?ALERT_NAME_CRAC_COMPRESSOR_OVERLOAD, ?ALERT_TYPE_CRAC_COMPRESSOR_OVERLOAD, ?ALERT_MSG_COMPRESSOR_OVERLOAD_1, [Compressor], Id);
dispatch(short_cycle, Compressor, Id) ->
    raise_alert(?ALERT_NAME_CRAC_SHORT_CYCLE, ?ALERT_TYPE_CRAC_SHORT_CYCLE, ?ALERT_MSG_SHORT_CYCLE_1, [Compressor], Id);
dispatch(over_current, _, Id) ->
    raise_alert(?ALERT_NAME_VFD_OVER_CURRENT, ?ALERT_TYPE_VFD_OVER_CURRENT, ?ALERT_MSG_OVER_CURRENT_0, [], Id);
dispatch(over_voltage, _, Id) ->
    raise_alert(?ALERT_NAME_VFD_OVER_VOLTAGE, ?ALERT_TYPE_VFD_OVER_VOLTAGE, ?ALERT_MSG_OVER_VOLTAGE_0, [], Id);
dispatch(under_voltage, _, Id) ->
    raise_alert(?ALERT_NAME_VFD_UNDER_VOLTAGE, ?ALERT_TYPE_VFD_UNDER_VOLTAGE, ?ALERT_MSG_UNDER_VOLTAGE_0, [], Id);
dispatch(over_temperature, _, Id) ->
    raise_alert(?ALERT_NAME_VFD_OVER_TEMPERATURE, ?ALERT_TYPE_VFD_OVER_TEMPERATURE, ?ALERT_MSG_OVER_TEMPERATURE_0, [], Id);
dispatch(change_filters, _, Id) ->
    raise_alert(?ALERT_NAME_CHANGE_FILTERS, ?ALERT_TYPE_CHANGE_FILTERS, ?ALERT_MSG_CHANGE_FILTERS_0, [], Id);
dispatch(general_alarm, crac, Id) ->
    raise_alert(?ALERT_NAME_CRAC_GENERAL, ?ALERT_TYPE_CRAC_GENERAL, ?ALERT_MSG_GENERAL_0, [], Id);
dispatch(general_alarm, crah, Id) ->
    raise_alert(?ALERT_NAME_CRAH_GENERAL, ?ALERT_TYPE_CRAH_GENERAL, ?ALERT_MSG_GENERAL_0, [], Id);
dispatch(general_alarm, vfd, Id) ->
    raise_alert(?ALERT_NAME_VFD_GENERAL, ?ALERT_TYPE_VFD_GENERAL, ?ALERT_MSG_GENERAL_0, [], Id);
dispatch(bad_room_geom, _, Id) ->
    raise_alert(?ALERT_NAME_BAD_GEOM, ?ALERT_TYPE_CRITICAL, ?ALERT_MSG_BAD_ROOM_GEOMETRY_0, [], Id);

dispatch(Alarm, Details, Id) ->
    lager:warning("Unhandled alarm: {~p, ~p, ~p}", [Alarm, Details, Id]).

raise_alert(Name, Type, Msg, Args, Key) when is_binary(Key) ->
    Now = shadoww_lib:millisecs_now(),
    esapi:raise_alert(Name, Type, Now, f(Msg,Args), esapi:to(Key));
raise_alert(Name, Type, Msg, Args, Key) ->
    Id = driver_lib:id(Key),
    Now = shadoww_lib:millisecs_now(),
    esapi:raise_alert(Name, Type, Now, f(Msg,Args), esapi:to(Id)).

f(M, A) -> list_to_binary(lists:flatten(io_lib:format(M, A))).

format_quotes(MsgVar) ->
	ToString = lists:flatten(io_lib:format("~p", [MsgVar])),
	re:replace(ToString, "\"", "\\\\\"", [global,{return,list}]).

