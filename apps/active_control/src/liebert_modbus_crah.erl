-module(liebert_modbus_crah).
-behaviour(crah_fsm).

-export([properties/0,read_setpoint/1,read_comm/1,read_standby/1,read_valve/1,read_return/1,read_supply/1,write_setpoint/2]).
-export([read_change_filters/1, read_general/1, write_clear_alarm/1]).
-import(io_handler,[get_ip/1, get_devid/1, get_port/1, liebert_status_register/4, liebert_alarm_register/4, liebert_control_register/5, bits_high/2]).

properties() ->
	[{"ip", r},
	 {"devid", r},
	 {"port", r}].

read_comm(_) ->
    true.

read_standby(Props) ->
	liebert_status_register(get_ip(Props), get_devid(Props), 40018, get_port(Props)) == 0
	.

read_valve(Props) ->
	liebert_status_register(get_ip(Props), get_devid(Props), 40009, get_port(Props))
	.

read_return(Props) ->
	liebert_status_register(get_ip(Props), get_devid(Props), 40001, get_port(Props))
	.

read_supply(_) ->
    [].

read_setpoint(Props) ->
	liebert_status_register(get_ip(Props), get_devid(Props), 40010, get_port(Props))
	.

write_setpoint(Props, Ratsp) ->
	liebert_control_register(get_ip(Props), get_devid(Props), 40350, get_port(Props), round(Ratsp))
	.

%Alerts I/O
read_change_filters(Props) ->
	ReadReg = liebert_alarm_register(get_ip(Props), get_devid(Props), 40289, get_port(Props)),
	io_expr_util:bit(8, ReadReg) =/= 0
	.

read_general(Props) ->
	R1 = liebert_alarm_register(get_ip(Props), get_devid(Props), 40289, get_port(Props)),
	R2 = liebert_alarm_register(get_ip(Props), get_devid(Props), 40290, get_port(Props)),
	R3 = liebert_alarm_register(get_ip(Props), get_devid(Props), 40291, get_port(Props)),
	bits_high(R1, [0,1,2,3,4,5,6,7]) or (R2 /= 0) or (R3 /= 0)
	.
	
write_clear_alarm(_Props) ->
	ok.

