
%%
%% This module is the interface for registered processes.
%% It should handle:
%% 	registering processes and their callback module
%% 	looking up and calling registered processes
%% 	NOTE: Maybe the process module should handle the key itself,
%% 	so that it could use the key in an ETS table instead of
%% 	calling a process in some situations?
%% 	WHY: The module could switch between ETS or process call without
%% 	the client needing to know.
%% 	This will enable much more complex patterns to be handled simply.
%%      For example, a process could periodically write it's status to an ETS table.
%%      When the status is queried, could just read the table instead of calling the process.
%%      Downside: wouldn't know if the status got out of date (though the
%%      supervisor would ensure the process got restarted...)
%%
%% This module should wrap all gproc calls.
%%
%% In the future, this module would have a way to look up and call processes across nodes...
%%

-module(driver_lib).

-export([
         % Call a registered process by key
         call/3,

         register_strategy/2,
         register_driver/2,
         register_heartbeat/1,

         register_callback/1,

         reg/1,
         unreg/1,

         driver_key/2,
         strategy_key/2,

         strategy_pid/2,
         driver_pid/2,
         heartbeat_pid/1,
         event_pid/2,

         with_resource/2,
         with_type/2,

         id/1,
         resource/1,

         strategy_running/2,
         driver_running/2,
         running/1,
         lookup/1,
         proc_key/3
        ]).

%% These used for {via, driver_lib, Key} in gen_event registration
-export([
         register_name/2,
         unregister_name/1,
         whereis_name/1,
         send/2
        ]).

-define(NAME, n).
-define(PROPERTY, p).
-define(SCOPE, l).

-include_lib("active_control/include/ac_api.hrl").

% driver - device driver, responsible for device I/O
% strategy - control algorithm, responsible for telling 1 device driver what to do
% balancer - room-level strategy, responsible for balancing among device strategies
% input - responsible for sensor input configuration and status including present value tracking and distribution to strategies
% aoe - aoe recalculation room level
% events - event manager with for device status and stage2 change events. room level
% geom - geometry about the room
-type process_type() :: driver | strategy | balancer | input | aoe | event | geom | heartbeat .
-type proc_key() :: {id(), controlled_resource() | none, process_type()} .
-export_type([
        process_type/0,
        proc_key/0
        ]).

register_name(Name, Pid) when Pid =:= self() ->
    reg(Name), yes.
unregister_name(Name) ->
    unreg(Name).
whereis_name(Name) ->
    lookup(Name).
send(Name, Msg) ->
    lookup(Name) ! Msg.

%%
%% gproc mini tutorial:
%% a name is unique.  A process can only have 1 name.
%% A property is non-unique.  A process can have many properties.
%%
%% Registration / lookup needs:
%% Each driver & strategy self-register
%% Strategy looks up driver using own ID & resource
%%
%% so, each process is uniquely identified by
%% {
%%  Id :: term(),
%%  Resource :: temperature | pressure,
%%  Type :: strategy | driver
%% }
%%

%% TODO: add some mapping functions to look up ES TOs given above info?
%% ie, strategy_object(Id, Resource), driver_object(Id, Resource) where ID is the ID of the CRAC object

-spec call(DriverId :: proc_key(), F :: atom(), A :: [any()]) -> any() .
call(DriverId, F, A) ->
    case gproc:lookup_pids({n, ?SCOPE, DriverId}) of
        [] -> error({badarg, DriverId});
        [DriverPid] when is_pid(DriverPid) ->
            % TODO: improve this gproc query to elimiate the proplists:get_value call
            Modules = gproc:lookup_values({p, ?SCOPE, callback_module}),
            DriverModule = proplists:get_value(DriverPid, Modules),
            apply(DriverModule, F, [DriverPid | A])
    end.

%% Called by a strategy so that it's Pid can be looked up later with strategy_pid
register_strategy(Id, Resource) ->
    reg(proc_key(Id, Resource, strategy)).

%% Called by a driver so that it's Pid can be looked up later with driver_pid
register_driver(Id, Resource) ->
    reg(proc_key(Id, Resource, driver)).

register_heartbeat(Id) ->
    reg(proc_key(Id, none, heartbeat)).

register_callback(Module) ->
    gproc:reg({p, ?SCOPE, callback_module}, Module).


reg({Id, Resource, Type} = K) ->
    gproc:reg({?NAME, ?SCOPE, K}, K),
    gproc:mreg(?PROPERTY, ?SCOPE, [{id, Id}, {resource, Resource}, {type, Type}]),
    K.

unreg({_Id, _Resource, _Type} = K) ->
    gproc:unreg({?NAME, ?SCOPE, K}),
    gproc:munreg(?PROPERTY, ?SCOPE, [id, resource, type]),
    gproc:goodbye().

driver_key(Id, Resource) ->
    proc_key(Id, Resource, driver).

strategy_key(Id, Resource) ->
    proc_key(Id, Resource, strategy).

%% Given an ID and resource type, lookup the strategy PID
strategy_pid(Id, Resource) ->
    lookup(proc_key(Id, Resource, strategy)).

%% Given an ID and resource type, lookup the driver PID
driver_pid(Id, Resource) ->
    lookup(proc_key(Id, Resource, driver)).

heartbeat_pid(Id) ->
    lookup(proc_key(Id, none, heartbeat)).

event_pid(Id, Resource) ->
    lookup(proc_key(Id, Resource, event)).

-spec lookup(proc_key()) -> pid().
lookup({Id, Resource, Type}) ->
    K = proc_key(Id, Resource, Type),
    gproc:where({?NAME, ?SCOPE, K}).

-spec with_resource(proc_key(), controlled_resource()) -> proc_key().
with_resource({Id, _, Type}, Resource) ->
    proc_key(Id, Resource, Type).

-spec with_type(proc_key(), process_type()) -> proc_key().
with_type({Id, Resource, _}, Type) ->
    proc_key(Id, Resource, Type).

id({Id, _Resource, _Type}) -> Id.
resource({_Id, Resource, _Type}) -> Resource.

strategy_running(Id, Resource) ->
    running(strategy_key(Id, Resource)).

driver_running(Id, Resource) ->
    running(driver_key(Id, Resource)).

running({_, _, _} = K) ->
    case gproc:lookup_pids({?NAME, ?SCOPE, K}) of
        [] -> false;
        _ -> true
    end.

-spec proc_key(id(), controlled_resource() | none, process_type()) -> proc_key() .
proc_key(Id, Resource, Type) ->
    {Id, Resource, Type}.

