% vim: et:ts=8:sw=4:sts=4:et

-module(rf_balancer_strategy).

-behaviour(gen_server).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

%% API
-export([
         start_link/4,
         stop/1,
         set_aggressiveness/3,
         get_aggressiveness/2,
         %% for testing
         device_tracker/3,
         build_cluster_trackers/3,
         neighborhood_clusters/2,
         balance_neighborhood_clusters/3,
         do_rebalance/4,
         balanced_stage2/4,
         load_in/4,
         load_out/4,
         childspec/2
        ]).

%% gen_server callbacks
-export([
         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3
        ]).

% This is the most neighbors any device can have under any conditions
-define(MAX_NEIGHBORS, 8).

%TODO: increase neighborhood sizes

-record(tracker, {
          id :: id(),
          strategy_status :: control_status(),
          device_status :: device_status(),
          location :: geom:point(),
          neighbors :: [id()],
          stage2 :: float()
         }).

-record(state, {
          room_id :: id(),
          resource :: controlled_resource(),
          aggressiveness :: aggressiveness(),
          proc_key :: driver_lib:proc_key(),

          trackers :: [#tracker{}],

          rebalance_timer :: reference() | undefined
         }).

%%%===================================================================
%%% API
%%%===================================================================

start_link(RoomId, Resource, Aggressiveness, CrahIds) ->
    gen_server:start_link(?MODULE, {RoomId, Resource, Aggressiveness, CrahIds}, []).

stop(Pid) ->
    gen_server:call(Pid, stop).

-spec set_aggressiveness(RoomId :: id(),
                         Resource :: controlled_resource(),
                         Aggressiveness :: aggressiveness()) ->
    ok.
set_aggressiveness(RoomId, Resource, Aggressiveness) ->
    Pid = pid(RoomId, Resource),
    gen_server:call(Pid, {aggressiveness, Aggressiveness}).

-spec get_aggressiveness(RoomId :: id(),
                         Resource :: controlled_resource()) ->
    Aggressiveness :: aggressiveness().
get_aggressiveness(RoomId, Resource) ->
    Pid = pid(RoomId, Resource),
    gen_server:call(Pid, aggressiveness).

pid(RoomId, Resource) ->
    driver_lib:lookup(key(RoomId, Resource)).

key(RoomId, Resource) ->
    driver_lib:proc_key(RoomId, Resource, balancer).

% TODO: refactor to remove record
childspec(#raised_floor_balancer_strategy{id=RoomId, resource=Res, aggressiveness=Agg}, CrahIds) ->
    {
     driver_lib:proc_key(RoomId, Res, balancer),
     {?MODULE, start_link, [RoomId, Res, Agg, CrahIds]},
     transient,
     100,
     worker,
     [?MODULE]
    }.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init({RoomId, Resource, Aggressiveness, CrahIds}) ->
    InitStart = os:timestamp(),
    K = driver_lib:reg(driver_lib:proc_key(RoomId, Resource, balancer)),

    Trackers = shadoww_lib:pmap(node(), {?MODULE, device_tracker}, [RoomId, Resource], CrahIds),

    room_event:relay_events(RoomId, room_event_handler(Resource, [device_status, strategy_status, stage2])),

    folsom_metrics:new_histogram({rf_rebalance_time, RoomId, Resource}),
    folsom_metrics:tag_metric({rf_rebalance_time, RoomId, Resource}, rf_balancer),
    folsom_metrics:new_history(rf_rebalance),
    folsom_metrics:tag_metric(rf_rebalance, rf_balancer),
    folsom_metrics:new_history(rf_redistribute),
    folsom_metrics:tag_metric(rf_redistribute, rf_balancer),

    % FIXME: persist state to distributed memory and load from it on restart
    S = #state{
           room_id = RoomId,
           resource = Resource,
           aggressiveness = Aggressiveness,
           proc_key = K,
           trackers = Trackers
          },

    lager:debug("rf_balancer_strategy(~p) init time: ~pms", [Resource, timer:now_diff(os:timestamp(), InitStart) div 1000]),
    {ok, start_rebalance_timer(S)}.

-spec room_event_handler(Resource::controlled_resource(), EventTypes::[room_event:room_event_type()]) ->
    fun((room_event:room_event()) -> room_event:room_event() | ok).
room_event_handler(Resource, EventTypes) ->
    Self = self(),
    fun(Event = {Key, {ET, _}}) ->
        R = driver_lib:resource(Key),
        Good2Go = lists:member(ET, EventTypes) andalso R =:= Resource,
        case Good2Go of
            true -> Self ! Event;
            false -> ok
        end
    end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(aggressiveness, _F, State = #state{aggressiveness = A}) ->
    {reply, A, State};
handle_call({aggressiveness, A}, _F, State) ->
    {reply, ok, State#state{aggressiveness = A}};
handle_call(stop, _F, State) ->
    {stop, normal, stop, State};
handle_call(Request, From, State) ->
    lager:warning("Unexpected call ~p from ~p with state ~p", [Request, From, State]),
    {reply, huh, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(Msg, State) ->
    lager:warning("unexpected cast ~p with state ~p", [Msg, State]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({Key, {strategy_status, Status}}, State = #state{trackers = Trackers}) ->
    Id = driver_lib:id(Key),
    {noreply, State#state{trackers = update_strategy_status(Trackers, Id, Status)}};
handle_info({Key, {device_status, Status}},
            State = #state{
                       room_id = RoomId,
                       resource = Resource,
                       aggressiveness = Aggressiveness,
                       trackers = Trackers
                      }) ->
    % Lookup previous device status
    % Is a load redistribution warranted?
    % Lookup config settings, like neighborhood size
    % Lookup neighborhood of transitioning device
    % Calculate new Stage2 for each device in neighborhood
    % Distribute new Stage2 values
    Id = driver_lib:id(Key),
    case lists:keyfind(Id, #tracker.id, Trackers) of
        false -> {noreply, State};
        Tracker ->
            folsom_metrics:notify(rf_redistribute, {Id, Resource, Status}),
            NewTracker = Tracker#tracker{device_status=Status},
            NeighborhoodSize = strategy_param:find(neighborhood_size, Resource, Aggressiveness, RoomId),
            Updates = case check_redistribute(Tracker, NewTracker) of
                none ->
                    [];
                load_in ->
                    load_in(Id, Trackers, Resource, NeighborhoodSize);
                load_out ->
                    load_out(Id, Trackers, Resource, NeighborhoodSize)
            end,
            update_devices(Updates, Resource),
            NewTrackers = lists:keyreplace(Id, #tracker.id, Trackers, NewTracker),
            {noreply, State#state{trackers=NewTrackers}}
    end;
handle_info({Key, {stage2, Stage2}}, State = #state{trackers = Trackers}) ->
    Id = driver_lib:id(Key),
    {noreply, State#state{trackers = update_stage2(Trackers, Id, Stage2)}};
handle_info({timeout, TRef, rebalance},
            State = #state{
                       room_id = RoomId,
                       resource = Resource,
                       aggressiveness = Aggressiveness,
                       trackers = Trackers,
                       rebalance_timer = RebalTimer
                      }) when TRef =:= RebalTimer ->

    Walls = rf_geom:room_walls(RoomId),
    % theoretically, a cluster centroid could be outside the room,
    % inside a subroom (Walls) or anything else weird.
    % Meaning, using the same algorithm for distance around
    % obstacles might not work...
    % what should be done in that case?

    VarianceThreshold = strategy_param:find(variance_threshold, Resource, Aggressiveness, RoomId),
    NeighborhoodSize = strategy_param:find(neighborhood_size, Resource, Aggressiveness, RoomId),
    Begin = folsom_metrics:histogram_timed_begin({rf_rebalance_time, RoomId, Resource}),
    folsom_metrics:notify(rf_rebalance, {RoomId, Resource}),
    Updates = do_rebalance(Trackers, VarianceThreshold, NeighborhoodSize, Walls),
    folsom_metrics:histogram_timed_notify(Begin),
    update_devices(Updates, Resource),
    {noreply, start_rebalance_timer(State)};
handle_info(Info, State) ->
    lager:warning("unexpected info ~p with state ~p", [Info, State]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(normal, _State) ->
    ok;
terminate(Reason, State) ->
    lager:warning("terminating with reason ~p and state ~p", [Reason, State]),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

-record(cluster, {
        id :: integer(),
        crah_ids :: [id()],
        centroid :: geom:point(),
        average :: float(),
        neighbors :: [ID :: integer()],
        neighborhood_var :: float(),
        neighborhood_avg :: float()
        }).

-spec do_rebalance(
        Trackers :: [#tracker{}],
        VarianceThreshold :: float(),
        NeighborhoodSize :: integer(),
        Walls :: [geom:poly()]
       ) -> [{Id :: id(), NewStage2 :: float()}].
do_rebalance(Trackers, VarianceThreshold, NeighborhoodSize, Walls) ->
    % TODO: only balance trackers in automatic/online.  Maybe filter them out completely?
    % that would affect clustering though...
    BalancedTrackers = [balanced_stage2(Tracker, Trackers, VarianceThreshold, NeighborhoodSize)
                        || Tracker <- Trackers],

    Clusters = neighborhood_clusters(Trackers, NeighborhoodSize),

    NewTrackers = if
        length(Clusters) > 1 ->
            ClusterTrackers = build_cluster_trackers(Clusters, BalancedTrackers, Walls),

            ClusterBalancedTrackers = balance_neighborhood_clusters(
                    ClusterTrackers, BalancedTrackers, VarianceThreshold),

            ClusterBalancedTrackers;
        length(Clusters) =:= 1 ->
            BalancedTrackers
    end,
    NewTrackers,
    OldNew = lists:zip(lists:keysort(#tracker.id, Trackers), lists:keysort(#tracker.id, NewTrackers)),
    Updates = [{Id, NewV} || {#tracker{id=Id,stage2=OldV}, #tracker{stage2=NewV}} <- OldNew, OldV /= NewV],
    Updates.

-spec update_devices(
        Updates :: [{Id :: id(), NewStage2 :: float()}],
        Resource :: controlled_resource()
       ) -> ok.
update_devices(Updates, Resource) ->
    [set_stage2(Id, Resource, S2) || {Id, S2} <- Updates],
    ok.

build_cluster_trackers(Clusters, Trackers, Walls) ->
    NClusters = length(Clusters),
    CTrackers = [cluster_tracker(N, Ids, Trackers) ||
            {N, Ids} <- lists:zip(lists:seq(1,NClusters),Clusters)],
    CTrackers1 = add_cluster_neighborhoods(CTrackers, Walls),
    CTrackers2 = add_cluster_neighborhood_stats(CTrackers1),
    CTrackers2.

cluster_tracker(CN, Ids, Trackers) ->
    #cluster{
        id = CN,
        crah_ids = Ids,
        centroid = geom:centroid([(lists:keyfind(Id, #tracker.id, Trackers))#tracker.location || Id <- Ids]),
        average = cluster_average(Ids, Trackers),
        neighbors = []
        }.

cluster_average(C, Trackers) ->
    S2s = [S2 || #tracker{id=Id, stage2=S2} <- Trackers, lists:member(Id, C), is_float(S2)],
    if length(S2s) =:= 0 -> 0.0;
       true -> lists:sum(S2s) / length(S2s)
    end.

add_cluster_neighborhood_stats(ClusterTrackers) ->
    CT = fun(C = #cluster{neighbors=Ns,average=Avg}) ->
            Navgs = [Avg | [(lists:keyfind(N, #cluster.id, ClusterTrackers))#cluster.average || N <- Ns]],
            C#cluster{
                neighborhood_var = lists:max(Navgs) - lists:min(Navgs),
                neighborhood_avg = lists:sum(Navgs) / length(Navgs)
                }
    end,
    [CT(C) || C <- ClusterTrackers].

balance_neighborhood_clusters(ClusterTrackers, Trackers, VarianceThreshold) ->
    % should cluster size be taken into account?
    % ie, the heuristic that a small cluster is more indicative of
    % local load than a large cluster, and therefore ...?
    lists:foldl(fun(CT = #cluster{neighborhood_var=Var}, Ts) ->
                if
                    Var >= VarianceThreshold ->
                        Adjust = 0.5 * (CT#cluster.neighborhood_avg - CT#cluster.average),
                        Crahs = CT#cluster.crah_ids,
                        Updater = fun(T = #tracker{id=Id,stage2=S2,strategy_status=SS,device_status=DS}) ->
                                InCrahs = lists:member(Id, Crahs),
                                if
                                    InCrahs, SS=:=automatic, DS=:=online ->
                                        T#tracker{stage2=S2+Adjust};
                                    true -> T
                                end
                        end,
                        [Updater(T) || T <- Ts];
                    Var < VarianceThreshold ->
                        Ts
                end
        end,
        Trackers,
        ClusterTrackers).

add_cluster_neighborhoods(ClusterTrackers, Walls) ->
    % another question.  does it make sense to use the same neighborhood size
    % for clusters?  Maybe it makes more sense to use something fixed here,
    % around 3 maybe.
    ClusterNeighborhoodSize = 3,

    ClusterDistances = shadoww_lib:pairmap(fun(#cluster{id=Id1, centroid=Centroid1}, #cluster{id=Id2, centroid=Centroid2}) ->
                    {Id1, Id2, geom:walk_distance(Centroid1, Centroid2, Walls)}
            end,
            ClusterTrackers),
    % If a cluster centroid is inside an obstruction, strip it out.
    % That means it won't be balanced with other clusters.
    FilterDistances = [{Id1, Id2, D} || {Id1, Id2, D} <- ClusterDistances, is_float(D)],

    ClusterNeighborhood = fun(Id) ->
                                  SortedNeighbors = lists:keysort(3,
                                                                  shadoww_lib:keyfindall(Id, 1, FilterDistances)
                                                                  ++ shadoww_lib:keyfindall(Id, 2, FilterDistances)),
                                  TrimmedNeighbors = lists:sublist(SortedNeighbors, ClusterNeighborhoodSize),
                                  [case N of {Id, B, _D} -> B; {A, Id, _D} -> A end  || N <- TrimmedNeighbors]
                          end,

    [C#cluster{neighbors=ClusterNeighborhood(Id)} || C = #cluster{id=Id} <- ClusterTrackers].

% Calculate a single new Stage2 value by looking at self and neighbors
-spec balanced_stage2(
        Tracker :: #tracker{},
        Trackers :: [#tracker{}],
        VarianceThreshold :: float(),
        NeighborhoodSize :: integer()
       ) -> #tracker{}.
balanced_stage2(Tracker=#tracker{
                           id = Id,
                           strategy_status=SS,
                           device_status=DS,
                           stage2=DeviceStage2
                          },
                Trackers,
                VarianceThreshold,
                NeighborhoodSize)
    when SS =:= automatic, DS =:= online, is_float(DeviceStage2) ->
    GoodNeighbors = [Tracker |
                     [T || T = #tracker{stage2=S2} <-
                           automatic_online_neighbors(Id,
                                                      Trackers,
                                                      NeighborhoodSize),
                           is_float(S2)]
                    ],
    if
        length(GoodNeighbors) > 1 ->
            NeighborhoodStage2 = [Stage2 || #tracker{stage2=Stage2} <- GoodNeighbors],
            Variance = lists:max(NeighborhoodStage2) - lists:min(NeighborhoodStage2),
            if
                Variance < VarianceThreshold ->
                    Tracker;
                Variance >= VarianceThreshold ->
                    Navg = lists:sum(NeighborhoodStage2) / length(NeighborhoodStage2),
                    Tracker#tracker{stage2=(DeviceStage2+Navg)/2}
            end;
        length(GoodNeighbors) =:= 1 ->
            Tracker
    end;
balanced_stage2(Tracker, _, _, _) -> Tracker.

% == algorithm for finding neighborhood clusters ==
% Consider neighborhoods as a directed graph Unit -> neighbors
% Many edges are bidirectional, ie, A -> B and B -> A
% From this directed graph, construct the undirected graph containing
% only edges that are symmetric, where A and B point to each other
%
% This new, undirected graph represents the clusters.
% Rip through it and find all independent subgraphs

-spec neighborhood_clusters([#tracker{}], integer()) -> [[id()]] .
neighborhood_clusters(Trackers, NeighborhoodSize) ->
    % FIXME: this is building the same neighborhood table regardless of device state.
    % Is that OK?
    Ns = [{Id, lists:sublist(Ns, NeighborhoodSize)} || #tracker{id=Id, neighbors=Ns} <- Trackers],
    {Ids, _} = lists:unzip(Ns),
    UA = undirected_assoc(Ns),
    Clusters = cluster(UA, []),
    Unclustered = Ids -- lists:flatten(Clusters),
    Clusters ++ [[Id] || Id <- Unclustered].

undirected_assoc(Neighborhoods) ->
    Undirected = shadoww_lib:pairmap(fun({A, AN}, {B, BN}) ->
                                             AB = lists:member(A, BN),
                                             BA = lists:member(B, AN),
                                             if
                                                 AB andalso BA -> [{A, B}];
                                                 true -> []
                                             end
                                     end,
                                     Neighborhoods),
    lists:flatten(Undirected).

cluster([], Clusters) -> Clusters;
cluster(Edges, Clusters) ->
    V = element(1, hd(Edges)),
    {Cluster, Eremain} = extend_cluster(V, {[], Edges}),
    cluster(Eremain, [lists:usort(Cluster) | Clusters]).

% TODO: should this really be closer to Dijkstra APSR?
% want to do more like a mapreduce here...
extend_cluster(V, {C, Es}) ->
    case lists:member(V, C) of
        true -> {C, Es};
        false ->
            {Linked, NL} = linked(V, Es),
            lists:foldl(fun extend_cluster/2, {[V | C], NL}, Linked)
    end.

% Find all vertices to/from V, according to Edges
% Remove the edges from the list
% Return the vertices and trimmed edges list
linked(V, Es) ->
    {Found1, Eremain} = shadoww_lib:keytakeall(V, 1, Es),
    {Found2, Eremain2} = shadoww_lib:keytakeall(V, 2, Eremain),
    Found = [B || {_, B} <- Found1] ++ [A || {A, _} <- Found2],
    {Found, Eremain2}.

-spec check_redistribute(OldState :: #tracker{}, NewState :: #tracker{}) -> none | load_in | load_out .
check_redistribute(
  #tracker{strategy_status = SS, device_status = OldDS},
  #tracker{device_status = NewDS}) ->
    case {SS, OldDS, NewDS} of
        {automatic, online, standby} ->
            load_out;
        {automatic, standby, online} ->
            load_in;
        _ -> none
    end.

% CRAH comes online

-spec load_in(
        Id :: id(),
        Trackers :: [#tracker{}],
        Resource :: controlled_resource(),
        NeighborhoodSize :: integer()
       ) -> [{Id :: id(), NewStage2 :: float()}].
load_in(Id, Trackers, _Resource = temperature, NeighborhoodSize) ->
    GoodNeighbors = automatic_online_neighbors(Id, Trackers, NeighborhoodSize),
    NeighborhoodStage2 = [Stage2 || #tracker{stage2=Stage2} <- GoodNeighbors],
    % FIXME what if no automatic/online neighbors?
    NewStage2 = lists:max(NeighborhoodStage2),
    [{Id, NewStage2}];
load_in(Id, Trackers, _Resource = pressure, NeighborhoodSize) ->
    GoodNeighbors = automatic_online_neighbors(Id, Trackers, NeighborhoodSize),
    NeighborhoodStage2 = [Stage2 || #tracker{stage2=Stage2} <- GoodNeighbors],
    % FIXME what if no automatic/online neighbors?
    NGoodNeighbors = length(GoodNeighbors),
    Stage2Average = lists:sum(NeighborhoodStage2) / NGoodNeighbors,
    Stage2Multiplier = NGoodNeighbors/(NGoodNeighbors+1),
    [{Id, Stage2Average*Stage2Multiplier} |
     [{Nid, Stage2*Stage2Multiplier} || #tracker{id=Nid, stage2=Stage2} <- GoodNeighbors]].

-spec load_out(
        Id :: id(),
        Trackers :: [#tracker{}],
        Resource :: controlled_resource(),
        NeighborhoodSize :: integer()
       ) -> [{Id :: id(), NewStage2 :: float()}].
load_out(Id, Trackers, _Resource = temperature, NeighborhoodSize) ->
    #tracker{stage2=OfflineSetpoint} = lists:keyfind(Id, #tracker.id, Trackers),
    GoodNeighbors = automatic_online_neighbors(Id, Trackers, NeighborhoodSize),
    %% FIXME: no need to make an adjustment if the Stage2 doesn't change
    [{Nid, min(Setpoint, OfflineSetpoint)}
     || #tracker{id=Nid,stage2=Setpoint} <- GoodNeighbors];
load_out(Id, Trackers, _Resource = pressure, NeighborhoodSize) ->
    #tracker{stage2=OfflineSpeed} = lists:keyfind(Id, #tracker.id, Trackers),
    GoodNeighbors = automatic_online_neighbors(Id, Trackers, NeighborhoodSize),
    Adjustment = OfflineSpeed / length(GoodNeighbors),
    [{Nid, Fanspeed + Adjustment}
     || #tracker{id=Nid,stage2=Fanspeed} <- GoodNeighbors].

set_stage2(Id, Resource, Stage2) ->
    driver_lib:call(driver_lib:proc_key(Id, Resource, strategy), set_stage2, [Stage2]).

automatic_online_neighbors(Id, Trackers, NeighborhoodSize) ->
    #tracker{neighbors = Ns} = lists:keyfind(Id, #tracker.id, Trackers),
    NeighborIds = lists:sublist(Ns, NeighborhoodSize),
    [N || N = #tracker{id=Tid,strategy_status=SS,device_status=DS} <- Trackers,
          SS =:= automatic, DS =:= online, lists:member(Tid, NeighborIds)].

% @doc
% Assemble a device tracker by assembling state from the strategy, driver and geometry
% This will be updated over time with events from room_event
% @end
device_tracker(Id, RoomId, Resource = temperature) ->
    DeviceStatus = device_status(driver_lib:call(driver_lib:proc_key(Id, Resource, driver), get_status, [])),
    StrategyInfo = driver_lib:call(driver_lib:proc_key(Id, Resource, strategy), get_status_info, []),
    {status, ControlStatus} = lists:keyfind(status, 1, StrategyInfo),
    {stage2, Stage2} = lists:keyfind(stage2, 1, StrategyInfo),
    Location = rf_geom:crah_location(RoomId, Id),
    NeighborIds = [NId || {_, NId} <- lists:sublist(rf_geom:get_distances(RoomId, Id, crah, crah), ?MAX_NEIGHBORS)],
    #tracker{
        id=Id,
        strategy_status=ControlStatus,
        device_status=DeviceStatus,
        stage2=Stage2,
        location=Location,
        neighbors=NeighborIds
        };
device_tracker(Id, RoomId, Resource = pressure) ->
    DeviceStatus= device_status(driver_lib:call(driver_lib:proc_key(Id, Resource, driver), get_status, [])),
    StrategyInfo = driver_lib:call(driver_lib:proc_key(Id, Resource, strategy), get_status_info, []),
    {status, ControlStatus} = lists:keyfind(status, 1, StrategyInfo),
    {stage2, Stage2} = lists:keyfind(stage2, 1, StrategyInfo),
    Location = rf_geom:crah_location(RoomId, Id),
    NeighborIds = [NId || {_, NId} <- lists:sublist(rf_geom:get_distances(RoomId, Id, crah, crah), ?MAX_NEIGHBORS)],
    #tracker{
        id=Id,
        strategy_status=ControlStatus,
        device_status=DeviceStatus,
        stage2=Stage2,
        location=Location,
        neighbors=NeighborIds
        }.

update_strategy_status(Trackers, Id, Status) ->
    case lists:keyfind(Id, #tracker.id, Trackers) of
        false ->
            Trackers;
        Tracker = #tracker{} ->
            lists:keyreplace(Id, #tracker.id, Trackers, Tracker#tracker{strategy_status = Status})
    end.

update_stage2(Trackers, Id, Stage2) ->
    case lists:keyfind(Id, #tracker.id, Trackers) of
        false ->
            Trackers;
        Tracker = #tracker{} ->
            lists:keyreplace(Id, #tracker.id, Trackers, Tracker#tracker{stage2 = Stage2})
    end.

start_rebalance_timer(State=#state{room_id=RoomId,resource=Resource,aggressiveness=Aggressiveness,rebalance_timer=OldTimer}) ->
    RebalanceTime = strategy_param:find(rebalance_time, Resource, Aggressiveness, RoomId, RoomId),
    if
        is_reference(OldTimer) ->
            catch erlang:cancel_timer(OldTimer);
        true -> ok
    end,
    State#state{rebalance_timer=erlang:start_timer(RebalanceTime, self(), rebalance)}.

device_status(#crah_status{status=DevStatus}) -> DevStatus;
device_status(#crac_status{status=DevStatus}) -> DevStatus;
device_status(#vfd_status{status=DevStatus}) -> DevStatus.

