%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(gen_3stage_strategy).

-behaviour(gen_server).

-include_lib("active_control/include/ac_api.hrl").

%% API
-export([
         start_link/5,
         stop/1,
         cast/2,
         get_status_info/1,
         set_stage1/3,
         get_stage2/1,
         set_stage2/2,
         set_manual_output/2,
         set_mode/2
        ]).

%% gen_server callbacks
-export([
         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3
        ]).

% convenience functions for callback modules to return
% processing actions to this module
-export([
         change_status/1,
         manual_out/1,
         new_stage2/1,
         recalc_s3/0
        ]).

-record(state, {
          room_id :: id(),
          proc_key :: driver_lib:proc_key(),
          id :: id(),
          resource :: controlled_resource(),

          callback_module :: module(),
          callback_state :: term(),

          output_hist :: datawindow:datawindow(), % for calculating failsafe output
          last_adjustment :: {float(), integer()}, % value, millisecs since epoch
          stage1 :: float(), % ie, present mean
          stage2 :: float(), % ie, target SAT
          stage3 :: float(), % ie, current S3 output

          manual_output :: float(),
          status :: control_status(),

          propagation_timer :: reference(),
          stage3_timer :: reference(),
          failsafe_timer :: reference()
         }).

-define(MS_7_DAYS, 1000 * 60 * 60 * 24 * 7).

-type strategy_action() :: {change_status, NewStatus :: control_status()}
                         | {new_manual_out, ManualOutput :: float()}
                         | {new_s2, Stage2 :: float()}
                         | recalc_s3.
-export_type([strategy_action/0]).

% Args is InitArgs from start_link
-callback init(Args :: term()) ->
    {ok, Mode :: control_mode(), ManualOutput :: float(), State :: term()}.
% PrevState is the most recent state cached in distributed memory
-callback reinit(PrevState :: term()) ->
    {ok, State :: term()}.

% Return strategy-specific info to be appended to generic info
-callback status_info(State :: term()) ->
    [proplists:property()].

% Calculate and return the Stage3 value from the Stage2 value
-callback stage3(K :: driver_lib:proc_key(), Stage2 :: float(), State :: term()) ->
    {ok, [gen_3stage_strategy:strategy_action()], Stage3 :: float(), NewState :: term()}
    | {error, Reason :: term(), NewState :: term()}.

-callback set_mode(OldMode :: control_status(), NewMode :: control_status(), State :: term()) ->
    {ok, [strategy_action()], NewState :: term()}
    | {error, Reason :: term(), NewState :: term()}.

-callback set_config(NewConfig :: term(), State :: term()) ->
    {ok, [strategy_action()], NewState :: term()}.

-callback handle_cast(Message :: term(), State :: term()) ->
    {noreply, [strategy_action()], NewState :: term()}.

-callback terminate(Reason :: term(), State :: term()) ->
    any().

% convenience functions for callback modules to return
% processing actions to this module
-spec change_status(NewStatus :: control_status()) -> {change_status, NewStatus :: control_status()}.
change_status(NewStatus) -> {change_status, NewStatus}.

-spec manual_out(NewMO :: float()) -> {new_manual_out, NewMO :: float()}.
manual_out(NewMO) -> {new_manual_out, NewMO}.

-spec new_stage2(NewS2 :: float()) -> {new_s2, NewS2 :: float()}.
new_stage2(NewS2) -> {new_s2, NewS2}.

-spec recalc_s3() -> recalc_s3.
recalc_s3() -> recalc_s3.

%%%===================================================================
%%% API
%%%===================================================================

start_link(RoomId, Id, Resource, CallbackModule, InitArgs) ->
    gen_server:start_link(?MODULE, [RoomId, Id, Resource, CallbackModule, InitArgs], []).

%%--------------------------------------------------------------------
%% @doc
%% Stop the strategy, removing any cached data
%%
%% @spec stop(Id) -> stop
%% @end
%%--------------------------------------------------------------------
stop(Pid) ->
    stop = gen_server:call(Pid, stop).

cast(Pid, Msg) ->
    gen_server:cast(Pid, {callback_cast, Msg}).

%%--------------------------------------------------------------------
%% @doc
%%
%% @spec get_status_info(Pid) -> raised_floor_strategy_status() | {error, Error}
%% @end
%%--------------------------------------------------------------------
get_status_info(Pid) ->
    gen_server:call(Pid, get_status_info).

%%--------------------------------------------------------------------
%% @doc
%% set the value of the Stage 1 output, called by the AoE controller
%%
%% @spec get_stage2(Pid) -> Value :: float() | {error, Reason}
%% @end
%%--------------------------------------------------------------------
set_stage1(Pid, Value, Aggressiveness) ->
    gen_server:cast(Pid, {set_stage1, Value, Aggressiveness}).

%%--------------------------------------------------------------------
%% @doc
%% get the value of the Stage 2 output, called by the AoE
%% controller to load balance
%%
%% @spec get_stage2(Pid) -> Value :: float() | {error, Reason}
%% @end
%%--------------------------------------------------------------------
get_stage2(Pid) ->
    gen_server:call(Pid, get_stage2).

%%--------------------------------------------------------------------
%% @doc
%% set the value of the Stage 2 output, called by the AoE
%% controller with the load balanced value
%%
%% @spec set_stage2(Pid, Value) -> ok | {error, Reason}
%% @end
%%--------------------------------------------------------------------
set_stage2(Pid, Value) ->
    gen_server:cast(Pid, {set_stage2, Value}).

set_manual_output(Pid, Value) ->
    gen_server:call(Pid, {set_manual_output, Value}).

set_mode(Pid, Mode) ->
    gen_server:call(Pid, {set_mode, Mode}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([RoomId, Id, Resource, CallbackModule, InitArgs]) ->
    K = driver_lib:register_strategy(Id, Resource),
    driver_lib:register_callback(?MODULE),

    MinMax = case Resource of
                 temperature -> min;
                 pressure -> max
             end,
    UseState = case process_cache:load_properties(K) of
                   {error, no_data} ->
                       {ok, InitMode, InitMO, CbState} = CallbackModule:init(InitArgs),

                       #state{
                          room_id = RoomId,
                          id = Id,
                          resource = Resource,
                          proc_key = K,
                          callback_module = CallbackModule,
                          callback_state = CbState,
                          output_hist = datawindow:new(MinMax, ?MS_7_DAYS),
                          last_adjustment = {0.0, shadoww_lib:millisecs_now()},
                          stage1 = undefined,
                          stage2 = undefined,
                          status = InitMode,
                          manual_output = InitMO
                         };
                   {ok, State = #state{
                                   callback_state = CbState0
                                  }} ->
                       {ok, CbState} = CallbackModule:reinit(CbState0),
                       State#state{callback_state = CbState,
                                   propagation_timer = undefined,
                                   stage3_timer = undefined,
                                   failsafe_timer = undefined
                                  }
               end,
    Status = UseState#state.status,
    SaveState = case change_mode(disengaged, Status, UseState) of
                    nochange -> UseState;
                    {ok, NewState} -> NewState
                end,
    {ok, save_state(SaveState)}.

handle_call(get_status_info, _F, State = #state{
                                            callback_module = CbMod,
                                            callback_state = CbState,
                                            manual_output = MO,
                                            last_adjustment = LA,
                                            stage1 = S1,
                                            stage2 = S2,
                                            status = Status,
                                            output_hist = OutputHistory
                                           }) ->
    StatusInfo = [
                  {status, Status},
                  {failsafe, value_or_null(OutputHistory)},
                  {stage1, S1},
                  {stage2, S2},
                  {user_setpoint, MO},
                  {last_adjustment, LA}
                 ] ++ CbMod:status_info(CbState),
    {reply, StatusInfo, State};
handle_call({set_mode, NewMode}, _F, State = #state{
                                                callback_module = CbMod,
                                                callback_state = CbState,
                                                status = Status
                                               }) ->
    case CbMod:set_mode(Status, NewMode, CbState) of
        {ok, Actions, NewCbState} ->
            State1 = process_actions(Actions, State#state{callback_state = NewCbState}),
            case change_mode(Status, NewMode, State1) of
                nochange -> {reply, ok, save_state(State1)};
                {ok, NewState} ->
                    {reply, ok, save_state(NewState)};
                {error, R} ->
                    {reply, {error, R}, State1}
            end;
        {error, R, NewCbState} ->
            {reply, {error, R}, save_state(State#state{callback_state = NewCbState})}
    end;
handle_call({set_manual_output, NewOutput}, _F, State) ->
    case State#state.status of
        manual ->
            write_setpoint(State#state.proc_key, NewOutput);
        _ ->
            ok
    end,
    {reply, ok, save_state(State#state{manual_output = NewOutput})};
handle_call(stop, _F, State = #state{proc_key = K}) ->
    process_cache:delete_properties(K),
    {stop, normal, stop, State};
handle_call(Request, From, State) ->
    lager:warning("Unexpected call ~p from ~p with state ~p", [Request, From, State]),
    {reply, huh, State}.

handle_cast({set_stage1, Stage1, Aggressiveness}, State = #state{status = Status}) ->
    NewState = handle_stage1(Status, Stage1, Aggressiveness, State),
    {noreply, save_state(NewState)};
handle_cast({set_stage2, Stage2}, State) ->
    {noreply, save_state(update_s2(State,Stage2))};
handle_cast({callback_cast, Msg}, State = #state{callback_module=CbMod,callback_state=CbState}) ->
    {noreply, Actions, NewCbState} = CbMod:handle_cast(Msg, CbState),
    State1 = process_actions(Actions, State),
    {noreply, save_state(State1#state{callback_state=NewCbState})};
handle_cast(Msg, State) ->
    lager:warning("unexpected cast ~p with state ~p", [Msg, State]),
    {noreply, State}.

handle_info({timeout, TRef, propagation_complete},
            State = #state{propagation_timer = PTimer}) when TRef =:= PTimer ->
    {noreply, State#state{propagation_timer = undefined}};
handle_info({timeout, TRef, go_failsafe},
            State = #state{
                       callback_module = CbMod,
                       callback_state = CbState,
                       status = Status,
                       failsafe_timer = FsTimer
                      }) when TRef =:= FsTimer ->
    case CbMod:set_mode(Status, failsafe, CbState) of
        {ok, Actions, NewCbState} ->
            State1 = process_actions(Actions, State#state{callback_state = NewCbState}),
            case change_mode(Status, failsafe, State1) of
                nochange -> {noreply, save_state(State1)};
                {ok, NewState} -> {noreply, save_state(NewState)};
                {error, R} ->
                    lager:error("Error (~p) going to failsafe", [R]),
                    {noreply, save_state(State1)}
            end;
        {error, R, NewCbState} ->
            lager:error("Callback disallowed going to failsafe: ~p", [R]),
            {noreply, save_state(State#state{callback_state = NewCbState})}
    end;
handle_info({timeout, TRef, calc_stage3},
            State = #state{
                       proc_key = K,
                       output_hist = OH,
                       callback_module = CbMod,
                       callback_state = CbState,
                       stage2 = S2,
                       stage3 = S3,
                       stage3_timer = S3Timer
                      }) when TRef =:= S3Timer ->
    NewState = case S2 of
                   undefined ->
                       lager:warning("~p skipping S3 calc: S2 is undefined", [K]),
                       State;
                   S2 when is_float(S2) ->
                       case CbMod:stage3(K, S2, CbState) of
                           {ok, Actions, NewS3, NewCbState} ->
                               State1 = process_actions(Actions, State#state{callback_state=NewCbState}),
                               if
                                   S3 == NewS3 ->
                                       State1;
                                   S3 /= NewS3, is_float(S3), is_float(NewS3) ->
                                       write_setpoint(K, NewS3),
                                       Adj = NewS3 - S3,
                                       Now = shadoww_lib:millisecs_now(),
                                       State1#state{
                                         output_hist = datawindow:push({NewS3, Now}, OH),
                                         last_adjustment = {Adj, Now}
                                        };
                                   S3 /= NewS3, is_float(NewS3) ->
                                       write_setpoint(K, NewS3),
                                       Now = shadoww_lib:millisecs_now(),
                                       State1#state{
                                         output_hist = datawindow:push({NewS3, Now}, OH)
                                        }
                               end;
                           {error, Actions, NewCbState} ->
                               process_actions(Actions, State#state{callback_state=NewCbState})
                       end
               end,
    {noreply, save_state(start_stage3_timer(NewState))};
handle_info(Info, State) ->
    lager:warning("unexpected handle_info(~p)", [Info]),
    {noreply, State}.

terminate(Reason, State) ->
    lager:warning("terminating with reason ~p and state ~p", [Reason, State]).

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

save_state(State = #state{proc_key=K}) ->
    process_cache:update_properties(K, State),
    State.

write_setpoint(K, Value) ->
    DriverKey = driver_lib:with_type(K, driver),
    DriverPid = driver_lib:lookup(DriverKey),
    base_driver:set_output(DriverPid, Value).

update_driver_mode(K, Status) ->
    DriverKey = driver_lib:with_type(K, driver),
    DriverPid = driver_lib:lookup(DriverKey),
    case Status of
        automatic ->
            base_driver:engage(DriverPid);
        manual ->
            base_driver:engage(DriverPid);
        disengaged ->
            base_driver:disengage(DriverPid)
    end.

start_stage3_timer(S = #state{room_id=RoomId,id=Id,resource=Res}) ->
    cancel_stage3_timer(S),
    Stage3Timeout = strategy_param:find(stage3_time, Res, RoomId, Id),
    S#state{stage3_timer = erlang:start_timer(Stage3Timeout, self(), calc_stage3)}.

cancel_stage3_timer(S = #state{stage3_timer = Stage3Timer}) when Stage3Timer =:= undefined ->
    S;
cancel_stage3_timer(S = #state{stage3_timer = Stage3Timer}) when is_reference(Stage3Timer) ->
    erlang:cancel_timer(Stage3Timer),
    S#state{stage3_timer = undefined}.

start_failsafe_timer(S = #state{room_id=RoomId,id=Id,resource=Res}) ->
    cancel_failsafe_timer(S),
    FailsafeTimeout = strategy_param:find(failsafe_timeout, Res, RoomId, Id),
    S#state{failsafe_timer = erlang:start_timer(FailsafeTimeout, self(), go_failsafe)}.

cancel_failsafe_timer(S = #state{failsafe_timer = FailsafeTimer}) when FailsafeTimer =:= undefined ->
    S;
cancel_failsafe_timer(S = #state{failsafe_timer = FailsafeTimer}) when is_reference(FailsafeTimer) ->
    erlang:cancel_timer(FailsafeTimer),
    S#state{failsafe_timer = undefined}.

value_or_null(DW) ->
    try
        datawindow:value(DW)
    catch
        _:_ -> null
    end.

is_propagating(#state{propagation_timer=PTimer}) ->
    case PTimer of
        undefined -> false;
        _ -> true
    end.

start_ptimer(State=#state{room_id=RoomId,id=Id,resource=Resource}) ->
    cancel_ptimer(State),
    PropagationDelay = strategy_param:find(propagation_delay, Resource, RoomId, Id),
    State#state{propagation_timer = erlang:start_timer(PropagationDelay, self(), propagation_complete)}.

cancel_ptimer(State = #state{propagation_timer = PTimer}) when PTimer =:= undefined ->
    State;
cancel_ptimer(State = #state{propagation_timer = PTimer}) when is_reference(PTimer) ->
    erlang:cancel_timer(PTimer),
    State#state{propagation_timer = undefined}.

update_s2(State=#state{room_id=RoomId,id=Id,resource=Resource}, S2) ->
    Min = strategy_param:find(min_s2, Resource, RoomId, Id),
    Max = strategy_param:find(max_s2, Resource, RoomId, Id),
    State#state{stage2=float(shadoww_lib:corralled(Min, Max, S2))}.

update_status(State, NewStatus) ->
    State#state{status = NewStatus}.

update_output_hist_with_s2(State = #state{stage2=S2, output_hist=OH}) ->
    State#state{output_hist = datawindow:push({S2, shadoww_lib:millisecs_now()}, OH)}.

-spec handle_stage1(control_status(), float(), aggressiveness(), #state{}) -> #state{}.
handle_stage1(disengaged, Stage1, _Agg, State) ->
    State#state{stage1 = Stage1};
handle_stage1(manual, Stage1, _Agg, State) ->
    State#state{stage1 = Stage1};
handle_stage1(automatic, Stage1, Agg,
              State = #state{
                         proc_key = K,
                         resource = Resource,
                         id = Id,
                         room_id = RoomId,
                         stage1 = RespondingTo,
                         stage2 = S2
                        }) ->
    NewState = case S2 of
                   undefined ->
                       lager:warning("~p Ignoring S1 update: S2 is undefined", [K]),
                       State#state{stage1 = Stage1};
                   S2 when is_float(S2) ->
                       ErrorScale = strategy_param:find(error_scale, Resource, Agg, RoomId, Id),
                       Deadband = strategy_param:find(deadband, Resource, Agg, RoomId, Id),
                       EarlyAdjustThreshold = strategy_param:find(early_adjust_threshold, Resource, Agg, RoomId, Id),

                       Propagating = is_propagating(State),

                       case rf_alg:calc_stage2(Stage1,
                                               ErrorScale,
                                               Deadband,
                                               S2,
                                               RespondingTo,
                                               EarlyAdjustThreshold,
                                               Propagating) of
                           hold ->
                               State#state{stage1 = Stage1};
                           {new_target, NewTarget} ->
                               lager:debug("New Stage2 target for ~p(~p): ~p", [Id, Resource, NewTarget]),
                               room_event:notify_stage2_change(RoomId, K, NewTarget),
                               start_ptimer(
                                 update_output_hist_with_s2(
                                    update_s2(
                                    State#state{
                                        stage1 = Stage1
                                        },
                                    NewTarget)
                                  )
                                )
                       end
               end,
    start_failsafe_timer(NewState);
handle_stage1(failsafe, Stage1, Agg,
              State = #state{
                         callback_module = CbMod,
                         callback_state = CbState
                        }) ->
    case CbMod:set_mode(failsafe, automatic, CbState) of
        {ok, Actions, NewCbState} ->
            NewState0 = State#state{callback_state = NewCbState},
            NewState1 = process_actions(Actions, NewState0),
            case change_mode(failsafe, automatic, NewState1) of
                nochange -> NewState1;
                {ok, NewState} -> handle_stage1(automatic, Stage1, Agg, NewState)
            end;
        {error, Reason, NewCbState} ->
            lager:warning("Callback rejected failsafe -> automatic transition: ~p", [Reason]),
            State#state{callback_state = NewCbState, stage1 = Stage1}
    end.

process_actions([], State) -> State;
process_actions([{change_status, NewStatus} | RemainingActions], State=#state{status=OldStatus}) ->
    State1 = case change_mode(OldStatus, NewStatus, State) of
                 nochange -> State;
                 {ok, NewState} -> NewState;
                 {error, E} ->
                     lager:error("Callback requested invalid mode change: ~p", [E]),
                     State
             end,
    process_actions(RemainingActions, State1);
process_actions([{new_manual_out, ManualOut} | RemainingActions], State) ->
    process_actions(RemainingActions, State#state{manual_output = ManualOut});
process_actions([{new_s2, NewS2} | RemainingActions], State) ->
    process_actions(RemainingActions, update_s2(State ,NewS2));
process_actions([recalc_s3 | RemainingActions], State) ->
    process_actions(RemainingActions, State).

% Handle all actions related to state (strategy status)
% transitions like start/stop timers, update driver setpoint,
% notify event managers
-spec change_mode(control_status(), control_status(), State :: #state{}) ->
    nochange | {ok, NewState :: #state{}} | {error, Reason :: any()}.
change_mode(OldStatus, NewMode, _State) when OldStatus =:= NewMode ->
    nochange;
change_mode(failsafe, automatic, State) ->
    {ok, start_failsafe_timer(
           start_stage3_timer(
             update_status(State, automatic)
            ))
    };
change_mode(failsafe, manual, State = #state{
                                         room_id=RoomId,
                                         proc_key=K,
                                         manual_output = MO
                                        }) ->
    write_setpoint(K, MO),
    room_event:notify_strategy_status_change(RoomId, K, manual),
    {ok, cancel_failsafe_timer(
           cancel_stage3_timer(
             update_status(State, manual)
            ))
    };
change_mode(failsafe, disengaged, State = #state{
                                             room_id=RoomId,
                                             proc_key=K
                                            }) ->
    update_driver_mode(K, disengaged),
    room_event:notify_strategy_status_change(RoomId, K, disengaged),
    {ok, cancel_failsafe_timer(
           cancel_stage3_timer(
             update_status(State, disengaged)
            ))};
change_mode(automatic, manual, State = #state{
                                          room_id=RoomId,
                                          proc_key=K
                                         }) ->
    room_event:notify_strategy_status_change(RoomId, K, manual),
    {ok, cancel_failsafe_timer(
           cancel_stage3_timer(
             cancel_ptimer(
               update_status(State, manual)
              )))};
change_mode(automatic, disengaged, State = #state{
                                              room_id=RoomId,
                                              proc_key=K
                                             }) ->
    update_driver_mode(K, disengaged),
    room_event:notify_strategy_status_change(RoomId, K, disengaged),
    {ok, cancel_failsafe_timer(
           cancel_stage3_timer(
             cancel_ptimer(
               update_status(State, disengaged)
              )))};
change_mode(automatic, failsafe, State = #state{
                                            room_id=RoomId,
                                            proc_key=K,
                                            output_hist = OH
                                           }) ->
    case value_or_null(OH) of
        null ->
            lager:warning("No history to infer a failsafe setpoint!");
        Failsafe when is_float(Failsafe) ->
            write_setpoint(K, Failsafe)
    end,
    room_event:notify_strategy_status_change(RoomId, K, failsafe),
    {ok, cancel_failsafe_timer(
           cancel_stage3_timer(
             cancel_ptimer(
               update_status(State, failsafe)
              )))};
change_mode(manual, automatic, State = #state{
                                          room_id=RoomId,
                                          proc_key=K
                                         }) ->
    room_event:notify_strategy_status_change(RoomId, K, automatic),
    {ok, start_failsafe_timer(
           start_stage3_timer(
             update_status(State, automatic)
            ))};
change_mode(manual, disengaged, State = #state{
                                           room_id=RoomId,
                                           proc_key=K
                                          }) ->
    update_driver_mode(K, disengaged),
    room_event:notify_strategy_status_change(RoomId, K, disengaged),
    {ok, cancel_failsafe_timer(
           cancel_stage3_timer(
             update_status(State, disengaged)
            ))};
change_mode(disengaged, automatic, State = #state{
                                              room_id=RoomId,
                                              proc_key=K,
                                              manual_output = MO
                                             }) ->
    % In disengaged, the device has no (recent, good) S2 value.
    % Initialize it to manual_output.
    update_driver_mode(K, automatic),
    room_event:notify_strategy_status_change(RoomId, K, automatic),
    room_event:notify_stage2_change(RoomId, K, MO),
    {ok, start_failsafe_timer(
           start_stage3_timer(
             update_s2(
               update_status(State, automatic),
               MO
              )))};
change_mode(disengaged, manual, State = #state{
                                           room_id=RoomId,
                                           proc_key=K,
                                           manual_output = MO
                                          }) ->
    write_setpoint(K, MO),
    update_driver_mode(K, manual),
    room_event:notify_strategy_status_change(RoomId, K, manual),
    {ok, update_status(State, manual)};
change_mode(disengaged, failsafe, State = #state{
                                            room_id=RoomId,
                                            proc_key=K,
                                            output_hist = OH
                                           }) ->
    % Happens when the strategy is initialized from distributed mem
    % after a failover
    case value_or_null(OH) of
        null ->
            lager:warning("No history to infer a failsafe setpoint!");
        Failsafe when is_float(Failsafe) ->
            write_setpoint(K, Failsafe)
    end,
    room_event:notify_strategy_status_change(RoomId, K, failsafe),
    {ok, update_status(State, failsafe)};
change_mode(A, B, _State) ->
    {error, {invalid_mode_change, A, B}}.

