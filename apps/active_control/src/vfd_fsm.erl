%%
%% Timer description:
%%  pull_timer is how often to poll the device for state/status changes
%%  push_timer is how often to write the setpoint to the device, or 0.0 for "on change"
%%
%%  usually: pull_time > push_time

%%
%% States and timers:
%% disengaged: no timers running
%% disconnected: only the pull timer is running
%% standby: no timers running
%% reconnecting: only the failed I/O timer (push | pull) is running
%% online: all timers running
%% override: pull timer running
%%

-module(vfd_fsm).
-behaviour(base_driver).

%base_driver required callbacks
-export([init/3,reinit/3,handle_event/4,handle_sync_event/5,handle_info/4,
	 terminate/4,code_change/5,properties/1, new_output/4,property_changed/6]).

-export([set_output/2, set_status/2, get_property/2, set_property/3, set_properties/2, get_status/1, get_status/3, speed_to_cfm/2]).

-export([online/3, standby/3, disconnected/3, reconnecting/3, disengaged/3, override/3,
	state_change/4, pull_all/2]).

-import(shadoww_lib, [getprop/2]).

-include("ac_constants.hrl").
-include("drv_handler.hrl").

-include_lib("active_control/include/ac_status.hrl").

%%% ---------------------------------------------------
%%% Interface functions.
%%% ---------------------------------------------------

-callback properties() ->
    [term()].
-callback read_setpoint(Props :: [term()]) ->
    Val :: term() | [].
-callback read_comm(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_override(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_kw(Props :: [term()]) ->
    Val :: term() | [].
-callback write_setpoint(Props :: [term()], Setpoint :: term()) ->
    Setpoint :: term().
-callback read_over_current(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_over_voltage(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_under_voltage(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_over_temperature(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_general(Props :: [term()]) ->
    Val :: boolean() | [].
-callback write_clear_alarm(Props :: [term()]) ->
    Val :: any().

-spec set_output(pid(), float()) -> ok.
%%@doc Sets a new setpoint value
set_output(Pid, NewSpeed) ->
	base_driver:set_output(Pid, NewSpeed, infinity)
	.

-spec set_status(pid(), string()) -> ok.
%%@doc Sets a unitstatus value
set_status(Pid, NewStatus) ->
	base_driver:send_all_state_event(Pid, {set_status, NewStatus})
	.

-spec get_property(pid(), string()) -> ok.
%%@doc Returns a property value
get_property(Pid, PropertyName) ->
	base_driver:get_property(Pid, PropertyName, infinity)
	.

-spec set_property(pid(), string(), any()) -> ok.
%%@doc Set a property value
set_property(Pid, PropertyName, Value) ->
	base_driver:set_property(Pid, PropertyName, Value)
	.

-spec set_properties(pid(), [{string(), any()}]) -> ok.
%%@doc Sets values to a list of properties
set_properties(Pid, PropertyList) ->
	base_driver:set_properties(Pid, PropertyList).

get_status(Pid) ->
	base_driver:get_status(Pid).

-spec properties(atom() | tuple()) -> [proplists:property()].
%%@doc Returns a list of properties
properties(#state{protocol_mod = ProtocolMod}) ->
    get_device_props() ++ ProtocolMod:properties();
properties(ProtocolMod)  ->
    get_device_props() ++ ProtocolMod:properties().
get_device_props() ->
	[
		{"resource", r},
		{"setpoint", w},
		{"pushTime", r},
		{"pullTime", r},
		{"engaged", r},
		{"unitstatus", w},
		{"cfm_table", r},
		{"kw", w}
	].

%% Behavior callbacks

init(Id,Properties,ProtocolMod) ->
    init_it(init, Id,Properties,ProtocolMod).
reinit(Id, Properties, ProtocolMod) ->
    init_it(reinit, Id,Properties,ProtocolMod).
init_it(Action, Id, Properties, ProtocolMod) ->
	lager:debug([{id, Id}], "starting vfd_fsm(action: ~p, properties: ~p, protocol_mod: ~p", [Action, Properties, ProtocolMod]),
    TData = #state{
		id = Id,
	        device_class_mod = vfd_fsm,
	        protocol_mod = ProtocolMod,
		reconnect_count = 0,
		setpoint = [],
                device_setpoint = [],
		device_kw = []
	},
        Return = case {getprop("engaged", Properties), getprop("unitstatus",Properties)} of
		 {?DISENGAGED, _Any} ->
		     {ok, disengaged, TData, Properties};
		 {?ENGAGED, ?ONLINE} ->
		     TData1 = drv_handler:start_timer(all, TData,Properties),
		     {ok, online, TData1, Properties};
		 {?ENGAGED, ?DISCONNECTED} ->
		     TData1 = drv_handler:start_timer(pull, TData,Properties),
		     {ok, disconnected, TData1, Properties};
		{?ENGAGED, ?STANDBY} ->
		     {ok, standby, TData, Properties};
		{?ENGAGED, ?OVERRIDE} ->
		     TData1 = drv_handler:start_timer(pull, TData,Properties),
		     {ok, override, TData1, Properties}
	end,
    lager:debug([{id, Id}], "vfd_fsm started"),
    Return.

handle_event({set_status, NewStatus}, StateName = disengaged, TData = #state{id = Id}, Properties) ->
    {next_state, StateName, TData, drv_handler:setprop(Id, {"unitstatus", NewStatus},Properties)};

handle_event({set_status, _NewStatus = ?ONLINE}, StateName, TData, Properties)
		when StateName == override; StateName == reconnecting; StateName == disconnected ->
	{next_state, StateName, TData, Properties}
	;

handle_event({set_status, NewStatus}, _StateName, TData, Properties) ->
	case {getprop("unitstatus",Properties), NewStatus} of
		{_, ?ONLINE} -> {next_state, online, TData, Properties};
		{_, ?STANDBY} -> {next_state, standby, TData, Properties}
	end;

handle_event(Ev, StateName, TData, Properties) ->
    drv_handler:unexp_event(Ev, StateName, TData, Properties).
handle_sync_event(Ev, _From, StateName, TData, Properties) ->
    drv_handler:unexp_event(Ev, StateName, TData, Properties).

%%%%%%%%%%%%%% Modbus GW responses

% there may have been pending I/O requests before entering the disengaged state,
% so clean up the replies
handle_info({txn_abort, {FnId, TxRef}, AbortReason}, StateName, TData, Properties) ->
    drv_handler:txn_abort(FnId, TxRef, AbortReason, StateName, TData, Properties);

% Hey, the reconnect succeeded!
handle_info({txn_result, {FnId, TxRef}, Result}, StateName = reconnecting, TData = #state{failed_txn = FailedTxn, id = Id}, Properties)
		when
			(FnId == FailedTxn)
			and
			?io_txn_match(FnId, TxRef, TData)
			->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	NTData = drv_handler:restart_timer(FnId, TxRef, TData,Properties),
	case getprop("unitstatus", Properties) of
		?ONLINE -> {next_state, online, NTData, Properties};
		?OVERRIDE -> {next_state, override, NTData, Properties}
	end;

% Hey, the reconnect succeeded, but only if "comm" is true!
handle_info({txn_result, {FnId = pull, TxRef}, Result}, StateName = disconnected, TData = #state{id = Id}, Properties)
		when ?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case {getprop("comm", Result), getprop("override", Result)} of
		{false, _Any} ->
			{next_state, disconnected, drv_handler:restart_timer(pull, TxRef, TData,Properties), Properties}
			;
		{true, false} ->
		       {next_state, online, drv_handler:restart_timer(pull, TxRef, TData,Properties), Properties};
		{true, true} ->
			{next_state, override, drv_handler:restart_timer(pull, TxRef, TData,Properties), Properties}
	end
	;

handle_info({txn_result, {FnId = push, TxRef}, Result}, StateName = online, TData = #state{id = Id}, Properties)
		when ?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case getprop("pushTime", Properties) of
		0.0 -> {next_state, online, drv_handler:cancel_timer(push, TData), Properties};
		_Time -> {next_state, online, drv_handler:restart_timer(push, TxRef, TData,Properties), Properties}
	end
	;

handle_info({txn_result, {FnId = pull, TxRef}, Result}, StateName = online, TData = #state{protocol_mod = ProtocolMod, id = Id}, Properties)
		when
		?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case {getprop("comm", Result), getprop("override", Result)} of
		{false, _Any} ->
			{next_state, disconnected, drv_handler:restart_timer(pull, TxRef, TData,Properties), Properties};
		{true, true} ->
			%% process alarms here??
			NTData = TData#state{
				   device_setpoint = getprop("setpoint", Result),
				   device_kw = getprop("power", Result)
				  },
			{next_state, override, drv_handler:restart_timer(pull, TxRef, NTData,Properties), Properties};
		{true, false} ->
			Alerts = [{A,C} || {A,C} <-Result, lists:member(A,?VFD_ALERTS)],
			drv_handler:process_alerts(Alerts,Id),
			TxnRef = make_ref(),
			io_eval:run_async({clear, TxnRef}, fun(P,M) -> clear_alarm(P,M) end, [Properties, ProtocolMod]),
			NTData = TData#state{
				device_kw = getprop("power", Result),
           		        device_setpoint = getprop("setpoint", Result),
				over_current = getprop("overCurrent", Result),
				over_voltage = getprop("overVoltage", Result),
				under_voltage = getprop("underVoltage", Result),
				over_temperature = getprop("overTemperature", Result),
				general_alarm = getprop("generalVfdAlarm", Result)

			},
			{next_state, StateName, drv_handler:restart_timer(pull, TxRef, NTData,Properties), Properties}
	end
	;

handle_info({txn_result, {FnId = pull, TxRef}, Result}, StateName = override, TData = #state{id = Id}, Properties)
		when
		?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case {getprop("comm", Result), getprop("override", Result)} of
		{false, _Any} ->
			{next_state, disconnected, drv_handler:restart_timer(pull, TxRef, TData, Properties), Properties};
		{true, true} ->
			NTData = TData#state{
				device_setpoint = getprop("setpoint", Result),
				device_kw = getprop("power", Result)
			},
			{next_state, StateName, drv_handler:restart_timer(pull, TxRef, NTData,Properties), Properties};
		{true, false} ->
		       NTData = TData#state{
				device_setpoint = getprop("setpoint", Result),
				device_kw = getprop("power", Result)
			},
			{next_state, online, drv_handler:restart_timer(pull, TxRef, NTData,Properties), Properties}
	end
	;
handle_info({txn_result, {clear, _}, _}, StateName, TData, Properties) ->
	{next_state, StateName, TData, Properties};
handle_info({txn_result, {FnId, _TxRef}, Results}, StateName, TData = #state{id = Id}, Properties) ->
	lager:info([{id, Id}], "Discarding I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Results]),
	{next_state, StateName, TData, Properties}
	;

handle_info(Info, StateName, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "Unexpected handle_info info=~p, state=~p", [Info, StateName]),
	{next_state, StateName, TData, Properties}
	.

property_changed("engaged", Old, New, StateName, TData, Properties) when Old == New ->
	{next_state, StateName, TData, Properties};
property_changed("engaged", _Old, New, _StateName, TData, Properties) ->
	case {New, getprop("unitstatus", Properties)} of
		{?DISENGAGED, _Any} ->{next_state, disengaged, TData, Properties};
		{?ENGAGED, ?ONLINE} -> {next_state, online, TData, Properties};
		{?ENGAGED, ?DISCONNECTED} -> {next_state, disconnected, TData, Properties};
		{?ENGAGED, ?STANDBY} -> {next_state, standby, TData, Properties};
		{?ENGAGED, ?OVERRIDE} -> {next_state, override, TData, Properties}
	end;

property_changed(Pname, Old, New, StateName, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "updated property ~p ~p => ~p", [Pname, Old, New]),
	{next_state, StateName, TData, Properties}.

new_output(Value, StateName, TData, Properties) when not ?in_range_v(Value) ->
	{reject, out_of_range, StateName, TData, Properties}
	;
new_output(Value, StateName, TData, Properties) ->
    drv_handler:new_output(Value, StateName, TData, Properties).

get_status(StateName, TData, Properties) ->
    {EnvId, _, _} = TData#state.id,
    Setpoint = case TData#state.device_setpoint of
		   F when is_float(F) -> F;
		   _ -> TData#state.setpoint
	       end,
    #vfd_status{
       id = EnvId,
       resource = drv_handler:proplist_get("resource", Properties),
       status = drv_handler:device_state_to_status(StateName),
       setpoint = drv_handler:null_or_float(Setpoint),
       cfm = drv_handler:null_or_float(speed_to_cfm(TData#state.device_setpoint, drv_handler:proplist_get("cfm_table", Properties))),
       kw = drv_handler:null_or_float(TData#state.device_kw)
      }.

terminate(driver_stop, _StateName, _TData, _Properties) ->
	ok
	;
terminate(Reason, StateName, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "terminating(reason: ~p, state: ~p, tdata: ~p, properties: ~p", [Reason, StateName, TData, Properties])
	.


code_change(_OldVsn, StateName, TData, Properties, _Extra) ->
	{ok, StateName, TData, Properties}
	.


%% State transitions
state_change(online, disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> disengaged"),
	{next_state, disengaged, drv_handler:cancel_timer(all, TData), Properties}
	;
state_change(online, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> disconnected"),
    NProperties = drv_handler:setprop(Id, {"unitstatus", ?DISCONNECTED},Properties),
    NTData = drv_handler:start_timer(pull, drv_handler:cancel_timer(all, TData),NProperties),
    control_alarm:comm_broken(Id),
    {next_state, disconnected, NTData#state{reconnect_count = 0}, NProperties};

state_change(online,standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> standby"),
	NTData = drv_handler:cancel_timer(all, TData),
	{next_state, standby, NTData, drv_handler:setprop(Id, {"unitstatus", ?STANDBY},Properties)}
	;
state_change(online, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> reconnecting"),
	TData1 = case TData#state.failed_txn of
		push -> drv_handler:cancel_timer(pull, TData);
		pull -> drv_handler:cancel_timer(push, TData)
	end,

	{next_state, reconnecting, TData1, Properties}
	;
state_change(online, override, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> override"),
	{next_state, override, drv_handler:cancel_timer(push, TData), drv_handler:setprop(Id, {"unitstatus", ?OVERRIDE},Properties)}
	;

% This state change always in response to model change, so no need to update the model
state_change(standby, disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: standby -> disengaged"),
	{next_state, disengaged, TData, Properties}
	;
state_change(standby, disconnected, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "invalid state change: standby -> disconnected!"),
	{next_state, standby, TData, Properties}
	;
state_change(standby, online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: standby -> online"),
	NProperties = drv_handler:setprop(Id, {"unitstatus", ?ONLINE},Properties),
        NTData = drv_handler:start_timer(all, TData,NProperties),
	{next_state, online, NTData, NProperties}
	;

state_change(standby,reconnecting, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "invalid state change: standby -> reconnecting"),
	{next_state, standby, TData, Properties}
	;
state_change(standby,override, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "invalid state change: standby -> override!"),
	{next_state, standby, TData, Properties}
	;

% This state change always in response to model change, so no need to update the model
state_change(reconnecting, disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> disengaged"),
	NTData = drv_handler:cancel_timer(all, TData),
	{next_state, disengaged, NTData, Properties}
	;
state_change(reconnecting, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> disconnected"),
	lager:warning([{id, Id}], "unable to reconnect in ~p tries", [?RECONNECT_LIMIT]),

	io_alert:raise_connect_alert(TData#state.io_abort_reason, Id),
	NProperties = drv_handler:setprop(Id, {"unitstatus", ?DISCONNECTED},Properties),
    NTData = drv_handler:start_timer(pull, drv_handler:cancel_timer(all, TData),NProperties),
    {next_state, disconnected, NTData#state{reconnect_count = 0}, NProperties}
	;
state_change(reconnecting,online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> online"),
	case getprop("unitstatus",Properties) of
		?ONLINE -> ok;
		Other -> lager:error([{id, Id}], "Incorrect unit status for ONLINE state: ~p", [Other])
	end,
	NTData = drv_handler:start_timer(all, drv_handler:cancel_timer(all, TData),Properties),
	{next_state, online, NTData, Properties}
	;
state_change(reconnecting, standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> standby"),
	NTData = drv_handler:cancel_timer(all, TData),
	{next_state, standby, NTData, drv_handler:setprop(Id, {"unitstatus", ?STANDBY},Properties)}
	;

state_change(reconnecting, override, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> override"),
	TData1 = drv_handler:start_timer(pull, drv_handler:cancel_timer(all, TData),Properties),
	{next_state, override, TData1, Properties}
	;


% This state change always in response to model change, so no need to update the model
state_change(disconnected, disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> disengaged"),
	NTData = drv_handler:cancel_timer(pull, TData),
	{next_state, disengaged, NTData, Properties}
	;

state_change(disconnected, online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> online"),
	NProperties = drv_handler:setprop(Id, {"unitstatus", ?ONLINE},Properties),
        TData1 = drv_handler:start_timer(push, TData,NProperties),
	{next_state, online, TData1, NProperties}
	;
state_change(disconnected, standby, TData = #state{id = Id},Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> standby"),
	{next_state, standby, drv_handler:cancel_timer(pull, TData), drv_handler:setprop(Id, {"unitstatus", ?STANDBY},Properties)}
	;
state_change(disconnected, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "invalid state change: disconnected -> reconnecting!"),
	{next_state, disconnected, TData, Properties}
	;
state_change(disconnected, override, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> override"),
	NProperties = drv_handler:setprop(Id, {"unitstatus", ?OVERRIDE}, Properties),
	{next_state, override, TData, NProperties}
	;

state_change(disengaged, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> disconnected"),
	NTData = drv_handler:start_timer(pull, TData,Properties),
	{next_state, disconnected, NTData, Properties}
	;
state_change(disengaged, online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> online"),
	NTData = drv_handler:start_timer(all, TData,Properties),
	{next_state, online, NTData, Properties}
	;
state_change(disengaged, standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> standby"),
	{next_state, standby, TData, Properties}
	;
state_change(disengaged, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "invalid state change: disengaged -> reconnecting!"),
	{next_state, disengaged, TData, Properties}
	;
state_change(disengaged, override, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> override"),
	NTData = drv_handler:start_timer(pull, TData,Properties),
	{next_state, override, NTData, Properties}
	;

state_change(override, disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: override -> disengaged"),
	NTData = drv_handler:cancel_timer(all, TData),
	{next_state, disengaged, NTData, Properties}
	;
state_change(override, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: override -> disconnected"),
	{next_state, disconnected, TData, drv_handler:setprop(Id, {"unitstatus", ?DISCONNECTED},Properties)}
	;
state_change(override, online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: override -> online"),
    NProperties = drv_handler:setprop(Id, {"unitstatus", ?ONLINE}, Properties),
    NTData = drv_handler:start_timer(push, TData,NProperties),
	{next_state, online, NTData, NProperties}
	;
state_change(override, standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: override -> standby"),
	NTData = drv_handler:cancel_timer(all, TData),
	{next_state, standby, NTData, drv_handler:setprop(Id, {"unitstatus", ?STANDBY}, Properties)}
	;
state_change(override, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: override -> reconnecting"),
	{next_state, reconnecting, TData, Properties}.

online(Ev, TData, Properties) ->
    drv_handler:online(Ev, TData, Properties).

standby(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in standby state ev=~p, state=~p", [Ev, TData]),
	{next_state, standby, TData, Properties}
	.

reconnecting(Ev, TData, Properties) ->
    drv_handler:reconnecting(Ev, TData, Properties).

disconnected(pull, TData = #state{protocol_mod = ProtocolMod, id = Id}, Properties) ->
	TxnRef = make_ref(),
	io_eval:run_async({pull, TxnRef}, fun(P,M) -> pull_all(P,M) end, [Properties,ProtocolMod]),
	lager:debug([{id, Id}], "Starting I/O request state=disconnected, fnid=pull"),
	{next_state, disconnected, TData#state{
		pull_timer = {pending, TxnRef}
	}, Properties}
	;

disconnected(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in disconnected state ev=~p, state=~p", [Ev, TData]),
	{next_state, disconnected, TData, Properties}
	.

override(pull, TData = #state{protocol_mod = ProtocolMod, id = Id}, Properties)->
	TxnRef = make_ref(),
	io_eval:run_async({pull, TxnRef}, fun(P,M) -> pull_all(P,M) end, [Properties,ProtocolMod]),
	lager:debug([{id, Id}], "Starting I/O request state=override, fnid=pull"),
	{next_state, override, TData#state{
		pull_timer = {pending, TxnRef}
	}, Properties}
	;

override(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in override state ev=~p, state=~p", [Ev, TData]),
	{next_state, override, TData, Properties}
	.


disengaged(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in disengaged state ev=~p, state=~p", [Ev, TData]),
	{next_state, disengaged, TData, Properties}
	.

pull_all(Props,PMod) ->
    case PMod:read_comm(Props) of
	false ->
	    [{"comm", false}, {"override", false}];
	true ->
	    [
	     {"comm", true},
	     {"override", PMod:read_override(Props)},
	     {"power", PMod:read_kw(Props)},
	     {"setpoint", PMod:read_setpoint(Props)},
	     {"overCurrent", PMod:read_over_current(Props)},
	     {"overVoltage", PMod:read_over_voltage(Props)},
	     {"underVoltage", PMod:read_under_voltage(Props)},
	     {"overTemperature", PMod:read_over_temperature(Props)},
	     {"generalVfdAlarm", PMod:read_general(Props)}
	    ]
    end
	.

clear_alarm(Props, PMod) ->
	PMod:write_clear_alarm(Props).

% CfmTable is a list of {Speed, Cfm} points
% Using this table, linearly interpolate to find the Cfm for actual Speed
speed_to_cfm([], _) -> [];
speed_to_cfm(Speed, CfmTable) ->
    r_interpolate(Speed, CfmTable).

r_interpolate(X0, [{X,Y} | Pts]) ->
    if
	X0 < X ->
	    r_interpolate(X0, Pts);
	X0 == X ->
	    Y;
	X0 > X ->
	    {X1, Y1} = hd(Pts),
	    XScale = (X0-X) / (X1-X),
	    Y + XScale * (Y1-Y)
    end.

