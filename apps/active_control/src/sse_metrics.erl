%%%----------------------------------------------------------------------
%%% File    : server_sent_events.erl
%%% Author  : Steve Vinoski <vinoski@ieee.org>
%%% Purpose : Server-Sent Events example
%%% Created : 1 June 2012 by Steve Vinoski <vinoski@ieee.org>
%%%----------------------------------------------------------------------
-module(sse_metrics).
-behaviour(gen_server).

-include_lib("yaws/include/yaws_api.hrl").

%% API
-export([out/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
        terminate/2, code_change/3]).

-export([
        tag_info/1,
        metric_type/1,
        metric_value/1
    ]).

-record(state, {
        sock,
        yaws_pid,
        timer,
        metrics,
        delay_msec
    }).

out(A) ->
    case (A#arg.req)#http_request.method of
        'GET' ->
            case yaws_api:get_header(A#arg.headers, accept) of
                undefined ->
                    {status, 406};
                Accept ->
                    case string:str(Accept, "text/event-stream") of
                        0 ->
                            {status, 406};
                        _ ->
                            {ok, Pid} = gen_server:start(?MODULE, [A], []),
                            yaws_sse:headers(Pid)
                    end
            end;
        _ ->
            [{status, 405},
                {header, {"Allow", "GET"}}]
    end.

init([Arg]) ->
    process_flag(trap_exit, true),
    QueryVars = yaws_api:parse_query(Arg),
    MetricStrList = proplists:get_all_values("m", QueryVars),
    {ok, DelayStr} = yaws_api:getvar(Arg, "d"),
    Metrics = [eval_term(MetricStr) || MetricStr <- MetricStrList],
    Delay = round(max(str_to_float(DelayStr), 0.1) * 1000),
    {ok, #state{
            sock = Arg#arg.clisock,
            metrics = Metrics,
            delay_msec = Delay
        }}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({ok, YawsPid}, #state{delay_msec=Delay}=State) ->
    {ok, Timer} = timer:send_interval(Delay, self(), tick),
    {noreply, State#state{yaws_pid=YawsPid, timer=Timer}};
handle_info({discard, _YawsPid}, State) ->
    %% nothing to do
    {stop, normal, State};
handle_info(tick, #state{sock=Socket,metrics=Metrics}=State) ->
    Json =  [{struct, [{"metric", term_to_string(Metric)}, {"value", metric_value(Metric)}]} || Metric <- Metrics],
    Data = yaws_sse:data(json2:encode(Json)),
    case yaws_sse:send_events(Socket, Data) of
        ok ->
            {noreply, State};
        {error, closed} ->
            {stop, normal, State};
        {error, Reason} ->
            {stop, Reason, State}
    end;
handle_info({tcp_closed, _}, State) ->
    {stop, normal, State#state{sock=closed}};
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, #state{sock=Socket, yaws_pid=YawsPid, timer=Timer}) ->
    case Timer of
        undefined ->
            ok;
        _ ->
            timer:cancel(Timer)
    end,
    yaws_api:stream_process_end(Socket, YawsPid),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% --


eval_term(Str) ->
    {ok, Tokens, _} = erl_scan:string(Str ++ " ."),
    {ok, Term} = erl_parse:parse_term(Tokens),
    Term
    .

str_to_float(Str) ->
    try float(list_to_integer(Str))
        catch error:_ -> list_to_float(Str)
    end
    .

% For a folsom tag, return all metric names and types
% think folsom_metrics:get_metric_info() for tags
tag_info(Tag) ->
    MetricNames = [Name || {Name, _Value} <- folsom_metrics:get_metrics_value(Tag)],
    [{MetricName, metric_type(MetricName)} || MetricName <- MetricNames]
    .

metric_type(memory) -> vm;
metric_type({system_info,_K}) -> vm;
metric_type({system_info,_K,_K1}) -> vm;
metric_type(statistics) -> vm;
metric_type({statistics,_K}) -> vm;
metric_type(Id) ->
    [{Id, Pl}] = folsom_metrics:get_metric_info(Id),
    proplists:get_value(type, Pl)
    .

% whatever is returned must be JSON encodeable
metric_value(memory) ->
    {struct, folsom_vm_metrics:get_memory()}
    ;
metric_value({system_info, K}) ->
    S = proplists:get_value(K, folsom_vm_metrics:get_system_info()),
    {struct, S}
    ;
metric_value({system_info, K, K1}) ->
    S = proplists:get_value(K, folsom_vm_metrics:get_system_info()),
    S1 = proplists:get_value(K1, S),
    {struct, S1}
    ;
metric_value(statistics) ->
    {struct, shadoww_lib:keyfilter(
               [context_switches, run_queue],
               1,
               folsom_vm_metrics:get_statistics()
              )}
    ;
metric_value({statistics,K}) ->
    S = proplists:get_value(K, folsom_vm_metrics:get_statistics()),
    {struct, S}
    ;
metric_value(Id) ->
    case metric_type(Id) of
        histogram -> {struct, shadoww_lib:keyfilter([min, max, arithmetic_mean, geometric_mean, harmonic_mean, median, variance, standard_deviation, skewness, kurtosis], 1, folsom_metrics:get_histogram_statistics(Id))};
        history -> evs_to_json(folsom_metrics:get_history_values(Id, 10));
        meter -> {struct, shadoww_lib:keyfilter([one, five, fifteen, day, mean], 1, folsom_metrics:get_metric_value(Id))};
        _ -> folsom_metrics:get_metric_value(Id)
    end
    .

evs_to_json(Evs) -> {struct, [{"events", {array, [ev_to_json(Ev) || Ev <- Evs]}}]} .
ev_to_json({TS, PL}) ->
    {struct, [{"timestamp", TS}, {"event", term_to_string(proplists:get_value(event, PL))}]}
    .

%format a term with ~p and return as a string
term_to_string(Term) ->
    list_to_binary(io_lib:format("~p", [Term]))
    .

