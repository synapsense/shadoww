
-module(protocol_bindings).

-export([
         modbus_bindings/2,
         bacnet_bindings/3,
         to_proplist/1,
         to_bindings/1
        ]).

-include_lib("active_control/include/ac_api.hrl").

modbus_bindings(Ip, DeviceId) when is_list(Ip), is_integer(DeviceId) ->
    #modbus_bindings{
       ip = Ip,
       device_id = DeviceId
      }.

bacnet_bindings(Ip, Network, Device) when is_list(Ip), is_integer(Network), is_integer(Device) ->
    #bacnet_bindings{
       ip = Ip,
       network = Network,
       device = Device
      }.

to_proplist(#modbus_bindings{ip = Ip, device_id = Devid}) ->
    [{"ip", Ip}, {"devid", Devid}]
    ;
to_proplist(#bacnet_bindings{ip = Ip, network = Network, device = Device}) ->
    [{"ip", Ip}, {"network", Network}, {"devid", Device}]
    .

to_bindings(#modbus_bindings{ip = Ip, device_id = Devid}) ->
    [{'IP', Ip}, {'Device', Devid}]
    ;
to_bindings(#bacnet_bindings{ip = Ip, network = Network, device = Device}) ->
    [{'IP', Ip}, {'Net', Network}, {'Device', Device}]
    .
