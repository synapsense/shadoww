
-module(sensor_data_feeder).


-behaviour(gen_server).


% api functions
-export([
	start_link/2
	]).

% behaviour callbacks
-export([
	init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3
	]).

% utility functions
-export([connect/1]).

-define(RECV_TIMEOUT, 5 * 60 * 1000).  % 5 minutes
-define(RETRY_TIMEOUT, 30000).  % 30 seconds
-define(SERVER, ?MODULE).

-define(SENSOR_EVT_MGR, sensor_event).

-record(state,
	{
		socket,
		host,
		port,
		remaining_bytes,
		reconnect_timer,
		inactivity_timer
	}).

start_link(DmHost, DmPort) ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [DmHost, DmPort], [])
	.

% behaviour callbacks
init([DmHost, DmPort]) ->
	State = #state{
		socket = undefined,
		host = DmHost,
		port = DmPort,
		remaining_bytes = []
	},
	IsActive = application:get_env(data_feeder_active),

	folsom_metrics:new_meter(dm_bytes),
	folsom_metrics:tag_metric(dm_bytes, sensor_data_feeder),

	folsom_metrics:new_meter(dm_sensor_data),
	folsom_metrics:tag_metric(dm_sensor_data, sensor_data_feeder),

	folsom_metrics:new_history(sdf_connect),
	folsom_metrics:tag_metric(sdf_connect, sensor_data_feeder),

	if
		{ok, false} == IsActive ->
			{ok, State};

		({ok, true} == IsActive) or (undefined == IsActive) ->
			{ok, State, 0}
	end
	.


handle_call(Message, From, State) ->
	lager:error("Unexpected handle_call. message: ~p, from: ~p, state: ~p", [Message, From, State]),
	{reply, huh, State}
	.


handle_cast(Request, State) ->
	lager:error("Unexpected handle_cast. request: ~p, state: ~p", [Request, State]),
	{noreply, State}
	.


handle_info(timeout, State) ->
	{noreply, connect(State)}
	;
handle_info(reconnect, State) ->
	{noreply, connect(close_socket(State))}
	;
handle_info(link_inactive, State) ->
	lager:error("ControlTransport shut down due to inactivity for ~b seconds", [?RECV_TIMEOUT div 1000]),
	self() ! reconnect,
	{noreply, close_socket(State)}
	;
handle_info({tcp, Socket, Data}, State) ->
	lager:debug("Received packet ~p", [Data]),
	folsom_metrics:notify({dm_bytes, size(Data)}),
	{Messages, RemainingBytes} = parse(State#state.remaining_bytes ++ binary_to_list(Data)),
	lager:debug("tcp packet size: ~b, JSON messages ~b, remaining bytes after parsing: ~b", [size(Data), length(Messages), length(RemainingBytes)]),
	[process_message(Type, TO, M) || {Type, TO, M} <- Messages],
	NewState = start_inactivity_timer(State#state{remaining_bytes = RemainingBytes}),
	ok = inet:setopts(Socket, [{active, once}]),
	{noreply, NewState}
	;
handle_info({tcp_closed, Socket}, State) ->
	lager:warning("ControlTransport [~p] closed by remote, reconnecting", [Socket]),
	self() ! reconnect,
	{noreply, cancel_inactivity_timer(close_socket(State))}
	;
handle_info(Info, State) ->
	lager:error("Unexpected handle_info. info: ~p, state: ~p", [Info, State]),
	{noreply, State}
	.


terminate(normal, _StateData) ->
	ok
	;
terminate(Reason, State) ->
	lager:warning("terminating(~p, ~p)", [Reason, State]),
	close_socket(State)
	.

code_change(_OldVsn, StateData, _Extra) ->
	{ok, StateData}
	.

% utility function

connect(State = #state{host = Host, port = Port}) ->
	lager:info("Connecting to ControlTransport at ~p:~p", [Host, Port]),
	folsom_metrics:notify(sdf_connect, Host),
	case gen_tcp:connect(Host, Port, [binary, {packet, 0}, {keepalive, true}, {active, once}]) of
		{ok, Socket} ->
			lager:info("Connection succeeded!"),
			start_inactivity_timer(cancel_reconnect_timer(State#state{socket = Socket}));
		{error, Reason} ->
			lager:error("(~p) Connection to ControlTransport failed!", [Reason]),
			start_reconnect_timer(State)
	end
	.

parse(RawBytes) ->
	{L, R} = parse0([], RawBytes),
	{lists:reverse(L), R}
	.

parse0(Accum, RawBytes) ->
	case string:str(RawBytes, "\r\n") of
		0 ->
			{Accum, RawBytes};

		N ->
			First = string:sub_string(RawBytes, 1, N-1),
			Remaining = string:sub_string(RawBytes, N+2),

			{ok, {struct, GenericMessage}} = json2:decode_string(First),
			{"type", Type} = lists:keyfind("type", 1, GenericMessage),
			{"to", Id} = lists:keyfind("to", 1, GenericMessage),
			parse0([{Type, list_to_binary(Id), GenericMessage} | Accum], Remaining)
	end
	.


process_message("WSN_SENSOR_DATA", TO, GenericMessage) ->
	folsom_metrics:notify({dm_sensor_data, 1}),
	{"DATA", DataValue} = lists:keyfind("DATA", 1, GenericMessage),
	{"TIMESTAMP", Timestamp} = lists:keyfind("TIMESTAMP", 1, GenericMessage),
	Value = sample:new(TO, DataValue, Timestamp),
	et:trace_me(50, ?MODULE, ?SENSOR_EVT_MGR, sensor_value, Value),
	gen_key_event:notify(?SENSOR_EVT_MGR, TO, Value)
	;

process_message(Type, _TO, GenericMessage) ->
	lager:error("Received unknown message ~p of type ~p from Device Manager", [GenericMessage, Type]),
	ok
	.

close_socket(State = #state{socket = Sock}) when is_port(Sock) ->
	catch gen_tcp:close(Sock),
	State#state{socket = undefined};
close_socket(State) ->
	State.

start_reconnect_timer(S = #state{}) ->
	cancel_reconnect_timer(S),
	S#state{reconnect_timer = erlang:send_after(?RETRY_TIMEOUT, self(), reconnect)}
	.

cancel_reconnect_timer(S = #state{reconnect_timer = RT}) when RT == undefined ->
	S;
cancel_reconnect_timer(S = #state{reconnect_timer = RT}) ->
	catch erlang:cancel_timer(RT),
	S#state{reconnect_timer = undefined}
	.

start_inactivity_timer(S = #state{}) ->
	cancel_inactivity_timer(S),
	S#state{inactivity_timer = erlang:send_after(?RECV_TIMEOUT, self(), link_inactive)}
	.

cancel_inactivity_timer(S = #state{inactivity_timer = IT}) when IT == undefined ->
	S;
cancel_inactivity_timer(S = #state{inactivity_timer = IT}) ->
	catch erlang:cancel_timer(IT),
	S#state{inactivity_timer = undefined}
	.

