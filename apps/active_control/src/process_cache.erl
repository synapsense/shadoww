-module(process_cache).

-export([
    load_properties/1,
    initial_config/2,
    delete_properties/1,
    update_properties/2,
    start/0,
    stop/0,
    isolated/1
    ]).

-define(TABLE, state_cache).


load_properties(Id = {_,_,_}) ->
    case dumbtable:get(?TABLE, Id) of
        none ->
            {error, no_data};
        {value, Val} ->
            {ok, Val}
    end.

initial_config(Id = {_,_,_}, Config) ->
    case dumbtable:get(?TABLE, Id) of
        none ->
            dumbtable:put(?TABLE, Id, Config),
            Config;
        {value, OldConfig} ->
            OldConfig
    end.

delete_properties(Id = {_,_,_}) ->
    dumbtable:remove(?TABLE, Id).

update_properties(Id = {_,_,_}, Properties) ->
    dumbtable:async_put(?TABLE, Id, Properties).

start() ->
    lager:info("Creating local copy of table ~p", [?TABLE]),
    ok = dumbtable:create_local_copy(?TABLE).

stop() ->
    lager:info("Deleting local copy of table ~p", [?TABLE]),
    ok = dumbtable:delete_local_copy(?TABLE).

isolated(Isolated) ->
    HasLocal = lists:member(?TABLE, dumbtable:list_local_tables()),
    if
        Isolated andalso HasLocal -> stop();
        not (Isolated andalso HasLocal) -> start()
    end.

