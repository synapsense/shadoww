%%
%% Used to create instances of the API records
%%

-module(room_builder).

-export([
	single_input/6,
	generic_crah/10,
	liebert_ahu/1,
	generic_crac/15,
	generic_vfd/11,
	ach550/0,
	crah_driver/8,
	crac_driver/9,
	vfd_driver/7,
	raised_floor_crah_strategy/6,
	raised_floor_vfd_strategy/4,
	remote_control_strategy/4,
	raised_floor_balancer_strategy/3,
	raised_floor_crah/6,
	raised_floor_room/8,
	raised_floor_room/9,
	remote_control_crah/5,
	remote_control_room/2,
	remote_control_room/3,
	heartbeat_manager/3
	]).

-include_lib("active_control/include/ac_api.hrl").

single_input(Id, SensorId, Location, Target, Enabled, Resource)
		when
		is_binary(Id), is_binary(SensorId), is_float(Target),
		is_boolean(Enabled), is_atom(Resource)
		->
	#single_input{
		id = Id,
		sensor_id = SensorId,
		location = Location,
		target = Target,
		enabled = Enabled,
		resource = Resource
	}.

generic_crah(
		SetpointWrite,
		SetpointRead,
		ValveRead,
		RatRead,
		SatRead,
		StandbyRead,
		CommRead,
		ChangeFiltersAlarmRead,
		GeneralAlarmRead,
		ClearAlarmWrite
			) ->
	#generic_crah{
		setpoint_write = SetpointWrite,
		setpoint_read = SetpointRead,
		valve_read = ValveRead,
		rat_read = RatRead,
		sat_read = SatRead,
		standby_read = StandbyRead,
		comm_read = CommRead,
		change_filters_alarm_read = ChangeFiltersAlarmRead,
		general_alarm_read = GeneralAlarmRead,
		clear_alarm_write = ClearAlarmWrite
	}.

liebert_ahu(SitelinkPort) ->
	#liebert_ahu{
		sitelink_port = SitelinkPort
	}.

generic_crac(
		SetpointWrite,
		SetpointRead,
		RatRead,
		SatRead,
		CompressorRead,
		CapacityRead,
		StandbyRead,
		CommRead,
		HighHeadPressureAlarmRead,
		LowSuctionPressureAlarmRead,
		CompressorOverloadAlarmRead,
		ShortCycleAlarmRead,
		ChangeFiltersAlarmRead,
		GeneralAlarmRead,
		ClearAlarmWrite
			) ->
	#generic_crac{
		setpoint_write = SetpointWrite,
		setpoint_read = SetpointRead,
		rat_read = RatRead,
		sat_read = SatRead,
		compressor_read = CompressorRead,
		capacity_read = CapacityRead,
		standby_read = StandbyRead,
		comm_read = CommRead,
		high_head_pressure_alarm_read = HighHeadPressureAlarmRead,
		low_suction_pressure_alarm_read = LowSuctionPressureAlarmRead,
		compressor_overload_alarm_read = CompressorOverloadAlarmRead,
		short_cycle_alarm_read = ShortCycleAlarmRead,
		change_filters_alarm_read = ChangeFiltersAlarmRead,
		general_alarm_read = GeneralAlarmRead,
		clear_alarm_write = ClearAlarmWrite
	}.

generic_vfd(
		SetpointWrite,
		SetpointRead,
		PowerRead,
		LocalOverrideRead,
		CommRead,
		OverCurrentAlarmRead,
		OverVoltageAlarmRead,
		UnderVoltageAlarmRead,
		OverTemperatureAlarmRead,
		GeneralAlarmRead,
		ClearAlarmWrite
			) ->
	#generic_vfd{
		setpoint_write = SetpointWrite,
		setpoint_read = SetpointRead,
		power_read = PowerRead,
		local_override_read = LocalOverrideRead,
		comm_read = CommRead,
		over_current_alarm_read = OverCurrentAlarmRead,
		over_voltage_alarm_read = OverVoltageAlarmRead,
		under_voltage_alarm_read = UnderVoltageAlarmRead,
		over_temperature_alarm_read = OverTemperatureAlarmRead,
		general_alarm_read = GeneralAlarmRead,
		clear_alarm_write = ClearAlarmWrite
	}.

ach550() -> #ach550{}.

crah_driver(
		Id,
		Resource,
		PullTime,
		PushTime,
		CoolingControl,
		CascadeStandby,
		ProtocolBindings,
		DriverConfig
			) ->
	#crah_driver{
		id = Id,
		resource = Resource,
		pull_time = PullTime,
		push_time = PushTime,
		cooling_control = CoolingControl,
		cascade_standby = CascadeStandby,
		protocol_bindings = ProtocolBindings,
		driver_config = DriverConfig
	}.

crac_driver(
		Id,
		Resource,
		PullTime,
		PushTime,
		CoolingControl,
		MaxStages,
		CascadeStandby,
		ProtocolBindings,
		DriverConfig
			) ->
	#crac_driver{
		id = Id,
		resource = Resource,
		pull_time = PullTime,
		push_time = PushTime,
		cooling_control = CoolingControl,
		max_stages = MaxStages,
		cascade_standby = CascadeStandby,
		protocol_bindings = ProtocolBindings,
		driver_config = DriverConfig
	}.

vfd_driver(
		Id,
		Resource,
		PullTime,
		PushTime,
		CfmTable,
		ProtocolBindings,
		DriverConfig
			) ->
	#vfd_driver{
		id = Id,
		resource = Resource,
		pull_time = PullTime,
		push_time = PushTime,
		cfm_table = CfmTable,
		protocol_bindings = ProtocolBindings,
		driver_config = DriverConfig
	}.

raised_floor_crah_strategy(
		Id,
		Resource,
		ManualOutput,
		Mode,
		SupplySensor,
		ReturnSensor
			) ->
	#raised_floor_crah_strategy{
		id = Id,
		resource = Resource,
		manual_output = ManualOutput,
		mode = Mode,
		supply_sensor = SupplySensor,
		return_sensor = ReturnSensor
	}.

raised_floor_vfd_strategy(
		Id,
		Resource,
		ManualOutput,
		Mode
			) ->
	#raised_floor_vfd_strategy{
		id = Id,
		resource = Resource,
		manual_output = ManualOutput,
		mode = Mode
	}.

remote_control_strategy(
		Id,
		Resource,
		ManualOutput,
		Mode
			) ->
	#remote_control_strategy{
		id = Id,
		resource = Resource,
		manual_output = ManualOutput,
		mode = Mode
	}.

raised_floor_balancer_strategy(
		Id,
		Resource,
		Aggressiveness
			) ->
	#raised_floor_balancer_strategy{
		id = Id,
		resource = Resource,
		aggressiveness = Aggressiveness
	}.

raised_floor_crah(
		Id,
		Location,
		none,
		none,
		PressureDriver,
		PressureStrategy
			) ->
	#raised_floor_crah{
		id = Id,
		location = Location,
		temperature_driver = none,
		temperature_strategy = none,
		pressure_driver = PressureDriver,
		pressure_strategy = PressureStrategy
	};
raised_floor_crah(
        Id,
        Location,
        #crah_driver{} = TemperatureDriver,
        TemperatureStrategy,
        PressureDriver,
        PressureStrategy) ->
    #raised_floor_crah{
        id = Id,
        location = Location,
        temperature_driver = TemperatureDriver,
        temperature_strategy = TemperatureStrategy,
        pressure_driver = PressureDriver,
        pressure_strategy = PressureStrategy
    }.

raised_floor_room(Id,
		  Bounds,
		  ContainedRooms,
		  TemperatureInputs,
		  PressureInputs,
		  Crahs,
		  TemperatureBalancer,
		  PressureBalancer) ->
    raised_floor_room(Id, Bounds, ContainedRooms, none,
		      TemperatureInputs, PressureInputs,
		      Crahs, TemperatureBalancer,
		      PressureBalancer).

raised_floor_room(Id,
		  Bounds,
		  ContainedRooms,
		  HeartbeatManager,
		  TemperatureInputs,
		  PressureInputs,
		  [#raised_floor_crah{temperature_driver = TmpDrv,temperature_strategy = TmpStrat} | _] = Crahs,
		  _TemperatureBalancer,
		  PressureBalancer)
    when TmpDrv =:= none, TmpStrat =:= none ->
    #raised_floor_room{
      id = Id,
      bounds = Bounds,
      contained_rooms = ContainedRooms,
      heartbeat_manager = HeartbeatManager,
      temperature_inputs = TemperatureInputs,
      pressure_inputs = PressureInputs,
      crahs = Crahs,
      temperature_balancer = none,
      pressure_balancer = PressureBalancer
    };

raised_floor_room(Id,
		  Bounds,
		  ContainedRooms,
		  HeartbeatManager,
		  TemperatureInputs,
		  PressureInputs,
		  Crahs,
		  TemperatureBalancer,
		  PressureBalancer) ->
    #raised_floor_room{
      id = Id,
      bounds = Bounds,
      contained_rooms = ContainedRooms,
      heartbeat_manager = HeartbeatManager,
      temperature_inputs = TemperatureInputs,
      pressure_inputs = PressureInputs,
      crahs = Crahs,
      temperature_balancer = TemperatureBalancer,
      pressure_balancer = PressureBalancer
    }.

remote_control_crah(Id,
		    #crac_driver{} = TemperatureDriver,
		    TemperatureStrategy,
		    AirflowDriver,
		    AirflowStrategy) ->
    #remote_control_crac{
       id = Id,
       temperature_driver = TemperatureDriver,
       temperature_strategy = TemperatureStrategy,
       airflow_driver = AirflowDriver,
       airflow_strategy = AirflowStrategy
      };
remote_control_crah(
		Id,
		TemperatureDriver,
		TemperatureStrategy,
		AirflowDriver,
		AirflowStrategy) ->
    #remote_control_crah{
       id = Id,
       temperature_driver = TemperatureDriver,
       temperature_strategy = TemperatureStrategy,
       airflow_driver = AirflowDriver,
       airflow_strategy = AirflowStrategy
      }.

remote_control_room(Id, Crahs) ->
	remote_control_room(Id, none, Crahs).

remote_control_room(Id, Heartbeat, Crahs) ->
    #remote_control_room{
       id = Id,
       heartbeat_manager = Heartbeat,
       crahs = Crahs
      }.

heartbeat_manager(Interval, HeartbeatWrite, ProtocolBindings) ->
    #heartbeat_manager{
       interval = Interval,
       heartbeat_write = HeartbeatWrite,
       protocol_bindings = ProtocolBindings
      }.


