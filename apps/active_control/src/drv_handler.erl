%%Helper module for crah_fsm,crac_fsm, vfd_fsm
-module(drv_handler).

-export([init/3, unexp_event/4, unexp_info/4, txn_abort/6,
	 txn_result/7, property_changed/6, new_output/4, state_change/4, online/3, standby/3,
	 reconnecting/3, disconnected/3, disengaged/3]).

-export([start_timer/3, r_start_timer/3, restart_timer/4, cancel_timer/2, r_cancel_timer/2, check_stop_timer/2,
	 setprop/3, proplist_get/2, setproplist/3]).
-export([
	 process_alerts/2,
	 crac_alarm_properties/0,
	 crah_alarm_properties/0,
	 vfd_alarm_properties/0
	]).
-export([
	 i_to_f/1,
	 null_or_float/1,
	 device_state_to_status/1
	]).

-import(shadoww_lib, [getprop/2]).

-include("ac_constants.hrl").
-include("ac_api.hrl").
-include("drv_handler.hrl").

init(_Id, Properties,TData) ->
    case {getprop("engaged",Properties), getprop("unitstatus",Properties)} of
	{?DISENGAGED, _Any} ->
	    {ok, disengaged, TData, Properties};
	{?ENGAGED, ?ONLINE} ->
	    NTData = start_timer(all, TData,Properties),
	    {ok, online, NTData, Properties};
	{?ENGAGED, ?DISCONNECTED} ->
	    NTData = start_timer(pull, TData,Properties),
	    {ok, disconnected, NTData, Properties};
	{?ENGAGED, ?STANDBY} ->
	    NTData = start_timer(pull, TData,Properties),
	    {ok, standby, NTData, Properties}
    end.

unexp_event(Ev, StateName, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "Unexpected handle_event event=~p, state=~p", [Ev, StateName]),
	{next_state, StateName, TData, Properties}.

unexp_info(Info, StateName, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "Unexpected handle_info info=~p, state=~p", [Info, StateName]),
	{next_state, StateName, TData, Properties}.

txn_abort(FnId, TxRef, AbortReason, StateName = disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "Discarding I/O failure state=~p, data=~p, fnid=~p, tx_ref=~p, reason=~p", [StateName, TData, FnId, TxRef, AbortReason]),
	{next_state, disengaged, TData, Properties}
	;

%% Aborts are expected in the disconnected state, keep trying to reconnect
txn_abort(FnId = pull, TxRef, AbortReason, StateName = disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "Discarding I/O failure state=~p, data=~p, fnid=~p, tx_ref=~p, reason=~p", [StateName, TData, FnId, TxRef, AbortReason]),
	NTData = restart_timer(pull, TxRef, TData,Properties),
	{next_state, disconnected, NTData, Properties}
	;

% There could have been an outstanding push I/O request before entering the disconnected state.
% Discard the results
txn_abort(FnId = push, TxRef, AbortReason, StateName = disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "Discarding I/O failure state=~p, data=~p, fnid=~p, tx_ref=~p, reason=~p", [StateName, TData, FnId, TxRef, AbortReason]),
	{next_state, disconnected, TData, Properties}
	;


% Note to reader: how to handle reconnects?
% need to guard against a failing push but a succeeding pull
% record the txn type that failed and use that one to reconnect

% If this is the response we think it is and we've reached the reconnect count, go ahead and report disconnected
txn_abort(FnId = pull, TxRef, AbortReason, _StateName = reconnecting, TData =
		#state{ reconnect_count = ReconnectCount, failed_txn = FailedTxn, id = Id}, Properties)
		when
			(FailedTxn == pull)
			and
			?io_txn_match(FnId, TxRef, TData)
			and
			(ReconnectCount > ?RECONNECT_LIMIT) ->

	NTData = restart_timer(pull, TxRef, TData,Properties),
	lager:warning([{id, Id}], "unable to reconnect fnid=~p, reason=~p, max_retries=~p", [FnId, AbortReason, ?RECONNECT_LIMIT]),
	{next_state, disconnected, NTData, Properties}
	;

% Make sure this is the response we think it is, and if so up the reconnect count
txn_abort(FnId = pull, TxRef, AbortReason, _StateName = reconnecting, TData =
		#state{reconnect_count = ReconnectCount, failed_txn = FailedTxn, id = Id}, Properties)
		when (FailedTxn == pull) and ?io_txn_match(FnId, TxRef, TData) ->

	NTData = restart_timer(pull, TxRef, TData,Properties),

	lager:warning([{id, Id}], "reconnect attempt failed fnid=~p, reason=~p, retry=~p", [FnId, AbortReason, ReconnectCount]),
	{next_state, reconnecting, NTData#state{
		io_abort_reason = AbortReason,
		reconnect_count = ReconnectCount + 1
	}, Properties}
	;

% If this is the response we think it is and we've reached the reconnect count, go ahead and report disconnected
txn_abort(FnId = push, TxRef, AbortReason, reconnecting, TData =
		#state{reconnect_count = ReconnectCount, failed_txn = FailedTxn, id = Id}, Properties)
		when
			(FailedTxn == push)
			and
			?io_txn_match(FnId, TxRef, TData)
			and
			(ReconnectCount > ?RECONNECT_LIMIT) ->

	NTData = restart_timer(push, TxRef, TData,Properties),
	lager:warning([{id, Id}], "unable to reconnect fnid=~p, reason=~p", [FnId, AbortReason]),
	{next_state, disconnected, NTData, Properties}
	;

% Make sure this is the response we think it is, and if so up the reconnect count
txn_abort(FnId = push, TxRef, AbortReason, reconnecting, TData =
		#state{reconnect_count = ReconnectCount, failed_txn = FailedTxn, id = Id}, Properties)
		when (FailedTxn == push) and ?io_txn_match(push, TxRef, TData) ->

	NTData = restart_timer(push, TxRef, TData,Properties),

	lager:warning([{id, Id}], "reconnect attempt failed fnid=~p, reason=~p, retry=~p", [FnId, AbortReason, ReconnectCount]),
	{next_state, reconnecting, NTData#state{
		io_abort_reason = AbortReason,
		reconnect_count = ReconnectCount + 1
	}, Properties}
	;

% Enter the reconnect loop on failed I/O
txn_abort(FnId, TxRef, AbortReason, StateName, TData = #state{id = Id}, Properties)
		when
		((StateName == online) or (StateName == standby) or (StateName == override))
		and
		?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "processing I/O failure fnid=~p, reason=~p", [FnId, AbortReason]),
	NTData = restart_timer(FnId, TxRef, TData,Properties),
    	{next_state, reconnecting, NTData#state{
		io_abort_reason = AbortReason,
		failed_txn = FnId
	}, Properties}
	;

% Quietly discard, since these are expected on (most) reconnects
txn_abort(FnId, _TxRef, AbortReason, StateName = reconnecting, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "unhandled aborted I/O transaction fnid=~p, reason=~p", [FnId, AbortReason]),
	{next_state, StateName, TData, Properties}
	;

% Discard and log other failures since the ones we need to track are (should be) already handled
txn_abort(FnId, _TxRef, AbortReason, StateName, TData = #state{id = Id}, Properties) ->
	lager:warning([{id, Id}], "unhandled aborted I/O transaction fnid=~p, reason=~p", [FnId, AbortReason]),
	{next_state, StateName, TData, Properties}.



txn_result(_Dev, FnId, TxRef, Result, StateName = reconnecting, TData = #state{failed_txn = FailedTxn, id = Id}, Properties)
		when
			(FnId == FailedTxn)
			and
			?io_txn_match(FnId, TxRef, TData)
			->
	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),
	case getprop("unitstatus",Properties) of
	    ?ONLINE -> {next_state, online, TData, Properties};
	    ?STANDBY -> {next_state, standby, TData, Properties}
	end
	;

txn_result(_Dev, FnId = pull, TxRef, Result, StateName = disconnected, TData = #state{id = Id}, Properties)
		when ?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case {proplist_get("comm", Result), proplist_get("standby", Result)} of
		{false, _Any} ->
			{next_state, StateName, restart_timer(pull, TxRef, TData,Properties), Properties}
			;
		{true, false} ->
		{next_state, online, restart_timer(pull, TxRef, TData,Properties), Properties}
			;
		{true, true} ->
		{next_state, standby, restart_timer(pull, TxRef, TData,Properties), Properties}
	end
	;

txn_result(_Dev, FnId = pull, TxRef, Result, StateName = standby, TData = #state{id = Id}, Properties)
		when ?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case {proplist_get("comm", Result), proplist_get("standby", Result)} of
		{false, _Any} ->
		{next_state, disconnected, restart_timer(pull, TxRef, TData,Properties), Properties}
			;
	    {true, false} ->

		{next_state, online, restart_timer(pull, TxRef, TData,Properties), Properties}
		    ;
	    {true, true} ->
		{next_state, StateName, restart_timer(pull, TxRef, TData,Properties), Properties}
	end
	;

txn_result(_Dev, FnId = push, TxRef, Result, StateName = online, TData = #state{id = Id}, Properties)
		when ?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case getprop("pushTime",Properties) of
		0.0 -> {next_state, StateName, cancel_timer(push, TData), Properties};
		_Time -> {next_state, StateName, restart_timer(push, TxRef, TData,Properties), Properties}
	end
	;

txn_result(Dev, FnId = pull, TxRef, Result, StateName = online, TData = #state{device_class_mod = ClassMod, protocol_mod = ProtocolMod, id = Id} , Properties)
		when ?io_txn_match(FnId, TxRef, TData) ->

	lager:debug([{id, Id}], "Processing I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Result]),

	case {proplist_get("comm", Result), proplist_get("standby", Result)} of
		{false, _Any} ->
		{next_state, disconnected, restart_timer(pull, TxRef, TData,Properties), Properties};
	    {true, true} ->
		{next_state, standby, restart_timer(pull, TxRef, TData,Properties), Properties};
		{true, false} ->
		NTData = case Dev of
		    crah ->
				Alerts = [{A,C} || {A,C} <-Result, lists:member(A,?CRAH_ALERTS)],
				process_alerts(Alerts,Id),

				TxnRef = make_ref(),
				io_eval:run_async({clear, TxnRef}, fun(P,M) -> ClassMod:clear_alarm(P,M) end, [Properties, ProtocolMod]),
				TData#state{
					capacity = proplist_get("valve", Result),
					device_rat = proplist_get("rat", Result),
					device_sat = proplist_get("sat", Result),
					device_setpoint = proplist_get("setpoint", Result),
					change_filters = proplist_get("changeFilters", Result),
					general_alarm = proplist_get("generalCrahAlarm", Result)
				};
		    crac ->
				Alerts = [{A,C} || {A,C} <-Result, lists:member(A,?CRAC_ALERTS)],
				process_alerts(Alerts,Id),
				TData#state{
					compressor_stages = proplist_get("compressor", Result),
					capacity = proplist_get("capacity", Result),
					device_rat = proplist_get("rat", Result),
					device_sat = proplist_get("sat", Result),
					device_setpoint = proplist_get("setpoint", Result),
					high_head = proplist_get("highHead", Result),
					low_suction = proplist_get("lowSuction", Result),
					compressor_overload = proplist_get("compressorOverload", Result),
					short_cycle = proplist_get("shortCycle", Result),
					change_filters = proplist_get("changeFilters", Result),
					general_alarm = proplist_get("generalCracAlarm", Result)
				}
		end,
		{next_state, StateName, restart_timer(pull, TxRef, NTData,Properties), Properties}
	end
	;
txn_result(_Dev, clear, _, _, StateName, TData, Properties) ->
	{next_state, StateName, TData, Properties};
txn_result(_Dev, FnId, _TxRef, Results, StateName, TData = #state{id = Id}, Properties) ->
	lager:info([{id, Id}], "Discarding I/O result state=~p, fnid=~p, result=~p", [StateName, FnId, Results]),
	{next_state, StateName, TData, Properties}.

property_changed("engaged", Old, New, StateName, TData, Properties)  when Old == New ->
	{next_state, StateName, TData, Properties};

property_changed("engaged", _Old, New, _StateName, TData, Properties) ->
    	case {New, getprop("unitstatus", Properties)} of
		{?DISENGAGED, _Any} -> {next_state, disengaged, TData, Properties};
		{?ENGAGED, ?ONLINE} -> {next_state, online, TData, Properties};
		{?ENGAGED, ?DISCONNECTED} -> {next_state, disconnected, TData, Properties};
	    {?ENGAGED, ?STANDBY} -> {next_state, standby, TData, Properties}
	end;

property_changed("cascadeStandby", Old, New, StateName, TData = #state{id = Id}, Properties)  when Old /= New ->
	case {New, getprop("unitstatus",Properties)} of
		{1, ?STANDBY} -> % cascade standby is being turned on, push our state to the VFD
			set_vfd_status(Id, ?STANDBY);
		{0, _} -> % cascade standby is being turned off, release the VFD no matter what state we're in
			set_vfd_status(Id, ?ONLINE);
		{_, _} -> ok % many other possibilities...
	end,
	{next_state, StateName, TData, Properties};

property_changed(Pname, Old, New, StateName, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "updated property ~p, old=~p, new=~p", [Pname, Old, New]),
	{next_state, StateName, TData, Properties}
	.

%% Accept a new value in disengaged so that the driver can
%% be properly configured before transitioning to online
new_output(Value, StateName, TData, Properties)
	    when StateName =:= online; StateName =:= disengaged ->
	NTData = TData#state{setpoint = Value},
	case getprop("pushTime",Properties) of
		0.0 when StateName =:= online ->
			{accept, Value, StateName, start_timer(push, NTData,Properties), Properties};
		T when is_float(T) ->
			{accept, Value, StateName, NTData, Properties}
	end
	;
new_output(_Value, StateName, TData, Properties) ->
	{reject, StateName, StateName, TData, Properties}.

state_change(online,disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> disengaged"),
	NTData = cancel_timer(all, TData),
	{next_state, disengaged, NTData, Properties}
	;
state_change(online, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> disconnected"),
	NProperties = setprop(Id, {"unitstatus", ?DISCONNECTED},Properties),
	NTData = drv_handler:cancel_timer(push, TData),
	control_alarm:comm_broken(Id),
	{next_state, disconnected, NTData#state{reconnect_count = 0}, NProperties}
	;
state_change(online, standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> standby"),
	control_alarm:device_in_standby(Id),
	update_vfd_status(Id, ?STANDBY,Properties),

	TData1 = cancel_timer(push, TData),

	{next_state, standby, TData1, setprop(Id, {"unitstatus", ?STANDBY},Properties)}
	;
state_change(online, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: online -> reconnecting"),
	TData1 = case TData#state.failed_txn of
		push -> cancel_timer(pull, TData);
		pull -> cancel_timer(push, TData)
	end,

	{next_state, reconnecting, TData1, Properties}
	;

state_change(standby, disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: standby -> disengaged"),
	NTData = cancel_timer(pull, TData),
	{next_state, disengaged, NTData, Properties}
	;

state_change(standby, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: standby -> disconnected"),
    {next_state, disconnected, TData, setprop(Id, {"unitstatus", ?DISCONNECTED},Properties)}
	;

state_change(standby,online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: standby -> online"),
	update_vfd_status(Id, ?ONLINE,Properties),
	NProperties = setprop(Id, {"unitstatus", ?ONLINE},Properties),

	TData1 = start_timer(push, TData,NProperties),

	{next_state, online, TData1, NProperties}
	;
state_change(standby, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: standby -> reconnecting"),
	{next_state, reconnecting, TData, Properties}
	;

state_change(reconnecting,disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> disengaged"),
	NTData = cancel_timer(all, TData),
	{next_state, disengaged, NTData, Properties}
	;

state_change(reconnecting, disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> disconnected"),
	lager:warning([{id, Id}], "unable to reconnect in ~p attempts", [?RECONNECT_LIMIT]),

	io_alert:raise_connect_alert(TData#state.io_abort_reason, Id),
	NProperties = setprop(Id, {"unitstatus", ?DISCONNECTED},Properties),

	NTData = start_timer(pull, cancel_timer(all, TData),NProperties),
	{next_state, disconnected, NTData#state{reconnect_count = 0}, NProperties}
	;

state_change(reconnecting, online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> online"),

	update_vfd_status(Id, ?ONLINE,Properties),
	NProperties = setprop(Id, {"unitstatus", ?ONLINE},Properties),

	NTData = start_timer(all, cancel_timer(all, TData),NProperties),
	{next_state, online, NTData, NProperties}
	;

state_change(reconnecting, standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: reconnecting -> standby"),

	update_vfd_status(Id, ?STANDBY,Properties),
	NProperties = setprop(Id, {"unitstatus", ?STANDBY},Properties),

	NTData = start_timer(pull, cancel_timer(all, TData),NProperties),
	{next_state, standby, NTData, NProperties}
	;

state_change(disconnected,disengaged, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> disengaged"),
	NTData = cancel_timer(pull, TData),
	{next_state, disengaged, NTData, Properties}
	;

state_change(disconnected,online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> online"),
	NProperties = setprop(Id, {"unitstatus", ?ONLINE},Properties),
        TData1 = start_timer(push, TData,NProperties),
	update_vfd_status(Id, ?ONLINE,NProperties),
	{next_state, online, TData1, NProperties}
	;

state_change(disconnected,standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disconnected -> standby"),
	NProperties = setprop(Id, {"unitstatus", ?STANDBY},Properties),
    update_vfd_status(Id, ?STANDBY,NProperties),
	{next_state, standby, TData, NProperties}
	;
state_change(disconnected,reconnecting, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "invalid state change: disconnected -> reconnecting!"),
	{next_state, disconnected, TData, Properties}
	;

state_change(disengaged,disconnected, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> disconnected"),
	NTData = start_timer(pull, TData,Properties),
	{next_state, disconnected, NTData, Properties}
	;

state_change(disengaged,online, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> online"),
    NProperties = setprop(Id, {"unitstatus", ?ONLINE},Properties),
    update_vfd_status(Id, ?ONLINE,NProperties),
    NTData = start_timer(all, TData,NProperties),
    {next_state, online, NTData, NProperties}
	;

state_change(disengaged,standby, TData = #state{id = Id}, Properties) ->
	lager:debug([{id, Id}], "state change: disengaged -> standby"),
    NProperties = setprop(Id, {"unitstatus", ?STANDBY},Properties),
    update_vfd_status(Id, ?STANDBY,NProperties),
	NTData = start_timer(pull, TData,NProperties),
	{next_state, standby, NTData, NProperties}
	;
state_change(disengaged, reconnecting, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "invalid state change: disengaged -> reconnecting!"),
	{next_state, disengaged, TData, Properties}.


online(pull, TData= #state{device_class_mod = ClassMod, protocol_mod = ProtocolMod, id = Id}, Properties) ->
	TxnRef = make_ref(),
	io_eval:run_async({pull, TxnRef}, fun(P,M) -> ClassMod:pull_all(P,M) end, [Properties, ProtocolMod]),
	lager:debug([{id, Id}], "Starting I/O request state=online, fnid=pull"),

	{next_state, online, TData#state{
		pull_timer = {pending, TxnRef}
	}, Properties}
	;

online(push, TData = #state{protocol_mod = ProtocolMod, setpoint = Setpoint, id = Id}, Properties) ->
	{Fn, Args} = if
		Setpoint == [] ->
			lager:warning([{id, Id}], "~p setpoint not initialized - skipping write", [Id]),
			{fun() -> ok end, []};
		true -> {fun(P,S) -> ProtocolMod:write_setpoint(P,S) end, [Properties,Setpoint]}
	end,

	TxnRef = make_ref(),
	io_eval:run_async({push, TxnRef}, Fn, Args),
	lager:debug([{id, Id}], "Starting I/O request state=online, fnid=push"),

	{next_state, online, TData#state{
		push_timer = {pending, TxnRef}
	}, Properties}
	;

online(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in online state ev=~p, state=~p", [Ev, TData]),
	{next_state, online, TData, Properties}
	.

standby(pull, TData= #state{protocol_mod = ProtocolMod, id = Id}, Properties) ->
    TxnRef = make_ref(),
	io_eval:run_async({pull, TxnRef}, fun(P,M) -> pull_unitstatus(P,M) end, [Properties,ProtocolMod]),
	lager:debug([{id, Id}], "Starting I/O request state=standby, fnid=pull"),
	{next_state, standby, TData#state{
		pull_timer = {pending, TxnRef}
	}, Properties}
	;

standby(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in standby state ev=~p, state=~p", [Ev, TData]),
	{next_state, standby, TData, Properties}.


reconnecting(pull, TData = #state{device_class_mod = ClassMod, protocol_mod = ProtocolMod, id = Id}, Properties) ->
	TxnRef = make_ref(),
	io_eval:run_async({pull, TxnRef}, fun(P,M) -> ClassMod:pull_all(P,M) end, [Properties,ProtocolMod]),
	lager:debug([{id, Id}], "Starting I/O request state=reconnecting, fnid=pull"),
	{next_state, reconnecting, TData#state{
		pull_timer = {pending, TxnRef}
	}, Properties}
	;

reconnecting(push, TData = #state{protocol_mod = ProtocolMod, setpoint = Setpoint, id = Id}, Properties) ->
	{Fn, Args} = if
		Setpoint == [] ->
			lager:warning([{id, Id}], "Setpoint not initialized.  Skipping setpoint write."),
			{fun() -> ok end, []};
		true -> {fun(P,S) -> ProtocolMod:write_setpoint(P,S) end, [Properties,Setpoint]}
	end,

	TxnRef = make_ref(),
	io_eval:run_async({push, TxnRef}, Fn, Args),
	lager:debug([{id, Id}], "Starting I/O request state=reconnecting, fnid=push"),

	{next_state, reconnecting, TData#state{
		push_timer = {pending, TxnRef}
	}, Properties}
	;

reconnecting(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in reconnecting state ev=~p, state=~p", [Ev, TData]),
	{next_state, reconnecting, TData, Properties}
	.

disconnected(pull, TData = #state{device_class_mod = ClassMod, protocol_mod = ProtocolMod, id = Id}, Properties) ->
	TxnRef = make_ref(),
	io_eval:run_async({pull, TxnRef}, fun(P,M) -> ClassMod:pull_all(P,M) end, [Properties,ProtocolMod]),
	lager:debug([{id, Id}], "Starting I/O request state=disconnected, fnid=pull"),
	{next_state, disconnected, TData#state{
		pull_timer = {pending, TxnRef}
	}, Properties}
	;

disconnected(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in disconnected state ev=~p, state=~p", [Ev, TData]),
	{next_state, disconnected, TData, Properties}.

disengaged(Ev, TData = #state{id = Id}, Properties) ->
	lager:error([{id, Id}], "unexpected event in disengaged state ev=~p, state=~p", [Ev, TData]),
	{next_state, disengaged, TData, Properties}.


pull_unitstatus(Props,PMod) ->
    case PMod:read_comm(Props) of
		false ->
			[{"comm", false}, {"standby", false}];
		true ->
			[{"comm", true}, {"standby", PMod:read_standby(Props)}]
	end
	.

%% utility functions

%% I/O functions

-spec proplist_get(any(), [tuple()]) -> any().

proplist_get(Key, Pl) ->
	{Key, Value} = lists:keyfind(Key, 1, Pl),
	Value
	.


-spec cancel_timer('all' | 'push' | 'pull', #state{}) -> #state{}.

cancel_timer(all, State = #state{pull_timer = PullTimer, push_timer = PushTimer}) ->
	catch base_driver:cancel_timer(PullTimer),
	catch base_driver:cancel_timer(PushTimer),
	State#state{
		pull_timer = undefined,
		push_timer = undefined
	}
	;

cancel_timer(pull, State = #state{pull_timer = PullTimer}) ->
	catch base_driver:cancel_timer(PullTimer),
	State#state{
		pull_timer = undefined
	}
	;

cancel_timer(push, State = #state{push_timer = PushTimer}) ->
	catch base_driver:cancel_timer(PushTimer),
	State#state{
		push_timer = undefined
	}
	.

r_cancel_timer([H|T], State) ->
	r_cancel_timer(T, cancel_timer(H,State))
	;

r_cancel_timer([], State) ->
	State
	.

% Start the specified timer, ensuring that it is not already
% running or pending I/O completion.  If it is pending, the
% timer will be cancelled and restarted.  If it is waiting on
% I/O, a new timer will be started and the results will be
% thrown away when they arrive.

-spec start_timer('all' | 'push' | 'pull', #state{}, [tuple()]) -> #state{}.

start_timer(all, State = #state{push_timer = PushTimer, pull_timer = PullTimer},Properties) ->
	check_stop_timer(PullTimer, pull),
	check_stop_timer(PushTimer, push),
	State#state{
		pull_timer = base_driver:send_event_after(round(getprop("pullTime",Properties) * 1000), pull),
		push_timer = base_driver:send_event_after(round(getprop("pushTime",Properties) * 1000), push)
	}
	;

start_timer(pull, State = #state{pull_timer = PullTimer},Properties) ->
	check_stop_timer(PullTimer, pull),
	State#state{
		pull_timer = base_driver:send_event_after(round(getprop("pullTime",Properties) * 1000), pull)
	}
	;

start_timer(push, State = #state{push_timer = PushTimer},Properties) ->
	check_stop_timer(PushTimer, push),
	State#state{
		push_timer = base_driver:send_event_after(round(getprop("pushTime",Properties) * 1000), push)
	}
	.

r_start_timer([H|T], State,Properties) ->
	r_start_timer(T,start_timer(H,State,Properties),Properties)
	;
r_start_timer([],State,_) ->
	State
	.


% Restart the specified timer if the IoRef matches
% Otherwise, leave it alone so that it can be restarted
% when the appropriate response arrives

-spec restart_timer('all' | 'push' | 'pull', reference(), #state{}, [tuple()]) -> #state{}.

restart_timer(pull, IoRef, State = #state{pull_timer = PullTimer, id = Id},Properties) ->
	case PullTimer of
		undefined ->
			lager:error([{id, Id}], "Timer restart error: can't restart undefined timer (pull)"),
			State;
		{pending, TRef} when TRef == IoRef ->
			State#state{pull_timer = base_driver:send_event_after(round(getprop("pullTime",Properties) * 1000), pull)};
		{pending, _TRef} ->
			lager:error([{id, Id}], "Timer restart error: timer ref didn't match I/O ref (pull)"),
			State;
		PullTimer when is_reference(PullTimer) ->
			lager:error([{id, Id}], "Timer restart error: timer is pending (pull)"),
			State
	end
	;

restart_timer(push, IoRef, State = #state{push_timer = PushTimer, id = Id},Properties) ->
	case PushTimer of
		undefined ->
			lager:error([{id, Id}], "Timer restart error: can't restart undefined timer (push)"),
			State;
		{pending, TRef} when TRef == IoRef ->
			State#state{push_timer = base_driver:send_event_after(round(getprop("pushTime",Properties) * 1000), push)};
		{pending, _TRef} ->
			lager:error([{id, Id}], "Timer restart error: timer ref didn't match I/O ref (push)"),
			State;
		PushTimer when is_reference(PushTimer) ->
			lager:error([{id, Id}], "Timer restart error: timer is pending (push)"),
			State
	end
	.


% Ensure that the timer is cancelled(undefined) or expired.
% If not, cancel it and log an error.

-spec check_stop_timer(tracked_timer(), atom()) -> ok.

check_stop_timer(undefined, _RefName) ->
	ok
	;

check_stop_timer({pending, IoRef}, RefName) when is_reference(IoRef) ->
	lager:error("Cancelling a timer with a pending I/O operation! timer=~p, ref=~p", [RefName, IoRef])
	;
check_stop_timer(TimerRef, RefName) when is_reference(TimerRef) ->
	case base_driver:cancel_timer(TimerRef) of
		false -> ok;
		_ -> lager:error("Timer was already running! timer=~p", [RefName])
	end
	.

update_vfd_status(Id, Status,Properties) ->
	case getprop("cascadeStandby",Properties) of
		true ->
			set_vfd_status(Id, Status);
		false ->
			ok
	end
	.

set_vfd_status(Key, Status) ->
	VfdId = driver_lib:with_resource(Key, pressure),
	driver_lib:call(VfdId, set_status, [Status]).

%%should we set empty value as property value and sync to ES??

setprop(_Id, {_,[]}, Properties) ->
    Properties;
setprop(_Id, Prop, Properties) ->
    setprop_i(Prop, Properties).

setprop_i(Prop, Properties) ->
    {_, NewProperties} = shadoww_lib:setprop(Prop, Properties),
    NewProperties.

setproplist(_Id, UpdList,Properties) ->
     setproplist_i(UpdList, Properties).

setproplist_i([P1|T],Properties) ->
    NewProperties = setprop_i(P1, Properties),
    setproplist_i(T, NewProperties);
setproplist_i([], Properties) ->
    Properties.

process_alerts(AlertList, Id) ->
	lists:foreach(fun(A) -> process_alert(A, Id) end, AlertList).

process_alert({"highHead", Val}, Id) ->
	ComprId = compressor_id(Val),
	case ComprId of
		[] -> ok;
		_ ->
			control_alarm:high_head_pressure(ComprId, Id)
	end;
process_alert({"lowSuction", Val}, Id) ->
	ComprId = compressor_id(Val),
	case ComprId of
		[] -> ok;
		_ ->
			control_alarm:low_suction_pressure(ComprId, Id)
	end;
process_alert({"compressorOverload", Val}, Id) ->
	ComprId = compressor_id(Val),
	case ComprId of
		[] -> ok;
		_ ->
			control_alarm:compressor_overload(ComprId, Id)
	end;
process_alert({"shortCycle", Val}, Id) ->
	ComprId = compressor_id(Val),
	case ComprId of
		[] -> ok;
		_ ->
			control_alarm:short_cycle(ComprId, Id)
	end;
process_alert({"overCurrent", true}, Id) ->
	control_alarm:over_current(Id);
process_alert({"overVoltage", true}, Id) ->
	control_alarm:over_voltage(Id);
process_alert({"underVoltage", true}, Id) ->
	control_alarm:under_voltage(Id);
process_alert({"overTemperature", true}, Id) ->
	control_alarm:over_temperature(Id);
process_alert({"changeFilters", true}, Id) ->
	control_alarm:change_filters(Id);
process_alert({"generalCracAlarm", true}, Id) ->
	control_alarm:general_alarm(crac, Id);
process_alert({"generalCrahAlarm", true}, Id) ->
	control_alarm:general_alarm(crah, Id);
process_alert({"generalVfdAlarm", true}, Id) ->
	control_alarm:general_alarm(vfd, Id);
process_alert({_, false},_) ->
	ok.


compressor_id(Val) ->
	case Val of
		true ->
			?UNKNOWN_COMPR;
		{true, true} ->
			?BOTH_COMPR;
		{true, false} ->
			?COMPR_1;
		{false,true} ->
			?COMPR_2;
		_ ->
			[]
	end.

crac_alarm_properties() ->
	[
		{"highHeadPressureAlarmRead", r},
		{"lowSuctionPressureAlarmRead", r},
		{"compressorOverloadAlarmRead", r},
		{"shortCycleAlarmRead", r},
		{"changeFiltersAlarmRead", r},
		{"generalAlarmRead", r},
		{"clearAlarmWrite", r}
	].
crah_alarm_properties() ->
	[
		{"changeFiltersAlarmRead", r},
		{"generalAlarmRead", r},
		{"clearAlarmWrite", r}
	].

vfd_alarm_properties() ->
	[
	        {"overCurrentAlarmRead",r},
		{"overVoltageAlarmRead",r},
		{"underVoltageAlarmRead",r},
		{"overTemperatureAlarmRead",r},
		{"generalAlarmRead",r},
		{"clearAlarmWrite",r}
	].

i_to_f(I) when is_integer(I) -> I * 1.0;
i_to_f(I) -> I.

null_or_float(F) when is_float(F) -> F;
null_or_float(_) -> null.

-spec device_state_to_status(State :: atom()) -> device_status().
device_state_to_status(reconnecting) -> online;
device_state_to_status(override) -> local_override;
device_state_to_status(O) -> O.

