
-module(control_alarm).

%% API
-export([
         start_link/0,
         stop/0,
         add_handler/2,
         add_sup_handler/2,
         delete_handler/2,
         gateway_unreachable/2,
         gateway_not_responding/2,
         modbus_gw_error/4,
         modbus_data_corruption/5,
         io_expression_failed/4,
         device_not_responding/5,
         device_in_standby/1,
         comm_broken/1,
         room_crashed/2,
         high_head_pressure/2,
         low_suction_pressure/2,
         compressor_overload/2,
         short_cycle/2,
         over_current/1,
         over_voltage/1,
         under_voltage/1,
         over_temperature/1,
         change_filters/1,
         general_alarm/2,
         bad_room_geom/1
        ]).

-include_lib("active_control/include/ac_api.hrl").

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API
%%%===================================================================

%-type alarm_event() :: {Event :: atom(), EventDetails :: term(), Id :: id()}.

start_link() ->
    gen_event:start_link({local, ?SERVER}).

stop() ->
    gen_event:stop(?SERVER).

add_handler(Handler, Args) ->
    gen_event:add_handler(?SERVER, Handler, Args).

add_sup_handler(Handler, Args) ->
    gen_event:add_sup_handler(?SERVER, Handler, Args).

delete_handler(Handler, Args) ->
    gen_event:delete_handler(?SERVER, Handler, Args).

gateway_unreachable(Ip, Id) ->
    raise(gateway_unreachable, Ip, Id).

gateway_not_responding(Ip, Id) ->
    raise(gateway_not_responding, Ip, Id).

modbus_gw_error(Ip, Device, ErrorCode, Id) ->
    raise(modbus_gw_error, {Ip, Device, ErrorCode}, Id).

modbus_data_corruption(Ip, Device, Request, Response, Id) ->
    raise(modbus_data_corruption, {Ip, Device, Request, Response}, Id).

io_expression_failed(Point, Expression, AbortMessage, Id) ->
    raise(io_expression_failed, {Point, Expression, AbortMessage}, Id).

device_not_responding(Point, Expression, Ip, Devid, Id) ->
    raise(device_not_responding, {Point, Expression, Ip, Devid}, Id).

device_in_standby(Id) ->
    raise(device_in_standby, none, Id).

comm_broken(Id) ->
    raise(comm_broken, none, Id).

room_crashed(Info, Id) ->
    raise(room_crashed, Info, Id).


high_head_pressure(Compressor, Id) ->
    raise(high_head_pressure, Compressor, Id).

low_suction_pressure(Compressor, Id) ->
    raise(low_suction_pressure, Compressor, Id).

compressor_overload(Compressor, Id) ->
    raise(compressor_overload, Compressor, Id).

short_cycle(Compressor, Id) ->
    raise(short_cycle, Compressor, Id).


over_current(Id) ->
    raise(over_current, none, Id).

over_voltage(Id) ->
    raise(over_voltage, none, Id).

under_voltage(Id) ->
    raise(under_voltage, none, Id).

over_temperature(Id) ->
    raise(over_temperature, none, Id).

change_filters(Id) ->
    raise(change_filters, none, Id).

general_alarm(Device, Id) ->
    raise(general_alarm, Device, Id).

bad_room_geom(Id) ->
    raise(bad_room_geom, none, Id).


raise(Alarm, Details, Id) ->
    gen_event:notify(?SERVER, {Alarm, Details, Id}).

