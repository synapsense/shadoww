
-module(heartbeat_manager).

-behaviour(gen_server).

%% API
-export([
         start_link/2,
         set_config/2,
         childspec/2,
         stop/1
        ]).

%% gen_server callbacks
-export([
         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3
        ]).

-include_lib("active_control/include/ac_api.hrl").

-record(state, {
          id :: id(),
          nextvalue :: integer(),
          interval :: integer(),
          tref :: reference(),
          expression :: string(),
          bindings :: proplists:proplist()
         }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(RoomId, #heartbeat_manager{
                      interval = Interval,
                      heartbeat_write = Expr,
                      protocol_bindings = Bindings
                     }) ->
    State = #state{
               id = RoomId,
               nextvalue = 0,
               interval = Interval,
               expression = Expr,
               bindings = protocol_bindings:to_bindings(Bindings)
              },
    gen_server:start_link(?MODULE, State, []).

%%--------------------------------------------------------------------
%% @doc
%% Update things like interval, bindings, expression
%%
%% @spec set_config(Id, Config) -> ok | {error, Error}
%% @end
%%--------------------------------------------------------------------
set_config(RoomId, #heartbeat_manager{
                      interval = Interval,
                      heartbeat_write = Expr,
                      protocol_bindings = BindingsRec
                     }) ->
    Pid = driver_lib:heartbeat_pid(RoomId),
    Bindings = protocol_bindings:to_bindings(BindingsRec),
    gen_server:call(Pid, {set_config, Interval, Expr, Bindings}).


%%--------------------------------------------------------------------
%% @doc
%% Generate a childspec from the config record
%%
%% @spec childspec(Id, heartbeat_manager()) -> supervisor:childspec()
%% @end
%%--------------------------------------------------------------------
childspec(RoomId, HeartbeatManager = #heartbeat_manager{}) ->
    {
     driver_lib:proc_key(RoomId, none, heartbeat),
     {?MODULE, start_link, [RoomId, HeartbeatManager]},
     transient,
     100,
     worker,
     [?MODULE]
    }.

%%--------------------------------------------------------------------
%% @doc
%% Stop the strategy, removing any cached data
%%
%% @spec stop(Id) -> stop
%% @end
%%--------------------------------------------------------------------
stop(Id) ->
    stop = gen_server:call(Id, stop).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init(S = #state{id = Id}) ->
    driver_lib:register_heartbeat(Id),
    {ok, start_timer(S)}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({set_config, Interval, Expr, Bindings}, _F, State) ->
    NewState = State#state{
                 interval = Interval,
                 expression = Expr,
                 bindings = Bindings
                },
    {reply, ok, NewState};
handle_call(stop, _F, State) ->
    {stop, normal, stop, State};
handle_call(Request, From, State) ->
    lager:warning("Unexpected call ~p from ~p with state ~p", [Request, From, State]),
    Reply = huh,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(Msg, State) ->
    lager:warning("unexpected cast ~p with state ~p", [Msg, State]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(ping, S = #state{id = Id, nextvalue = V, expression = Expr, bindings = Bindings}) ->
    lager:debug("~p heartbeat", [Id]),
    io_eval:run_async(pinger, fun(E,B) -> io_eval:eval("heartbeatmanager.heartbeatwrite", E, B) end, [Expr, [{'Value', V} | Bindings]]),
    {noreply, S#state{tref = undefined, nextvalue = nextvalue(V)}}
    ;
handle_info({txn_result, pinger, _R}, S = #state{}) ->
    lager:debug("Heartbeat written, scheduling the next one"),
    {noreply, start_timer(S)}
    ;
handle_info({txn_abort, pinger, R}, S = #state{}) ->
    lager:warning("Heartbeat write had an error (ignoring and continuing): ~p", [R]),
    {noreply, start_timer(S)}
    ;
handle_info(Info, State) ->
    lager:warning("unexpected info ~p with state ~p", [Info, State]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(Reason, State) ->
    lager:warning("terminating with reason ~p and state ~p", [Reason, State]),
    cancel_timer(State).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

start_timer(S = #state{interval = Interval}) ->
    S1 = cancel_timer(S),
    S1#state{tref = erlang:send_after(Interval * 1000, self(), ping)}.

cancel_timer(S = #state{tref = Tref}) when Tref =:= undefined ->
    S;
cancel_timer(S = #state{tref = Tref}) ->
    catch erlang:cancel_timer(Tref),
    S#state{tref = undefined}.

nextvalue(0) -> 1;
nextvalue(1) -> 0.

