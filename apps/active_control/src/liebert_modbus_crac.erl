-module(liebert_modbus_crac).
-behaviour(crac_fsm).

-export([properties/0,read_setpoint/1,read_comm/1,read_standby/1,read_compressor/1,read_capacity/1,read_return/1,read_supply/1,write_setpoint/2]).
-export([read_high_head/1, read_low_suction/1, read_compressor_overload/1, read_short_cycle/1,
	 read_change_filters/1, read_general/1, write_clear_alarm/1]).

-import(shadoww_lib, [getprop/2]).
-import(io_handler,[get_ip/1, get_devid/1, get_port/1, liebert_status_register/4, liebert_alarm_register/4, liebert_control_register/5, bits_high/2]).

properties() ->
	[{"ip", r},
	 {"devid", r},
	 {"port", r}].

read_comm(_) ->
    true.

read_standby(Props) -> liebert_modbus_crah:read_standby(Props) .

read_compressor(Props) ->
	liebert_status_register(get_ip(Props), get_devid(Props), 40008, get_port(Props)) .

read_capacity(Props) -> liebert_modbus_crah:read_valve(Props) .

read_return(Props) -> liebert_modbus_crah:read_return(Props) .

read_supply(_) ->
    [].

read_setpoint(Props) -> liebert_modbus_crah:read_setpoint(Props) .

write_setpoint(Props, Ratsp) -> liebert_modbus_crah:write_setpoint(Props, Ratsp) .

%Alerts I/O
read_high_head(Props) ->
	ReadReg = liebert_alarm_register(get_ip(Props), get_devid(Props), 40289, get_port(Props)),
	{io_expr_util:bit(3, ReadReg) =/= 0,
	 io_expr_util:bit(4, ReadReg) =/= 0}.

read_low_suction(Props) ->
	ReadReg = liebert_alarm_register(get_ip(Props), get_devid(Props), 40291, get_port(Props)),
	{io_expr_util:bit(0, ReadReg) =/= 0,
	 io_expr_util:bit(0, ReadReg) =/= 0}.

read_compressor_overload(Props) ->
	ReadReg = liebert_alarm_register(get_ip(Props), get_devid(Props), 40290, get_port(Props)),
	{io_expr_util:bit(4, ReadReg) =/= 0,
	 io_expr_util:bit(5, ReadReg) =/= 0}.
       
read_short_cycle(Props) ->
	ReadReg = liebert_alarm_register(get_ip(Props), get_devid(Props), 40291, get_port(Props)),
	{io_expr_util:bit(1, ReadReg) =/= 0,
	 io_expr_util:bit(1, ReadReg) =/= 0}.
	
read_change_filters(Props) ->
	ReadReg = liebert_alarm_register(get_ip(Props), get_devid(Props), 40289, get_port(Props)),
	io_expr_util:bit(8, ReadReg) =/= 0.	

read_general(Props) ->
	R289 = liebert_alarm_register(get_ip(Props), get_devid(Props), 40289, get_port(Props)),
	R290 = liebert_alarm_register(get_ip(Props), get_devid(Props), 40290, get_port(Props)),
	R291 = liebert_alarm_register(get_ip(Props), get_devid(Props), 40291, get_port(Props)),
	bits_high(R289, [0,1,5,6,7])
		or bits_high(R290, [0,1,2,3,6,7,8])
		or bits_high(R291, [0,2,3,4,5,6,7,8])
	.

write_clear_alarm(_Props) ->
	ok.

