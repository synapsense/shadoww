
-module(rf_vfd_strategy).

-behaviour(gen_3stage_strategy).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-export([
         start_link/2,
         stop/1
        ]).

% callback functions
-export([
         init/1,
         reinit/1,
         status_info/1,
         stage3/3,
         set_mode/3,
         set_config/2,
         handle_cast/2,
         terminate/2
        ]).

-record(state, {
          mode,
          manual_output
         }).

start_link(RoomId,
           C = #raised_floor_vfd_strategy{
                  id = Id,
                  resource = Resource
                 }) ->
    gen_3stage_strategy:start_link(RoomId, Id, Resource, ?MODULE, C).

stop(Pid) ->
    gen_3stage_strategy:stop(Pid).

%% callback functions

% Args is InitArgs from start_link
init(#raised_floor_vfd_strategy{
        mode = Mode,
        manual_output = MO
       }) ->
    {ok, Mode, MO,
     #state{
        mode = Mode,
        manual_output = MO
       }}.

% PrevState is the most recent state cached in distributed memory
reinit(PrevState) ->
    {ok, PrevState}.

% Return strategy-specific info to be appended to generic info
status_info(_State) -> [].

% Calculate and return the Stage3 value from the Stage2 value
-spec stage3(K :: driver_lib:proc_key(), Stage2 :: float(), State :: term()) ->
    {ok, [gen_3stage_strategy:strategy_action()], Stage3 :: float(), NewState :: term()}
    | {error, Reason :: term(), NewState :: term()}.
stage3(_K, Stage2, State) ->
    {ok, [], Stage2, State}.

set_mode(manual, automatic, State=#state{manual_output=MO}) ->
    {ok, [gen_3stage_strategy:new_stage2(MO)], State};
set_mode(_Old, _New, State) ->
    {ok, [], State}.

set_config(#raised_floor_vfd_strategy{
              mode = NewMode,
              manual_output = NewMO
             },
           State = #state{
                      mode = Mode,
                      manual_output = MO
                     }) ->
    Actions = [
               if NewMode =/= Mode -> gen_3stage_strategy:change_status(NewMode);
                  true -> [] end,
               if NewMO =/= MO -> gen_3stage_strategy:manual_out(NewMO);
                  true -> [] end
              ],
    {ok, lists:flatten(Actions), State#state{mode = NewMode, manual_output = NewMO}}.

handle_cast(_M, S) ->
    {noreply, [], S}.

terminate(Reason, _State) ->
    Reason.

