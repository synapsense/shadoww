
%% A "dynamic" supervisor, meant to be in a largish (both deep and wide), data-driven, supervisor tree.

-module(dyn_sup).

-behaviour(supervisor).

% public API
-export([start_link/2, start_link/3, shutdown/1]).
% behavior callbacks
-export([init/1]).


-define(WORKER_STOP_TIMEOUT, 1000).


start_link(Id, ChildSpecs) ->
	RestartStrategy		= rest_for_one,	% any process dies, kill all after it and resart
	MaxRestarts		= 2,		% number of restarts allowed in
	MaxTimeBetRestarts	= 3600,		% this number of seconds
	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},
	start_link(Id, SupFlags, ChildSpecs)
	.

start_link(Id, SupFlags, ChildSpecs) ->
	case supervisor:check_childspecs(ChildSpecs) of
		ok ->
			{ok, Pid} = supervisor:start_link(?MODULE, {Id, SupFlags}),
			try
				ChildSupPids = do_start_children(Id, Pid, ChildSpecs, []),
				{ok, Pid, lists:flatten(ChildSupPids, [{Id, Pid}])}
			catch
				throw:Error -> Error
			end
			;
		{error, Error} ->
			Error
	end
	.

do_start_children(_Id, _Pid, [], SupPids) ->
	SupPids
	;
do_start_children(Id, Pid, [C | Cs], SupPids) ->
	lager:debug("Supervisor ~p starting child : ~p", [Id, C]),
	ChildId = element(1, C),
	case supervisor:start_child(Pid, C) of
		{ok, ChildPid} ->
			lager:debug("Child ~p started at ~p", [ChildId, ChildPid]),
			do_start_children(Id, Pid, Cs, SupPids)
			;
		{ok, ChildPid, ChildSupPids} ->
			lager:debug("Child ~p started at ~p", [ChildId, ChildPid]),
			do_start_children(Id, Pid, Cs, [ChildSupPids | SupPids])
			;
		{error, already_present} ->
			lager:error("Supervisor ~p already has a (dead) child with ID ~p", [Id, ChildId]),
			throw(duplicate_child)
			;
		{error, {already_started, Pid}} ->
			lager:error("Supervisor ~p already has a (running) child with ID ~p at ~p", [Id, ChildId, Pid]),
			throw(duplicate_child)
			;
		{error, {E, _}} ->
			lager:error("child process ~p didn't start correctly.  MFA ~p returned: ~p", [ChildId, element(2, C), E]),
			throw(E)
	end
	.

shutdown(Pid) ->
	%% Look at my own childspecs, recursively call 'shutdown' on children that are supervisors
	%% kill my own kids
	%% commit suicide
	Children = lists:reverse(supervisor:which_children(Pid)),
	lists:foreach(fun(C) -> rec_shutdown(Pid, C) end, Children)
	.

%% behavior callbacks

init({Id, SupFlags}) ->
	lager:debug("supervisor ~p starting: ~p", [Id, SupFlags]),
	{ok, {SupFlags, []}}
	.

%% internal utility function

rec_shutdown(_SupPid, {_Id, undefined, _Type, _Modules}) ->
	ok ;
rec_shutdown(SupPid, {Id, Child, supervisor, [dyn_sup]}) ->
	ok = dyn_sup:shutdown(Child),
	ok = supervisor:terminate_child(SupPid, Id) ;
rec_shutdown(SupPid, {Id, restarting, _Type, _Modules}) ->
	ok = supervisor:terminate_child(SupPid, Id) ;
rec_shutdown(_SupPid, {_Id, Child, _Type, [Module]}) when is_pid(Child) ->
	Mref = monitor(process, Child),
	lager:debug("Calling ~p:stop(~p) to stop child", [Module, Child]),
	Module:stop(Child),
	receive
		{'DOWN', Mref, _, _, normal} -> ok;
		{'DOWN', Mref, _, _, Reason} ->
			lager:warning("Driver (~p) exited with reason '~p' instead of 'normal'", [Child, Reason])
	after
		?WORKER_STOP_TIMEOUT ->
			lager:error("Worker process failed to stop gracefully.  Murdering ~p ...", [Child]),
			exit(Child, kill)
	end,
	ok
	;
rec_shutdown(_SupPid, {Id, _Child, _Type, _Modules}) ->
	lager:warning("Can't figure what to do with child ~p", [Id]),
	ok
	.

