
-module(room_supervisor).


-behaviour(supervisor).


-export([start_link/0,
	 start/1,
	 stop/1,
	 delete_child/1,
	 sup_pid/1]).

-export([init/1]).


-define(SERVER, ?MODULE).


start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, [])
	.

init(_Args) ->
	RestartStrategy		= one_for_one,
	MaxRestarts		= 1,
	MaxTimeBetRestarts	= 60 * 60,

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpecs 	= [],

	{ok, {SupFlags, ChildSpecs}}
	.

start(ChildSpec) ->
	lager:debug("starting new child: ~p", [ChildSpec]),
	supervisor:start_child(?SERVER, ChildSpec).

stop(Pid) ->
	supervisor:terminate_child(?SERVER, Pid).

delete_child(Id) ->
	supervisor:delete_child(?SERVER, Id).

sup_pid(Id) ->
	case [Cpid || {Cid, Cpid, _, _} <- supervisor:which_children(?SERVER), Cid == Id] of
		[] -> undefined;
		[Pid] when is_pid(Pid) -> Pid
	end
	.

