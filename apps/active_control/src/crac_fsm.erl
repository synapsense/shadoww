%%
%% Timer description:
%%  pull_timer is how often to poll the device for state/status changes
%%  push_timer is how often to write the setpoint to the device, or 0.0 for "on change"
%%
%%  usually: pull_time > push_time

%%
%% States and timers:
%% disengaged: no timers running
%% disconnected: only the pull timer is running
%% standby: only the pull timer is running
%% reconnecting: only the failed I/O timer (push | pull) is running
%% online: all timers running
%%
-module(crac_fsm).
-behaviour(base_driver).

%base_driver required callbacks
-export([init/3,reinit/3,handle_event/4,handle_sync_event/5,handle_info/4,
	 terminate/4,code_change/5,properties/1, new_output/4,property_changed/6]).

-export([
	 set_output/2,
	 get_property/2,
	 set_property/3,
	 set_properties/2,
	 get_status/1,
	 get_config/1,
	 get_status/3
	]).

-export([online/3, standby/3, disconnected/3, reconnecting/3, disengaged/3,
	 state_change/4, pull_all/2, clear_alarm/2]).

-include("ac_constants.hrl").
-include("drv_handler.hrl").
-include_lib("active_control/include/ac_status.hrl").

%%% ---------------------------------------------------
%%% Interface functions.
%%% ---------------------------------------------------

-callback properties() ->
    [term()].
-callback read_setpoint(Props :: [term()]) ->
    Val :: term() | [].
-callback read_comm(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_standby(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_compressor(Props :: [term()]) ->
    Val :: term() | [].
-callback read_capacity(Props :: [term()]) ->
    Val :: term() | [].
-callback read_return(Props :: [term()]) ->
    Val :: term() | [].
-callback read_supply(Props :: [term()]) ->
    Val :: term() | [].
-callback write_setpoint(Props :: [term()], Setpoint :: term()) ->
    Setpoint :: term().
-callback read_high_head(Props :: [term()]) ->
    Val :: boolean() | {boolean(), boolean()} | [].
-callback read_low_suction(Props :: [term()]) ->
    Val :: boolean() | {boolean(), boolean()} | [].
-callback read_compressor_overload(Props :: [term()]) ->
    Val :: boolean() | {boolean(), boolean()} | [].
-callback read_short_cycle(Props :: [term()]) ->
    Val :: boolean() | {boolean(), boolean()} | [].
-callback read_change_filters(Props :: [term()]) ->
    Val :: boolean() | [].
-callback read_general(Props :: [term()]) ->
    Val :: boolean() | [].
-callback write_clear_alarm(Props :: [term()]) ->
    Val :: any().


-spec set_output(pid(), float()) -> ok.
%%@doc Sets a new setpoint value
set_output(Pid, NewSpeed) ->
	base_driver:set_output(Pid, NewSpeed, infinity).

-spec get_property(pid(), string()) -> ok.
%%@doc Returns a property value
get_property(Pid, PropertyName) ->
	base_driver:get_property(Pid, PropertyName, infinity)
	.

-spec set_property(pid(), string(), any()) -> ok.
%%@doc Set a property value
set_property(Pid, PropertyName, Value) ->
	base_driver:set_property(Pid, PropertyName, Value)
	.

-spec set_properties(pid(), [{string(), any()}]) -> ok.
%%@doc Sets values to a list of properties
set_properties(Pid, PropertyList) ->
	base_driver:set_properties(Pid, PropertyList).

get_status(Pid) ->
	base_driver:get_status(Pid).

get_config(Pid) ->
	base_driver:get_config(Pid).

-spec properties(atom() | tuple()) -> [proplists:property()].
%%@doc Returns a list of properties
properties(#state{protocol_mod = ProtocolMod}) ->
    get_device_props() ++ ProtocolMod:properties();
properties(ProtocolMod)  ->
    get_device_props() ++ ProtocolMod:properties().
get_device_props() ->
    	[
		{"resource", r},
		{"setpoint", w},
		{"pushTime", r},
		{"pullTime", r},
	        {"cascadeStandby", r},
 		{"coolingControl", r},
		{"engaged", r},
		{"compressorStages", w},
		{"capacity", w},
		{"deviceRat", w},
		{"deviceSat", w},
		{"unitstatus", w}
      ] .
%% Behavior callbacks

init(Id, Properties,ProtocolMod) ->
    init_it(init,Id, Properties,ProtocolMod).

reinit(Id, Properties,ProtocolMod) ->
    init_it(reinit,Id, Properties,ProtocolMod).

init_it(Action, Id, Properties,ProtocolMod) ->
    lager:debug([{id, Id}], "starting crac_fsm. id: ~p, action: ~p, module: ~p", [Id, Action, ProtocolMod]),
    TData = #state{
      id = Id,
      device_class_mod = crac_fsm,
      protocol_mod = ProtocolMod,
      reconnect_count = 0,
      setpoint = [],
      device_setpoint = [],
      device_rat = [],
      device_sat = [],
      compressor_stages = [],
      capacity = []
     },
    Return = drv_handler:init(Id, Properties,TData),
    lager:debug([{id, Id}], "crac_fsm started. id: ~p, action: ~p", [Id, Action]),
    Return.

handle_event(Ev, StateName, TData, Properties) ->
    drv_handler:unexp_event(Ev, StateName, TData, Properties).
handle_sync_event(Ev, _From, StateName, TData, Properties) ->
    drv_handler:unexp_event(Ev, StateName, TData, Properties).

%%%%%%%%%%%%%% Modbus GW responses

% there may have been pending I/O requests before entering the disengaged state,
% so clean up the replies
handle_info({txn_abort, {FnId, TxRef}, AbortReason}, StateName, TData, Properties) ->
    drv_handler:txn_abort(FnId, TxRef, AbortReason, StateName, TData, Properties);

% Hey, the reconnect succeeded!
handle_info({txn_result, {FnId, TxRef}, Result}, StateName, TData, Properties) ->
    drv_handler:txn_result(crac, FnId, TxRef, Result, StateName, TData, Properties);

handle_info(Info, StateName, TData, Properties) ->
    drv_handler:unexp_info(Info, StateName, TData, Properties).

property_changed(Pname, Old, New, StateName, TData, Properties)  ->
    drv_handler:property_changed(Pname, Old, New, StateName, TData, Properties).

new_output(Value, StateName, TData, Properties) when not ?in_range_c(Value) ->
	{reject, out_of_range, StateName, TData, Properties}
	;
new_output(Value, StateName, TData, Properties) ->
    drv_handler:new_output(Value, StateName, TData, Properties).

get_status(StateName, TData, Properties) ->
    {EnvId, _, _} = TData#state.id,
    #crac_status{
       id = EnvId,
       resource = drv_handler:proplist_get("resource", Properties),
       status = StateName,
       setpoint = null_or_float(TData#state.device_setpoint),
       rat = null_or_float(TData#state.device_rat),
       sat = null_or_float(TData#state.device_sat),
       capacity = null_or_float(TData#state.capacity),
       stages = null_or_float(TData#state.compressor_stages)
      }.

terminate(driver_stop, _StateName, _TData, _Properties) ->
	ok
	;
terminate(Reason, _StateName, _TData, _Properties) ->
	lager:warning("terminating. reason: ~p", [Reason])
	.


code_change(_OldVsn, StateName, TData, Properties, _Extra) ->
	{ok, StateName, TData, Properties}
	.

%state transitions
state_change(OldState, NewState, TData, Properties) ->
    drv_handler:state_change(OldState, NewState, TData, Properties).

online(Ev, TData, Properties) ->
    drv_handler:online(Ev, TData, Properties).

standby(Ev, TData, Properties) ->
    drv_handler:standby(Ev, TData, Properties).

reconnecting(Ev, TData, Properties) ->
    drv_handler:reconnecting(Ev, TData, Properties).

disconnected(Ev, TData, Properties) ->
    drv_handler:disconnected(Ev, TData, Properties).

disengaged(Ev, TData, Properties) ->
    drv_handler:disengaged(Ev, TData, Properties).


pull_all(Props,PMod) ->
    %{Ip, Devid} = get_info_m(Properties),
    case PMod:read_comm(Props) of
		false ->
			[{"comm", false}, {"standby", false}];
		true ->
			case PMod:read_standby(Props) of
				true ->
					[{"comm", true}, {"standby", true}];
				false ->
					[
						{"comm", true},
						{"standby", false},
						{"compressor", PMod:read_compressor(Props)},
					        {"capacity", PMod:read_capacity(Props)},
						{"rat", PMod:read_return(Props)},
						{"sat", PMod:read_supply(Props)},
						{"setpoint", PMod:read_setpoint(Props)},
					        {"highHead", PMod:read_high_head(Props)},
					        {"lowSuction", PMod:read_low_suction(Props)},
					        {"compressorOverload", PMod:read_compressor_overload(Props)},
					        {"shortCycle", PMod:read_short_cycle(Props)},
					        {"changeFilters", PMod:read_change_filters(Props)},
					        {"generalCracAlarm", PMod:read_general(Props)}
					]
			end
	end
	.

clear_alarm(Props, PMod) ->
	PMod:write_clear_alarm(Props).

null_or_float(F) when is_float(F) -> F;
null_or_float(_) -> null.

