%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(room_manager).

%% @doc
%% Manages a collection of rooms distributed across the cluster.
%% Knows as little as possible about the details of a room.
%% @end

-behaviour(locks_leader).

%% API
-export([
         start_link/0,
         stop_room/1,
         start_room/3,
         room_status/1,
         room_node/1,
         exit_reason/1,
         list_rooms/0,
         room_type/1,
         rooms_of_type/1,
         rooms_on_node/1,
         isolated/1,
         is_isolated/0,
         cluster_nodes/0
        ]).

%% API for callback modules
-export([
         async_call/2,
         call/2
        ]).

%% locks_leader callbacks
-export([
         init/1,
         handle_cast/3,
         handle_call/4,
         handle_info/3,
         handle_leader_call/4,
         handle_leader_cast/3,
         handle_DOWN/3,
         elected/3,
         surrendered/3,
         from_leader/3,
         code_change/4,
         terminate/2
        ]).

%% internal functions
-export([
         start_success/3
        ]).


-include_lib("active_control/include/ac_api.hrl").


-define(SERVER, ?MODULE).
-define(TABLE_NAME, ?MODULE).

-type room_status() :: starting | running | startup_failure | exited.

-define(ROOM_TRACKER, room_tracker).
-record(?ROOM_TRACKER, {
           id :: id(),
           status :: room_status(),
           running_at :: node(),
           mref :: reference(),
           exit_reason :: term(),
           cb_mod :: module(),
           cb_state :: term()
          }).

-record(state, {
          isolated = true :: boolean(),
          cluster_nodes = [] :: [node()]
         }).

%% Generate a childspec for starting an entire room
-callback room_childspec(Model :: any()) -> supervisor:childspec().

%% List the CRAH Ids running in the room
-callback crah_ids(Model :: any()) -> [id()].

-callback handle_async_call(Message :: term(), State :: term()) ->
    {reply, Reply :: term()} | {error, Reason :: term()}.

-callback handle_call(Message :: term(), State :: term()) ->
    {reply, Reply :: term(), NewState :: term()} |
    {error, Reason :: term(), NewState :: term()} |
    {stop, Reason :: term()}.

%%% Callback API

-spec async_call(Id :: id(), Message :: term()) ->
    Reply :: term() |
    {error, not_started} |
    {error, {exited, Status :: room_status(), Detail :: term()}}.
async_call(Id, Message) ->
    case dumbtable:get(?TABLE_NAME, Id) of
        {value, #?ROOM_TRACKER{cb_mod = CbMod, cb_state = CbState, status = Status, exit_reason = ER}} ->
            case Status of
                running ->
                    case CbMod:handle_async_call(Message, CbState) of
                        {reply, Reply} -> Reply;
                        {error, Reason} -> error(Reason)
                    end;
                Status ->
                    error({exited, Status, ER})
            end;
        none ->
            error(not_started)
    end.

-spec call(Id :: id(), Message :: term()) ->
    Reply :: term() |
    {error, not_started} |
    {error, {exited, ExitReason :: term()}}.
call(Id, Message) ->
    case dumbtable:get(?TABLE_NAME, Id) of
        {value, #?ROOM_TRACKER{status = Status, exit_reason = ER}} ->
            case Status of
                running ->
                    case locks_leader:leader_call(?SERVER, {call, Id, Message}) of
                        {error, R} -> error(R);
                        Reply -> Reply
                    end;
                Status ->
                    error({exited, Status, ER})
            end;
        none ->
            error(not_started)
    end.

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
    Nodes = env_cluster_nodes(),
    lager:info("load-balancing across: ~p", [Nodes]),
    locks_leader:start_link(?SERVER, ?MODULE, Nodes, []).

stop_room(RoomId) ->
    case dumbtable:has_key(?TABLE_NAME, RoomId) of
        true ->
            locks_leader:leader_call(?SERVER, {stop, RoomId})
            ;
        false ->
            error({no_room, RoomId})
    end.

start_room(CbMod, Id, RoomModel) ->
    locks_leader:leader_call(?SERVER, {start, CbMod, Id, RoomModel}, 5 * 60 * 1000).

-spec room_status(binary()) -> room_status() | {error, not_started}.
room_status(RoomId) ->
    case dumbtable:get(?TABLE_NAME, RoomId) of
        {value, #?ROOM_TRACKER{status = Status}} -> Status;
        none -> {error, not_started}
    end.

-spec room_mref(binary()) -> reference() | {error, not_started}.
room_mref(RoomId) ->
    case dumbtable:get(?TABLE_NAME, RoomId) of
        {value, #?ROOM_TRACKER{mref = Mref}} -> Mref;
        none -> {error, not_started}
    end.

-spec room_node(RoomId :: id()) -> {ok, node()} | {error, Reason :: term()}.
room_node(RoomId) ->
    case dumbtable:get(?TABLE_NAME, RoomId) of
        {value, #?ROOM_TRACKER{running_at = Node}} -> {ok, Node};
        none -> {error, not_started}
    end.

%%% @doc
%%% If room_status/1 above returns startup_failure or exited, this will return the exit reason.
%%% @end
exit_reason(RoomId) ->
    case dumbtable:get(?TABLE_NAME, RoomId) of
        {value, #?ROOM_TRACKER{exit_reason = ExitReason}} -> ExitReason;
        none -> {error, not_started}
    end.

list_rooms() ->
    dumbtable:keys(?TABLE_NAME).

-spec room_type(RoomId :: id()) -> {error, not_started} | {ok, module()}.
room_type(RoomId) ->
    case dumbtable:get(?TABLE_NAME, RoomId) of
        {value, #?ROOM_TRACKER{cb_mod = CbMod}} ->
            {ok, CbMod};
        none -> {error, not_started}
    end.

rooms_of_type(RoomType) ->
    [Id || #?ROOM_TRACKER{id=Id, cb_mod=CallbackModule} <- dumbtable:values(?TABLE_NAME), CallbackModule =:= RoomType].

rooms_on_node(Node) ->
    [Id || #?ROOM_TRACKER{id=Id, running_at=RunNode} <- dumbtable:values(?TABLE_NAME), Node =:= RunNode].

isolated(Isolated) ->
    locks_leader:leader_call(?SERVER, {isolated, Isolated}).

is_isolated() ->
    locks_leader:call(?SERVER, is_isolated).

cluster_nodes() ->
    locks_leader:call(?SERVER, cluster_nodes).

start_success(Pid, RoomId, RoomSupPid) ->
    locks_leader:cast(Pid, {start_success, RoomId, RoomSupPid}).

%%%===================================================================
%%% locks_leader callbacks
%%%===================================================================

init(ClusterNodes) ->
    lager:info("starting, so alone..."),
    {ok, #state{isolated = true,
                cluster_nodes = ClusterNodes
               }}.

%% @spec elected(State::state(), I::info(), Cand::pid() | undefined) ->
%%   {ok, Broadcast, NState}
%% | {reply, Msg, NState}
%% | {ok, AmLeaderMsg, FromLeaderMsg, NState}
%% | {error, term()}
%%
%%     Broadcast = broadcast()
%%     NState    = state()
%%
%% @doc Called by the leader when it is elected leader, and each time a
%% candidate recognizes the leader.
%%
%% This function is only called in the leader instance, and `Broadcast'
%% will be sent to all candidates (when the leader is first elected),
%% or to the new candidate that has appeared.
%%
%% `Broadcast' might be the same as `NState', but doesn't have to be.
%% This is up to the application.
%%
%% If `Cand == undefined', it is possible to obtain a list of all new
%% candidates that we haven't synced with (in the normal case, this will be
%% all known candidates, but if our instance is re-elected after a netsplit,
%% the 'new' candidates will be the ones that haven't yet recognized us as
%% leaders). This gives us a chance to talk to them before crafting our
%% broadcast message.
%%
%% We can also choose a different message for the new candidates and for
%% the ones that already see us as master. This would be accomplished by
%% returning `{ok, AmLeaderMsg, FromLeaderMsg, NewState}', where
%% `AmLeaderMsg' is sent to the new candidates (and processed in
%% {@link surrendered/3}, and `FromLeaderMsg' is sent to the old
%% (and processed in {@link from_leader/3}).
%%
%% If `Cand == Pid', a new candidate has connected. If this affects our state
%% such that all candidates need to be informed, we can return `{ok, Msg, NSt}'.
%% If, on the other hand, we only need to get the one candidate up to speed,
%% we can return `{reply, Msg, NSt}', and only the candidate will get the
%% message. In either case, the candidate (`Cand') will receive the message
%% in {@link surrendered/3}. In the former case, the other candidates will
%% receive the message in {@link from_leader/3}.
%%
%% @end
%%
elected(State = #state{isolated = Isolated}, _Election, undefined) ->
    lager:info("There's a new sheriff in town... me!"),
    % This will be called in the new leader if the old leader dies.
    if
        Isolated -> ok;
        not Isolated -> locks_leader:leader_cast(self(), rebalance)
    end,
    {ok, {isolated, Isolated}, State}
    ;
elected(State = #state{isolated = Isolated}, _Election, Node) ->
    lager:info("New node ~p joined the cluster", [Node]),
    if
        Isolated -> ok;
        not Isolated -> locks_leader:leader_cast(self(), rebalance)
    end,
    {ok, {isolated, Isolated}, State}
    .

%% @spec surrendered(State::state(), Synch::broadcast(), I::info()) ->
%%          {ok, NState}
%%
%%    NState = state()
%%
%% @doc Called by each candidate when it recognizes another instance as
%% leader.
%%
%% Strictly speaking, this function is called when the candidate
%% acknowledges a leader and receives a Synch message in return.
%%
%% @end
surrendered(State = #state{isolated=WasIsolated}, {isolated, Isolated}, El) ->
    lager:info("Surrendered to leader ~p, was_isolated=~p, isolated=~p",
               [locks_leader:leader(El), WasIsolated, Isolated]),
    case {WasIsolated, Isolated} of
        {true, true} ->
            ok;
        {false, false} ->
            ok;
        {false, true} ->
            lager:error("Help, this shouldn't be happening to me!"),
            ok;
        {true, false} ->
            lager:info("Firing up the local storage!"),
            ok = dumbtable:create_local_copy(?TABLE_NAME)
    end,
    {ok, State#state{isolated = Isolated}}
    .

%% @spec handle_leader_call(Msg::term(), From::callerRef(), State::state(),
%%                          I::info()) ->
%%    {reply, Reply, NState} |
%%    {reply, Reply, Broadcast, NState} |
%%    {noreply, state()} |
%%    {stop, Reason, Reply, NState} |
%%    commonReply()
%%
%%   Broadcast = broadcast()
%%   NState    = state()
%%
%% @doc Called by the leader in response to a
%% {@link locks_leader:leader_call/2. leader_call()}.
%%
%% If the return value includes a `Broadcast' object, it will be sent to all
%% candidates, and they will receive it in the function {@link from_leader/3}.
%%
%% In this particular example, `leader_lookup' is not actually supported
%% from the {@link gdict. gdict} module, but would be useful during
%% complex operations, involving a series of updates and lookups. Using
%% `leader_lookup', all dictionary operations are serialized through the
%% leader; normally, lookups are served locally and updates by the leader,
%% which can lead to race conditions.
%% @end
handle_leader_call({call, Id, Message}, _From,
                   State = #state{isolated = Isolated},
                   _Election) when not Isolated ->
    case dumbtable:get(?TABLE_NAME, Id) of
        {value, Tracker = #?ROOM_TRACKER{status = Status,
                               running_at = Node,
                               exit_reason = ER,
                               cb_mod = CbMod,
                               cb_state = CbState}} ->
            case Status of
                running ->
                    case rpc:call(Node, CbMod, handle_call, [Message, CbState]) of
                        {reply, CbReply, CbNewState} ->
                            dumbtable:async_put(?TABLE_NAME, Id, Tracker#?ROOM_TRACKER{cb_state = CbNewState}),
                            {reply, CbReply, State};
                        {error, Reason, CbNewState} ->
                            dumbtable:async_put(?TABLE_NAME, Id, Tracker#?ROOM_TRACKER{cb_state = CbNewState}),
                            {reply, {error, Reason}, State};
                        {stop, Reason} ->
                            leader_stop_room(Id),
                            {reply, {stop, Reason}, State}
                    end;
                _ ->
                    {reply, {error, {exited, Status, ER}}, State}
            end;
        none ->
            {reply, {error, not_started}, State}
    end
    ;
handle_leader_call({start, CbMod, Id, CbState},
                   From,
                   State = #state{isolated = Isolated},
                   Election) when not Isolated ->
    case room_status(Id) of
        X when X =:= starting; X =:= running ->
            {reply, {error, X}, State};
        _ ->
            StartAt = least_loaded(candidate_nodes(Election)),
            dumbtable:async_put(?TABLE_NAME, Id,
                          #?ROOM_TRACKER{id = Id,
                                         status = starting,
                                         cb_mod = CbMod,
                                         cb_state = CbState,
                                         running_at = StartAt}),
            leader_start_at(Id, StartAt, From),
            {noreply, State}
    end
    ;
handle_leader_call({stop, RoomId}, _From, State = #state{isolated = Isolated}, _Election) when not Isolated ->
    Result = leader_stop_room(RoomId),
    {reply, Result, State}
    ;
handle_leader_call({isolated, Isolated}, _From, State = #state{isolated = WasIsolated}, _E) ->
    case {WasIsolated, Isolated} of
        {true, false} ->
            lager:info("back among friends!"),
            % if this is already started, means that we crashed and restarted?
            ok = dumbtable:create_local_copy(?TABLE_NAME);
        {false, true} ->
            lager:info("all alone... so dark."),
            Rooms = dumbtable:keys(?TABLE_NAME),
            [leader_stop_room(Room) || Room <- Rooms],
            ok = dumbtable:delete_local_copy(?TABLE_NAME);
        {true, true} -> ok;
        {false, false} -> ok
    end,
    {reply, ok, {isolated, Isolated}, State#state{isolated = Isolated}}
    ;
handle_leader_call(M, _From, State = #state{isolated = Isolated}, _E)
  when Isolated ->
    lager:warning("Isolated.  User request ignored: ~p", [M]),
    {reply, {error, isolated}, State}
    ;
handle_leader_call(Request, _From, State = #state{}, _Election) ->
    lager:warning("unexpected handle_leader_call(~p)", [Request]),
    {reply, ok, State}
    .

%% @spec handle_leader_cast(Msg::term(), State::term(), I::info()) ->
%%   commonReply()
%%
%% @doc Called by the leader in response to a {@link locks_leader:leader_cast/2.
%% leader_cast()}.
%% @end
handle_leader_cast(rebalance, State = #state{isolated = Isolated}, Election) when not Isolated ->
    RunningNodes = candidate_nodes(Election),
    check_restart_down_rooms(RunningNodes),
    rebalance(RunningNodes),
    {ok, State};
handle_leader_cast(rebalance, State = #state{isolated = Isolated}, _Election) when Isolated ->
    lager:warning("Isolated - not rebalancing"),
    {ok, State};
handle_leader_cast(Request, State = #state{}, _Election) ->
    lager:warning("unexpected handle_leader_cast(~p)", [Request]),
    {ok, State}.

%% @spec from_leader(Msg::term(), State::state(), I::info()) ->
%%    {ok, NState}
%%
%%   NState = state()
%%
%% @doc Called by each candidate in response to a message from the leader.
%%
%% In this particular module, the leader passes an update function to be
%% applied to the candidate's state.
%% @end
from_leader({isolated, Isolated}, State = #state{isolated = WasIsolated}, _E) ->
    case {WasIsolated, Isolated} of
        {true, false} ->
            lager:info("back among friends!"),
            ok = dumbtable:create_local_copy(?TABLE_NAME);
        {false, true} ->
            lager:info("all alone... so dark."),
            ok = dumbtable:delete_local_copy(?TABLE_NAME);
        {true, true} -> ok;
        {false, false} -> ok
    end,
    {ok, State#state{isolated = Isolated}}
    ;
from_leader(Sync, State, _Election) ->
    lager:warning("unexpected from_leader(~p, ~p)", [Sync, State]),
    {ok, State}
    .

%% @spec handle_DOWN(Candidate::pid(), State::state(), I::info()) ->
%%    {ok, NState} | {ok, Broadcast, NState}
%%
%%   Broadcast = broadcast()
%%   NState    = state()
%%
%% @doc Called by the leader when it detects loss of a candidate.
%%
%% If the function returns a `Broadcast' object, this will be sent to all
%% candidates, and they will receive it in the function {@link from_leader/3}.
%% @end
handle_DOWN(DownPid, State = #state{isolated = Isolated}, El) when not Isolated ->
    DownNode = node(DownPid),
    lager:warning("DOWN message: ~p", [DownNode]),
    RunningNodes = candidate_nodes(El),
    restart_down_load(DownNode, RunningNodes),
    lager:info("Restarting down load complete: ~p", [crah_run_counts()]),
    {ok, State}
    ;
handle_DOWN(DownNode, State = #state{isolated = Isolated}, _El) when Isolated ->
    lager:warning("Node ~p went down, doing nothing since I'm isolated", [DownNode]),
    {ok, State}
    .

%% @spec handle_call(Request::term(), From::callerRef(), State::state(),
%%                   I::info()) ->
%%    {reply, Reply, NState}
%%  | {noreply, NState}
%%  | {stop, Reason, Reply, NState}
%%  | commonReply()
%%
%% @doc Equivalent to `Mod:handle_call/3' in a gen_server.
%%
%% Note the difference in allowed return values. `{ok,NState}' and
%% `{noreply,NState}' are synonymous.
%%
%% `{noreply,NState}' is allowed as a return value from `handle_call/3',
%% since it could arguably add some clarity, but mainly because people are
%% used to it from gen_server.
%% @end
%%
handle_call({start, RoomId, ReplyTo}, _From, State = #state{isolated = Isolated}, _Election) when not Isolated ->
    do_start_room(RoomId, ReplyTo),
    {reply, ok, State}
    ;
handle_call({start, Model}, _From, State = #state{isolated = Isolated}, _Election) when Isolated ->
    lager:warning("Isolated - not starting room ~p", [Model]),
    {reply, {error, isolated}, State}
    ;
handle_call({stop, RoomId}, _From, State = #state{isolated = Isolated}, _Election) when not Isolated ->
    Reply = do_stop_room(RoomId),
    {reply, Reply, State}
    ;
handle_call({kill_room, RoomId}, _From, State = #state{isolated = Isolated}, _Election) when not Isolated ->
    Reply = do_kill_room(RoomId),
    {reply, Reply, State}
    ;
handle_call({stop, RoomId}, _From, State = #state{isolated = Isolated}, _Election) when Isolated ->
    lager:warning("Isolated - not stopping room ~p", [RoomId]),
    {reply, {error, Isolated}, State}
    ;
handle_call(is_isolated, _From, State = #state{isolated = Isolated}, _Election) ->
    {reply, Isolated, State}
    ;
handle_call(cluster_nodes, _From, State = #state{cluster_nodes = ClusterNodes}, _Election) ->
    {reply, ClusterNodes, State}
    ;
handle_call(Request, From, State = #state{}, _Election) ->
    lager:warning("Unexpected handle_call(message: ~p, from: ~p, state: ~p)", [Request, From, State]),
    {reply, huh, State}
    .

%% @spec handle_cast(Msg::term(), State::state(), I::info()) ->
%%    {noreply, NState}
%%  | commonReply()
%%
%% @doc Equivalent to `Mod:handle_call/3' in a gen_server, except
%% (<b>NOTE</b>) for the possible return values.
%%
handle_cast({start_success, RoomId, RoomSupPid}, State = #state{}, _Election) ->
    {value, #?ROOM_TRACKER{} = Tracker} = dumbtable:get(?TABLE_NAME, RoomId),
    Mref = monitor(process, RoomSupPid),
    dumbtable:async_put(?TABLE_NAME, RoomId, Tracker#?ROOM_TRACKER{status = running, mref = Mref}),
    {ok, State}
    ;
handle_cast(Msg, State = #state{}, _Election) ->
    lager:warning("Unexpected handle_cast(message: ~p, state: ~p)", [Msg, State]),
    {ok, State}.

%% @spec handle_info(Msg::term(), State::state(), I::info()) ->
%%     {noreply, NState}
%%   | commonReply()
%%
%% @doc Equivalent to `Mod:handle_info/3' in a gen_server,
%% except (<b>NOTE</b>) for the possible return values.
%%
%% This function will be called in response to any incoming message
%% not recognized as a call, cast, leader_call, leader_cast, from_leader
%% message, internal leader negotiation message or system message.
%% @end
%% Received when a room supervisor dies
handle_info({'DOWN', Mref, _, Pid, Info}, State, _E) ->
    case [Tracker || #?ROOM_TRACKER{mref = M} = Tracker <- dumbtable:values(?TABLE_NAME), M == Mref] of
        [#?ROOM_TRACKER{id = Id} = Tracker] ->
            lager:warning([{id, Id}], "A room has exited unexpectedly. id: ~p, pid: ~p, info: ~p", [Id, Pid, Info]),
            control_alarm:room_crashed(Info, Id),
            dumbtable:async_put(?TABLE_NAME, Id, Tracker#?ROOM_TRACKER{status = exited, exit_reason = Info, mref = undefined})
            ;
        [] ->
            lager:error("Unexpected DOWN message. pid: ~p, info: ~p", [Pid, Info])
    end,
    {ok, State};
handle_info(Info, State = #state{}, _E) ->
    lager:warning("Unexpected handle_info(info: ~p, state: ~p", [Info, State]),
    {ok, State}.

%% @spec terminate(Reason::term(), State::state()) -> Void
%%
%% @doc Equivalent to `terminate/2' in a gen_server callback
%% module.
%% @end
terminate(_Reason, _State) ->
    ok = dumbtable:delete_local_copy(?TABLE_NAME)
    .

%% @spec code_change(FromVsn::string(), OldState::term(),
%%                   I::info(), Extra::term()) ->
%%       {ok, NState}
%%
%%    NState = state()
%%
%% @doc Similar to `code_change/3' in a gen_server callback module, with
%% the exception of the added argument.
%% @end
code_change(_OldVsn, State, _Election, _Extra) ->
    {ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================

% get a summary of number of CRAHs on each node
% If nothing is running on a node, returns '0'
-spec crah_run_counts([node()]) -> [{non_neg_integer(), node()}].
crah_run_counts(Nodes) ->
    Counts = [{C,Node} || {C,Node} <- crah_run_counts(), lists:member(Node, Nodes)],
    CountedNodes = element(2, lists:unzip(Counts)),
    [{0, N} || N <- Nodes -- CountedNodes] ++ Counts.

-spec crah_run_counts() -> [{non_neg_integer(), node()}].
crah_run_counts() ->
    Groups = shadoww_lib:keygroup(3, run_counts()),
    SumGroup = fun(Group) ->
                       {CrahCounts, _, Nodes} = lists:unzip3(Group),
                       {lists:sum(CrahCounts), hd(Nodes)}
               end,
    [SumGroup(G) || G <- Groups].

% get the least loaded (fewest CRAHs running) among Nodes
-spec least_loaded([node()]) -> node().
least_loaded(Nodes) ->
    {_Running, StartAt} = hd(lists:sort(crah_run_counts(Nodes))),
    StartAt.

update_running_at(RoomId, NewNode) ->
    {value, RoomTracker} = dumbtable:get(?TABLE_NAME, RoomId),
    dumbtable:async_put(?TABLE_NAME, RoomId, RoomTracker#?ROOM_TRACKER{running_at = NewNode}).

check_restart_down_rooms(RunningNodes) ->
    DownNodes = lists:usort([Node || {_, _, Node} <- run_counts(),
                                     not lists:member(Node, RunningNodes)]),
    [restart_down_load(N, RunningNodes) || N <- DownNodes].

% Move all load running on DownNode to a node in RunningNodes
% Work from largest to smallest room from the down node, moving it to the
% least-loaded up node
restart_down_load(DownNode, RunningNodes) ->
    DownRooms = lists:reverse(
                  lists:sort(
                    [{NCrahs, RoomId}
                     || {NCrahs, RoomId, Node} <- run_counts(),
                        Node == DownNode])),
    lager:info("Restarting ~p from down node ~p", [DownRooms, DownNode]),
    do_restart_down_load(DownRooms, RunningNodes).

do_restart_down_load([], _) -> done;
do_restart_down_load([{_, DownRoom} | T], RunningNodes) ->
    leader_start_at(DownRoom, least_loaded(RunningNodes)),
    do_restart_down_load(T, RunningNodes).

% Among the nodes given, move load from highest loaded node to lowest loaded
% node until the load count is even
% Load can only be move in increments of 1 room, but the size of the room
% should be taken into account.  The largest room that can be moved and decrease
% the delta between the two will be moved.
% only executed on the leader node
rebalance(AliveNodes) ->
    lager:info("Starting a rebalance across ~p", [AliveNodes]),
    RunCounts = run_counts(),
    CrahRunCounts = lists:sort(crah_run_counts(AliveNodes)),
    {LowestLoad, LowestNode} = hd(CrahRunCounts),
    {HighestLoad, HighestNode} = lists:last(CrahRunCounts),

    lager:info("Lowest load ~p at ~p", [LowestLoad, LowestNode]),
    lager:info("Highest load ~p at ~p", [HighestLoad, HighestNode]),

    Delta = HighestLoad - LowestLoad,
    % Find all rooms on HighestNode with a CRAH count not exceeding Delta
    MoveCandidates = lists:sort([{CrahCount, RoomId}
                                 || {CrahCount, RoomId, Node} <- RunCounts,
                                    CrahCount < Delta,
                                    Node =:= HighestNode]),
    case MoveCandidates of
        [] ->
            lager:info("Rebalance complete: ~p", [RunCounts]);
        MoveCandidates ->
            {CrahCount, RoomId} = lists:last(MoveCandidates),
            lager:info("Moving room ~p with ~p CRAHs from ~p to ~p",
                       [RoomId, CrahCount, HighestNode, LowestNode]),
            leader_kill_room(RoomId),
            leader_start_at(RoomId, LowestNode),
            rebalance(AliveNodes)
    end.

% leader_*_room() are executed by the leader, in a handle_leader_* callback,
% to dispatch to appropriate node
%
% FIXME: what to do if the remote node dies while starting/stoping the room?
% Depends on what: if start, fail the call
% if stop, just remove the room from local dumbtable
% if kill, just abort and catch it on the rebalance

% When this is called, the room model has already been written to dumbtable
-spec leader_start_at(id(), node(), ReplyTo :: pid() | none) -> no_return().
leader_start_at(RoomId, StartAt, ReplyTo) ->
    update_running_at(RoomId, StartAt),
    if
        StartAt =:= node() -> do_start_room(RoomId, ReplyTo);
        StartAt =/= node() -> locks_leader:call({?SERVER, StartAt}, {start, RoomId, ReplyTo})
    end.

% Convenience function. no one is waiting on a reply that the room is started
leader_start_at(RoomId, StartAt) ->
    leader_start_at(RoomId, StartAt, none).

leader_stop_room(RoomId) ->
    RunningAt = where_running(RoomId),
    if
        RunningAt =:= node() -> do_stop_room(RoomId);
        RunningAt =/= node() -> locks_leader:call({?SERVER, RunningAt}, {stop, RoomId})
    end.

leader_kill_room(RoomId) ->
    RunningAt = where_running(RoomId),
    if
        RunningAt =:= node() -> do_kill_room(RoomId);
        RunningAt =/= node() -> locks_leader:call({?SERVER, RunningAt}, {kill_room, RoomId})
    end.

% do_*_room() are called on the node where the room is executing
-spec do_start_room(RoomId :: id(), ReplyTo :: pid() | none) -> {starting, Pid :: pid()}.
do_start_room(RoomId, ReplyTo) ->
    ManagerPid = self(),
    P = spawn(fun() ->
                      {value, #?ROOM_TRACKER{
                                  cb_mod = CbMod,
                                  cb_state = CbState
                                 } = Tracker} = dumbtable:get(?TABLE_NAME, RoomId),
                      Childspec = CbMod:room_childspec(CbState),
                      lager:info("Starting room ~p", [RoomId]),
                      case room_supervisor:start(Childspec) of
                          {ok, RoomSupPid, _ChildPids} ->
                              lager:info("Room ~p started successfully with supervisor pid ~p", [RoomId, RoomSupPid]),
                              send_reply(ReplyTo, ok),
                              start_success(ManagerPid, RoomId, RoomSupPid);
                          {error, E} ->
                              lager:error("Error starting room ~p!: ~p", [RoomId, E]),
                              dumbtable:async_put(?TABLE_NAME, RoomId, Tracker#?ROOM_TRACKER{status = startup_failure, exit_reason = E}),
                              send_reply(ReplyTo, {error, start_failure})
                      end
              end),
    {starting, P}.

send_reply(none, _Reply) -> ok;
send_reply(ReplyTo, Reply) -> locks_leader:leader_reply(ReplyTo, Reply).

% Kill everything without stopping it so that state is preserved across migration.
% Still want to cleanup the mailbox of DOWN messages though for a clean migration
do_kill_room(RoomId) ->
    StopResult = case room_status(RoomId) of
                     running ->
                         Mref = room_mref(RoomId),
                         ok = room_supervisor:stop(RoomId),
                         receive
                             {'DOWN', Mref, _, _, _} -> ok
                         end,
                         stopped ;
                     S -> S
                 end,
    StopResult.

do_stop_room(RoomId) ->
    RoomStatus = room_status(RoomId),
    lager:info("Stopping room ~p in status ~p", [RoomId, RoomStatus]),
    case RoomStatus of
        running ->
            demonitor(room_mref(RoomId)),
            Pid = room_supervisor:sup_pid(RoomId),
            ok = dyn_sup:shutdown(Pid),
            ok = room_supervisor:stop(RoomId),
            ok = dumbtable:remove(?TABLE_NAME, RoomId),
            stopped ;
        starting ->
            % TODO: how could this be handled without returning an error?
            {error, starting};
        {error, not_started} = E ->
            E;
        startup_failure ->
            ok = dumbtable:remove(?TABLE_NAME, RoomId),
            stopped;
        exited ->
            ok = dumbtable:remove(?TABLE_NAME, RoomId),
            stopped
    end.

where_running(Id) ->
    {value, #?ROOM_TRACKER{running_at = Node}} = dumbtable:get(?TABLE_NAME, Id),
    Node
    .

%% A list of CRAHs (that should be) running on node Node
%% This does *not* take into account whether the CRAHs have crashed,
%% so more like "CRAHs assigned to run on node Node"
-spec run_counts() -> [{NumCrahs::non_neg_integer(), RoomId::id(), Node::node()}].
run_counts() ->
    RoomConfigs = dumbtable:values(?TABLE_NAME),
    [{length(CbMod:crah_ids(State)), Id, R}
     || #?ROOM_TRACKER{id=Id, running_at=R, cb_mod=CbMod, cb_state=State} <- RoomConfigs].

env_cluster_nodes() ->
    {ok, Nodes} = application:get_env(loadbalance_nodes),
    Nodes.

% all nodes that are candidates for election, including self
% IMPORTANT: assumes that all candidates are on different nodes.
% Holds for shadoww, not for locks_leader in general.
candidate_nodes(El) ->
    [node() | [node(Pid) || Pid <- locks_leader:candidates(El)]].

