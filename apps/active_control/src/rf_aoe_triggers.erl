%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(rf_aoe_triggers).

-export([
    init/3,
    triggering_event/2,
    present_status/2,
    present_setpoint/2,
    present_as_prev/1
]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-record(resource_state, {
    status = #{}, % id() -> {Previous :: device_status(), Present :: device_status()}
    setpoint = #{} % id() -> {Previous :: float(), Present :: float()}
}).

-record(state, {
    room_id :: id(),
    pressure :: resource_state(),
    temperature :: resource_state()
}).

-type resource_state() :: #resource_state{} | none.

-opaque state_tracker() :: #state{}.

-export_type([state_tracker/0]).

-spec init(Ids :: [id()], RoomId :: id(), Resource :: controlled_resource()) -> state_tracker().
init(Ids, RoomId, pressure) when is_binary(RoomId) ->
    #state{
        room_id = RoomId,
        temperature = none,
        pressure = new_resource_state(Ids, pressure)
    };

init(Ids, RoomId, temperature) when is_binary(RoomId) ->
    #state{
        room_id = RoomId,
        temperature = new_resource_state(Ids, temperature),
        pressure = new_resource_state(Ids, pressure)
    }.

-spec triggering_event(room_event:room_event(), State :: state_tracker()) -> {boolean(), state_tracker()}.
triggering_event({Key, {input_status, Enabled}},
        State = #state{
            room_id = RoomId,
            temperature = TS,
            pressure = PS
        }) ->
    {Triggering, NewPS, NewTS} = input_status(RoomId, Key, Enabled, PS, TS),
    {
        Triggering,
        State#state{
            pressure = NewPS,
            temperature = NewTS
        }
    };
triggering_event({_, {device_status, reconnecting}}, State) ->
    {
        false,
        State
    };
triggering_event({Key, {device_status, NewStatus}},
        State = #state{
            room_id = RoomId,
            temperature = TS,
            pressure = PS
        }) ->
    {Triggering, NewPS, NewTS} = device_status(RoomId, Key, NewStatus, PS, TS),
    {
        Triggering,
        State#state{
            pressure = NewPS,
            temperature = NewTS
        }
    };
triggering_event({Key, {device_setpoint, NewSetpoint}},
        State = #state{
            room_id = RoomId,
            temperature = TS,
            pressure = PS
        }) ->
    {Triggering, NewPS, NewTS} = device_setpoint(RoomId, Key, NewSetpoint, PS, TS),
    {
        Triggering,
        State#state{
            pressure = NewPS,
            temperature = NewTS
        }
    }.

-spec present_status(Resource :: controlled_resource(), State :: state_tracker()) -> [{id(), device_status()}].
present_status(temperature, #state{temperature = #resource_state{status = Status}}) ->
    all_present(Status);
present_status(pressure, #state{pressure = #resource_state{status = Status}}) ->
    all_present(Status).

-spec present_setpoint(Resource :: controlled_resource(), State :: state_tracker()) -> [{id(), float()}].
present_setpoint(temperature, #state{temperature = #resource_state{setpoint = Setpoint}}) ->
    all_present(Setpoint);
present_setpoint(pressure, #state{pressure = #resource_state{setpoint = Setpoint}}) ->
    all_present(Setpoint).

-spec present_as_prev(State :: state_tracker()) -> state_tracker().
present_as_prev(State = #state{
    temperature = none,
    pressure = #resource_state{status = PST, setpoint = PSP} = PS
}) ->
    State#state{
        temperature = none,
        pressure = PS#resource_state{
            status = present_as_prev_m(PST),
            setpoint = present_as_prev_m(PSP)
        }
    };
present_as_prev(State = #state{
    temperature = #resource_state{status = TST, setpoint = TSP} = TS,
    pressure = PS
}) ->
    State#state{
        temperature = TS#resource_state{
            status = present_as_prev_m(TST),
            setpoint = present_as_prev_m(TSP)
        },
        pressure = PS
    }.

present_as_prev_m(M = #{}) ->
    maps:map(fun(_, {_Prev, Pres}) -> {Pres, Pres} end, M).


%% PRIVATE functions
-spec new_resource_state(Ids :: [id()], Resource :: controlled_resource()) -> resource_state().
new_resource_state(Ids, Resource) ->
    ST = [get_status(Id, Resource) || Id <- Ids],
    #resource_state{
        status = lists:foldl(fun({Id, Status, _}, M) ->
            maps:put(Id, {Status, Status}, M)
                             end,
            #{},
            ST),
        setpoint = lists:foldl(fun({Id, _, Setpoint}, M) ->
            maps:put(Id, {Setpoint, Setpoint}, M)
                               end,
            #{},
            ST)
    }.

-spec get_status(Id :: id(), Resource :: controlled_resource()) -> {id(), device_status(), float()}.
get_status(Id, Resource) ->
    Status = driver_lib:call(driver_lib:proc_key(Id, Resource, driver), get_status, []),
    process_status(Status).

-spec process_status(vfd_status() | crah_status() | crac_status()) -> {id(), device_status(), float()}.
process_status(#vfd_status{id = Id, status = Status, setpoint = SetPoint}) -> {Id, Status, SetPoint};
process_status(#crah_status{id = Id, status = Status, setpoint = SetPoint}) -> {Id, Status, SetPoint};
process_status(#crac_status{id = Id, status = Status, setpoint = SetPoint}) -> {Id, Status, SetPoint}.

-spec input_status(RoomId :: id(),
                   Key :: driver_lib:proc_key(),
                   Enabled :: boolean(),
                   PS :: resource_state(),
                   TS :: resource_state()) -> {boolean(), resource_state(), resource_state()}.
input_status(_RoomId, Key, _Enabled, PS, TS) ->
    EventResource = driver_lib:resource(Key),
    input_status(EventResource, PS, TS).

-spec input_status(controlled_resource(), resource_state(), resource_state()) ->
    {boolean(), resource_state(), resource_state()}.
input_status(pressure, PS, none) ->
    {true, PS, none};
input_status(temperature, PS, TS) ->
    {true, PS, TS};
input_status(_, PS, TS) ->
    {false, PS, TS}.

-spec device_status(RoomId :: id(),
                    Key :: driver_lib:proc_key(),
                    NewStatus :: device_status(),
                    PS :: resource_state(),
                    TS :: resource_state()) -> {boolean(), resource_state(), resource_state()}.
device_status(_RoomId, Key, NewStatus, PS = #resource_state{status = PStatus}, none) ->
    Id = driver_lib:id(Key),
    CurrentTrigger = p_aoe_triggering_status(present(Id, PStatus), NewStatus),
    CumulativeTrigger = p_aoe_triggering_status(previous(Id, PStatus), NewStatus),
    {
        CurrentTrigger or CumulativeTrigger,
        PS#resource_state{status = present(Id, PStatus, NewStatus)},
        none
    };
device_status(_RoomId, Key, NewStatus, PS = #resource_state{status = PStatus},
        TS =
            #resource_state{status = TStatus}) ->
    Id = driver_lib:id(Key),
    EventResource = driver_lib:resource(Key),
    CurrentTrigger = t_aoe_triggering_status(EventResource, present(Id, PStatus), present(Id, TStatus), NewStatus),
    CumulativeTrigger = t_aoe_triggering_status(EventResource, previous(Id, PStatus), present(Id, TStatus), NewStatus),
    case EventResource of
        pressure ->
            {
                CurrentTrigger or CumulativeTrigger,
                PS#resource_state{status = present(Id, PStatus, NewStatus)},
                TS
            };
        temperature ->
            {
                CurrentTrigger or CumulativeTrigger,
                PS,
                TS#resource_state{status = present(Id, TStatus, NewStatus)}
            }
    end;
device_status(_, _, _, PS, TS) ->
    {false, PS, TS}.

-spec device_setpoint(RoomId :: id(),
                      Key :: driver_lib:proc_key(),
                      NewSetpoint :: float(),
                      PS :: resource_state(),
                      TS :: resource_state()) -> {boolean(), resource_state(), resource_state()}.
device_setpoint(RoomId, Key, NewSetpoint, PS = #resource_state{setpoint = Setpoint}, TS = #resource_state{}) ->
    Id = driver_lib:id(Key),
    Threshold = strategy_param:find(fanspeed_delta_trigger, pressure, RoomId),
    CurrentTrigger = triggering_setpoint(present(Id, Setpoint), NewSetpoint, Threshold),
    CumulativeTrigger = triggering_setpoint(previous(Id, Setpoint), NewSetpoint, Threshold),
    {
        CurrentTrigger or CumulativeTrigger,
        PS#resource_state{setpoint = present(Id, Setpoint, NewSetpoint)},
        TS
    };
device_setpoint(_, _, _, PS, TS) ->
    {false, PS, TS}.

previous(Id, M) ->
    {Prev, _Pres} = maps:get(Id, M),
    Prev.
present(Id, M) ->
    {_Prev, Pres} = maps:get(Id, M),
    Pres.
present(Id, M, New) ->
    {Prev, _Pres} = maps:get(Id, M),
    M#{Id := {Prev, New}}.

all_present(M) ->
    maps:fold(fun(Id, {_, Pres}, L) -> [{Id, Pres} | L] end,
        [],
        M).

-spec p_controlling(Status :: device_status()) -> boolean().
p_controlling(Status) when Status == online; Status == local_override ->
    true;
p_controlling(Status) when Status == disengaged; Status == disconnected; Status == standby; Status == reconnecting ->
    false.

-spec t_controlling(Status :: device_status()) -> boolean().
t_controlling(Status) when Status == online ->
    true;
t_controlling(Status) when Status == disengaged; Status == disconnected; Status == standby; Status == reconnecting ->
    false.

-spec p_aoe_triggering_status(POld :: device_status(), PNew :: device_status()) -> boolean().
p_aoe_triggering_status(POld, PNew) ->
    % If P transitions
    (p_controlling(POld) /= p_controlling(PNew)).

-spec t_aoe_triggering_status(Resource :: controlled_resource(),
                              POld :: device_status(),
                              TOld :: device_status(),
                              PNew :: device_status()) -> boolean().
t_aoe_triggering_status(pressure, POld, TOld, PNew) ->
    % If T is controlling and P transitions
    t_controlling(TOld) andalso (p_controlling(POld) /= p_controlling(PNew));
t_aoe_triggering_status(temperature, POld, TOld, TNew) ->
    % If P is controlling and T transitions
    p_controlling(POld) andalso (t_controlling(TOld) /= t_controlling(TNew)).

-spec triggering_setpoint(Old :: float(), New :: float(), Threshold :: float()) -> boolean().
triggering_setpoint(Old, New, Threshold)
    when is_float(Old), is_float(New) ->
    abs(Old - New) > Threshold;
triggering_setpoint(_Old, _New, _Threshold) ->
    true.


