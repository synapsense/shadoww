%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(ac_api_json).

-compile({parse_transform, runtime_types}).

-export([
         register_rpc/0,
         list_rooms/0,
         list_crahs/1,
         get_control_status/1,
         get_control_status/2,
         set_control_config/1,
         set_control_config/2,
         set_control_mode/3,
         set_manual_output/4,
         get_input_status/2,
         set_input_config/2,
         set_input_target/4
        ]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-export_records([
                 single_input,
                 modbus_bindings,
                 bacnet_bindings,
                 generic_crah,
                 liebert_ahu,
                 generic_crac,
                 generic_vfd,
                 ach550,
                 crah_driver,
                 crac_driver,
                 vfd_driver,

                 raised_floor_room,
                 raised_floor_balancer_strategy,
                 raised_floor_crah_strategy,
                 raised_floor_vfd_strategy,
                 raised_floor_crah,

                 raised_floor_room_status,
                 raised_floor_balancer_strategy_status,
                 raised_floor_crah_status,
                 raised_floor_strategy_status,

                 remote_control_room,
                 remote_control_strategy,
                 remote_control_crah,
                 remote_control_crac,

                 remote_control_room_status,
                 remote_control_strategy_status,
                 remote_control_crah_status,
                 remote_control_crac_status,

                 crah_status,
                 crac_status,
                 vfd_status,
                 input_status
                ]).

register_rpc() ->
    Methods = [
               <<"list_rooms">>,
               <<"list_crahs">>,
               <<"get_control_status">>,
               <<"set_control_config">>,
               <<"set_control_mode">>,
               <<"set_manual_output">>,
               <<"get_input_status">>,
               <<"set_input_config">>,
               <<"set_input_target">>
              ],
    [json_rpc:register_method(M, ?MODULE) || M <- Methods].

list_rooms() ->
    {ok, ac_api:list_rooms()}.

list_crahs(RoomId)
    when is_binary(RoomId) ->
    {ok, ac_api:list_crahs(RoomId)}.

-type room_status_t() :: raised_floor_room_status()
                       | remote_control_room_status().

get_control_status(Id) ->
    with_detail(ac_api:get_control_status(Id), room_status_t).

-type device_status_t() :: raised_floor_crah_status()
                         | remote_control_crac_status()
                         | remote_control_crah_status().

get_control_status(RoomId, CrahId)
        when is_binary(RoomId), is_binary(CrahId) ->
    with_detail(ac_api:get_control_status(RoomId, CrahId), device_status_t).

set_control_config(ConfigJson) ->
    Config = aeon:to_record(ConfigJson, ?MODULE, raised_floor_balancer_strategy),
    with_detail(ac_api:set_control_config(Config), ok).

-type set_config_t() :: raised_floor_crah_strategy()
                      | raised_floor_vfd_strategy()
                      | remote_control_strategy().
set_control_config(RoomId, ConfigJson)
        when is_binary(RoomId) ->
    Config = aeon:to_type(ConfigJson, ?MODULE, set_config_t),
    with_detail(ac_api:set_control_config(RoomId, Config), ok).

set_control_mode(RoomId, StrategyId, ControlModeJson)
        when is_binary(RoomId), is_binary(StrategyId) ->
    ControlMode = aeon:to_type(ControlModeJson, ?MODULE, control_mode),
    with_detail(ac_api:set_control_mode(RoomId, StrategyId, ControlMode), ok).

set_manual_output(RoomId, StrategyId, ResourceJson, Value)
        when is_binary(RoomId), is_binary(StrategyId), is_number(Value) ->
    Resource = aeon:to_type(ResourceJson, ?MODULE, controlled_resource),
    with_detail(ac_api:set_manual_output(RoomId, StrategyId, Resource, float(Value)), ok).

get_input_status(RoomId, Id)
        when is_binary(RoomId), is_binary(Id) ->
    with_detail(ac_api:get_input_status(RoomId, Id), input_status).

set_input_config(RoomId, ConfigJson)
        when is_binary(RoomId) ->
    Config = aeon:to_type(ConfigJson, ?MODULE, single_input),
    with_detail(ac_api:set_input_config(RoomId, Config), ok).

set_input_target(RoomId, InputId, ResourceJson, Target)
        when is_binary(RoomId), is_binary(InputId), is_number(Target) ->
    Resource = aeon:to_type(ResourceJson, ?MODULE, controlled_resource),
    with_detail(ac_api:set_input_target(RoomId, InputId, Resource, float(Target)), ok).

with_detail(ok, ok) ->
    ok;
with_detail({ok, Value}, SuccessType) ->
    {ok, aeon:type_to_jsx(Value, ?MODULE, SuccessType)};
with_detail({error, Reason}, _SuccessType) ->
    {ok, error_detail(Reason)}.

error_detail(no_such_id) -> [{<<"code">>, 100}, {<<"message">>, <<"ID not found">>}];
error_detail(resource_not_configured) -> [{<<"code">>, 101}, {<<"message">>, <<"Control device not configured for resource">>}];
error_detail(no_inputs) -> [{<<"code">>, 102}, {<<"message">>, <<"No inputs for this mechanical type">>}];
error_detail({'EXIT', {noproc, _}}) -> [{<<"code">>, 100}, {<<"message">>, <<"ID not found">>}];
error_detail(target_out_of_range) -> [{<<"code">>, 200}, {<<"message">>, <<"Target out of range">>}];
error_detail(Message) when is_list(Message) ->
    [{<<"code">>, 300}, {<<"message">>, fb("Cannot change control mode: ~s", [Message])}];
error_detail(Message) ->
    [{<<"code">>, 500}, {<<"message">>, fb("Internal error: ~p", [Message])}].

fb(F, A) ->
    list_to_binary(io_lib:format(F, A)).

