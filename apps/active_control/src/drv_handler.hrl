-record(state,
	{id :: tuple(),
	 device_class_mod,
	 protocol_mod,
	 io_abort_reason,
	 setpoint :: float_or_empty(),
	 device_setpoint :: float_or_empty(),
	 device_rat :: float_or_empty(),
	 device_sat :: float_or_empty(),
	 device_speed :: float_or_empty(),
	 device_kw :: float_or_empty(),
	 compressor_stages :: float_or_empty(),
	 capacity :: float_or_empty(),
	 pull_timer :: tracked_timer(),
	 push_timer :: tracked_timer(),
	 reconnect_count :: non_neg_integer(),
	 failed_txn :: 'push' | 'pull',
	 high_head,
	 low_suction,
	 compressor_overload,
	 short_cycle,
	 change_filters,
	 over_current,
	 over_voltage,
	 under_voltage,
	 over_temperature,
	 general_alarm	 
	}).

-define(RECONNECT_LIMIT, 3). % 3 times = 3 * PING_TIME before an alert will be raised
-define(MOVING_TIMEOUT, 200000).
-define(RECONNECT_TIME, 20000).


% This macro checks that the I/O result ref matches the cached ref.
% This check ensures that I/O results from canceled transactions are discarded
-define(io_txn_match(FnId, TxRef, State),
	(
		((FnId == pull) andalso ({pending, TxRef} == State#state.pull_timer))
		or
		((FnId == push) andalso ({pending, TxRef} == State#state.push_timer))
	)
).

% meaning of the *_timer fields:
% undefined - the timer is not running, nor is there a pending I/O transaction for the timer
% {pending, Ref} - the timer is not running, but there is a pending I/O transaction
% Ref - the timer has not yet expired, or the event has not yet been processed
-type tracked_timer() :: 'undefined' | reference() | {'pending', reference()} | {reference(), reference()}.

-type float_or_empty() :: float() | [].

-define(SP_MIN_C, 35.0).
-define(SP_MAX_C, 120.0).
-define(SP_MIN_V, 15.0).
-define(SP_MAX_V, 100.0).
-define(SP_MIN_R, 0.0).
-define(SP_MAX_R, 100.0).

-define(in_range_c(V), ((V >= ?SP_MIN_C) andalso (V =< ?SP_MAX_C))).
-define(in_range_v(V), ((V >= ?SP_MIN_V) andalso (V =< ?SP_MAX_V))).
-define(in_range_r(V), ((V >= ?SP_MIN_R) andalso (V =< ?SP_MAX_R))).

-define(CRAC_ALERTS, ["highHead","lowSuction","compressorOverload","shortCycle","changeFilters","generalCracAlarm"]).
-define(CRAH_ALERTS, ["changeFilters","generalCrahAlarm"]).
-define(VFD_ALERTS, ["overCurrent","overVoltage","underVoltage","overTemperature","generalVfdAlarm"]).
