-module(base_driver).

-behaviour(gen_server).

%%gen_server required callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([start/9, start_link/9]).

%%behaviour's APIs (similar to gen_fsm's APIs)
-export([
    send_event/2,
    sync_send_event/2,
    sync_send_event/3,
    send_all_state_event/2,
    sync_send_all_state_event/2,
    sync_send_all_state_event/3,
    start_timer/2,
    send_event_after/2,
    cancel_timer/1,
    stop_driver/1,
    drv_state/1,
    childspec/2
    ]).

%% API for other processess to interact with the driver
-export([
    start_link/2,
    get_property/2,
    get_property/3,
    set_property/3,
    set_properties/2,
    driver_properties/1,
    set_output/2,
    set_output/3,
    get_status/1,
    get_config/1,
    set_config/2,
    engage/1,
    disengage/1,
    stop/1
    ]).


-include("ac_constants.hrl").
-include_lib("active_control/include/ac_api.hrl").


-define(STATE, base_driver_state).
-define(SCOPE, l).

-record(?STATE, {
	   room_id,
	   id,
	   key,
	   state_name,
	   config_record,
	   t_data,
	   properties,
	   device_class_mod
	  }).

%%% ---------------------------------------------------
%%% API.
%%% ---------------------------------------------------
-callback init(Id :: tuple(), Properties :: [proplists:property()], ProtocolMod :: atom()) ->
	{ok, StateName :: atom(), StateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: term()} | ignore.
-callback reinit(Id :: tuple(), Properties :: [proplists:property()], ProtocolMod :: atom()) ->
	{ok, StateName :: atom(), StateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: term()} | ignore.
-callback handle_event(Event :: term(), StateName :: atom(),
		       StateData :: term(), Properties :: [proplists:property()]) ->
	{next_state, NextStateName :: atom(), NewStateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: term(), NewStateData :: term()}.
-callback handle_sync_event(Event :: term(), From :: {pid(), Tag :: term()},
			    StateName :: atom(), StateData :: term(), Properties :: [proplists:property()]) ->
	{reply, Reply :: term(), NextStateName :: atom(), NewStateData :: term(), Properties :: [proplists:property()]} |
	{next_state, NextStateName :: atom(), NewStateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: term(), Reply :: term(), NewStateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: term(), NewStateData :: term()}.
-callback handle_info(Info :: term(), StateName :: atom(),
		      StateData :: term(), Properties :: [proplists:property()]) ->
	{next_state, NextStateName :: atom(), NewStateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: normal | term(), NewStateData :: term()}.
-callback terminate(Reason :: normal | shutdown | {shutdown, term()}
			    | term(), StateName :: atom(), StateData :: term(), Properties :: [proplists:property()]) ->
	term().
-callback code_change(OldVsn :: term() | {down, term()}, StateName :: atom(),
		      StateData :: term(), Properties :: [proplists:property()], Extra :: term()) ->
	{ok, NextStateName :: atom(), NewStateData :: term(), Properties :: [proplists:property()]}.
-callback property_changed(PropertyName :: term(), OldValue :: term(),
			   NewValue :: term(), StateName :: atom(), StateData :: term(),
			   Properties :: [proplists:property()]) ->
	{next_state, NextStateName :: atom(), NewStateData :: term(), Properties :: [proplists:property()]} |
	{stop, Reason :: normal | term(), NewStateData :: term(), Properties :: [proplists:property()]}.
-callback new_output(Value :: float(), StateName :: atom(), StateData :: term(),
		     Properties :: [proplists:property()]) ->
	{reject, Reason :: term(), StateName :: atom(), TData :: term(), Properties:: [proplists:property()]} |
	{accept, Value :: float(), StateName :: atom(), TData :: term(), Properties:: [proplists:property()]}.
-callback properties(ProtocolMod :: atom() | tuple()) ->
	Properties:: [proplists:property()]
	.

-spec start(binary(), binary(), atom(), module(), module(), [tuple()], tuple(), tuple(), list()) -> pid() | {error, {already_started, term()}}.
%% @doc Creates a gen_server process
start(RoomId, Id, Resource, ClassMod, ProtocolMod, Properties, ConfigRec, Args, Options) ->
	start_driver(start, RoomId, Id, Resource, ClassMod, ProtocolMod, Properties, ConfigRec, Args, Options).

-spec start_link(binary(), binary(), atom(), module(), module(), [tuple()], tuple(), tuple(), list()) -> pid() | {error, {already_started, term()}}.
%% @doc Creates a gen_server process
start_link(RoomId, Id, Resource, ClassMod, ProtocolMod, Properties, ConfigRec, Args, Options) ->
	start_driver(start_link, RoomId, Id, Resource, ClassMod, ProtocolMod, Properties, ConfigRec, Args, Options).

start_link(RoomId, C = #crah_driver{
	      id = Id,
	      resource = Res,
	      protocol_bindings = Bindings,
	      driver_config = DriverConfig
	     }) ->
    ClassMod = crah_fsm,
    ProtocolMod = protocol_module(crah, DriverConfig, Bindings),
    Properties = driver_properties(C),
    ProcId = driver_lib:proc_key(Id, Res, driver),
    start_link(RoomId, Id, Res, ClassMod, ProtocolMod, Properties, C, ProcId, [])
    ;
start_link(RoomId, C = #crac_driver{
	      id = Id,
	      resource = Res,
	      protocol_bindings = Bindings,
	      driver_config = DriverConfig
	     }) ->
    ClassMod = crac_fsm,
    ProtocolMod = protocol_module(crac, DriverConfig, Bindings),
    Properties = driver_properties(C),
    ProcId = driver_lib:proc_key(Id, Res, driver),
    start_link(RoomId, Id, Res, ClassMod, ProtocolMod, Properties, C, ProcId, [])
    ;
start_link(RoomId, C = #vfd_driver{
	      id = Id,
	      resource = Res,
	      protocol_bindings = Bindings,
	      driver_config = DriverConfig
	     }) ->
    ClassMod = vfd_fsm,
    ProtocolMod = protocol_module(vfd, DriverConfig, Bindings),
    Properties = driver_properties(C),
    ProcId = driver_lib:proc_key(Id, Res, driver),
    start_link(RoomId, Id, Res, ClassMod, ProtocolMod, Properties, C, ProcId, [])
    .

protocol_module(vfd, #ach550{}, #modbus_bindings{}) -> ach550_vfd ;
protocol_module(vfd, #generic_vfd{}, #modbus_bindings{}) -> generic_modbus_vfd ;
protocol_module(vfd, #generic_vfd{}, #bacnet_bindings{}) -> generic_bacnet_vfd ;
protocol_module(crah, #liebert_ahu{}, #modbus_bindings{}) -> liebert_modbus_crah ;
protocol_module(crah, #generic_crah{}, #modbus_bindings{}) -> generic_modbus_crah ;
protocol_module(crah, #generic_crah{}, #bacnet_bindings{}) -> generic_bacnet_crah ;
protocol_module(crac, #liebert_ahu{}, #modbus_bindings{}) -> liebert_modbus_crac ;
protocol_module(crac, #generic_crac{}, #modbus_bindings{}) -> generic_modbus_crac ;
protocol_module(crac, #generic_crac{}, #bacnet_bindings{}) -> generic_bacnet_crac ;
protocol_module(_, _, _) -> error(no_module) .

driver_properties(#crah_driver{
		     resource = Res,
		     pull_time = Pull,
		     push_time = Push,
		     cooling_control = CoolingControl,
		     cascade_standby = Cascade,
		     protocol_bindings = Bindings,
		     driver_config = DriverConfig
		    }) ->
    [
     {"resource", Res},
     {"setpoint", []},
     {"pushTime", Push},
     {"pullTime", Pull},
     {"cascadeStandby", Cascade},
     {"coolingControl", CoolingControl},
     {"engaged", ?DISENGAGED},
     {"deviceRat", []},
     {"deviceSat", []},
     {"capacity", []},
     {"unitstatus", ?ONLINE}
    ]
    ++ driver_config_properties(DriverConfig)
    ++ protocol_bindings:to_proplist(Bindings);
driver_properties(#crac_driver{
		     resource = Res,
		     pull_time = Pull,
		     push_time = Push,
		     cooling_control = CoolingControl,
		     max_stages = _MaxStages,
		     cascade_standby = Cascade,
		     protocol_bindings = Bindings,
		     driver_config = DriverConfig
		    }) ->
    [
     {"resource", Res},
     {"setpoint", []},
     {"pushTime", Push},
     {"pullTime", Pull},
     {"cascadeStandby", Cascade},
     {"coolingControl", CoolingControl},
     {"engaged", ?DISENGAGED},
     {"compressorStages", []},
     {"deviceRat", []},
     {"deviceSat", []},
     {"capacity", []},
     {"unitstatus", ?ONLINE}
    ]
    ++ driver_config_properties(DriverConfig)
    ++ protocol_bindings:to_proplist(Bindings);
driver_properties(#vfd_driver{
		     resource = Res,
		     pull_time = Pull,
		     push_time = Push,
		     cfm_table = CfmTable,
		     protocol_bindings = Bindings,
		     driver_config = DriverConfig
		    }) ->
    [
     {"resource", Res},
     {"setpoint", []},
     {"pushTime", Push},
     {"pullTime", Pull},
     {"engaged", ?DISENGAGED},
     {"cfm_table", CfmTable},
     {"kw", []},
     {"unitstatus", ?ONLINE}
    ]
    ++ driver_config_properties(DriverConfig)
    ++ protocol_bindings:to_proplist(Bindings).

driver_config_properties(#ach550{}) ->
    []
    ;
driver_config_properties(#liebert_ahu{sitelink_port = Port}) ->
    [{"port", Port}]
    ;
driver_config_properties(#generic_vfd{
		     setpoint_write = SpWrite,
		     setpoint_read = SpRead,
		     power_read = PowerRead,
		     local_override_read = LORead,
		     comm_read = CommRead,
		     over_current_alarm_read = OCARead,
		     over_voltage_alarm_read = OVARead,
		     under_voltage_alarm_read = UVARead,
		     over_temperature_alarm_read = OTARead,
		     general_alarm_read = GARead,
		     clear_alarm_write = CAWrite
		    }) ->
    [
     {"setpointWrite", SpWrite},
     {"setpointRead", SpRead},
     {"powerRead", PowerRead},
     {"localOverrideRead", LORead},
     {"commRead", CommRead},
     {"overCurrentAlarmRead", OCARead},
     {"overVoltageAlarmRead", OVARead},
     {"underVoltageAlarmRead", UVARead},
     {"overTemperatureAlarmRead", OTARead},
     {"generalAlarmRead", GARead},
     {"clearAlarmWrite", CAWrite}
    ];
driver_config_properties(#generic_crah{
		     setpoint_write = SpWrite,
		     setpoint_read = SpRead,
		     valve_read = ValveRead,
		     rat_read = RatRead,
		     sat_read = SatRead,
		     standby_read = StandbyRead,
		     comm_read = CommRead,
		     change_filters_alarm_read = CFARead,
		     general_alarm_read = GARead,
		     clear_alarm_write = CAWrite
		    }) ->
    [
     {"setpointRead", SpRead},
     {"setpointWrite", SpWrite},
     {"ratRead", RatRead},
     {"satRead", SatRead},
     {"valveRead", ValveRead},
     {"standbyRead", StandbyRead},
     {"commRead", CommRead},
     {"changeFiltersAlarmRead", CFARead},
     {"generalAlarmRead", GARead},
     {"clearAlarmWrite", CAWrite}
    ];
driver_config_properties(#generic_crac{
		     setpoint_write = SpWrite,
		     setpoint_read = SpRead,
		     rat_read = RatRead,
		     sat_read = SatRead,
		     compressor_read = CompressorRead,
		     capacity_read = CapacityRead,
		     standby_read = StandbyRead,
		     comm_read = CommRead,
		     high_head_pressure_alarm_read = HighHeadPressureAlarmRead,
		     low_suction_pressure_alarm_read = LowSuctionPressureAlarmRead,
		     compressor_overload_alarm_read = CompressorOverloadAlarmRead,
		     short_cycle_alarm_read = ShortCycleAlarmRead,
		     change_filters_alarm_read = CFARead,
		     general_alarm_read = GARead,
		     clear_alarm_write = CAWrite
		    }) ->
    [
     {"setpointRead", SpRead},
     {"setpointWrite", SpWrite},
     {"ratRead", RatRead},
     {"satRead", SatRead},
     {"compressorRead", CompressorRead},
     {"capacityRead", CapacityRead},
     {"standbyRead", StandbyRead},
     {"commRead", CommRead},
     {"highHeadPressureAlarmRead", HighHeadPressureAlarmRead},
     {"lowSuctionPressureAlarmRead", LowSuctionPressureAlarmRead},
     {"compressorOverloadAlarmRead", CompressorOverloadAlarmRead},
     {"shortCycleAlarmRead", ShortCycleAlarmRead},
     {"changeFiltersAlarmRead", CFARead},
     {"generalAlarmRead", GARead},
     {"clearAlarmWrite", CAWrite}
    ].

where(Name) ->
    driver_lib:lookup(Name)
    .

start_driver(Fun, RoomId, Id, Resource, ClassMod, ProtocolMod, Properties, ConfigRec, Args, Options) ->
    FilteredProps = check_properties(ClassMod, ProtocolMod, Properties),
    case driver_lib:driver_running(Id, Resource) of
	true -> error({already_started, Id});
	false -> gen_server:Fun(?MODULE, [RoomId, Id, Resource, ClassMod, ProtocolMod, FilteredProps, ConfigRec, Args], Options)
    end
    .

stop(Id) ->
    stop_driver(Id)
    .

-spec stop_driver(pid() | atom()) -> no_return().
%% @doc Stopps driver
stop_driver(Pid) when is_pid(Pid) ->
	Mref = monitor(process, Pid),
	Pid ! driver_stop,
	receive
		{'DOWN', Mref, process, Pid, _Info} -> ok
	after
		1000 -> error({wont_stop, Pid})
	end
	;

stop_driver(Name) ->
    Pid = where(Name),
    stop_driver(Pid).

-spec drv_state(pid() | atom()) -> [tuple()].
%% @doc Returns the driver's state
drv_state(Name) ->
    call(Name, drv_state).

-spec send_event(pid() | atom(), term()) -> no_return().
%%@doc Sends an asynchronous request to the gen_server and returns ok imidiately.
send_event(Name, Event) when is_pid(Name)->
    catch gen_server:cast(Name, {send_event,Event}),
    ok;

send_event(Name, Event) ->
    Pid = where(Name),
    et:phone_home(30, Name, Pid, send_event, Event),
    send_event(Pid, Event).

-spec sync_send_event(pid() | atom(), term()) -> atom().
%%@doc Sends a synchronous request to gen_server and waits unil reply arrives
sync_send_event(Name, Event)  when is_pid(Name) ->
    call(Name, sync_send_event, Event);

sync_send_event(Name, Event) ->
    Pid = where(Name),
    sync_send_event(Pid, Event).

-spec sync_send_event(pid() | atom(), term(),reference()) -> atom().
%%@doc Sends a synchronous request to gen_server and waits unil reply arrives ot  timeout occurs
sync_send_event(Name, Event, Timeout) ->
    call(Name, sync_send_event, Event, Timeout).

-spec send_all_state_event(pid() | atom(), term()) -> no_return().
%%@doc Sends an asynchronous request to gen_server
send_all_state_event(Name, Event) when is_pid(Name) ->
    catch gen_server:cast(Name, {send_all_state_event, Event}),
    ok;

send_all_state_event(Name, Event) ->
    Pid = where(Name),
    send_all_state_event(Pid, Event).

-spec sync_send_all_state_event(pid() | atom(), term()) -> atom().
%%@doc Sends a synchronous request to gen_server and waits unil reply arrives
sync_send_all_state_event(Name, Event) ->
    call(Name, sync_send_all_state_event, Event).

-spec sync_send_all_state_event(pid() | atom(), term(), integer()|infinity) -> atom().
%%@doc Sends a synchronous request to gen_server and waits unil reply arrives or timeout occurs
sync_send_all_state_event(Name, Event, Timeout) ->
    call(Name, sync_send_all_state_event, Event, Timeout).

-spec get_property(pid() | atom(), term()) -> any().
%%@doc Returns property value via synchronous call to gen_server
get_property(Name, PName) ->
    call(Name, get_property, PName).

-spec get_property(pid() | atom(), term(), integer()|infinity) -> any().
%%@doc Returns a property value via synchronous call to gen_server. Wait until a reply
%%arrives or timeout occurs
%%@end
get_property(Name, PName, Timeout) ->
    call(Name, get_property, PName, Timeout).

-spec set_property(pid() | atom(), term(), term()) -> 'ok'.
%%@doc Set a value to a property via asynchronous call to gen_server, returns ok imidiately
set_property(Name, PName, Val) ->
    set_properties(Name, [{PName, Val}]).

set_properties(Pid, Proplist) ->
    call(Pid, set_properties, Proplist).

-spec set_output(pid() | atom(), NewValue :: float()) ->
    {accepted, AcceptedValue :: float()}
    | {rejected, Reason :: term()}.
%%@doc Sets a new setpoint value via a synchronous call to gen_server.
%% Waits until reply is received or timeout occurs
%% @end
set_output(Name, Val) ->
    call(Name, set_output, Val).

set_output(Name, Val, Timeout) ->
    call(Name, set_output, Val,Timeout).

get_status(Pid) ->
    call(Pid, get_status).

get_config(Pid) ->
    call(Pid, get_config).

set_config(Pid, Config) ->
    call(Pid, set_config, Config).

engage(Pid) ->
    set_property(Pid, "engaged", ?ENGAGED).

disengage(Pid) ->
    set_property(Pid, "engaged", ?DISENGAGED).

childspec(RoomId, #crah_driver{id = Id, resource = Res} = DriverSpec) ->
    {
     driver_lib:proc_key(Id, Res, driver),
     {?MODULE, start_link, [RoomId, DriverSpec]},
     transient,
     1000,
     worker,
     [?MODULE]
    };
childspec(RoomId, #crac_driver{id = Id, resource = Res} = DriverSpec) ->
    {
     driver_lib:proc_key(Id, Res, driver),
     {?MODULE, start_link, [RoomId, DriverSpec]},
     transient,
     1000,
     worker,
     [?MODULE]
    };
childspec(RoomId, #vfd_driver{id = Id, resource = Res} = DriverSpec) ->
    {
     driver_lib:proc_key(Id, Res, driver),
     {?MODULE, start_link, [RoomId, DriverSpec]},
     transient,
     1000,
     worker,
     [?MODULE]
    }.

%%%-----------------------------------------------------------------------------------------------------
%%Timers

-spec start_timer(integer(), 'pull' | 'push') -> reference().
%%@doc Starts a timer
start_timer(Time, Msg) ->
    erlang:start_timer(Time, self(), {timer,Msg}).

-spec send_event_after(integer(), term()) -> reference().
%%@doc Sends a delayed message to the process itself
send_event_after(Time,Event) ->
    erlang:start_timer(Time, self(), {send_event_after,Event}).

-spec cancel_timer(reference()) -> integer() | false.
%%@doc Cancel a timer
cancel_timer(Ref) when is_reference(Ref) ->
    case erlang:cancel_timer(Ref) of
	false ->
	    receive {timeout, Ref, _} -> 0
	    after 0 -> false
	    end;
	RemainingTime ->
	    RemainingTime
    end.
call(Name, Req) when is_pid(Name) ->
    case catch gen_server:call(Name, Req) of
	{'EXIT',Reason} ->
	    exit({Reason, {?MODULE, Req, [Name, Req]}});
	Res ->
	    Res
    end;
call(Name, Req) ->
    call(where(Name), Req).

call(Name, ReqType, Req) when is_pid(Name) ->
    case catch gen_server:call(Name, {ReqType, Req}) of
	{'EXIT',Reason} ->
	    exit({Reason, {?MODULE, ReqType, [Name, Req]}});
	Res ->
	    Res
    end;
call(Name, ReqType, Req) ->
    call(where(Name), ReqType, Req).

call(Name, ReqType, Req,Timeout) when is_pid(Name) ->
    case catch gen_server:call(Name, {ReqType, Req}, Timeout) of
	{'EXIT',Reason} ->
	    exit({Reason, {?MODULE, ReqType, [Name, Req, Timeout]}});
	Res ->
	    Res
    end;
call(Name, ReqType, Req,Timeout) ->
    call(where(Name), ReqType, Req, Timeout).
%%====================================================================
%% gen_server callbacks
%%====================================================================

%%--------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Initiates the server
%%--------------------------------------------------------------------
init([RoomId, Id, Resource, ClassMod, ProtocolMod, Properties, ConfigRec, Args]) ->
    Key = driver_lib:register_driver(Id, Resource),
    driver_lib:register_callback(ClassMod),
    InitResult = case process_cache:load_properties(Key) of
		      {error, no_data}->
			 catch ClassMod:init(Args,Properties,ProtocolMod);
		     {ok, OldProperties} ->
			 catch ClassMod:reinit(Args,OldProperties,ProtocolMod)
		 end,
    case InitResult of
	{ok, StateName, TransientData, NewProperties} ->
	    process_flag(trap_exit, true),
	    process_cache:update_properties(Key, NewProperties),
	    State = #?STATE{
			room_id = RoomId,
			id = Id,
			key = Key,
			config_record = ConfigRec,
			state_name = StateName,
			t_data = TransientData,
			properties = NewProperties,
			device_class_mod = ClassMod
		       },
	    {ok, State};
	{ok, StateName, TransientData, NewProperties, Timeout} ->
	    process_flag(trap_exit, true),
	    process_cache:update_properties(Key, NewProperties),
	    State = #?STATE{
			room_id = RoomId,
			id = Id,
			key = Key,
			config_record = ConfigRec,
			state_name = StateName,
			t_data = TransientData,
			properties = NewProperties,
			device_class_mod = ClassMod
		       },
	    {ok, State, Timeout};
	{stop, Reason} ->
	    {stop, Reason};
	ignore ->
	    ignore;
	{'EXIT', Reason} ->
	    {stop,Reason};
	Else ->
	    {stop,{bad_return_value,Else}}
    end.

%%--------------------------------------------------------------------
%% Function: %% handle_call(Request, From, State) -> {reply, Reply, State} |
%%                                      {reply, Reply, State, Timeout} |
%%                                      {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, Reply, State} |
%%                                      {stop, Reason, State}
%% Description: Handling call messages
%%--------------------------------------------------------------------
handle_call(drv_state,_From,State) ->
    {reply, fmt_state(State), State};
handle_call({set_config, NewConfig},
            From,
            State = #?STATE{
                       state_name = StateName,
                       device_class_mod = Mod,
                       t_data = TData,
                       properties = Properties}) ->
    handle_return(
      i_set_properties(proplists:delete("engaged",
                                        driver_properties(NewConfig)),
                       Mod,
                       StateName,
                       TData,
                       Properties),
      From,
      State#?STATE{config_record = NewConfig});
handle_call(Msg, From, State)->
	handle_return(dispatch(Msg, State), From, State).

handle_return(Return, From, State = #?STATE{key = Key}) ->
    case Return of
        {next_state, StateName, TData, Properties} ->
            NState = calculate_new_state(State, {StateName, TData, Properties}),
            {reply, ok, NState};
        {next_state, StateName, TData, Properties,Timeout} ->
            NState = calculate_new_state(State, {StateName, TData, Properties}),
            {reply, ok, NState, Timeout};
        {accept, NewValue, StateName, TData, Properties} ->
            NState = calculate_new_state(State, {StateName, TData, Properties}),
            room_event:notify_device_setpoint_change(
              State#?STATE.room_id,
              Key,
              NewValue),
            {reply, {accepted,NewValue}, NState};
        {reject, Reason, StateName, TData, Properties} ->
            NState = calculate_new_state(State, {StateName, TData, Properties}),
            {reply, {rejected,Reason}, NState};
        {reply, Reply, StateName, TData, Properties} when From =/= undefined ->
            NState = calculate_new_state(State, {StateName, TData, Properties}),
            {reply, Reply, NState};
        {reply, Reply, StateName, TData, Properties, Timeout} when From =/= undefined ->
            NState = calculate_new_state(State, {StateName, TData, Properties}),
            {reply,Reply,NState, Timeout};
        {stop, Reason, _NTData, NProperties} ->
            process_cache:update_properties(Key, NProperties),
            {stop,Reason,State};
        {stop, Reason, Reply, _NTData, NProperties} ->
            process_cache:update_properties(Key, NProperties),
            {stop,Reason,Reply,State};
        {'EXIT', What} ->
            {stop,What,State};
        Reply ->
            {stop,{bad_return_value, Reply},State}
    end.

%%--------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%--------------------------------------------------------------------
handle_cast(Msg, State = #?STATE{key = Key}) ->
    case dispatch(Msg, State) of
	{next_state, StateName, TData, Properties} ->
	    NState = calculate_new_state(State, {StateName, TData, Properties}),
	    {noreply, NState};
	{next_state, StateName, TData, Properties, Timeout} ->
	    NState = calculate_new_state(State, {StateName, TData, Properties}),
	    {noreply,NState,Timeout};
	{stop, Reason, _NTData, NProperties} ->
	    process_cache:update_properties(Key, NProperties),
	    {stop,Reason,State};
	{noreply, NState} ->
	    {noreply, NState};
	{'EXIT', What} ->
	    {stop,What,State};
	Reply ->
	    {stop,{bad_return_value, Reply},State}
    end.

% add sync send event
-spec dispatch(term(), #?STATE{}) -> {next_state, _, _, [tuple()]} |
				   {next_state, _, _, [tuple()], reference()} |
				   {accept, _,_,_,[tuple()]} |
				   {reject, _,_,_,[tuple()]} |
				   {reply, _, _, _,[tuple()]} |
				   {noreply, _} |
				   {reply, _,_,_,[tuple()], timeout()} |
				   {stop, _, _, [tuple()]} |
				   {stop, _,_,_,[tuple()]} |
				   {'EXIT', term()}
				   .

dispatch({send_event,Event}, #?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData, properties = Properties}) ->
    Mod:StateName(Event, TData,Properties);
dispatch({sync_send_event,Event}, #?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData, properties = Properties}) ->
    Mod:StateName(Event, TData,Properties);
dispatch({send_all_state_event,Event}, #?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData,properties = Properties}) ->
    Mod:handle_event(Event, StateName, TData,Properties);
dispatch({sync_send_all_state_event,Event}, #?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData,properties = Properties}) ->
    Mod:handle_sync_event(Event, StateName, TData,Properties);
%% This is from handle_info
dispatch({timeout, _Ref, {timer, Event}},#?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData,properties = Properties}) ->
    Mod:StateName(Event, TData,Properties);
dispatch({timeout, _Ref, {send_event_after, Event}},#?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData,properties = Properties}) ->
    Mod:StateName(Event, TData,Properties);
dispatch({get_property, PName}, #?STATE{state_name = StateName, t_data = TData,properties = Properties}) ->
    {reply, shadoww_lib:getprop(PName,Properties), StateName, TData, Properties};
dispatch(get_status, #?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData, properties = Properties}) ->
  {reply, Mod:get_status(StateName, TData, Properties), StateName, TData, Properties};
dispatch(get_config, #?STATE{state_name = StateName, config_record = ConfigRec, t_data = TData, properties = Properties}) ->
  {reply, ConfigRec, StateName, TData, Properties};
dispatch({set_properties, Proplist},
		 #?STATE{state_name = StateName,
				 device_class_mod = Mod,
				 t_data = TData,
				 properties = Properties}) ->
	%TODO filter out unknown properties before passing to i_set_properties?
	i_set_properties(Proplist, Mod, StateName, TData, Properties);
dispatch({set_output,Event}, #?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData, properties = Properties}) ->
    Mod:new_output(Event, StateName, TData, Properties);
dispatch(Info,#?STATE{state_name = StateName, device_class_mod = Mod, t_data = TData,properties = Properties} ) ->
    Mod:handle_info(Info, StateName, TData, Properties).


handle_info(driver_stop, State = #?STATE{key = Key}) ->
	driver_lib:unreg(Key),
	process_cache:delete_properties(Key),
	{stop, normal, State}
	;
%%--------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%--------------------------------------------------------------------
handle_info(Msg, State = #?STATE{key = Key}) ->
    case dispatch(Msg, State) of
	{next_state, StateName, TData, Properties} ->
	    NState = calculate_new_state(State, {StateName, TData, Properties}),
	    {noreply, NState};
	{next_state, StateName, TData, Properties, Timeout} ->
	    NState = calculate_new_state(State, {StateName, TData, Properties}),
	    {noreply,NState,Timeout};
	{stop, Reason, _NTData, NProperties} ->
	    process_cache:update_properties(Key, NProperties),
	    {stop,Reason,State};
	{'EXIT', What} ->
	    {stop,What,State};
	Reply ->
	    {stop,{bad_return_value, Reply},State}
    end.



%%--------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%--------------------------------------------------------------------
terminate(Reason, #?STATE{
			      state_name = StateName,
			      device_class_mod = Mod,
			      t_data = TData,
			      properties = Properties}) ->
    catch Mod:terminate(Reason, StateName, TData, Properties)
    .

%%--------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%--------------------------------------------------------------------
%%% Internal functions
%%--------------------------------------------------------------------
-spec calculate_new_state(#?STATE{}, tuple()) -> #?STATE{}.

calculate_new_state(State = #?STATE{room_id = RoomId,
				    key = Key,
				    state_name = OldStateName,
				    device_class_mod = Mod},
		    {StateName,
		    TData,
		    Properties}) ->
    {NewStateName, NewTData,NewProperties} = state_change(Mod, RoomId, Key, OldStateName, StateName, TData, Properties),
    process_cache:update_properties(Key, NewProperties),
    State#?STATE{
	     state_name = NewStateName,
	     t_data = NewTData,
	     properties = NewProperties
	    }.

-spec state_change(module(), binary(), term(), term(), term(), term(),[tuple()]) -> {term(), term(), [tuple()]}.

state_change(_Mod, _RoomId, _Key, OldStateName, StateName, TData, Properties) when OldStateName =:= StateName ->
    {StateName, TData,Properties};
state_change(Mod, RoomId, Key, OldStateName, StateName, TData, Properties) ->
    Id = driver_lib:id(Key),
    et:phone_home(60, Id, state_change, {OldStateName, StateName}),
    room_event:notify_device_status_change(RoomId, Key, drv_handler:device_state_to_status(StateName)),
    {next_state, NStateName, NTData,NProperties} =
	Mod:state_change(OldStateName, StateName, TData,Properties),
    state_change(Mod, RoomId, Key, StateName, NStateName, NTData,NProperties).

-spec check_properties(module(),module(), [tuple()]) -> [tuple()] | {error, _}.

check_properties(ClassMod, ProtocolMod, Properties) ->
	{DeclaredProperties, _Flags} = lists:unzip(ClassMod:properties(ProtocolMod)),
	Filtered = shadoww_lib:keyfilter(DeclaredProperties, 1, Properties),
    if length(Filtered) < length(DeclaredProperties) ->
		error({property_mismatch, {expected, DeclaredProperties}, {got, Properties}});
		true -> Filtered
	end.

i_set_properties([], _Mod, StateName, TData, Properties) ->
    {next_state, StateName, TData, Properties};
i_set_properties([{PName, Val} | Ps], Mod, StateName, TData, Properties) ->
	case {lists:keymember(PName, 1, Properties), proplists:get_value(PName, Mod:properties(TData))} of
		{true, r} ->
			{OldVal,NewProperties} = shadoww_lib:setprop({PName,Val},Properties),
			{next_state, NStateName, NTData, NNProperties} =
				Mod:property_changed(PName, OldVal, Val, StateName, TData, NewProperties),
			i_set_properties(Ps, Mod, NStateName, NTData, NNProperties);
		{true, _} ->
			i_set_properties(Ps, Mod, StateName, TData, Properties);
		{false, _} ->
			{stop, {bad_property, PName}, {bad_property, PName}, TData, Properties}
	end.

-spec fmt_state(#?STATE{}) -> [tuple()].

fmt_state(#?STATE{id = Id, state_name = StateName, t_data = TData, properties = Properties, device_class_mod = Mod}) ->
    [
		{"id", Id},
                {"state name", StateName},
                {"transient_data", TData},
                {"properties", Properties},
                {"device_class_module", Mod}
    ].


