%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(raised_floor_room).

-behaviour(room_manager).

-export([
         start/1,
         list_rooms/0,
         requires_restart/1,
         update_config/1,
         room_status/1,
         list_crahs/1,
         crah_status/2,
         stop/1,

         handle_call/2,
         handle_async_call/2,
         room_childspec/1,
         crah_ids/1
        ]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

start(#raised_floor_room{id = Id} = Model) ->
    room_manager:start_room(?MODULE, Id, Model).

list_rooms() ->
    room_manager:rooms_of_type(?MODULE).

% @doc
% Check whether or not the room structure changed (and should therefore be restarted)
% IMPORTANT:
% If this function returns 'false', update_room(Id, NewModel) will be called next.
% So, anything not updated by update_room must be considered a "structural"
% change requiring the room to be restarted from scratch.
% @end
requires_restart(#raised_floor_room{id=Id} = NewModel) ->
    room_manager:async_call(Id, {requires_restart, NewModel}).

update_config(#raised_floor_room{id=Id} = M) ->
    room_manager:call(Id, {update_config, M}).

-spec room_status(Id :: id()) -> {ok, raised_floor_room_status()}.
room_status(Id) ->
    room_manager:call(Id, status).

-spec list_crahs(RoomId :: id()) -> [id()].
list_crahs(RoomId) ->
    room_manager:async_call(RoomId, list_crahs).

-spec crah_status(RoomId :: id(), CrahId :: id()) ->
    {ok, raised_floor_crah_status()}
    | {error, no_such_id}.
crah_status(RoomId, CrahId) ->
    room_manager:call(RoomId, {crah_status, RoomId, CrahId}).

stop(Id) ->
    room_manager:stop_room(Id).

handle_call({update_config,
             #raised_floor_room{
                id = RoomId,
                bounds = Bounds,
                contained_rooms = ContainedRooms,
                heartbeat_manager = HbManager,
                temperature_inputs = TemperatureInputs,
                pressure_inputs = PressureInputs,
                temperature_balancer = TB,
                crahs = Crahs
               } = NewState},
             _OldState) ->
    % need to refresh:
    %   room boundaries & included rooms
    %   crah properties
    %   heartbeat properties
    % ---- These do not need a refresh.  can't be changed through ES without
    % going through API, or is considered a "structural" change (see above)
    % not an "update" ----
    % input properties
    % temp/pressure balancer properties
    % adding / removing a heartbeat manager
    HbManager =/= none andalso heartbeat_manager:set_config(RoomId, HbManager),
    rf_geom:update_geometry(RoomId,
                            [{crah_id(C), crah_location(C)} || C <- Crahs],
                            [{Id, L} || #single_input{id=Id, location=L} <- TemperatureInputs],
                            [{Id, L} || #single_input{id=Id, location=L} <- PressureInputs],
                            [Bounds | ContainedRooms]),
    [update_crah(C) || C <- Crahs],
    rf_aoe_manager:schedule_recalc(RoomId, pressure),
    TB =/= none andalso rf_aoe_manager:schedule_recalc(RoomId, temperature),
    {reply, ok, NewState};
handle_call(status, State) ->
    {reply, {ok, get_room_status(State)}, State};
handle_call({crah_status, RoomId, CrahId}, State = #raised_floor_room{crahs=Crahs}) ->
    case [Crah || Crah <- Crahs, crah_id(Crah) == CrahId] of
        [] -> {reply, {error, no_such_id}, State};
        [Crah] -> {reply, {ok, get_crah_status(RoomId, Crah)}, State}
    end;
handle_call(_Message, State) ->
    {reply, error, State}.

handle_async_call({requires_restart,
                   #raised_floor_room{
                      crahs = NewCrahs,
                      pressure_inputs = NPI,
                      temperature_inputs = NTI,
                      heartbeat_manager = NewHbManager
                     }},
                  #raised_floor_room{
                     crahs = OldCrahs,
                     pressure_inputs = OPI,
                     temperature_inputs = OTI,
                     heartbeat_manager = OldHbManager
                    }) ->
    CrahIds = gb_sets:from_list([crah_id(Crah) || Crah <- OldCrahs]),
    NewCrahIds = gb_sets:from_list([crah_id(Crah) || Crah <- NewCrahs]),
    InputIds = gb_sets:from_list([input_id(I) || I <- OPI ++ OTI]),
    NewInputIds = gb_sets:from_list([input_id(I) || I <- NPI ++ NTI]),
    HbChanged = heartbeat_manager_changed(OldHbManager, NewHbManager),
    Reply = not (sets_equal(CrahIds, NewCrahIds) andalso
         sets_equal(InputIds, NewInputIds) andalso
         (not HbChanged)
        ),
    {reply, Reply};
handle_async_call(list_crahs, State) ->
    {reply, crah_ids(State)};
handle_async_call(_Message, _State) ->
    {reply, error}.


update_crah(#raised_floor_crah{id = Id,
                                pressure_driver = PD,
                                temperature_driver = TD}) ->
    base_driver:set_config(driver_lib:driver_key(Id, pressure),
                           PD),
    TD =/= none andalso
        base_driver:set_config(
          driver_lib:driver_key(Id, temperature),
          TD).

room_childspec(#raised_floor_room{
                  id = RoomId,
                  bounds = Bounds,
                  contained_rooms = ContainedRooms,
                  heartbeat_manager = HeartbeatManager,
                  temperature_inputs = TemperatureInputs,
                  pressure_inputs = PressureInputs,
                  temperature_balancer = TempBalancer,
                  crahs = Crahs
                 }=Room) ->
    CrahIds = [crah_id(C) || C <- Crahs],

    % Must include childspecs in specific order
    %  rf_geom
    %  room_event()
    %  rf_crah
    %  rf_input(pressure)
    %  rf_input(temperature)
    %  rf_balancer_strategy(pressure)
    %  rf_balancer_strategy(temperature)
    %  rf_aoe_manager(pressure)
    %  rf_aoe_manager(temperature)

    Resources = case TempBalancer of
                    none -> [pressure];
                    _ -> [pressure, temperature]
                end,

    Childspecs = [geom_childspec(RoomId, Crahs, Bounds, ContainedRooms, TemperatureInputs, PressureInputs)]
        ++ room_event:childspec(RoomId)
        ++ [rf_crah_childspec(RoomId, C) || C <- Crahs]
        ++ [rf_input_childspec(Room, Resource) || Resource <- Resources]
        ++ [rf_balancer_strategy_childspec(Room, CrahIds, Resource) || Resource <- Resources]
        ++ [rf_aoe_manager:childspec(RoomId, Resource, low, CrahIds) || Resource <- Resources],

    HbChildspecs = case HeartbeatManager of
                       none -> Childspecs;
                       HeartbeatManager ->
                           HeartbeatSpec = heartbeat_manager:childspec(RoomId, HeartbeatManager),
                           [HeartbeatSpec | Childspecs]
                   end,
    {
        RoomId,
        {dyn_sup, start_link, [RoomId, {rest_for_one, 1, 3600}, HbChildspecs]},
        temporary,
        infinity,
        supervisor,
        [dyn_sup]
    }.

geom_childspec(RoomId, Crahs, Bounds, ContainedRooms, TemperatureInputs, PressureInputs) ->
    CrahLocations = [{crah_id(C), crah_location(C)} || C <- Crahs],
    RackLocations = [{Id, L} || #single_input{id=Id, location=L} <- TemperatureInputs],
    PressureLocations = [{Id, L} || #single_input{id=Id, location=L} <- PressureInputs],
    rf_geom:childspec(RoomId,
        CrahLocations,
        RackLocations,
        PressureLocations,
        [Bounds | ContainedRooms]).

rf_input_childspec(#raised_floor_room{id = RoomId, temperature_inputs = TI, temperature_balancer = TB}, temperature = R) ->
    TA = TB#raised_floor_balancer_strategy.aggressiveness,
    rf_input:childspec(RoomId, R, TA, TI);
rf_input_childspec(#raised_floor_room{id = RoomId, pressure_inputs = PI, pressure_balancer = PB}, pressure = R) ->
    PA = PB#raised_floor_balancer_strategy.aggressiveness,
    rf_input:childspec(RoomId, R, PA, PI).

rf_balancer_strategy_childspec(#raised_floor_room{temperature_balancer = TB}, CrahIds, temperature) ->
    rf_balancer_strategy:childspec(TB, CrahIds);
rf_balancer_strategy_childspec(#raised_floor_room{pressure_balancer = PB}, CrahIds, pressure) ->
    rf_balancer_strategy:childspec(PB, CrahIds).

crah_ids(#raised_floor_room{crahs = Crahs}) ->
    [Id || #raised_floor_crah{id=Id} <- Crahs].

rf_crah_childspec(RoomId, #raised_floor_crah{
                     id = Id,
                     temperature_driver = TmpDrv,
                     temperature_strategy = TmpStrat,
                     pressure_driver = PressureDrv,
                     pressure_strategy = PressureStrat
                    }) ->
    % add some assertions that temp driver is remote control?
    ChildSpecs = crah_childspecs(RoomId, PressureDrv, PressureStrat)
        ++ crah_childspecs(RoomId, TmpDrv, TmpStrat),
    {
     Id,
     {dyn_sup, start_link, [Id, {rest_for_one, 1, 3600}, ChildSpecs]},
     permanent,
     2000,
     supervisor,
     [dyn_sup]
    }.

crah_childspecs(_RoomId, none, none) ->
    [];
crah_childspecs(RoomId, Drv, Strat) ->
    [
        base_driver:childspec(RoomId, Drv),
        strategy_childspec(RoomId, Strat)
    ].

strategy_childspec(RoomId, S = #raised_floor_crah_strategy{id=Id, resource=Res}) ->
    strategy_childspec(RoomId, Res, Id, S, rf_crah_strategy);
strategy_childspec(RoomId, S = #raised_floor_vfd_strategy{id=Id, resource=Res}) ->
    strategy_childspec(RoomId, Res, Id, S, rf_vfd_strategy).

strategy_childspec(RoomId, Res, StrategyId, Strategy, Module) ->
    {
        driver_lib:proc_key(StrategyId, Res, strategy),
        {Module, start_link, [RoomId, Strategy]},
        transient,
        1000,
        worker,
        [Module]
    }.

heartbeat_manager_changed(none, none) ->
    false;
heartbeat_manager_changed(OldHb, NewHb) when OldHb =:= none; NewHb =:= none ->
    true;
heartbeat_manager_changed(_OldHb, _NewHb) ->
    false.

crah_id(#raised_floor_crah{id=Id}) -> Id.

input_id(#single_input{id=Id}) -> Id.

crah_location(#raised_floor_crah{location=Loc}) -> Loc.

sets_equal(Set1, Set2) ->
    Set1Sz = gb_sets:size(Set1),
    Set2Sz = gb_sets:size(Set2),
    if
        Set1Sz =:= Set2Sz ->
            Intersection = gb_sets:intersection(Set1, Set2),
            Set1Sz =:= gb_sets:size(Intersection)
            ;
        Set1Sz /= Set2Sz ->
            false
    end.

get_room_status(#raised_floor_room{
                   id=RoomId,
                   crahs=Crahs,
                   temperature_balancer=TmpBal})
  when TmpBal =:= none->
    CrahsStatus = [get_crah_status(RoomId, Crah) || Crah <- Crahs],
    #raised_floor_room_status{
                              id = RoomId,
                              pressure_balancer_status = balancer_status(RoomId, pressure, CrahsStatus),
                              crahs_status = CrahsStatus
                             };
get_room_status(#raised_floor_room{id=RoomId, crahs=Crahs}) ->
    CrahsStatus = [get_crah_status(RoomId, Crah) || Crah <- Crahs],
    #raised_floor_room_status{
                              id = RoomId,
                              temperature_balancer_status = balancer_status(RoomId, temperature, CrahsStatus),
                              pressure_balancer_status = balancer_status(RoomId, pressure, CrahsStatus),
                              crahs_status = CrahsStatus
                             }.

-spec get_crah_status(id(), raised_floor_crah()) -> raised_floor_crah_status().
get_crah_status(RoomId, #raised_floor_crah{id=CrahId,temperature_driver=TmpDrv,temperature_strategy=TmpStrat}) when TmpDrv =:= none, TmpStrat =:= none ->
    PSStatus = strategy_status(RoomId, CrahId, pressure),
    #raised_floor_crah_status{
                              id = CrahId,
                              pressure_driver_status = ac_api:status(CrahId, pressure, driver),
                              pressure_strategy_status = PSStatus,
                              mode = ac_api:control_status_to_mode(PSStatus#raised_floor_strategy_status.status)
                             };
get_crah_status(RoomId, #raised_floor_crah{id=CrahId}) ->
    PSStatus = strategy_status(RoomId, CrahId, pressure),
    #raised_floor_crah_status{
                              id = CrahId,
                              temperature_driver_status = ac_api:status(CrahId, temperature, driver),
                              temperature_strategy_status = strategy_status(RoomId, CrahId, temperature),
                              pressure_driver_status = ac_api:status(CrahId, pressure, driver),
                              pressure_strategy_status = PSStatus,
                              mode = ac_api:control_status_to_mode(PSStatus#raised_floor_strategy_status.status)
                             }.

-spec strategy_status(id(), id(), controlled_resource()) -> raised_floor_strategy_status().
strategy_status(RoomId, CrahId, Resource) ->
    K = driver_lib:proc_key(CrahId, Resource, strategy),
    Info = driver_lib:call(K, get_status_info, []),

    AoeStatus = rf_input:get_aoe_status(RoomId, Resource, CrahId),

    #raised_floor_strategy_status{
                                  id = CrahId,
                                  resource = Resource,
                                  location = rf_geom:crah_location(RoomId, CrahId),
                                  status = proplists:get_value(status, Info),
                                  failsafe = null_or_float(proplists:get_value(failsafe, Info)),
                                  stage2 = null_or_float(proplists:get_value(stage2, Info)),
                                  user_setpoint = proplists:get_value(user_setpoint, Info),
                                  last_adjustment = proplists:get_value(last_adjustment, Info),
                                  aoe_target = proplists:get_value(aoe_target, AoeStatus),
                                  aoe_actual = proplists:get_value(aoe_actual, AoeStatus),
                                  inputs_status = proplists:get_value(inputs_status, AoeStatus)
                                 }.

-spec balancer_status(id(), controlled_resource(), [raised_floor_crah_status()]) -> raised_floor_balancer_strategy_status().
balancer_status(RoomId, Resource, CrahsStatus) ->
    AllStatus = [device_capacity_status(Resource, CrahStatus) || CrahStatus <- CrahsStatus],
    SortedStatus = lists:sort([{Cap, Id} || {Cap, Id, Status} <- AllStatus, Status =:= online, is_float(Cap)]),
    Statuses = case SortedStatus of
        [] -> [null];
        S -> S
    end,
    #raised_floor_balancer_strategy_status{
                                           id = RoomId,
                                           resource = Resource,
                                           aggressiveness = rf_balancer_strategy:get_aggressiveness(RoomId, Resource),
                                           highest_capacity = lists:last(Statuses),
                                           lowest_capacity = hd(Statuses),
                                           inputs_status = rf_input:get_all_input_status(RoomId, Resource)
                                          }.

-spec device_capacity_status(controlled_resource(), raised_floor_crah_status()) -> { float() | null, id(), device_status()}.
device_capacity_status(temperature,
                       #raised_floor_crah_status{
                                                 id = Id,
                                                 temperature_driver_status = #crah_status{
                                                 capacity = Capacity,
                                                 status = Status
                      }}) ->
    {Capacity, Id, Status};
device_capacity_status(pressure,
                       #raised_floor_crah_status{
                                                 id = Id,
                                                 pressure_driver_status = #vfd_status{
                                                     setpoint = Setpoint,
                                                     status = Status
                                                 }
                      }) ->
    {Setpoint, Id, Status}.

null_or_float(F) when is_float(F) -> F;
null_or_float(_) -> null.

