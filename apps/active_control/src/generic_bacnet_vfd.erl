-module(generic_bacnet_vfd).
-behaviour(vfd_fsm).

-export([properties/0,read_setpoint/1,read_comm/1,read_override/1,read_kw/1,write_setpoint/2]).
-export([read_over_current/1, read_over_voltage/1, read_under_voltage/1, read_over_temperature/1,
	 read_general/1, write_clear_alarm/1]).
-import(shadoww_lib, [getprop/2]).
-import(io_handler, [addr_b/1,get_net/1,get_ip/1,get_devid/1]).

properties() ->
	% TODO: flag these with r,w,rw to prevent property cycles and race conditions.
	[
		{"ip", r},
		{"devid", r},
		{"network", r},
		{"setpointRead", r},
		{"setpointWrite", r},
		{"powerRead", r},
		{"localOverrideRead", r},
		{"commRead", r}		
	] 
	++
	drv_handler:vfd_alarm_properties()
	.

read_comm(Props) ->
	io_handler:read_comm("vfd.commread", get_net(Props),get_ip(Props), get_devid(Props), getprop("commRead", Props))
	.

read_override(Props) ->
	Override = case getprop("localOverrideRead", Props) of
			   [] -> false;
			   OverrideReadStr -> io_eval:eval("vfd.localoverrideread", OverrideReadStr, addr_b(Props) ++ [{'Value', none}])
		   end,
	case Override of
		_ when is_boolean(Override) -> Override;
		1 -> true;
		0 -> false
	end
	.

read_kw(Props) ->
	io_handler:read_property("vfd.powerread", get_net(Props), get_ip(Props), get_devid(Props), getprop("powerRead", Props))
	.

read_setpoint(Props) ->
	io_handler:read_property("vfd.setpointread", get_net(Props), get_ip(Props), get_devid(Props), getprop("setpointRead", Props))
	.
    
write_setpoint(Props,Setpoint) ->
	io_eval:eval("vfd.setpointwrite", getprop("setpointWrite", Props), addr_b(Props) ++ [{'Value', Setpoint}]),
	Setpoint
	.

%Alerts I/O
read_over_current(Props) ->
	io_handler:read_alarm("vfd.overcurrentalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("overCurrentAlarmRead", Props))
	.
read_over_voltage(Props) ->
	io_handler:read_alarm("vfd.overvoltagealarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("overVoltageAlarmRead", Props))
	.
read_under_voltage(Props) ->
	io_handler:read_alarm("vfd.undervoltagealarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("underVoltageAlarmRead", Props))
	.
read_over_temperature(Props) ->
	io_handler:read_alarm("vfd.overtemperaturealarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("overTemperatureAlarmRead", Props))
	.
read_general(Props) ->
	io_handler:read_alarm("vfd.generalalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("generalAlarmRead", Props))
	.

write_clear_alarm(Props) ->
	case  getprop("clearAlarmWrite", Props) of
		[] -> ok;
		WriteClear -> io_eval:eval("vfd.clearalarmwrite", WriteClear, addr_b(Props) ++ [{'Value', 0}])
   	end
	.

