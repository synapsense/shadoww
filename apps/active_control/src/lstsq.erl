
-module(lstsq).

-export([ssr/2]).

-spec ssr([X :: number()], [Y :: number()]) -> Ssr :: float() .
%% @doc
%% Calculate the sum of squared residual errors from a least squares fit
%% The actual regression line is not calculated (because it isn't needed)
%% Equations derived from those on Wolfram Alpha:
%% http://mathworld.wolfram.com/CorrelationCoefficient.html
%% @end
ssr(Xs0, Ys) ->
	% shift each point to the left so that 0.0 is the first X coord
	% doesn't change the results, and means we're working with much
	% smaller numbers (timestamps tend to be large...)
	Xs = [X - lmin(Xs0) || X <- Xs0],

	Xsum = sum(Xs),
	X2sum = sum([X*X || X <- Xs]),

	Ysum = sum(Ys),
	Y2sum = sum([Y*Y || Y <- Ys]),

	XYprod = sum([X*Y || {X,Y} <- lists:zip(Xs, Ys)]),

	N = length(Xs),

	SSxx = X2sum - (Xsum * Xsum) / N,
	SSyy = Y2sum - (Ysum * Ysum) / N,

	SSxy = XYprod - (Xsum * Ysum) / N,

	SSR = case SSxx of
		0.0 -> SSyy;
		_ -> SSyy - SSxy * SSxy / SSxx

	end,

	SSR
	.

sum([X]) ->
	X;
sum([X | Xs]) ->
	X + sum(Xs).

lmin([X]) ->
	X;
lmin([X | Xs]) ->
	min(X, lmin(Xs)).



