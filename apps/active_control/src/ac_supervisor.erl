
-module(ac_supervisor).

-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(SERVER, ?MODULE).


start_link() ->
	lager:info("AC Supervisor starting"),
	Return = supervisor:start_link({local, ?SERVER}, ?MODULE, []),
	lager:info("AC Supervisor started: ~p", [Return]),
	Return
	.

init(_Args) ->
	RestartStrategy		= rest_for_one,	% each process that dies, resart it and all after it
	MaxRestarts		= 0,		% number of restarts allowed in
	MaxTimeBetRestarts	= 3600,		% this number of seconds

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpecs = [{
		control_alarm,
		{control_alarm, start_link, []},
		permanent,
		infinity,
		worker,
		dynamic
	}, {
		input_supervisor,		% name of the process
		{input_supervisor, start_link, []}, % module, function, args
		permanent,			% death of the process is never expected, so always restart
		infinity,			% how many ms the process is given to stop gracefully before being killed
		supervisor,			% this process is supervising other processes
		[input_supervisor]		% module dependencies
	}, {
		room_supervisor,
		{room_supervisor, start_link, []},
		permanent,
		infinity,
		supervisor,
		[room_supervisor]
	}, {
		room_manager,
		{room_manager, start_link, []},
		permanent,
		1000,
		worker,
		[room_manager]
	}, {
		ac_api_json_erl,
		{ac_api_json_erl, start_link, []},
		permanent,
		1000,
		worker,
		[ac_api_json_erl]
	}],

	{ok, {SupFlags, ChildSpecs}}
	.

