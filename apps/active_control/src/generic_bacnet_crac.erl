-module(generic_bacnet_crac).
-behaviour(crac_fsm).

-export([properties/0,read_setpoint/1,read_comm/1,read_standby/1,read_compressor/1,read_capacity/1,read_return/1,read_supply/1,write_setpoint/2]).
-export([read_high_head/1, read_low_suction/1, read_compressor_overload/1, read_short_cycle/1,
	 read_change_filters/1, read_general/1, write_clear_alarm/1]).

-import(shadoww_lib, [getprop/2]).
-import(io_handler, [addr_b/1, get_net/1,get_ip/1, get_devid/1]).

properties() ->
	% TODO: flag these with r,w,rw to prevent property cycles and race conditions.
	[
		{"ip", r},
		{"devid", r},
		{"network", r},
		{"setpointRead", r},
		{"setpointWrite", r},
		{"ratRead", r},
		{"satRead", r},
		{"compressorRead", r},
		{"capacityRead", r},
		{"standbyRead", r},
		{"commRead", r},
		{"commRead", r}	
	]
	++
	drv_handler:crac_alarm_properties()
	.

read_comm(Props) ->
	io_handler:read_comm("crac.commread", get_net(Props),get_ip(Props), get_devid(Props), getprop("commRead", Props))
	.

read_standby(Props) ->
	io_handler:read_standby("crac.standbyread", get_net(Props),get_ip(Props), get_devid(Props), getprop("standbyRead", Props))
	.

read_compressor(Props) ->
	io_handler:read_property("crac.compressorread", get_net(Props),get_ip(Props), get_devid(Props), getprop("compressorRead", Props))
	.
	
read_capacity(Props) ->
	io_handler:read_property("crac.capacityread", get_net(Props),get_ip(Props), get_devid(Props), getprop("capacityRead", Props))
	.
	
read_return(Props) ->
	io_handler:read_property("crac.ratread", get_net(Props),get_ip(Props), get_devid(Props), getprop("ratRead", Props))
	.

read_supply(Props) ->
	io_handler:read_property("crac.satread", get_net(Props),get_ip(Props), get_devid(Props), getprop("satRead", Props))
	.

read_setpoint(Props) ->
	io_handler:read_setpoint("crac.setpointread", get_net(Props),get_ip(Props), get_devid(Props), getprop("setpointRead", Props), getprop("setpoint", Props))
	.

write_setpoint(Props,Setpoint) ->
	io_eval:eval("crac.setpointwrite", getprop("setpointWrite", Props), addr_b(Props) ++ [{'Value', Setpoint}]),
	Setpoint
	.

%Alerts I/O
read_high_head(Props) ->
	io_handler:read_alarm("crac.highheadpressurealarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("highHeadPressureAlarmRead", Props))
	.
read_low_suction(Props) ->
	io_handler:read_alarm("crac.lowsuctionpressurealarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("lowSuctionPressureAlarmRead", Props))
	.
read_compressor_overload(Props) ->
	io_handler:read_alarm("crac.compressoroverloadalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("compressorOverloadAlarmRead", Props))
	.
read_short_cycle(Props) ->
	io_handler:read_alarm("crac.shortcyclealarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("shortCycleAlarmRead", Props))
	.
read_change_filters(Props) ->
	io_handler:read_alarm("crac.changefiltersalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("changeFiltersAlarmRead", Props))
	.
read_general(Props) ->
	io_handler:read_alarm("crac.generalalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("generalAlarmRead", Props))
	.

write_clear_alarm(Props) ->
	case  getprop("clearAlarmWrite", Props) of
		[] -> ok;
		WriteClear -> io_eval:eval("crac.clearalarmwrite", WriteClear, addr_b(Props) ++ [{'Value', 0}])
   	end
	.
