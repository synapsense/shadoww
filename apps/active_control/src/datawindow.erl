
-module(datawindow).

%%
%% Track a minimum/maximum value over time in a stream of data.
%% Each value is a {Value, Timestamp} tuple.
%% Timestamps can be any integer, but must be strictly increasing.
%%
%% WindowSz should be in the same units as Timestamp.
%% When new samples are added, samples with a Timestamp less
%% than NewTimestamp - WindowSz will be discarded.
%%

-export([
    new/2,
    push/2,
    value/1
    ]).

-record(datawindow, {
        values :: [{float(), integer()}],
        minmax :: min | max,
        windowsz :: integer()
        }).

-type datawindow() :: #datawindow{} .

-export_type([datawindow/0]).

-spec new(min | max, integer()) -> datawindow() .
new(MinMax, WindowSz) when is_atom(MinMax), is_integer(WindowSz) ->
    #datawindow{
        values = [],
        minmax = MinMax,
        windowsz = WindowSz
        }.

-spec push({float(), integer()}, datawindow()) -> datawindow().
push(ValTs = {Val,Ts}, DW = #datawindow{values = Values, minmax = MinMax, windowsz = WindowSz}) when is_float(Val), is_integer(Ts), is_record(DW, datawindow) ->
    InWindow = lists:takewhile(fun({_Val,T}) -> T > Ts-WindowSz end, Values),
    NewValues = case MinMax of
        min -> lists:dropwhile(fun({V,_Ts}) -> V >= Val end, InWindow) ;
        max -> lists:dropwhile(fun({V,_Ts}) -> V =< Val end, InWindow)
    end,
    NDW = DW#datawindow{values = [ValTs | NewValues]},
    NDW.

-spec value(datawindow()) -> float() .
value(DW = #datawindow{values = V}) when is_record(DW, datawindow) ->
    {Val,_Ts} = lists:last(V),
    Val.


