%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(remote_control_room).

-behaviour(room_manager).

-export([
         start/1,
         list_rooms/0,
         requires_restart/1,
         update_config/1,
         room_status/1,
         list_crahs/1,
         crah_status/2,
         stop/1,

         handle_call/2,
         handle_async_call/2,
         room_childspec/1,
         crah_ids/1
        ]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

start(#remote_control_room{id = Id} = Model) ->
    room_manager:start_room(?MODULE, Id, Model).

list_rooms() ->
    room_manager:rooms_of_type(?MODULE).

% @doc
% Check whether or not the room structure changed (and should therefore be restarted)
% IMPORTANT:
% If this function returns 'false', update_room(Id, NewModel) will be called next.
% So, anything not updated by update_room must be considered a "structural"
% change requiring the room to be restarted from scratch.
% @end
requires_restart(#remote_control_room{id=Id} = NewModel) ->
    room_manager:async_call(Id, {requires_restart, NewModel}).

update_config(#remote_control_room{id=Id} = M) ->
    room_manager:call(Id, {update_config, M}).

-spec room_status(Id :: id()) -> {ok, remote_control_room_status()}.
room_status(Id) ->
    room_manager:call(Id, status).

-spec list_crahs(RoomId :: id()) -> [id()].
list_crahs(RoomId) ->
    room_manager:async_call(RoomId, list_crahs).

-spec crah_status(RoomId :: id(), CrahId :: id()) ->
    {ok, remote_control_crah_status() | remote_control_crac_status()}
    | {error, no_such_id}.
crah_status(RoomId, CrahId) ->
    room_manager:call(RoomId, {crah_status, CrahId}).

stop(Id) ->
    room_manager:stop_room(Id).

handle_call({update_config,
             #remote_control_room{
                id = RoomId,
                crahs = Crahs,
                heartbeat_manager = HbManager
               } = NewState},
            _OldState) ->
    HbManager =/= none andalso heartbeat_manager:set_config(RoomId, HbManager),
    [update_crah(C) || C <- Crahs],
    {reply, ok, NewState};
handle_call(status, State) ->
    {reply, {ok, get_room_status(State)}, State};
handle_call({crah_status, CrahId}, State = #remote_control_room{crahs=Crahs}) ->
    case [Crah || Crah <- Crahs, crah_id(Crah) == CrahId] of
        [] -> {reply, {error, no_such_id}, State};
        [Crah] -> {reply, {ok, get_crah_status(Crah)}, State}
    end;
handle_call(_Message, State) ->
    {reply, error, State}.

handle_async_call({requires_restart,
                   #remote_control_room{
                      crahs = NewCrahs,
                      heartbeat_manager = NewHbManager
                     }},
                  #remote_control_room{
                     crahs = OldCrahs,
                     heartbeat_manager = OldHbManager
                    }) ->
    CrahIds = gb_sets:from_list([crah_id(Crah) || Crah <- OldCrahs]),
    NewCrahIds = gb_sets:from_list([crah_id(Crah) || Crah <- NewCrahs]),
    HbChanged = heartbeat_manager_changed(OldHbManager, NewHbManager),
    Reply = not (sets_equal(CrahIds, NewCrahIds) andalso
                 (not HbChanged)
                ),
    {reply, Reply};
handle_async_call(list_crahs, State) ->
    {reply, crah_ids(State)};
handle_async_call(_Message, _State) ->
    {reply, error}.

room_childspec(#remote_control_room{
                  id = RoomId,
                  heartbeat_manager = HeartbeatManager,
                  crahs = Crahs}) ->
    % Must include childspecs in specific order
    %  room_event()
    %  rf_crah
    Childspecs = room_event:childspec(RoomId)
        ++[rc_crah_childspec(RoomId, RcCrah) || RcCrah <- Crahs],
    HbChildspecs = case HeartbeatManager of
                  none -> Childspecs;
                  HeartbeatManager ->
                      HeartbeatSpec = heartbeat_manager:childspec(RoomId, HeartbeatManager),
                      [HeartbeatSpec | Childspecs]
              end,
    {
     RoomId,
     {dyn_sup, start_link, [RoomId, {rest_for_one, 1, 3600}, HbChildspecs]},
     temporary,
     infinity,
     supervisor,
     [dyn_sup]
    }.

update_crah(#remote_control_crah{id = Id,
                                  airflow_driver = AD,
                                  temperature_driver = TD}) ->
    AD =/= none andalso base_driver:set_config(
                          driver_lib:driver_key(Id, pressure),
                          AD),
    TD =/= none andalso base_driver:set_config(
                          driver_lib:driver_key(Id, temperature),
                          TD).

crah_ids(#remote_control_room{crahs = Crahs}) ->
    [Id || #remote_control_crah{id=Id} <- Crahs].

rc_crah_childspec(RoomId, #remote_control_crah{
    id = Id,
    temperature_driver = TmpDrv,
    temperature_strategy = TmpStrat,
    airflow_driver = AirDrv,
    airflow_strategy = AirStrat
}) ->
    rc_crah_childspec(RoomId, Id, TmpDrv, TmpStrat, AirDrv, AirStrat);
rc_crah_childspec(RoomId, #remote_control_crac{
    id = Id,
    temperature_driver = TmpDrv,
    temperature_strategy = TmpStrat,
    airflow_driver = AirDrv,
    airflow_strategy = AirStrat
}) ->
    rc_crah_childspec(RoomId, Id, TmpDrv, TmpStrat, AirDrv, AirStrat).

rc_crah_childspec(RoomId, CrahId, TmpDrv, TmpStrat, AirDrv, AirStrat) ->
    ChildSpecs = crah_childspecs(RoomId, AirDrv, AirStrat)
        ++ crah_childspecs(RoomId, TmpDrv, TmpStrat),
    {
        CrahId,
        {dyn_sup, start_link, [CrahId, ChildSpecs]},
        permanent,
        2000,
        supervisor,
        [dyn_sup]
    }.

crah_childspecs(_RoomId, none, none) ->
    [];
crah_childspecs(RoomId, Drv, Strat) ->
    [
        base_driver:childspec(RoomId, Drv),
        strategy_childspec(RoomId, Strat)
    ].

strategy_childspec(RoomId, #remote_control_strategy{id = Id, resource = Res} = StrategySpec) ->
    {
     driver_lib:proc_key(Id, Res, strategy),
     {remote_control_strategy, start_link, [RoomId, StrategySpec]},
     transient,
     1000,
     worker,
     [remote_control_strategy]
    }.

heartbeat_manager_changed(none, none) ->
    false;
heartbeat_manager_changed(OldHb, NewHb) when OldHb =:= none; NewHb =:= none ->
    true;
heartbeat_manager_changed(_OldHb, _NewHb) ->
    false.

crah_id(#remote_control_crah{id=Id}) -> Id;
crah_id(#remote_control_crac{id=Id}) -> Id.

sets_equal(Set1, Set2) ->
    Set1Sz = gb_sets:size(Set1),
    Set2Sz = gb_sets:size(Set2),
    if
        Set1Sz =:= Set2Sz ->
            Intersection = gb_sets:intersection(Set1, Set2),
            Set1Sz =:= gb_sets:size(Intersection)
            ;
        Set1Sz /= Set2Sz ->
            false
    end.

get_room_status(#remote_control_room{id=RoomId,crahs=Crahs}) ->
    #remote_control_room_status{
                                id = RoomId,
                                crahs_status = [get_crah_status(Crah) || Crah <- Crahs]
                               }.

-spec get_crah_status(remote_control_crah() | remote_control_crac()) ->
    remote_control_crah_status() | remote_control_crac_status().
get_crah_status(#remote_control_crac{id=CrahId, temperature_driver=TD}) when TD =:= none->
    Status = strategy_status(CrahId, pressure),
    %TODO: Verify that all that needed to change for CRAC is the record type.
    #remote_control_crac_status{
                                id = CrahId,
                                pressure_driver_status = ac_api:status(CrahId, pressure, driver),
                                pressure_strategy_status = Status,
                                mode = ac_api:control_status_to_mode(Status#remote_control_strategy_status.status)
                               };
get_crah_status(#remote_control_crac{id=CrahId, airflow_driver=PD}) when PD =:= none ->
    Status = strategy_status(CrahId, temperature),
    #remote_control_crac_status{
                                id = CrahId,
                                temperature_driver_status = ac_api:status(CrahId, temperature, driver),
                                temperature_strategy_status = Status,
                                mode = ac_api:control_status_to_mode(Status#remote_control_strategy_status.status)
                               };
get_crah_status(#remote_control_crac{id=CrahId}) ->
    PSStatus = strategy_status(CrahId, pressure),
    #remote_control_crac_status{
                                id = CrahId,
                                temperature_driver_status = ac_api:status(CrahId, temperature, driver),
                                temperature_strategy_status = strategy_status(CrahId, temperature),
                                pressure_driver_status = ac_api:status(CrahId, pressure, driver),
                                pressure_strategy_status = PSStatus,
                                mode = ac_api:control_status_to_mode(PSStatus#remote_control_strategy_status.status)
                               };
get_crah_status(#remote_control_crah{id=CrahId, temperature_driver=TD}) when TD =:= none->
    Status = strategy_status(CrahId, pressure),
    #remote_control_crah_status{
                                id = CrahId,
                                pressure_driver_status = ac_api:status(CrahId, pressure, driver),
                                pressure_strategy_status = Status,
                                mode = ac_api:control_status_to_mode(Status#remote_control_strategy_status.status)
                               };
get_crah_status(#remote_control_crah{id=CrahId, airflow_driver=PD}) when PD =:= none ->
    Status = strategy_status(CrahId, temperature),
    #remote_control_crah_status{
                                id = CrahId,
                                temperature_driver_status = ac_api:status(CrahId, temperature, driver),
                                temperature_strategy_status = Status,
                                mode = ac_api:control_status_to_mode(Status#remote_control_strategy_status.status)
                               };
get_crah_status(#remote_control_crah{id=CrahId}) ->
    PSStatus = strategy_status(CrahId, pressure),
    #remote_control_crah_status{
                                id = CrahId,
                                temperature_driver_status = ac_api:status(CrahId, temperature, driver),
                                temperature_strategy_status = strategy_status(CrahId, temperature),
                                pressure_driver_status = ac_api:status(CrahId, pressure, driver),
                                pressure_strategy_status = PSStatus,
                                mode = ac_api:control_status_to_mode(PSStatus#remote_control_strategy_status.status)
                               }.


-spec strategy_status(id(), controlled_resource()) -> remote_control_strategy_status().
strategy_status(CrahId, Resource) ->
    K = driver_lib:proc_key(CrahId, Resource, strategy),
    Info = driver_lib:call(K, get_status_info, []),
    #remote_control_strategy_status{
                                    id = CrahId,
                                    resource = Resource,
                                    status = proplists:get_value(status, Info),
                                    user_setpoint = proplists:get_value(user_setpoint, Info)
                                   }.

