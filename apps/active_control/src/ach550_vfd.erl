-module(ach550_vfd).
-behaviour(vfd_fsm).
-export([properties/0,read_setpoint/1,read_comm/1,read_override/1,read_kw/1,write_setpoint/2]).
-export([read_over_current/1, read_over_voltage/1, read_under_voltage/1, read_over_temperature/1,
	 read_general/1, write_clear_alarm/1]).
-import(io_handler, [get_ip/1, get_devid/1, bits_high/2]).

properties() ->
    %no specific properties
	[{"ip", r},
	 {"devid", r}].

read_comm(_) ->
	true.

read_override(_) ->
	false
	.

read_kw(Props) ->
	mbgw:read_uint16(get_ip(Props), get_devid(Props), 40007) / 10
	.

read_setpoint(Props) ->
	mbgw:read_uint16(get_ip(Props), get_devid(Props), 40005) / 6
	.

write_setpoint(Props,Value) ->
	mbgw:write_register16(get_ip(Props), get_devid(Props), 40002, 200 * round(Value))
	.

%Alerts I/O
read_over_current(Props) ->
	ReadReg = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40308),
	io_expr_util:bit(0, ReadReg)=/=0.
read_over_voltage(Props) ->
	ReadReg = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40308),
	io_expr_util:bit(1, ReadReg)=/=0.	
read_under_voltage(Props) ->
	ReadReg = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40308),
	io_expr_util:bit(2, ReadReg)=/=0.	
read_over_temperature(Props) ->
	ReadReg = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40308),
	io_expr_util:bit(8, ReadReg)=/=0.	
read_general(Props) ->
	R5 = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40305),
	R6 = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40306),
	R8 = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40308),
	R9 = mbgw:read_uint16(get_ip(Props), get_devid(Props), 40309),
	(R5 /= 0) or (R6 /= 0) or bits_high(R8, [3,4,5,6,7]) or (R9 /= 0)
	.

write_clear_alarm(Props) ->
	mbgw:write_register16(get_ip(Props), get_devid(Props), 40308, 0),
	mbgw:write_register16(get_ip(Props), get_devid(Props), 40309, 0),
	ok.

