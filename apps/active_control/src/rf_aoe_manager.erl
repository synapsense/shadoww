-module(rf_aoe_manager).

% Responsible to detect when an AoE recalculation is required,
% manage the recalculation, and distribute the AoE mapping to
% the aggregation process responsible for managing AoE values.
%
% NOT Responsible for input configuration
%
% Recalculation triggers are:
%   Device state change (online <=> standby <=> disengaged)
%   Input enable / disable
%   Stage 2 delta since last recalc > threshold (delta of pressure for temp AoE)
%
%   Device state change and Stage 2 change events are received from room_event event manager.
%   Input enable/disable notification is received from API
%
% When a recalc is first triggered, a cooldown window is entered.
% Additional tiggering events received in the cooldown window extend it.
% Once a period of time with no events passes, the recalc is begun.

-behaviour(gen_fsm).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

%% API
-export([
    start_link/4,
    stop/1,
    childspec/4,
    schedule_recalc/2
]).

%% gen_fsm callbacks
-export([
    init/1,
    handle_event/3,
    handle_sync_event/4,
    handle_info/3,
    terminate/3,
    code_change/4
]).

-export([
    watching/2,
    watching/3,
    cooldown/2,
    cooldown/3,
    recalculating/2,
    recalculating/3,
    recalc_proc/4
]).

-record(state, {
    room_id :: id(),
    resource :: controlled_resource(),
    aggressiveness :: aggressiveness(),
    proc_key :: driver_lib:proc_key(),
    reenter_cooldown = false :: boolean(),

    trigger_tracker :: rf_aoe_triggers:state_tracker(),

    cooldown_timer :: reference(),

    calc_ref :: reference()
}).

%%%===================================================================
%%% API
%%%===================================================================

start_link(RoomId, Resource, Aggressiveness, CrahIds) ->
    gen_fsm:start_link(?MODULE, {RoomId, Resource, Aggressiveness, CrahIds}, []).

stop(Pid) ->
    gen_fsm:sync_send_all_state_event(Pid, stop).

childspec(RoomId, Res, Agg, CrahIds) ->
    {
        driver_lib:proc_key(RoomId, Res, aoe),
        {?MODULE, start_link, [RoomId, Res, Agg, CrahIds]},
        transient,
        100,
        worker,
        [?MODULE]
    }.

schedule_recalc(RoomId, Resource) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, aoe)),
    gen_fsm:send_event(Pid, schedule_recalc).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm is started using gen_fsm:start/[3,4] or
%% gen_fsm:start_link/[3,4], this function is called by the new
%% process to initialize.
%%
%% @spec init(Args) -> {ok, StateName, State} |
%%                     {ok, StateName, State, Timeout} |
%%                     ignore |
%%                     {stop, StopReason}
%% @end
%%--------------------------------------------------------------------
init({RoomId, Resource, Aggressiveness, CrahIds}) ->
    K = driver_lib:proc_key(RoomId, Resource, aoe),
    driver_lib:reg(K),

    install_event_handlers(RoomId, Resource),

    Tracker0 = rf_aoe_triggers:init(CrahIds, RoomId, Resource),

    Tracker = process_cache:initial_config(K, Tracker0),

    gen_fsm:send_event(self(), recalc),

    folsom_metrics:new_history(aoe_recalc_extend),
    folsom_metrics:tag_metric(aoe_recalc_extend, aoe),
    folsom_metrics:new_histogram({aoe_recalc_time, RoomId, Resource}),
    folsom_metrics:tag_metric({aoe_recalc_time, RoomId, Resource}, tag_metric),

    State = #state{
        room_id = RoomId,
        resource = Resource,
        aggressiveness = Aggressiveness,
        proc_key = K,
        trigger_tracker = Tracker,
        cooldown_timer = undefined,
        calc_ref = undefined
    },
    {ok, cooldown, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% The 'watching' state is watching for trigger events.
%% When a trigger event happens, go to the 'cooldown' state
%%
%% @spec watching(Event, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
watching(schedule_recalc, State) ->
    {NextState, NewState} = handle_triggering_event(true,
        watching,
        schedule_recalc,
        State),
    {next_state, NextState, NewState};
watching(Ev = {_, {ET, _}}, State = #state{trigger_tracker = Tracker})
        when ET =:= input_status; ET =:= device_status; ET =:= device_setpoint ->
    {Triggering, NewTracker} = rf_aoe_triggers:triggering_event(Ev, Tracker),
    {NextState, NewState} = handle_triggering_event(Triggering,
        watching,
        Ev,
        State),
    {next_state, NextState, update_tracker(NewState, NewTracker)};
watching(Ev, State) ->
    lager:warning("Unexpected event in 'watching': ~p", [Ev]),
    {next_state, watching, State}.

cooldown(recalc, State = #state{
    room_id = RoomId,
    trigger_tracker = Tracker,
    resource = Resource
}) ->

    {Pid, Mref} = spawn_monitor(?MODULE, recalc_proc, [RoomId, Resource, Tracker, self()]),
    lager:info("Spawned recalc(~p) process: ~p", [Resource, Pid]),
    {next_state, recalculating, clear_doover(State#state{
        cooldown_timer = undefined,
        calc_ref = Mref})};
cooldown(schedule_recalc, State) ->
    {next_state, cooldown, State};
cooldown(Ev = {_, {ET, _}}, State = #state{trigger_tracker = Tracker})
        when ET =:= input_status; ET =:= device_status; ET =:= device_setpoint ->
    {Triggering, NewTracker} = rf_aoe_triggers:triggering_event(Ev, Tracker),
    {NextState, NewState} = handle_triggering_event(Triggering,
        cooldown,
        Ev,
        State),
    {next_state, NextState, update_tracker(NewState, NewTracker)};
cooldown(Ev, State) ->
    lager:warning("Unexpected event in 'cooldown': ~p", [Ev]),
    {next_state, cooldown, State}.

recalculating(recalc_complete, State = #state{calc_ref = CalcRef,
    trigger_tracker = Tracker}) ->
    demonitor(CalcRef, [flush]),
    NewTracker = rf_aoe_triggers:present_as_prev(Tracker),
    case need_doover(State) of
        true ->
            {next_state, cooldown, start_cooldown(
                update_tracker(
                    State#state{calc_ref = undefined}, NewTracker))};
        false ->
            {next_state, watching, update_tracker(State#state{calc_ref = undefined}, NewTracker)}
    end;
recalculating(retry_later, State = #state{calc_ref = CalcRef}) ->
    demonitor(CalcRef, [flush]),
    {next_state, watching, State#state{calc_ref = undefined}};
recalculating(schedule_recalc, State) ->
    {NextState, NewState} =
        handle_triggering_event(true,
            recalculating,
            schedule_recalc,
            State),
    {next_state, NextState, NewState};
recalculating(Ev = {_, {ET, _}}, State = #state{trigger_tracker = Tracker})
        when ET =:= input_status; ET =:= device_status; ET =:= device_setpoint ->
    {Triggering, NewTracker} = rf_aoe_triggers:triggering_event(Ev, Tracker),
    {NextState, NewState} = handle_triggering_event(Triggering, recalculating, Ev, State),
    {next_state, NextState, update_tracker(NewState, NewTracker)};
recalculating(Ev, State) ->
    lager:warning("Unexpected event in 'recalculating': ~p", [Ev]),
    {next_state, recalculating, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% There should be one instance of this function for each possible
%% state name. Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_event/[2,3], the instance of this function with
%% the same name as the current state name StateName is called to
%% handle the event.
%%
%% @spec state_name(Event, From, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
watching(Ev, _From, State) ->
    lager:warning("Unexpected sync event in 'watching': ~p", [Ev]),
    {reply, ok, watching, State}.

cooldown(Ev, _From, State) ->
    lager:warning("Unexpected sync event in 'cooldown': ~p", [Ev]),
    {reply, ok, cooldown, State}.

recalculating(Ev, _From, State) ->
    lager:warning("Unexpected sync event in 'recalculating': ~p", [Ev]),
    {reply, ok, recalculating, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:send_all_state_event/2, this function is called to handle
%% the event.
%%
%% @spec handle_event(Event, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
handle_event(Event, StateName, State) ->
    lager:warning("Unexpected handle_event(~p, ~p, _)", [Event, StateName]),
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a gen_fsm receives an event sent using
%% gen_fsm:sync_send_all_state_event/[2,3], this function is called
%% to handle the event.
%%
%% @spec handle_sync_event(Event, From, StateName, State) ->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {reply, Reply, NextStateName, NextState} |
%%                   {reply, Reply, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState} |
%%                   {stop, Reason, Reply, NewState}
%% @end
%%--------------------------------------------------------------------
handle_sync_event(stop, _From, StateName, State = #state{proc_key = ProcKey}) ->
    lager:info("rf_aoe_manager stopping in state ~p", [StateName]),
    process_cache:delete_properties(ProcKey),
    {stop, normal, stop, State};
handle_sync_event(Event, _From, StateName, State) ->
    lager:warning("Unexpected handle_sync_event(~p, _, ~p, _)", [Event, StateName]),
    {reply, huh, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it receives any
%% message other than a synchronous or asynchronous event
%% (or a system message).
%%
%% @spec handle_info(Info,StateName,State)->
%%                   {next_state, NextStateName, NextState} |
%%                   {next_state, NextStateName, NextState, Timeout} |
%%                   {stop, Reason, NewState}
%% @end
%%--------------------------------------------------------------------
handle_info({'DOWN', Ref, process, _Pid, Reason}, recalculating, State = #state{calc_ref = CalcRef})
    when Ref =:= CalcRef ->
    lager:error("AoE recalculation process failed! ~p", [Reason]),
    {next_state, cooldown, start_cooldown(State#state{calc_ref = undefined})};
handle_info(Info, StateName, State) ->
    lager:warning("Unexpected handle_info(~p, ~p, _)", [Info, StateName]),
    {next_state, StateName, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_fsm when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_fsm terminates with
%% Reason. The return value is ignored.
%%
%% @spec terminate(Reason, StateName, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(Reason, StateName, _State) ->
    lager:warning("Terminating with reason ~p in state ~p", [Reason, StateName]),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, StateName, State, Extra) ->
%%                   {ok, StateName, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

update_tracker(State, Tracker) ->
    State#state{trigger_tracker = Tracker}.

handle_triggering_event(false, S = watching, Ev, State) ->
    lager:debug("Event ~p had no effect, staying in 'watching'", [Ev]),
    {S, State};
handle_triggering_event(false, S = cooldown, Ev, State) ->
    lager:debug("Event ~p had no effect, staying in 'cooldown'", [Ev]),
    {S, State};
handle_triggering_event(false, S = recalculating, Ev, State) ->
    lager:debug("Event ~p had no effect, staying in 'recalculating'", [Ev]),
    {S, State};
handle_triggering_event(true, _S = watching, Ev, State) ->
    lager:info("Event ~p triggered 'watching' -> 'cooldown'", [Ev]),
    {cooldown, start_cooldown(State)};
handle_triggering_event(true, S = cooldown, Ev,
        State = #state{room_id = RoomId, resource = Resource}) ->
    lager:info("Event ~p extended timer in 'cooldown'", [Ev]),
    folsom_metrics:notify(aoe_recalc_extend, {RoomId, Resource}),
    {S, start_cooldown(State)};
handle_triggering_event(true, S = recalculating, Ev, State) ->
    lager:info("Event ~p in 'recalculating', I need a do-over!", [Ev]),
    {S, doover(State)}.

recalc_proc(RoomId, Resource, Tracker, AoePid) ->
    StartTime = os:timestamp(),
    Begin = folsom_metrics:histogram_timed_begin({aoe_recalc_time, RoomId, Resource}),
    Result = case Resource of
                 temperature ->
                     rf_aoe_generate:temperature_aoe(RoomId,
                         rf_aoe_triggers:present_status(temperature, Tracker),
                         rf_aoe_triggers:present_status(pressure, Tracker),
                         rf_aoe_triggers:present_setpoint(pressure, Tracker));
                 pressure ->
                     rf_aoe_generate:pressure_aoe(RoomId,
                         rf_aoe_triggers:present_status(pressure, Tracker))
             end,

    folsom_metrics:histogram_timed_notify(Begin),
    case Result of
        {solution, Flows} ->
            AoE = [{From, To} || {From, To, Flow} <- Flows, Flow > 0.0],
            lager:info("New ~p AoE in room ~p", [Resource, RoomId]),
            rf_input:new_aoe(RoomId, Resource, AoE),
            lager:info("~p AoE calc time: ~ps", [Resource, timer:now_diff(os:timestamp(), StartTime) / 1000000]),
            gen_fsm:send_event(AoePid, recalc_complete);
        no_solution ->
            lager:error("no solution for ~p ~p AoE", [RoomId, Resource]),
            gen_fsm:send_event(AoePid, retry_later);
        {error, N, St} when is_integer(N) ->
            lager:error("model run failed! error code: ~B stack trace:~n~p", [N, St]),
            gen_fsm:send_event(AoePid, retry_later);
        {error, X, St} ->
            lager:error("model run failed! ~p, ~p", [X, St]),
            gen_fsm:send_event(AoePid, retry_later)
    end.

start_cooldown(State = #state{room_id = RoomId, resource = Resource, cooldown_timer = OldTimer}) ->
    R = case OldTimer of
            undefined -> undefined;
            _ -> gen_fsm:cancel_timer(OldTimer)
        end,
    AoeCooldownTime = strategy_param:find(aoe_cooldown_time, Resource, RoomId),
    lager:debug("Setting cooldown to expire in ~p.  Old timer had ~p remaining.", [AoeCooldownTime, R]),
    State#state{cooldown_timer = gen_fsm:send_event_after(AoeCooldownTime, recalc)}.

-spec install_event_handlers(RoomId :: id(), Resource :: controlled_resource()) -> ok.
install_event_handlers(RoomId, pressure) ->
    room_event:relay_events(RoomId, room_event_handler(pressure, [device_status, input_status]));
install_event_handlers(RoomId, temperature) ->
    room_event:relay_events(RoomId, room_event_handler(temperature, [device_status, input_status])),
    room_event:relay_events(RoomId, room_event_handler(pressure, [device_setpoint, device_status, input_status])).

-spec room_event_handler(Resource :: controlled_resource(), EventTypes :: [room_event:room_event_type()]) ->
    fun((room_event:room_event()) -> {room_event, room_event:room_event()} | ok).
room_event_handler(Resource, EventTypes) ->
    Self = self(),
    fun(Event = {Key, {ET, _}}) ->
        R = driver_lib:resource(Key),
        Good2Go = lists:member(ET, EventTypes) andalso R =:= Resource,
        case Good2Go of
            true -> gen_fsm:send_event(Self, Event);
            false -> ok
        end
    end.

doover(S) -> S#state{reenter_cooldown = true}.
clear_doover(S) -> S#state{reenter_cooldown = false}.
need_doover(#state{reenter_cooldown = Reenter}) -> Reenter.

