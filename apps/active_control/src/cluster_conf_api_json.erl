%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(cluster_conf_api_json).

-compile({parse_transform, runtime_types}).

-export([
         register_rpc/0,
         list_overrides/0,
         save_override/1,
         remove_override/1
        ]).

-record(rf_override, {
          name :: min_s2 | max_s2,
          resource = null :: null | temperature | pressure,
          value :: float(),
          % add resource & aggressiveness here too?
          room = null :: null | binary(),
          crah = null :: null | binary()
         }).
-type rf_override() :: #rf_override{}.

-record(modbus_override, {
          name :: mbtcp_pipeline | mbtcp_rx_timeout,
          value :: integer(),
          address = null :: null | binary()
         }).
-type modbus_override() :: #modbus_override{}.

-type override() :: rf_override() | modbus_override().

-export_records([
                 rf_override,
                 modbus_override
                ]).

register_rpc() ->
    Methods = [
               <<"list_overrides">>,
               <<"save_override">>,
               <<"remove_override">>
              ],
    [json_rpc:register_method(M, ?MODULE) || M <- Methods].

list_overrides() ->
    Overrides = cluster_config:list_overrides(),
    {ok, [override_to_json(O) || O <- Overrides]}.

save_override(Json) ->
    try
        O = json_to_override(Json),
        P = override_path(O),
        K = override_name(O),
        V = override_value(O),
        case validate_override(P, K, V) of
            ok -> cluster_config:set_override(P, K, V);
            {error, R} -> {ok, R}
        end
    catch
        Err:Reason ->
            lager:warning("Setting override failed: {~p,~p}", [Err, Reason]),
            {ok, <<"Invalid override">>}
    end.

remove_override(Json) ->
    try
        O = json_to_override(Json),
        P = override_path(O),
        K = override_name(O),
        cluster_config:remove_override(P, K)
    catch
        _:_ ->
            {ok, <<"Invalid override">>}
    end.

%%
%% Helper functions
%%

override_name(#rf_override{name=N, resource=R}) ->
    case R of
        null -> N;
        R -> {N, R}
    end;
override_name(#modbus_override{name=N}) ->
    N.

override_path(#rf_override{room=R, crah=C}) ->
    case {R, C} of
        {null, null} -> [rf_control];
        {R, null} -> [rf_control, R];
        {null, C} -> throw(invalid_override);
        {R, C} -> [rf_control, R, C]
    end;
override_path(#modbus_override{address=A}) ->
    case A of
        null -> [mbgw];
        A -> [mbgw, shadoww_lib:dotted_to_tuple(A)]
    end.

override_value(#rf_override{value=V}) -> V;
override_value(#modbus_override{value=V}) -> V.

validate_override(_P, {min_s2, pressure}, V) ->
    case inrange(20.0, 50.0, V) of
        true -> ok;
        false -> {error, <<"Minimum fanspeed must be between 20 and 50%">>}
    end;
validate_override(_P, {max_s2, pressure}, V) ->
    case inrange(80.0, 100.0, V) of
        true -> ok;
        false -> {error, <<"Maximum fanspeed must be between 80 and 100%">>}
    end;
validate_override(_P, {min_s2, temperature}, V) ->
    case inrange(45.0, 60.0, V) of
        true -> ok;
        false -> {error, <<"Minimum supply temp must be between 45 and 60F">>}
    end;
validate_override(_P, {max_s2, temperature}, V) ->
    case inrange(70.0, 80.0, V) of
        true -> ok;
        false -> {error, <<"Maximum supply temp must be between 70 and 80F">>}
    end;
validate_override(_P, min_s2, _V) ->
    {error, <<"Must specify resource (temperature or pressure)">>};
validate_override(_P, max_s2, _V) ->
    {error, <<"Must specify resource (temperature or pressure)">>};
validate_override(_P, mbtcp_pipeline, V) ->
    case inrange(0, 20, V) of
        true -> ok;
        false -> {error, <<"Modbus pipeline must be between 0 and 20">>}
    end;
validate_override(_P, mbtcp_rx_timeout, V) ->
    case inrange(500, 10000, V) of
        true -> ok;
        false -> {error, <<"Modbus receive timeout must be between 500 and 10000ms">>}
    end;
validate_override(P, N, V) ->
    lager:warning("Invalid override: path: ~p, name: ~p, value: ~p", [P, N, V]),
    {error, <<"Invalid override">>}.

inrange(Min, Max, V) ->
    Min =< V andalso V =< Max.

override_to_json(O) ->
    aeon:type_to_jsx(override_record(O), ?MODULE, override).

json_to_override(Json) ->
    aeon:to_type(Json, ?MODULE, override).

override_record({[rf_control | P], Key, Value}) ->
    rf_override_record(P, Key, Value);
override_record({[mbgw | P], Key, Value}) ->
    mb_override_record(P, Key, Value).

rf_override_record(P, K, V) ->
    rf_path_components(P, rf_key(K, #rf_override{value=V})).

rf_path_components([], R) ->
    R;
rf_path_components([RoomId], R) ->
    R#rf_override{room=RoomId};
rf_path_components([RoomId,CrahId], R) ->
    R#rf_override{room=RoomId, crah=CrahId}.

rf_key({N,Res}, R) ->
    R#rf_override{name=N,resource=Res};
rf_key(A, R) when is_atom(A) ->
    R#rf_override{name=A}.

mb_override_record([], N, Value) ->
    #modbus_override{
       name = N,
       value = Value,
       address = null
      };
mb_override_record([Addr], N, Value) ->
    #modbus_override{
       name = N,
       value = Value,
       address = list_to_binary(shadoww_lib:tuple_to_dotted(Addr))
      }.

