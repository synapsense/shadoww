%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(rf_aoe_generate).

-export([
         pressure_aoe/2,
         temperature_aoe/4
        ]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-spec get_distances(RoomId :: id(), FromId :: id(), From :: atom(), To :: atom()) ->
    [{FromId :: id(), ToId :: id(), Distance :: float()}] | {FromId :: id(), bad_geom}.
get_distances(RoomId, FromId, From, To) ->
    case rf_geom:get_distances(RoomId, FromId, From, To) of
        bad_geom -> {FromId, bad_geom};
        Distances -> [{FromId, InputId, D} || {D, InputId} <- Distances]
    end.

-spec get_distances(RoomId :: id(), FromId :: id(), From :: atom(), To :: atom(), Inputs :: [InputId :: id()]) ->
    [{FromId :: id(), ToId :: id(), Distance :: float()} | {FromId :: id(), bad_geom}].
get_distances(RoomId, FromId, From, To, Inputs) ->
    case get_distances(RoomId, FromId, From, To) of
        {FromId, bad_geom} -> {FromId, bad_geom};
        Distances ->
            lists:filter(fun({_FromId, InputId, _D}) -> lists:member(InputId, Inputs) end, Distances)
    end.

-spec pressure_aoe(RoomId :: id(),
                   VfdStatus :: [{id(), control_status()}]
    ) ->
    {solution, Flows :: [{id(), id(), float()}]}
    | no_solution
    | {error, N :: any(), Stacktrace :: any()}.
pressure_aoe(RoomId, VfdStatus) ->
    % Collect VFD statuses
    % For pressure inputs that are disabled, use a "pressure" of 0.0
    % For VFDs with status 'online', use a "flow" of 1.0
    % For VFDs with status !'online', use a "flow" of 0.0
    % why not have AoE manager store CRAH airflow as well as status?
    CrahFlows = [{Id, 1.0} || {Id,Status} <- VfdStatus, lists:member(Status, [online, local_override])],
    PressureInputs = enabled_inputs(RoomId, pressure),
    Distances = lists:flatten([get_distances(RoomId, CrahId, crah, pressure, PressureInputs) || {CrahId, _} <- CrahFlows]),

    Good2Go = lists:all(
        fun({true, F}) -> F(), false;
            ({false, _}) -> true
        end,
        [
            {
                length(CrahFlows) < 4,
                fun() ->
                    lager:warning("Not enough CRAHs (~p) online, can't calculate pressure AoE", [length(CrahFlows)])
                end
            },
            {
                length(PressureInputs) < 4,
                    fun() ->
                        lager:warning("Not enough pressure nodes (~p) enabled, can't calculate pressure AoE",
                            [length(PressureInputs)])
                    end
            },
            {
                lists:any(
                    fun({_CrahId, bad_geom}) -> true;
                        ({_CrahId, _PressureId, _Distance}) -> false
                    end,
                    Distances),
                fun() ->
                    lager:warning("The geometry of the room is preventing AoE estimation, can't calculate pressure AoE.")
                end
            }
        ]),

    case Good2Go of
        false ->
            lager:warning("Generating default AoE"),
            default_aoe([Id || {Id, _} <- CrahFlows], PressureInputs);
        true ->
            lager:info("pressure AoE calling glpsol: Flows: ~p Pressures: ~p Distances: ~p",
                [length(CrahFlows), length(PressureInputs), length(Distances)]),
            PressurePressures = [{Id, 1.0} || Id <- PressureInputs],
            glpsol:solve("pmodel", CrahFlows, PressurePressures, Distances)
    end.

-spec temperature_aoe(RoomId :: id(),
                      CrahStatus :: [{id(), control_status()}],
                      VfdStatus :: [{id(), control_status()}],
                      VfdSetpoint :: [{id(), float()}]
        ) ->
    {solution, Flows :: [{id(), id(), float()}]}
    | no_solution
    | {error, N :: any(), Stacktrace :: any()}.
temperature_aoe(RoomId, CrahStatus, VfdStatus, VfdSetpoint) ->
    % Collect CRAH states & current airflow
    % Collect Pressure config, and current pressure
    % Calc nearest PNode to each Input
    % Calc distances between each Crah and Input
    % Send off to glpsol

    CrahFlows = crah_flows(CrahStatus, VfdStatus, VfdSetpoint),
    PressureValues = room_pressure_values(RoomId),
    TempInputs = enabled_inputs(RoomId, temperature),
    CrahRackDistances = lists:flatten(
        [get_distances(RoomId, CrahId, crah, rack, TempInputs)
            || {CrahId, _} <- CrahFlows]),
    PressureDistancesByRack = lists:flatten([get_distances(RoomId, Id, rack, pressure) || Id <- TempInputs]),

    Good2Go = lists:all(
        fun({true, F}) -> F(), false;
            ({false, _}) -> true
        end,
        [
            {
                length(CrahFlows) < 4,
                fun() ->
                    lager:warning("Too few CRAHs (~p) moving air, can't calculate temperature AoE",
                        [length(CrahFlows)])
                end
            },
            {
                length(TempInputs) < 4,
                fun() ->
                    lager:warning("Too few control inputs (~p) enabled, can't calculate temperature AoE",
                        [length(TempInputs)])
                end
            },
            {
                lists:any(
                    fun({_CrahId, bad_geom}) -> true;
                        ({_CrahId, _RackId, _Distance}) -> false
                    end,
                    CrahRackDistances),
                fun() ->
                    lager:warning("The geometry of the room is preventing AoE estimation, can't calculate temperature AoE.")
                end
            },
            {
                lists:any(
                    fun({_RackId, bad_geom}) -> true;
                        ({_RackId, _PressureId, _Distance}) -> false
                    end,
                    PressureDistancesByRack),
                fun() ->
                    lager:warning("The geometry of the room is preventing AoE estimation, can't calculate temperature AoE.")
                end
            }
        ]),

    case Good2Go of
        false ->
            lager:warning("Generating default AoE"),
            default_aoe([Id || {Id, _} <- CrahFlows], TempInputs);
        true ->
            RackPressures = case length(PressureValues) of
                                TooLow when TooLow =< 1 ->
                                    lager:warning("No usable pressure for temperature AoE, using default"),
                                    [{Id, 1.0} || Id <- TempInputs];
                                I when I > 1 ->
                                    [{Id, first_in(lists:foldl(fun({RackId, PressureId, Distance}, AccIn) when RackId =:= Id -> [{PressureId, Distance} | AccIn];
                                        (_, AccIn) -> AccIn
                                                               end,
                                        [],
                                        PressureDistancesByRack), PressureValues)} || Id <- TempInputs]
                            end,
            lager:info("recalc_proc(temperature) calling glpsol Flows: ~p Pressures: ~p Distances: ~p",
                [length(CrahFlows), length(RackPressures), length(CrahRackDistances)]),
            glpsol:solve("model", CrahFlows, RackPressures, CrahRackDistances)
    end.

default_aoe(CrahIds, RackIds) ->
    {solution, [{CrahId, RackId, 1.0} || CrahId <- CrahIds, RackId <- RackIds]}.

-spec crah_flows(CrahStatus :: [{id(), Status :: control_status()}],
                 VfdStatus :: [{id(), Status :: control_status()}],
                 VfdFanspeed :: [{id(), Fanspeed :: float()}]) ->
    [{id(), Flow :: float()}].
crah_flows(CrahStatus, VfdStatus, VfdSetpoint) ->
    %% Ignore crahs that aren't online, and fans that are not blowing air
    CrahFlows1 = [{Id1, VF} || {Id1, CS} <- CrahStatus,
                               {Id2, VS} <- VfdStatus,
                               {Id3, VF} <- VfdSetpoint,
                               Id1 =:= Id2,
                               Id2 =:= Id3,
                               lists:member(CS, [online]),
                               lists:member(VS, [online, local_override]),
                               is_float(VF)],

    CrahFlows = [{Id, vfd_fsm:speed_to_cfm(VF, cfm_table(Id))} || {Id, VF} <- CrahFlows1],
    CrahFlows.

room_pressure_values(RoomId) ->
    [{Id, P}
     || #input_status{id=Id,enabled=E,actual=P} <- rf_input:get_all_input_status(RoomId, pressure),
        E, is_float(P), P > 0.0].

enabled_inputs(RoomId, Resource) ->
    [Id || #single_input{id=Id,enabled=E}
           <- rf_input:get_all_input_config(RoomId, Resource),
           E].

first_in([{PressureId, _Dist} | Next], Pressures) ->
    case lists:keyfind(PressureId, 1, Pressures) of
        {_, P} -> P;
        false -> first_in(Next, Pressures)
    end.

cfm_table(Id) ->
    driver_lib:call(driver_lib:proc_key(Id, pressure, driver),
                    get_property,
                    ["cfm_table"]).

