% vim: et:ts=8:sw=4:sts=4:et
%
-module(rf_geom).

% Design:
% pre-calculate everything in and store in public ETS table, along with RoomId for easy cleanup.
% query functions can then directly access the ETS table
% the table is still owned by the process, so it gets cleaned up when the process dies
%
% How does this handle things getting enabled/disabled?  Maybe it doesn't, that's up to the client.

%% Fundamental raised floor geometry needs:
%% distance CRAH to CRAH, for neighborhood & load balancing
%% distance from CRAHs to RACKs, for temperature AoE calculation
%% distance from CRAHs to PRESSUREs, for pressure AoE calculation
%% distance from RACK to PRESSURE, for pressure near rack
%%
%% ... and all taking walls into account
%%

-behaviour(gen_server).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([
        get_distances/4,
        crah_location/2,
        room_walls/1,
        update_geometry/5,
        childspec/5
        ]).

%% API
-export([
        start_link/5,
        stop/1,
        wall_walk_dist/3
        ]).

%% gen_server callbacks
-export([
        init/1,
        handle_call/3,
        handle_cast/2,
        handle_info/2,
        terminate/2,
        code_change/3
        ]).

-record(distances, {
    crah_to_crah :: gb_trees:tree(id(), [{float(), id()}] | bad_geom),
    crah_to_rack :: gb_trees:tree(id(), [{float(), id()}] | bad_geom),
    crah_to_pressure :: gb_trees:tree(id(), [{float(), id()}] | bad_geom),
    rack_to_pressure :: gb_trees:tree(id(), [{float(), id()}] | bad_geom)
}).

-record(state, {
        room_id :: id(),
        crahs :: [{id(), geom:point()}],
        racks :: [{id(), geom:point()}],
        pressures :: [{id(), geom:point()}],
        walls :: [{id(), geom:poly()}],
        distances :: #distances{}
        }).

%%%===================================================================
%%% API
%%%===================================================================

-spec start_link(
        RoomId :: id(),
        Crahs :: [{id(), geom:point()}],
        Racks :: [{id(), geom:point()}],
        Pressures :: [{id(), geom:point()}],
        Walls :: [geom:poly()]
       ) -> {error, any()} | {ok, pid()}.
start_link(RoomId, Crahs, Racks, Pressures, Walls) ->
    gen_server:start_link(?MODULE, {RoomId, Crahs, Racks, Pressures, Walls}, []).

stop(Pid) ->
    gen_server:call(Pid, stop).

-spec get_distances(RoomId :: id(), FromId :: id(), FromType :: crah | rack, ToType :: crah | rack | pressure) ->
    [{Distance :: float(), ToId :: id()}] | bad_geom.
% Distance from given CRAH to all CRAHs in the room
get_distances(RoomId, FromId, crah, crah) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, {crah_neighbors, FromId});
% Distance from given CRAH to all Pressures in the room
get_distances(RoomId, FromId, crah, pressure) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, {crah_to_pressure, FromId});
% Distance from given CRAH to all Racks in the room
get_distances(RoomId, FromId, crah, rack) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, {crah_to_rack, FromId});
% Distance from given Rack to all Pressures in the room
get_distances(RoomId, FromId, rack, pressure) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, {rack_to_pressure, FromId}).

-spec crah_location(RoomId :: id(), CrahId :: id()) -> geom:point() | false.
crah_location(RoomId, CrahId) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, {crah_location, CrahId}).

room_walls(RoomId) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, walls).

-spec update_geometry(RoomId :: id(),
                      Crahs :: [{id(), geom:point()}],
                      Racks :: [{id(), geom:point()}],
                      Pressures :: [{id(), geom:point()}],
                      Walls :: [geom:poly()]) -> ok.
update_geometry(RoomId, Crahs, Racks, Pressures, Walls) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, none, geom)),
    gen_server:call(Pid, {update_geometry, RoomId, Crahs, Racks, Pressures, Walls}).

childspec(RoomId, CrahLocations, RackLocations, PressureLocations, Walls) ->
    {
     driver_lib:proc_key(RoomId, none, geom),
     {?MODULE, start_link, [RoomId, CrahLocations, RackLocations, PressureLocations, Walls]},
     transient,
     1000,
     worker,
     [?MODULE]
    }.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init({RoomId, Crahs, Racks, Pressures, Walls}) ->
    % Generate and cache distances between all relevant objects
    % crah to crah distance
    % crah to rack distance
    % crah to pressure distance
    % rack to pressure distance
    InitStart = os:timestamp(),

    driver_lib:reg(driver_lib:proc_key(RoomId, none, geom)),

    Distances = build_room_geometry(RoomId, Crahs, Racks, Pressures, Walls),

    % NOTE: nothing is cached in distributed memory because this information is static.
    % However, it could take a while to re-calc everything after a
    % failover so it might be a good idea.  Need to do some
    % performance testing first.
    lager:info("rf_geom init complete in ~psec",
        [timer:now_diff(os:timestamp(), InitStart) div 1000000]),

    {ok, #state{
        room_id = RoomId,
        crahs = Crahs,
        racks = Racks,
        pressures = Pressures,
        walls = Walls,
        distances = Distances
    }}.

handle_call({crah_neighbors, CrahId}, _From, State = #state{distances = Distances}) ->
    {reply, gb_trees:get(CrahId, Distances#distances.crah_to_crah), State};
handle_call({crah_to_rack, CrahId}, _From, State = #state{distances = Distances}) ->
    {reply, gb_trees:get(CrahId, Distances#distances.crah_to_rack), State};
handle_call({crah_to_pressure, CrahId}, _From, State = #state{distances = Distances}) ->
    {reply, gb_trees:get(CrahId, Distances#distances.crah_to_pressure), State};
handle_call({rack_to_pressure, RackId}, _From, State = #state{distances = Distances}) ->
    {reply, gb_trees:get(RackId, Distances#distances.rack_to_pressure), State};
handle_call({crah_location, CrahId}, _From,
            State = #state{crahs = Crahs}) ->
    Loc = case lists:keyfind(CrahId, 1, Crahs) of
              {CrahId, Pt} -> Pt;
              false -> false
          end,
    {reply, Loc, State};
handle_call(walls, _From, State = #state{walls = Walls}) ->
    {reply, Walls, State};
handle_call({update_geometry, Crahs, Racks, Pressures, Walls}, _From, State) ->

    StartTime = os:timestamp(),

    Distances = build_room_geometry(State#state.room_id, Crahs, Racks, Pressures, Walls),

    lager:info("rf_geom recalc complete in ~psec",
                [timer:now_diff(os:timestamp(), StartTime) div 1000000]),

    {reply, ok, State#state{
        crahs = Crahs,
        racks = Racks,
        pressures = Pressures,
        walls = Walls,
        distances = Distances
    }};
handle_call(stop, _From, State) ->
    {stop, normal, stop, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

-spec check_for_bad_geometry([{FromId :: id(), {float(), ToId :: id()} | bad_geom}]) -> boolean().
check_for_bad_geometry([{_Id, bad_geom} | _]) ->
    true;
check_for_bad_geometry([{_Id, _Distances} | _]) ->
    false.

-spec build_room_geometry(RoomId :: id(),
        Crahs :: [{id(), geom:point()}],
        Racks :: [{id(), geom:point()}],
        Pressures :: [{id(), geom:point()}],
        Walls :: [{id(), geom:poly()}])
            -> #distances{}.
build_room_geometry(RoomId, Crahs, Racks, Pressures, Walls) ->
    CrahCrahK = async_wall_walk_dist(Crahs, Crahs, Walls),
    CrahRackK = async_wall_walk_dist(Crahs, Racks, Walls),
    CrahPressureK = async_wall_walk_dist(Crahs, Pressures, Walls),
    RackPressureK = async_wall_walk_dist(Racks, Pressures, Walls),

    CrahCrah = yield(CrahCrahK),
    CrahRack = yield(CrahRackK),
    CrahPressure = yield(CrahPressureK),
    RackPressure = yield(RackPressureK),

    Good2Go = lists:all(
        fun({true, F}) -> F(), false;
            ({false, _}) -> true
        end,
        [
            {check_for_bad_geometry(CrahCrah),
                fun() ->
                    lager:warning("Can't determine distances between Crahs")
                end},
            {check_for_bad_geometry(CrahRack),
                fun() ->
                    lager:warning("Can't determine distances from Crahs to Racks")
                end},
            {check_for_bad_geometry(RackPressure),
                fun() ->
                    lager:warning("Can't determine distances from Racks to Pressure Nodes")
                end},
            {check_for_bad_geometry(CrahPressure),
                fun() ->
                    lager:warning("Can't determine distances from Crahs to PressureNodes")
                end}
        ]),

    case Good2Go of
        false -> control_alarm:bad_room_geom(RoomId);
        true -> none
    end,

    #distances{
        crah_to_crah = gb_trees:from_orddict(CrahCrah),
        crah_to_rack = gb_trees:from_orddict(CrahRack),
        crah_to_pressure = gb_trees:from_orddict(CrahPressure),
        rack_to_pressure = gb_trees:from_orddict(RackPressure)
    }.

async_wall_walk_dist(From, To, Walls) ->
    rpc:async_call(node(), ?MODULE, wall_walk_dist, [From, To, Walls]).

yield(K) ->
    case rpc:yield(K) of
        {badrpc, R} ->
            error(R);
        Value -> Value
    end.

-spec wall_walk_dist(From :: [{id(), geom:point()}],
                     To :: [{id(), geom:point()}],
                     Walls :: [geom:poly()]) ->
    [{FromId :: id(), {ToId :: id(), float()} | bad_geom}].
%% @doc
%% Calculate point-to-point distance from each thing in From to each thing
%% in To.  Except, if there is a wall between From and To, distance will include
%% the distance around the wall between them.  So, it would be the distance to
%% walk from From to To, going around the wall on the way.
%%
%% Sometimes there are too many walls in the way, and the algorithm fails.  In
%% this case, the distance used is the longest distance of From to To.
%% @end
wall_walk_dist(From, To, Walls) ->
    FromTo = [{FromId,
               sub_max(
                       [{geom:walk_distance(FromPt, ToPt, Walls), ToId}
                        || {ToId, ToPt} <- To, ToId =/= FromId])
              }
              || {FromId, FromPt} <- From],
    FinalDistances = case lists:any(fun({FromId, bad_geom}) -> true;
                                        (_) -> false
                                    end,
                                    FromTo) of
                         true -> [{FromId, bad_geom} || {FromId, _} <- FromTo];
                         false -> FromTo
                     end,
    lists:sort(FinalDistances).

-spec sub_max([{Distance :: float() | max_dist, ToId :: id()}]) -> [{Distance :: float(), ToId :: id()}] | bad_geom.
sub_max([]) -> [];
sub_max(Distances) ->
    case [D || {D, _} <- Distances, D =/= max_dist] of
        [] -> bad_geom;
        ValidDistances ->
            Max = lists:max(ValidDistances),
            lists:sort([{case D of max_dist -> Max; _ -> D end, Id} || {D, Id} <- Distances])
    end.
