%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(rf_alg).

-export([
         calc_stage1/4,
         check_quorum/3,
         calc_stage2/7,
         calc_stage3/5
        ]).

-include_lib("active_control/include/ac_api.hrl").

-spec calc_stage1(
        Deltas :: [{Delta :: float(), Timestamp :: integer() | undefined}],
        AgeCutoff :: integer(),
        Quorum :: {PercentLimit :: float(), CountLimit :: integer()},
        MeanShift :: float()
       ) -> ControlDelta :: no_quorum | float().
calc_stage1(Deltas, AgeCutoff, Quorum, MeanShift) ->
    case check_quorum(Deltas, AgeCutoff, Quorum) of
        no_quorum ->
            no_quorum;
        DeltaValues ->
            {Mean, StdDev} = mean_and_std(DeltaValues),
            Mean + MeanShift * StdDev
    end.

-spec check_quorum(
        Deltas :: [{Delta :: float(), Timestamp :: integer() | undefined}],
        AgeCutoff :: integer(),
        Quorum :: {PercentLimit :: float(), CountLimit :: integer()}
       ) -> no_quorum | [Delta :: float()].
% The number of usable (younger than AgeCutoff) deltas must be the greater of
% PercentLimit and CountLimit
% Said another way, quorum is PercentLimit, but at least CountLimit
check_quorum(Deltas, AgeCutoff, {PercentLimit, CountLimit}) ->
    NTotal = length(Deltas),
    Recent = [Delta || {Delta, TS} <- Deltas, is_integer(TS), TS >= AgeCutoff],
    NRecent = length(Recent),
    Nlimit = max(round(PercentLimit * NTotal), CountLimit),
    if
        NRecent >= Nlimit -> Recent;
        NRecent < Nlimit -> no_quorum
    end.

-spec calc_stage2(
        Delta :: float(),          % The output of stage1.  Adjusted mean of input error (not value).
        ErrorScale :: float(),     % How much to scale the error.
        Deadband :: float(),       % Smallest adjustment (PresentMean * ErrorScale) to apply
        TargetSat :: float(),      % Current target supply air temp (previous stage2 output)
        RespondingTo :: float(),   % Stage 1 output that triggered the previous adjustment
        EarlyAdjustThreshold :: float(), % Adjustments over this threshold should be made even if a previous one is already propagating
        Propagating :: boolean()   % Flag to indicate whether or not an adjustment is propagating
       ) ->
    hold | {new_target, Target :: float()}.
calc_stage2(Delta, ErrorScale, Deadband, TargetSat,
            RespondingTo, EarlyAdjustThreshold, Propagating) ->
    ScaledDelta = Delta * ErrorScale,
    case Propagating of
        true ->
            case abs(ScaledDelta) >= EarlyAdjustThreshold of
                true ->
                    NewS2 = TargetSat + ErrorScale * (Delta-RespondingTo),
                    {new_target, NewS2};
                false ->
                    hold
            end;
        false ->
            case abs(ScaledDelta) >= Deadband of
                true ->
                    NewS2 = TargetSat + ScaledDelta,
                    {new_target, NewS2};
                false ->
                    hold
            end
    end.

-spec calc_stage3(
        TargetSat :: float(),
        PresentSat :: float(),
        PresentRat :: float(),
        ValveControl :: cooling_control(),
        ValvePosition :: float()
       ) -> Stage3 :: float().
calc_stage3(TargetSat, PresentSat, PresentRat, ValveControl, ValvePosition) ->
    TargetSat1 = if
                     ValvePosition > 90.0 ->
                         max(PresentSat, TargetSat);
                     ValvePosition < 10.0 ->
                         min(PresentSat, TargetSat);
                     ValvePosition >= 10.0, ValvePosition =< 90.0 ->
                         TargetSat
                 end,
    Stage3 = case ValveControl of
                 supply ->
                     TargetSat1;
                 return ->
                     PresentRat - (PresentSat - TargetSat1)
             end,
    Stage3.

mean_and_std(Values) ->
    L = length(Values),
    Mean = lists:sum(Values) / L,
    Variance = lists:foldl(fun(X,Var) -> Var + math:pow(X-Mean,2) end, 0.0, Values) / L,
    {Mean, math:sqrt(Variance)}.

