-module(room_event).

-include_lib("active_control/include/ac_api.hrl").

%% API
-export([
         start_link/1,
         stop/1,
         relay_events/2,
         notify_stage2_change/3,
         notify_device_status_change/3,
         notify_device_setpoint_change/3,
         notify_strategy_status_change/3,
         notify_input_status_change/3,
         childspec/1
        ]).

-type room_event() :: {driver_lib:proc_key(), room_event_details()}.
-type room_event_type() :: device_status | device_setpoint | input_status | stage2 | strategy_status.
-type room_event_details() :: {device_status, device_status()} |
                              {device_setpoint, float()} |
                              {input_status, boolean()} |
                              {stage2, float()} |
                              {strategy_status, control_status()}.

-export_type([room_event/0, room_event_type/0]).

%%%===================================================================
%%% API
%%%===================================================================

-spec start_link(RoomId ::id()) -> {ok, pid()} | {error, {already_started, pid()}}.
start_link(RoomId) ->
    Key = driver_lib:proc_key(RoomId, none, event),
    gen_event:start_link({via, driver_lib, Key}).

-spec stop(Pid ::pid()) -> ok.
stop(Pid) ->
    gen_event:stop(Pid).

childspec(RoomId) ->
    [{
        driver_lib:proc_key(RoomId, none, event),
        {?MODULE, start_link, [RoomId]},
        transient,
        100,
        worker,
        [?MODULE]
    }].

-spec relay_events(RoomId :: id(), Fun :: fun((room_event()) -> ignored)) -> ok.
relay_events(RoomId, EventHandlerFun) ->
    Ref = driver_lib:lookup(event_key(RoomId)),
    gen_event_fun:add_sup_handler(Ref, self(), EventHandlerFun).

-spec notify_stage2_change(RoomId::id(), Id::driver_lib:proc_key(), Stage2::float()) -> ok.
notify_stage2_change(RoomId, Id, Stage2) ->
    notify(RoomId, Id, {stage2, Stage2}).

-spec notify_device_status_change(RoomId::id(), Id::driver_lib:proc_key(), Status::device_status()) -> ok.
notify_device_status_change(RoomId, Id, Status) ->
    notify(RoomId, Id, {device_status, Status}).

-spec notify_device_setpoint_change(RoomId::id(), Id::driver_lib:proc_key(), Setpoint::float()) -> ok.
notify_device_setpoint_change(RoomId, Id, Setpoint) ->
    notify(RoomId, Id, {device_setpoint, Setpoint}).

-spec notify_strategy_status_change(RoomId::id(), Id::driver_lib:proc_key(), Status::control_status()) -> ok.
notify_strategy_status_change(RoomId, Id, Status) ->
    notify(RoomId, Id, {strategy_status, Status}).

-spec notify_input_status_change(RoomId::id(), Id::driver_lib:proc_key(), Status::boolean()) -> ok.
notify_input_status_change(RoomId, Id, Status) ->
    notify(RoomId, Id, {input_status, Status}).

-spec notify(RoomId::id(), Id::driver_lib:proc_key(), EventDetails:: room_event_details()) -> ok.
notify(RoomId, Id, EventDetails) ->
    Ref = driver_lib:lookup(event_key(RoomId)),
    gen_event:notify(Ref, {Id, EventDetails}).

-spec event_key(RoomId::id()) -> driver_lib:proc_key().
event_key(RoomId) ->
    driver_lib:proc_key(RoomId, none, event).

