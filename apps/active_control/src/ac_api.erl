%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(ac_api).

-export([
    list_rooms/0,
    list_crahs/1,
    get_control_status/1,
    get_control_status/2,
    set_control_config/1,
    set_control_config/2,
    set_control_mode/3,
    set_manual_output/4,
    get_input_status/2,
    set_input_config/2,
    set_input_target/4
]).

% internal functions
-export([
    do_set_control_mode/2,
    set_strategy_mode/3,
    control_status_to_mode/1,
    status/3
]).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").
-include_lib("active_control/include/ac_constants.hrl").

-type api_error() ::
no_such_id
| target_out_of_range
| resource_not_configured
| no_inputs.

-spec list_rooms() -> [id()].
list_rooms() ->
    room_manager:list_rooms().

-spec list_crahs(id()) -> [id()] | {error, not_started}.
list_crahs(RoomId) when is_binary(RoomId) ->
    case room_manager:room_type(RoomId) of
        {ok, remote_control_room} ->
            remote_control_room:list_crahs(RoomId);
        {ok, raised_floor_room} ->
            raised_floor_room:list_crahs(RoomId);
        {error, not_started} = E ->
            E
    end.

%%% Aireo-required API calls
% Get CRAH / VFD device status (combo of device / strategy)
% Get RF Temp / RF Pressure / Manual strategy status
% Set RF Temp / RF Pressure / Manual config
% Get input status
% Set input config

-spec get_control_status(id()) ->
    {ok, raised_floor_room_status() | remote_control_room_status()}
    | {error, api_error()}.
get_control_status(<<"ROOM:", _/binary>> = Id) ->
    case build_room_status(Id) of
        {error, not_started} ->
            {error, no_such_id};
        {ok, Status} -> {ok, Status}
    end.

-spec get_control_status(id(), id()) ->
    {ok, raised_floor_crah_status()
    | remote_control_crac_status()
    | remote_control_crah_status()}
    | {error, api_error()}.
get_control_status(<<"ROOM:", _/binary>> = RoomId, <<"CRAC:", _/binary>> = CrahId) ->
    case build_crah_status(RoomId, CrahId) of
        {error, not_started} ->
            {error, no_such_id};
        {error, no_such_id} = E ->
            E;
        {ok, Status} -> {ok, Status}
    end.

-spec set_control_config(raised_floor_balancer_strategy()) -> ok | {error, api_error()}.
set_control_config(#raised_floor_balancer_strategy{id=RoomId, resource=Resource, aggressiveness=A}) ->

    %TODO: Change to use the strategy type that is correct for the crah
    on_success(
        begin
            room_node_call(RoomId,
                           rf_balancer_strategy,
                           set_aggressiveness,
                           [RoomId, Resource, A]),
            room_node_call(RoomId,
                           rf_input,
                           set_aggressiveness,
                           [RoomId, Resource, A])
        end,
        fun() ->
            set_child_property_by_resource(RoomId,
                                           es_model_info:raised_floor_room_strategy_types(Resource),
                                           Resource,
                                           <<"aggressiveness">>,
                                           atom_to_binary(A, utf8))
        end).

-spec set_control_mode(RoomId :: id(),
                       CrahId :: id(),
                       Mode :: control_mode()
                      ) -> ok | {error, api_error()}.
set_control_mode(RoomId, CrahId, Mode)
    when is_binary(RoomId),
         is_binary(CrahId),
         is_atom(Mode)
    ->
    room_node_call(RoomId,
                   ?MODULE,
                   do_set_control_mode,
                   [CrahId, Mode]).

do_set_control_mode(CrahId, Mode)
    ->
    % When this function is called, it will be called on the node the room is running on,
    % so there is no need to use room_node_call().
    driver_lib:strategy_running(CrahId, pressure)
    andalso (ok = set_strategy_mode(CrahId, pressure, Mode)),
    driver_lib:strategy_running(CrahId, temperature)
    andalso (ok = set_strategy_mode(CrahId, temperature, Mode)),
    ok.

set_strategy_mode(CrahId, Resource, Mode)
    when
    is_binary(CrahId),
    is_atom(Mode)
    ->

    on_success(catch driver_lib:call(
        driver_lib:strategy_key(CrahId, Resource),
        set_mode,
        [Mode]),
               fun() ->
                   %TODO: Change to use the strategy type that is correct for the crah
                   set_child_property_by_resource(CrahId,
                                                  es_model_info:crah_strategy_types(Resource),
                                                  Resource,
                                                  <<"mode">>,
                                                  atom_to_binary(Mode, utf8))
               end).

-spec set_manual_output(
                         RoomId :: id(),
                         StrategyId :: id(),
                         Resource :: controlled_resource(),
                         Value :: float()
                       ) -> ok | {error, api_error()}.
set_manual_output(RoomId, StrategyId, Resource, Value)
    when
    is_binary(RoomId),
    is_binary(StrategyId),
    is_atom(Resource),
    is_float(Value)
    ->
    on_success(room_node_call(RoomId,
                              driver_lib,
                              call,
                              [driver_lib:proc_key(StrategyId, Resource, strategy),
                               set_manual_output,
                               [Value]]),
               fun() ->
                   set_child_property_by_resource(StrategyId,
                                                  es_model_info:crah_strategy_types(Resource),
                                                  Resource,
                                                  <<"manualOutput">>,
                                                  Value)
               end).

-spec set_control_config(RoomId :: id(),
                         raised_floor_crah_strategy()
                         | raised_floor_vfd_strategy()
                         | remote_control_strategy()
                        ) -> ok | {error, api_error()}.
set_control_config(RoomId, Config = #raised_floor_crah_strategy{id=CrahId, resource=R}) ->
    K = driver_lib:proc_key(CrahId, R, strategy),
    room_node_call(RoomId, driver_lib, call, [K, set_config, [Config]]);
set_control_config(RoomId, Config = #raised_floor_vfd_strategy{id=CrahId, resource=R}) ->
    K = driver_lib:proc_key(CrahId, R, strategy),
    room_node_call(RoomId, driver_lib, call, [K, set_config, [Config]]);
set_control_config(RoomId, Config = #remote_control_strategy{id=CrahId, resource=R}) ->
    K = driver_lib:proc_key(CrahId, R, strategy),
    room_node_call(RoomId, driver_lib, call, [K, set_config, [Config]]).

% TODO this should be get_input_status(id(), resource(), id()) since a single ID
% might (in the future) have multiple resources.
-spec get_input_status(id(), id()) -> {ok, input_status()} | {error, api_error()}.
get_input_status(RoomId, Id = <<"PRESSURE:", _/binary>>) ->
    get_input_status(RoomId, pressure, Id);
% TODO: this doesn't support things like vertical_temp, generic_temp, ...
get_input_status(RoomId, Id = <<"RACK:", _/binary>>) ->
    get_input_status(RoomId, temperature, Id).

get_input_status(RoomId, Resource, InputId) ->
    case room_manager:room_type(RoomId) of
        {ok, remote_control_room} ->
            {error, no_inputs};
        {ok, raised_floor_room} ->
            case room_node_call(RoomId,
                                rf_input,
                                get_input_status,
                                [RoomId, Resource, InputId]) of
                {error, {'EXIT', {noproc, _}}} ->
                    {error, resource_not_configured};
                V ->
                    V
            end
    end.

-spec set_input_config(id(), single_input()) -> ok | {error, api_error()}.
set_input_config(RoomId, Config = #single_input{}) ->
    #single_input{id=Id, resource=R, target=T, enabled=E} = Config,
    on_success(room_node_call(RoomId,
                              rf_input,
                              set_input_config,
                              [RoomId, Config]),
               fun() ->
                   set_child_property_by_resource(Id, es_model_info:input_types(R), R, <<"target">>, T),

                   Status = case E of
                                true -> ?STATUS_OK;
                                false -> ?STATUS_DISABLED
                            end,
                   % TODO: rename "status" field to "enabled" with saner
                   % boolean logic
                   set_child_property_by_resource(Id, es_model_info:input_types(R), R, <<"status">>, Status)
               end).

-spec set_input_target(id(), id(), controlled_resource(), float()) ->
    ok | {error, api_error()}.
set_input_target(RoomId, InputId, Resource, Target) ->
    on_success(room_node_call(RoomId,
                              rf_input,
                              set_input_target,
                              [RoomId, InputId, Resource, Target]),
               fun() ->
                   set_child_property_by_resource(InputId,
                                                  es_model_info:input_types(Resource),
                                                  Resource,
                                                  <<"target">>,
                                                  Target)
               end).


%% Internal functions

%% Find the 1 and only 1 child whose (type matches one in the Types list
%% and whose resource property matches the Resource passed in)
%% and set that child's property
-spec set_child_property_by_resource(Parent :: id(),
                                     Types :: [es_model_info:input_types() | es_model_info:strategy_types()],
                                     Resource :: 'pressure' | 'temperature',
                                     Key :: binary(),
                                     Value :: any()) -> ok.
set_child_property_by_resource(Parent, Types, Resource, Key, Value) ->
    Children = lists:flatten([esapi:get_children(esapi:to(Parent), Type) || Type <- Types]),
    lager:debug("Found ~p children of type ~p", [length(Children), Types]),
    [ResourceChild] = lists:filter(fun(Elem) ->
        ResourceBinary = esapi:get_property_value(Elem, <<"resource">>),
        Resource =:= binary_to_existing_atom(ResourceBinary, utf8)
                                   end,
                                   Children),
    esapi:set_property_value(ResourceChild, Key, Value).

% Call the room node to build the room status from the model
-spec build_room_status(id()) ->
    {ok, raised_floor_room_status() | remote_control_room_status()}
    | {error, not_started}.
build_room_status(RoomId) ->
    case room_manager:room_type(RoomId) of
        {error, _} = Err -> Err;
        {ok, raised_floor_room} ->
            raised_floor_room:room_status(RoomId);
        {ok, remote_control_room} ->
            remote_control_room:room_status(RoomId)
    end.

% Call the room node to build the crah status
-spec build_crah_status(id(), id()) ->
    {ok, raised_floor_crah_status() | remote_control_crac_status() | remote_control_crah_status()}
    | {error, not_started | no_such_id}.
build_crah_status(RoomId, CrahId) ->
    case room_manager:room_type(RoomId) of
        {error, _} = Err -> Err;
        {ok, raised_floor_room} ->
            raised_floor_room:crah_status(RoomId, CrahId);
        {ok, remote_control_room} ->
            remote_control_room:crah_status(RoomId, CrahId)
    end.

control_status_to_mode(failsafe) -> automatic;
control_status_to_mode(S) -> S.

status(Id, R, T) ->
    driver_lib:call(driver_lib:proc_key(Id, R, T), get_status, []).

on_success(Result, F) ->
    case Result of
        {error, _R} = Error -> Error;
        Ok ->
            try
                F()
            catch
                E:R ->
                    lager:error("ES Model update function failed {~p,~p}\n~p",
                                [E, R, erlang:get_stacktrace()])
            end,
            Ok
    end.

%% refactor this into something like room_node_call(RoomId, K, F, Args)
-spec room_node_call(RoomId :: id(),
                     Module :: atom(),
                     Function :: atom(),
                     Args :: [any()]) ->
                        {error, R :: any()} | term().
room_node_call(RoomId, M, F, A) ->
    case room_manager:room_node(RoomId) of
        {ok, RoomNode} ->
            case rpc:call(RoomNode, M, F, A) of
                {badrpc, R} -> {error, R};
                Result -> Result
            end;
        E -> E
    end.

