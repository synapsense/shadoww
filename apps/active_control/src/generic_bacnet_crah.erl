-module(generic_bacnet_crah).
-behaviour(crah_fsm).

-export([properties/0,read_setpoint/1,read_comm/1,read_standby/1,read_valve/1,read_return/1,read_supply/1,write_setpoint/2]).
-export([read_change_filters/1, read_general/1, write_clear_alarm/1]).
-import(shadoww_lib, [getprop/2]).
-import(io_handler, [addr_b/1,get_net/1,get_ip/1,get_devid/1]).

properties() ->
	% TODO: flag these with r,w,rw to prevent property cycles and race conditions.
	[
		{"ip", r},
		{"devid", r},
		{"network", r},
		{"setpointRead", r},
		{"setpointWrite", r},
		{"ratRead", r},
		{"satRead", r},
		{"valveRead", r},
		{"standbyRead", r},
		{"commRead", r}
	]
	++
	drv_handler:crah_alarm_properties()
	.

read_comm(Props) ->
	io_handler:read_comm("crah.commread", get_net(Props),get_ip(Props), get_devid(Props), getprop("commRead", Props))
	.

read_standby(Props) ->
	io_handler:read_standby("crah.standbyread", get_net(Props),get_ip(Props), get_devid(Props), getprop("standbyRead", Props))
	.

read_valve(Props) ->
	io_handler:read_property("crah.valveread", get_net(Props),get_ip(Props), get_devid(Props), getprop("valveRead", Props))
	.
	
read_return(Props) ->
	io_handler:read_property("crah.ratread", get_net(Props),get_ip(Props), get_devid(Props), getprop("ratRead", Props))
	.

read_supply(Props) ->
	io_handler:read_property("crah.satread", get_net(Props),get_ip(Props), get_devid(Props), getprop("satRead", Props))
	.

read_setpoint(Props) ->
	io_handler:read_setpoint("crah.setpointread", get_net(Props),get_ip(Props), get_devid(Props), getprop("setpointRead", Props), getprop("setpoint", Props))
	.

write_setpoint(Props,Setpoint) ->
	io_eval:eval("crah.setpointwrite", getprop("setpointWrite", Props), addr_b(Props) ++ [{'Value', Setpoint}]),
	Setpoint
	.

%%Alerts I/O
read_change_filters(Props) ->
	io_handler:read_alarm("crah.changefiltersalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("changeFiltersAlarmRead", Props))
	.
read_general(Props) ->
	io_handler:read_alarm("crah.generalalarmread", get_net(Props), get_ip(Props), get_devid(Props), getprop("generalAlarmRead", Props))
	.

write_clear_alarm(Props) ->
	case  getprop("clearAlarmWrite", Props) of
		[] -> ok;
		WriteClear -> io_eval:eval("crah.clearalarmwrite", WriteClear, addr_b(Props) ++ [{'Value', 0}])
   	end
	.
