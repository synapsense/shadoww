%% @author Dmitry Grudzinskiy <dmitryg@synapsense.com>
%% @copyright 2012 Dmitry Grudzinskiy
%% @version 1.0.0
%% @doc
%% Module contains structures and functions for processing evaporator sensors (halleys) values that help controlling
%% CRAC units.
%% @end
-module(evaps).

-include("ac_constants.hrl").

-export([new/2, add_value/6, action/1, conditions/1, values/1, residuals/1]).

-export_type([evap_condition/0, action/0, evaps/0]).

-record(evap_value, {
		value :: float(),
		timestamp :: integer()
	}).
-type evap_value() :: #evap_value{}.

-type evap_condition() :: all_below | all_above | above_below | below_abs_min.
-type action() :: raise_now | raise_now_low_temp | reject_decrease | accept_all.

-record(evap_state, {
		id :: any(),
		values = [] :: [evap_value()],
		residuals = [] :: [float()],
		condition = all_below :: evap_condition()
	}).
-type evap_state() :: #evap_state{}.

-record(evaps, {
		evap1 :: evap_state(),
		evap2 :: evap_state(),
		action = accept_all :: action()
	}).
-type evaps() :: #evaps{}.


%% Public API

-spec new(any(), any()) -> evaps().
%% @doc
%% Returns new evaps instance with two evaporators with specified identifiers.
%% @end
new(EvapId1, EvapId2) ->
	#evaps{
		evap1 = #evap_state{id = EvapId1},
		evap2 = #evap_state{id = EvapId2}
	}
	.

-spec add_value(evap_value(), evaps(), integer(), integer(), float(), float()) -> evaps().
%% @doc
%% Updates evaps conditions and action based on new reported value.
%%
%% The following list of actions is performed here:
%% <li>New value is added to the value list of the reported evaporator, the oldest value is discarded from the list if it exceeds RegressionWindow.</li>
%% <li>New residual error is calculated based on the updated valuesand added to the residuals list. The oldest residual error is discarded
%% if the list exceeds ResidualWindow.</li>
%% <li>If the residual list size of the reported evaporator has reached ResidualWindow then evaporator condition is updated.</li>
%% <li>Action is recalculated based on new value and evaporators conditions.</li>
%% @end
add_value({ValId, Value, Timestamp}, Evaps = #evaps{evap1 = #evap_state{id = Evap1Id}}, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp)
	when ValId == Evap1Id->
	UpdatedEvaps = Evaps#evaps{evap1 = process_evap_value(#evap_value{value = Value, timestamp = Timestamp}, Evaps#evaps.evap1, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp)},
	update_action(UpdatedEvaps)
	;
add_value({ValId, Value, Timestamp}, Evaps = #evaps{evap2 = #evap_state{id = Evap2Id}}, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp)
	when ValId == Evap2Id ->
	UpdatedEvaps = Evaps#evaps{evap2 = process_evap_value(#evap_value{value = Value, timestamp = Timestamp}, Evaps#evaps.evap2, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp)},
	update_action(UpdatedEvaps)
	.

-spec action(evaps()) -> action().
%% @doc
%% Returns current evaporators action.
%% @end
action(#evaps{action = Action}) ->
	Action
	.

-spec conditions(evaps()) -> {evap_condition(), evap_condition()}.
%% @doc
%% Returns tuple of both evaporators conditions.
%% @end
conditions(#evaps{evap1 = #evap_state{condition = Condition1}, evap2 = #evap_state{condition = Condition2}}) ->
	{Condition1, Condition2}
	.

-spec values(evaps()) -> {[float()], [float()]}.
%% @doc
%% Returns tuple of both evaporators values.
%% @end
values(#evaps{evap1 = #evap_state{values = Values1}, evap2 = #evap_state{values = Values2}}) ->
	{[Value || #evap_value{value = Value} <- Values1], [Value || #evap_value{value = Value} <- Values2]}
	.

-spec residuals(evaps()) -> {[float()], [float()]}.
%% @doc
%% Returns tuple of both evaporators residual errors.
%% @end
residuals(#evaps{evap1 = #evap_state{residuals = Residuals1}, evap2 = #evap_state{residuals = Residuals2}}) ->
	{Residuals1, Residuals2}
	.

%% Private functions

-spec process_evap_value({any(), float(), integer()}, evap_state(), integer(), integer(), float(), float()) -> evap_state().
%% @doc
%% Adds new reported value to a specific evaporator, calculates new residual error and evaporator condition (all_bellow, all_above, above_bellow).
%% @end
process_evap_value(NewValue, Evap = #evap_state{values = Values, residuals = Residuals}, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp) ->

	% add value to the list
	NewValues =  lists:sublist(
		lists:sort(fun(#evap_value{timestamp = Timestamp1}, #evap_value{timestamp = Timestamp2}) -> Timestamp1 > Timestamp2 end, [NewValue | Values]),
		RegressionWindow),

	% update residual errors list
	NewResiduals = update_residuals(NewValues, Residuals, RegressionWindow, ResidualWindow),

	% update condition
	update_evap_condition(NewValue, Evap#evap_state{values = NewValues, residuals = NewResiduals}, ResidualWindow, ResidualThreshold, MinAbsTemp)
	.

-spec update_residuals([evap_value()], [float()], integer(), integer()) -> [float()].
%% @doc
%% Calculated residual error based on the value list and adds it to the residual list.
%% @end
update_residuals(Values, Residuals, RegressionWindow, ResidualWindow) when RegressionWindow == length(Values) ->
	{Xs, Ys} = lists:foldl(fun(#evap_value{value = Value, timestamp = Timestamp}, {XsIn, YsIn}) ->
								{[Timestamp | XsIn], [Value | YsIn]} end, {[],[]}, Values),

	lists:sublist([lstsq:ssr(Xs, Ys) | Residuals], ResidualWindow)
	;
update_residuals(Values, Residuals, RegressionWindow, _ResidualWindow) when RegressionWindow > length(Values) ->
	Residuals
	.

-spec update_evap_condition(evap_value(), evap_state(), integer(), float(), float()) -> evap_state().
%% @doc
%% Updates evaporator condition based on the residual errors and reported value.
%% @end
update_evap_condition(#evap_value{value = Value}, Evap, _ResidualWindow, _ResidualThreshold, MinAbsTemp)
	when Value < MinAbsTemp ->
	Evap#evap_state{condition = below_abs_min}
	;
update_evap_condition(_NewValue, Evap = #evap_state{residuals = Residuals}, ResidualWindow, ResidualThreshold, _MinAbsTemp)
	when ResidualWindow == length(Residuals) ->
	{Below, Above} = lists:partition(fun(SSR) -> SSR < ResidualThreshold end, Residuals),

	Condition = case {length(Below), length(Above)} of
		{_, 0} -> all_below;
		{0, _} -> all_above;
		{_, _} -> above_below
	end,
	Evap#evap_state{condition = Condition}
	;
update_evap_condition(_NewValue, Evap = #evap_state{residuals = Residuals}, ResidualWindow, _ResidualThreshold, _MinAbsTemp)
	when ResidualWindow > length(Residuals) ->
	Evap
	.


-spec update_action(evaps()) -> evaps().
%% @doc
%% Updates resulting action (raise, reject_decrease, accept_all) based on new value and both evaporators conditions.
%% @end
update_action(Evaps = #evaps{evap1 = #evap_state{condition = Condition1}, evap2 = #evap_state{condition = Condition2}}) ->
	Action = case {Condition1, Condition2} of
		{all_below, all_below} -> accept_all;
		{all_above, _} -> raise_now;
		{_, all_above} -> raise_now;
		{below_abs_min, _} -> raise_now_low_temp;
		{_, below_abs_min} -> raise_now_low_temp;
		{_, _} -> reject_decrease
	end,
	Evaps#evaps{action = Action}
	.
