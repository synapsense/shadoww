
-module(ac_api_json_erl).

-behaviour(gen_server).

-export([
		start_link/0,
		start/0,
		stop/0,
		call/1
	]).

-export([
		init/1,
		handle_call/3,
		handle_cast/2,
		handle_info/2,
		terminate/2,
		code_change/3
	]).

-define(SERVER, ?MODULE).


start_link() -> gen_server:start_link({local, ?SERVER}, ?MODULE, no_state, []) .

start() -> gen_server:start({local, ?SERVER}, ?MODULE, no_state, []) .

stop() -> gen_server:call(?SERVER, stop) .

call(Json) ->
	gen_server:call(?SERVER, {json_call, Json}).

%% API
%% This process handles API calls from Java nodes, so they are all messages, not Erlang function invocations.
%% To help, every message is expected to be of the form {ac_api, From :: pid(), Call}
%% Where Call is {Function, Args...}
%%
%% It also registers ac_api_json to handle JSON over HTTP requests

init(Config) ->
	erlang:process_flag(trap_exit, true),
	lager:info("starting"),
	ac_api_json:register_rpc(),
	cluster_conf_api_json:register_rpc(),
	{ok, Config}
	.

handle_call(stop, _, State) ->
	{stop, normal, ok, State}
	;
handle_call({call, Json}, _From, State) ->
	{reply, json_rpc:json_call(Json), State}
	;
handle_call(Message, From, State) ->
	lager:error("Unexpected handle_call. message: ~p, from: ~p, state: ~p", [Message, From, State]),
	{reply, huh, State}
	.

handle_cast(Request, State) ->
	lager:error("Unexpected handle_cast. request: ~p, state: ~p", [Request, State]),
	{noreply, State}
	.

handle_info({'EXIT', Pid, Reason}, State) ->
	lager:warning("Link to ~p at ~p broke: ~p", [Pid, node(Pid), Reason]),
	{noreply, State}
	;
handle_info({link, From}, State) ->
	link(From),
	From ! {linked, self()},
	lager:info("Linked to ~p at ~p", [From, node(From)]),
	{noreply, State};
handle_info({ac_api_json, From, JsonRpc}, State) ->
	spawn(fun() -> From ! json_rpc:json_call(JsonRpc) end),
	{noreply, State};
handle_info(Info, State) ->
	lager:error("Unexpected handle_info. info: ~p, state: ~p", [Info, State]),
	{noreply, State}
	.

terminate(Reason, State) ->
	lager:warning("terminating. reason: ~p, state: ~p", [Reason, State])
	.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}
	.

