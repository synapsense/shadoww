
-module(rf_input).

%% interface module for input configuration: enable/disable, sensor ID, target value.
%%
%% 1. Receive sensor sample
%% 2. Calculate input delta
%% 3. Cache state & update ES
%% 4. Lookup all AoEs that the input belongs to
%% 5. Calculate Stage1 value for each AoE
%% 6. Push new Stage1 value to each device strategy
%%

-behaviour(gen_server).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-include_lib("stdlib/include/ms_transform.hrl").

%% API
-export([
    start_link/4,
    stop/1,
    new_sample/2,
    new_aoe/3,
    set_input_config/2,
    get_input_config/3,
    set_input_target/4,
    get_all_input_config/2,
    get_aoe_status/3,
    get_input_status/3,
    get_all_input_status/2,
    set_aggressiveness/3,
    childspec/4
    ]).

%% gen_server callbacks
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
    ]).

-record(state, {
          room_id :: id(),
          resource :: controlled_resource(),
          proc_key :: driver_lib:proc_key(),
          tracker_tid :: ets:tid(),
          aggressiveness :: aggressiveness(),
          aoe :: [{Crah :: id(), Input :: id()}]
         }).

-record(input_tracker, {
          input_id :: '_' | '$1' | id(),
          sensor_id :: '_' | '$2' | id(),
          sample :: '_' | float(),
          sample_time :: '_' | integer(),
          target :: '_' | float(),
          delta :: '_' | float()
         }).

-define(SENSOR_EVENT, sensor_event).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(RoomId, Resource, Aggressiveness, InputConfig) ->
    gen_server:start_link(?MODULE, {RoomId, Resource, Aggressiveness, InputConfig}, []).

stop(Pid) ->
    gen_server:call(Pid, stop).

% called by internal sample forwarder
new_sample(Pid, Sample) ->
    gen_server:cast(Pid, {new_sample, Sample}).

-spec new_aoe(RoomId :: id(),
              Resource :: controlled_resource(),
              AoE :: [{id(), id()}]) ->
    ok | {error, Reason :: term()}.
new_aoe(RoomId, Resource, AoE) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, {new_aoe, AoE}).

% handle things like enable/disable, set target value, set sensor
-spec set_input_config(RoomId :: id(),
                       Config :: single_input()) ->
    ok | {error, Reason :: term()}.
set_input_config(RoomId, Config = #single_input{resource=Resource}) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, {set_config, Config}).

-spec get_input_config(RoomId :: id(),
                       Resource :: controlled_resource(),
                       InputId :: id()) -> single_input();
                      (RoomId :: id(),
                       Resource :: controlled_resource(),
                       InputIds :: [id()]) -> [single_input()].
get_input_config(RoomId, Resource, InputIds) when is_list(InputIds) ->
    [get_input_config(RoomId, Resource, InputId) || InputId <- InputIds];
get_input_config(_RoomId, Resource, InputId) ->
    {ok, V} = process_cache:load_properties(input_key(InputId, Resource)),
    V.

-spec set_input_target(RoomId :: id(),
                       InputId :: id(),
                       Resource :: controlled_resource(),
                       Target :: float()) ->
    ok | {error, Reason :: term()}.
set_input_target(RoomId, InputId, Resource, Target) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, {set_target, InputId, Target}).

-spec get_all_input_config(RoomId :: id(),
                           Resource :: controlled_resource()) ->
    [single_input()].
get_all_input_config(RoomId, Resource) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    Ids = gen_server:call(Pid, input_ids),
    get_input_config(RoomId, Resource, Ids).

-spec get_aoe_status(RoomId :: id(),
                     Resource :: controlled_resource(),
                     CrahId :: id()) ->
    [Info :: {aoe_target, float() | null} |
             {aoe_actual, float() | null} |
             {inputs_status, [input_status()]}].
get_aoe_status(RoomId, Resource, CrahId) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, {aoe_status, CrahId}).

-spec get_input_status(RoomId :: id(),
                       Resource :: controlled_resource(),
                       InputId :: id()) -> input_status().
get_input_status(RoomId, Resource, Id) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, {input_status, Id}).

-spec get_all_input_status(RoomId :: id(),
                           Resource :: controlled_resource()) ->
    [input_status()].
get_all_input_status(RoomId, Resource) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, all_input_status).

-spec set_aggressiveness(RoomId :: id(),
                         Resource :: controlled_resource(),
                         Aggressiveness :: aggressiveness()) -> ok.
set_aggressiveness(RoomId, Resource, Aggressiveness) ->
    Pid = driver_lib:lookup(driver_lib:proc_key(RoomId, Resource, input)),
    gen_server:call(Pid, {aggressiveness, Aggressiveness}).

childspec(RoomId, Resource, Aggressiveness, Inputs) ->
    {
     driver_lib:proc_key(RoomId, Resource, input),
     {?MODULE, start_link, [RoomId, Resource, Aggressiveness, Inputs]},
     transient,
     1000,
     worker,
     [?MODULE]
    }.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init({RoomId, Resource, Aggressiveness, InputConfig0}) ->
    K = driver_lib:reg(driver_lib:proc_key(RoomId, Resource, input)),
    InputConfig = [process_cache:initial_config(driver_lib:proc_key(Id, Resource, input), Input)
     || Input = #single_input{id = Id} <- InputConfig0],

    Table = ets:new(tracker, [set, protected, {keypos, #input_tracker.input_id}]),
    Trackers = [tracker_for_single(S) || S <- InputConfig],
    ets:insert_new(Table, Trackers),

    gen_key_event:add_sup_handler(?SENSOR_EVENT, sensor_ids(Table), sample_forwarder()),

    % FIXME: need to store state in process_cache so that aggressiveness is preserved
    % across a failover
    {ok, #state{
            room_id = RoomId,
            resource = Resource,
            aggressiveness = Aggressiveness,
            proc_key = K,
            tracker_tid = Table,
            aoe = []
            }}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({set_config, NewConfig}, _From,
            State = #state{room_id = RoomId, tracker_tid = Table}) ->
    Key = input_key(NewConfig),
    % TODO: check that Key is in Keys?
    {ok, OldConfig} = process_cache:load_properties(Key),
    process_cache:update_properties(Key, NewConfig),
    % FIXME: not checking for location changes!
    % as of release 7.0, location changes without
    % complete reconfigurations are not possible,
    % therefore don't need to be handled.
    % Shouldn't be too hard, just fire off an event and recalc all distances and AoEs
    SensorChanged = OldConfig#single_input.sensor_id =/= NewConfig#single_input.sensor_id,
    TargetChanged = OldConfig#single_input.target =/= NewConfig#single_input.target,

    InputId = NewConfig#single_input.id,

    if
        TargetChanged ->
            update_target(Table, InputId, NewConfig#single_input.target);
        true -> ok
    end,

    if
        SensorChanged ->
           gen_key_event:swap_handler(
             ?SENSOR_EVENT,
             sample_forwarder(),
             sensor_ids(Table),
             sample_forwarder()),
           update_sensor_id(Table, InputId, NewConfig#single_input.sensor_id);
        true -> ok
    end,

    room_event:notify_input_status_change(RoomId,
                                             Key,
                                             NewConfig#single_input.enabled),

    {reply, ok, State};
handle_call({set_target, Id, Target}, _From,
            State = #state{tracker_tid = Table,
                           aoe = AoE,
                           room_id = RoomId,
                           aggressiveness = Aggressiveness,
                           resource = Resource}) ->
    Key = driver_lib:proc_key(Id, Resource, input),
    {ok, Config} = process_cache:load_properties(Key),
    NewConfig = Config#single_input{target = Target},
    process_cache:update_properties(Key, NewConfig),
    update_target(Table, Id, Target),
    CRAHs = crahs_for_input(Id, AoE),
    [update_stage1(deltas(Table, CrahId, AoE),
                   CrahId,
                   Resource,
                   Aggressiveness,
                   RoomId)
     || CrahId <- CRAHs],
    {reply, ok, State};
handle_call(input_ids, _From, State = #state{tracker_tid = Table}) ->
    Ids = ets:select(Table, ets:fun2ms(fun(#input_tracker{input_id=Id}) -> Id end)),
    {reply, Ids, State};
handle_call({aoe_status, CrahId}, _From,
            State = #state{
                       room_id = RoomId,
                       resource = Resource,
                       aggressiveness = Aggressiveness,
                       aoe = AoE,
                       tracker_tid = Table
                      }) ->
    InputIds = inputs_for_crah(CrahId, AoE),
    Trackers = [tracker(InputId, Table) || InputId <- InputIds],
    Reply = aoe_status(CrahId, Trackers, RoomId, Resource, Aggressiveness),
    {reply, Reply, State};
handle_call({input_status, Id}, _From,
            State = #state{
                       resource = Resource,
                       tracker_tid = Table
                      }) ->
    try
        {reply, {ok, input_status(tracker(Id, Table), Resource)}, State}
    catch
        error:{badmatch, _} ->
            {reply, {error, {no_input, Id}}, State}
    end;
handle_call(all_input_status, _From,
            State = #state{
                       resource = Resource,
                       tracker_tid = Table
                      }) ->
    Trackers = ets:tab2list(Table),
    {reply, [input_status(Tracker, Resource) || Tracker <- Trackers], State};
handle_call({new_aoe, AoE}, _From, State) ->
    {reply, ok, State#state{aoe = AoE}};
handle_call({aggressiveness, Aggressiveness}, _From, State) ->
    {reply, ok, State#state{aggressiveness = Aggressiveness}};
handle_call(stop, _From, State = #state{proc_key = K}) ->
    process_cache:delete_properties(K),
    {stop, normal, stop, State};
handle_call(R, _From, State) ->
    lager:warning("unexpected handle_call(~p)", [R]),
    {reply, huh, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast({new_sample, Sample},
            State = #state{
                       room_id = RoomId,
                       resource = Resource,
                       aggressiveness = Aggressiveness,
                       tracker_tid = Table,
                       aoe = AoE
                      }) ->
    % From the sample, find the input ID
    % Update the input with new delta & TS
    % Find out which AoEs the input belongs to
    % Recalculate all AoEs and push to device drivers
    case input_for_sensor(Table, sample:id(Sample)) of
        none ->
            lager:warning("unexpected sensor sample ~p", [Sample]);
        InputId ->
            update_sample(Table, InputId, Sample),
            CRAHs = crahs_for_input(InputId, AoE),
            [update_stage1(deltas(Table, CrahId, AoE),
                           CrahId,
                           Resource,
                           Aggressiveness,
                           RoomId)
             || CrahId <- CRAHs]
    end,
    {noreply, State};
handle_cast(Msg, State) ->
    lager:warning("unexpected handle_cast(~p)", [Msg]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(Info, State) ->
    lager:warning("unexpected handle_info(~p)", [Info]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

tracker_for_single(#single_input{id=Id,sensor_id=Sid,target=T}) ->
    #input_tracker{
       input_id = Id,
       sensor_id = Sid,
       target = T
      }.

input_key(#single_input{id=Id,resource=R}) -> input_key(Id, R).
input_key(Id, R) -> driver_lib:proc_key(Id, R, input).

sample_forwarder() ->
    Self = self(),
    fun(_Key, Sample) -> ?MODULE:new_sample(Self, Sample) end.

sensor_ids(Table) ->
    lists:usort(ets:foldl(fun(#input_tracker{sensor_id=Id}, L) ->
                                  [Id|L]
                          end,
                          [],
                          Table)).

update_target(Table, Id, Target) ->
    [Tracker = #input_tracker{sample=Value}] = ets:lookup(Table, Id),
    NewDelta = case Value of
                   undefined -> undefined;
                   Value when is_float(Value) ->
                       Target - Value
               end,
    ets:insert(Table, Tracker#input_tracker{target=Target,delta=NewDelta}).

update_sensor_id(Table, Id, SensorId) ->
    ets:update_element(Table, Id, {#input_tracker.sensor_id, SensorId}).

update_sample(Table, Id, Sample) ->
    Value = sample:value(Sample),
    TS = sample:timestamp(Sample),
    [Tracker] = ets:lookup(Table, Id),
    Delta = Tracker#input_tracker.target - Value,
    ets:insert(Table, Tracker#input_tracker{sample=Value,
                                            sample_time=TS,
                                            delta=Delta}).

-spec input_for_sensor(ets:tid(), id()) -> id() | none.
input_for_sensor(Table, SensorId) ->
    MatchSpec = ets:fun2ms(
                  fun(#input_tracker{input_id=InputId, sensor_id=Id})
                        when Id =:= SensorId -> InputId
                  end),
    case ets:select(Table, MatchSpec) of
        [] -> none;
        [InputId] -> InputId
    end.

crahs_for_input(_, []) ->
    [];
crahs_for_input(InputId, AoE) ->
    [CrahId || {CrahId, Id} <- AoE, Id =:= InputId].

inputs_for_crah(_, []) ->
    [];
inputs_for_crah(CrahId, AoE) ->
    [InputId || {Id, InputId} <- AoE, Id =:= CrahId].

update_stage1(AllDeltas, CrahId, Resource, Aggressiveness, RoomId) ->
    {Quorum, MeanShift, AgeCutoff} = config_settings(Resource,
                                                     Aggressiveness,
                                                     RoomId,
                                                     CrahId),
    case rf_alg:calc_stage1(AllDeltas, AgeCutoff, Quorum, MeanShift) of
        no_quorum ->
            lager:warning("quorum not met for ~p, ~p, ~p",
                          [CrahId, Resource, Aggressiveness]);
        AoeDelta when is_float(AoeDelta) ->
            driver_lib:call(driver_lib:proc_key(CrahId, Resource, strategy),
                            set_stage1,
                            [AoeDelta, Aggressiveness])
    end.

deltas(Table, CrahId, AoE) ->
    InputIds = inputs_for_crah(CrahId, AoE),
    Trackers = [hd(ets:lookup(Table, InputId)) || InputId <- InputIds],
    [{Delta, SampleTS} ||
     #input_tracker{delta=Delta,sample_time=SampleTS} <- Trackers].

tracker(Id, Table) ->
    [T] = ets:lookup(Table, Id),
    T.

aoe_status(CrahId, Trackers, RoomId, Resource, Aggressiveness) ->
    Statuses = [input_status(Tracker, Resource) || Tracker <- Trackers],
    AllDeltas = [{Delta, TS} || #input_tracker{delta=Delta,sample_time=TS} <- Trackers],

    {Quorum, MeanShift, AgeCutoff} = config_settings(Resource,
                                                     Aggressiveness,
                                                     RoomId,
                                                     CrahId),
    case rf_alg:calc_stage1(AllDeltas,
                            AgeCutoff,
                            Quorum,
                            MeanShift) of
        no_quorum ->
            [{aoe_target, null},
             {aoe_actual, null},
             {inputs_status, Statuses}];
        AoeDelta when is_float(AoeDelta) ->
            {TSum, NT} = lists:foldl(
                           fun(#input_tracker{sample_time=TS,
                                              target=T},
                               {TSum, NT}) ->
                                   if
                                       TS >= AgeCutoff -> {TSum+T, NT+1};
                                       TS < AgeCutoff -> {TSum, NT}
                                   end
                           end,
                           {0.0, 0},
                           Trackers),
            MeanTarget = TSum / NT,
            [{aoe_target, MeanTarget},
             {aoe_actual, MeanTarget - AoeDelta},
             {inputs_status, Statuses}]
    end.

input_status(#input_tracker{
                input_id=InputId,
                sensor_id=SensorId,
                target=Target,
                delta=Delta,
                sample=Actual},
             Resource) ->
    {ok, #single_input{enabled = Enabled, location = Location}} =
        process_cache:load_properties(driver_lib:proc_key(InputId, Resource, input)),
    #input_status{
       id = InputId,
       resource = Resource,
       sensor_id = SensorId,
       location = Location,
       target = Target,
       deviation = null_or_float(Delta),
       actual = null_or_float(Actual),
       enabled = Enabled
      }.

null_or_float(F) when is_float(F) -> F;
null_or_float(_) -> null.

-spec config_settings(Resource :: controlled_resource(),
                      Aggressiveness :: aggressiveness(),
                      RoomId :: id(),
                      CrahId :: id()) ->
    {Quorum :: {_,_},
     MeanShift :: float(),
     AgeCutoff :: integer()}.
config_settings(Resource, Aggressiveness, RoomId, CrahId) ->
    Quorum = strategy_param:find(quorum, Resource, Aggressiveness, RoomId),
    MeanShift = strategy_param:find(mean_shift, Resource,
                                    Aggressiveness, RoomId, CrahId),
    MaxAge = strategy_param:find(max_sample_age, Resource, Aggressiveness, RoomId),
    AgeCutoff = shadoww_lib:millisecs_now() - MaxAge,
    {Quorum, MeanShift, AgeCutoff}.

