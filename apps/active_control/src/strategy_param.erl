
-module(strategy_param).

-include_lib("active_control/include/ac_api.hrl").

-export([
    find/5,
    find/4,
    find/3
    ]).

-spec find(Param :: atom(),
           Resource :: controlled_resource(),
           Agg :: aggressiveness(),
           RoomId :: id(),
           Id :: id()
          ) -> term().
find(Param, Resource, Agg, RoomId, Id)
        when is_atom(Resource), is_atom(Agg), is_binary(RoomId), is_binary(Id) ->
    case cluster_config:get_value([rf_control, RoomId, Id], {Param, Resource, Agg}) of
        error ->
            find(Param, Resource, RoomId, Id);
        {ok, Value} ->
            Value
    end.

-spec find
          (Param :: atom(),
           Resource :: controlled_resource(),
           Agg :: aggressiveness(),
           RoomId :: id()
          ) -> term();
          (Param :: atom(),
           Resource :: controlled_resource(),
           RoomId :: id(),
           Id :: id()
          ) -> term().
find(Param, Resource, Agg, RoomId)
        when is_atom(Resource), is_atom(Agg), is_binary(RoomId) ->
    case cluster_config:get_value([rf_control, RoomId], {Param, Resource, Agg}) of
        error ->
            find(Param, Resource, RoomId);
        {ok, Value} -> Value
    end;
find(Param, Resource, RoomId, Id)
        when is_atom(Resource), is_binary(RoomId), is_binary(Id) ->
    case cluster_config:get_value([rf_control, RoomId, Id], {Param, Resource}) of
        error ->
            {ok, Value} = cluster_config:get_value([rf_control, RoomId, Id], Param),
            Value;
        {ok, Value} ->
            Value
    end.

find(Param, Resource, RoomId)
        when is_atom(Resource), is_binary(RoomId) ->
    case cluster_config:get_value([rf_control, RoomId], {Param, Resource}) of
        error ->
            {ok, Value} = cluster_config:get_value([rf_control, RoomId], Param),
            Value;
        {ok, Value} ->
            Value
    end.

