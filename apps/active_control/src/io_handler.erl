%%Helper module for protocol-specific modules
-module(io_handler).
-export([get_net/1,get_ip/1, get_devid/1, get_port/1, addr_m/1, addr_b/1,read_comm/4, read_comm/5,read_standby/4, read_standby/5, read_property/4, read_property/5, read_setpoint/5, read_setpoint/6, read_alarm/4,read_alarm/5,
	 liebert_status_register/4, liebert_alarm_register/4, liebert_control_register/5,
	 bits_high/2]).
-import(shadoww_lib, [getprop/2]).


%%------------------------------------------------
%% Protocol layer specific functions
%%------------------------------------------------
read_comm(Name, Ip, Devid, CommReadStr) ->
    read_comm_i(Name, [{'IP', Ip}, {'Device', Devid}, {'Value', none}], CommReadStr).
read_comm(Name, Net, Ip, Devid, CommReadStr) ->
    read_comm_i(Name, [{'Net', Net}, {'IP', Ip}, {'Device', Devid}, {'Value', none}], CommReadStr).
read_comm_i(Name, Attr, CommReadStr) ->
    Comm = case CommReadStr of
	       [] -> true;
	       CommReadStr ->  io_eval:eval(Name, CommReadStr, Attr)
	   end,
    case Comm of
	_ when is_boolean(Comm) -> Comm;
	1 -> true;
	0 -> false
    end.

read_standby(Name, Ip, Devid, StandbyReadStr) ->
    read_standby_i(Name, [{'IP', Ip}, {'Device', Devid}, {'Value', none}], StandbyReadStr).
read_standby(Name, Net, Ip, Devid, StandbyReadStr) ->
    read_standby_i(Name, [{'Net', Net}, {'IP', Ip}, {'Device', Devid}, {'Value', none}], StandbyReadStr).
read_standby_i(Name, Attr, StandbyReadStr) ->
	Standby = case StandbyReadStr of
		[] -> false;
		StandbyReadStr -> io_eval:eval(Name, StandbyReadStr, Attr)
	end,
	case Standby of
		_ when is_boolean(Standby) -> Standby;
		1 -> true;
		0 -> false
	end
	.

read_property(Pname, Ip, Devid, ReadStr) ->
    read_property_i(Pname, [{'IP', Ip}, {'Device', Devid}, {'Value', none}], ReadStr).
read_property(Pname, Net, Ip, Devid, ReadStr) ->
    read_property_i(Pname, [{'Net', Net}, {'IP', Ip}, {'Device', Devid}, {'Value', none}], ReadStr).
read_property_i(Pname, Attr, ReadStr) ->
	Val = case ReadStr of
		[] -> [];
		ReadStr -> 1.0 * io_eval:eval(Pname, ReadStr, Attr)
	end,
	Val
	.

read_setpoint(Name, Ip, Devid, SetpointReadStr, Setpoint) ->
	read_setpoint_i(Name, [{'IP', Ip}, {'Device', Devid}, {'Value', none}], SetpointReadStr, Setpoint).
read_setpoint(Name, Net, Ip, Devid, SetpointReadStr, Setpoint) ->
	read_setpoint_i(Name, [{'Net', Net}, {'IP', Ip}, {'Device', Devid}, {'Value', none}], SetpointReadStr, Setpoint).
read_setpoint_i(Name, Attr, SetpointReadStr, Setpoint) ->
	case SetpointReadStr of
		[] -> Setpoint;
		SetpointReadStr -> 1.0 * io_eval:eval(Name, SetpointReadStr, Attr)
	end
	.

read_alarm(Pointname, Ip, Devid, Expr) ->
	read_alarm_i(Pointname, [{'IP', Ip}, {'Device', Devid}, {'Value', none}], Expr).
read_alarm(Pointname, Net, Ip, Devid, Expr) ->
	read_alarm_i(Pointname, [{'Net', Net},{'IP', Ip}, {'Device', Devid}, {'Value', none}], Expr).
read_alarm_i(Pointname, Attr, Expr) -> 
	Condition = case Expr of
		[] -> false;
		Expr -> case io_eval:eval(Pointname, Expr, Attr) of
				B when is_boolean(B) -> B;
				0 -> false;
				_ -> true
			end
	end.
	%case Condition of
	%	_ when is_boolean(Condition) -> Condition;
	%	1 -> true;
	%	0 -> false




get_net(Props) ->
    getprop("network", Props).
get_ip(Props) ->
    getprop("ip", Props).
get_devid(Props) ->
    getprop("devid", Props).
get_port(Props) ->
    getprop("port", Props).

addr_m(Props) ->
    [{'IP', get_ip(Props)}, {'Device', get_devid(Props)}].
addr_b(Props) ->
    [{'Net', get_net(Props)}, {'IP', get_ip(Props)}, {'Device', get_devid(Props)}].


liebert_status_register(Ip, Devid, BaseRegister, Port)
		when BaseRegister >= 40001, BaseRegister =< 40024, Port >= 1, Port =< 12 ->
	Reg = BaseRegister + (24 * (Port - 1)),
	mbgw:read_int16(Ip, Devid, Reg)
	.

liebert_alarm_register(Ip, Devid, BaseRegister, Port)
		when BaseRegister >= 40289, BaseRegister =< 400293, Port >= 1, Port =< 12 ->
	Reg = BaseRegister + (5 * (Port - 1)),
	mbgw:read_int16(Ip, Devid, Reg)
	.

liebert_control_register(Ip, Devid, BaseRegister, Port, Value)
		when BaseRegister >= 40349, BaseRegister =< 40351, Port >= 1, Port =< 12 ->
	Reg = BaseRegister + (3 * (Port - 1)),
	mbgw:write_register16(Ip, Devid, Reg, Value)
	.

%% Return true if any of the bits in Bits are '1', false if all bits are '0'
-spec bits_high(integer(), [integer()]) -> boolean() .
bits_high(Value, Bits) -> [Bit || Bit <- Bits, io_expr_util:bit(Bit, Value) == 1] /= [] .

