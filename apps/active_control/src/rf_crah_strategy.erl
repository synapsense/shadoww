
-module(rf_crah_strategy).

-behaviour(gen_3stage_strategy).

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-export([
         start_link/2,
         stop/1
        ]).

% callback functions
-export([
         init/1,
         reinit/1,
         status_info/1,
         stage3/3,
         set_mode/3,
         set_config/2,
         handle_cast/2,
         terminate/2
        ]).

-export([
         new_sample/2
        ]).

-record(state, {
          mode,
          supply_sensor,
          return_sensor,
          manual_output,

          cooling_control :: cooling_control(),

          supply_sample :: sample:sample(),
          return_sample :: sample:sample()
         }).

-define(SENSOR_EVENT, sensor_event).
-define(MAX_SAMPLE_AGE, 1000 * 60 * 5).

-define(DATA_TOO_OLD_MSG, "CRAH sensor data is too old").

start_link(RoomId,
           C = #raised_floor_crah_strategy{
                  id = Id,
                  resource = Resource
                 }) ->
    gen_3stage_strategy:start_link(RoomId, Id, Resource, ?MODULE, C).

stop(Pid) ->
    gen_3stage_strategy:stop(Pid).

%% callback functions

% Args is InitArgs from start_link
init(#raised_floor_crah_strategy{
        id = Id,
        resource = Resource,
        mode = Mode,
        supply_sensor = SupplyId,
        return_sensor = ReturnId,
        manual_output = MO
       }) ->

    gen_key_event:add_sup_handler(?SENSOR_EVENT, [SupplyId, ReturnId], sample_forwarder()),

    K = driver_lib:proc_key(Id, Resource, strategy),
    CoolingControl = crah_cooling_control(K),

    {ok, Mode, MO,
     #state{
        mode = Mode,
        supply_sensor = SupplyId,
        return_sensor = ReturnId,
        manual_output = MO,
        cooling_control = CoolingControl
       }}.

% PrevState is the most recent state cached in distributed memory
reinit(PrevState=#state{supply_sensor=SupplyId,return_sensor=ReturnId}) ->
    gen_key_event:add_sup_handler(?SENSOR_EVENT, [SupplyId, ReturnId], sample_forwarder()),
    {ok, PrevState}.

% Return strategy-specific info to be appended to generic info
status_info(#state{supply_sample=Supply, return_sample=Return}) ->
    [{supply_sample, Supply}, {return_sample, Return}].

% Calculate and return the Stage3 value from the Stage2 value
-spec stage3(K :: driver_lib:proc_key(), Stage2 :: float(), State :: term()) ->
    {ok, [gen_3stage_strategy:strategy_action()], Stage3 :: float(), NewState :: term()}
    | {error, Reason :: term(), NewState :: term()}.
stage3(K, Stage2, State=#state{
                           cooling_control = CoolingControl,
                           supply_sample = Supply,
                           return_sample = Return
                          }) ->
    case required_sensor_data_present(Supply, Return, CoolingControl) of
        true ->
            Capacity = crah_capacity(K),
            Stage3 = rf_alg:calc_stage3(Stage2, sample:value(Supply), sample:value(Return), CoolingControl, Capacity),
            {ok, [], Stage3, State};
        false ->
            {error, [gen_3stage_strategy:change_status(failsafe)], State}
    end.

set_mode(failsafe, automatic, State=#state{supply_sample=Supply,
                                           return_sample=Return,
                                           cooling_control = CoolingControl}) ->
    case required_sensor_data_present(Supply, Return, CoolingControl) of
        true ->
            {ok, [gen_3stage_strategy:new_stage2(sample:value(Supply))], State};
        false ->
            {error, ?DATA_TOO_OLD_MSG, State}
    end;
set_mode(manual, automatic, State=#state{manual_output = MO}) ->
    {ok, [gen_3stage_strategy:new_stage2(MO)], State};
set_mode(_Old, _New, State) ->
    {ok, [], State}.

set_config(#raised_floor_crah_strategy{
              mode = NewMode,
              supply_sensor = NewSupplyId,
              return_sensor = NewReturnId,
              manual_output = NewMO
             },
           State = #state{
                      mode = Mode,
                      manual_output = MO
                     }) ->
    Actions = [
               if NewMode =/= Mode -> gen_3stage_strategy:change_status(NewMode);
                  true -> [] end,
               if NewMO =/= MO -> gen_3stage_strategy:manual_out(NewMO);
                  true -> [] end
              ],
    %% NOTE: these Ids should never really change,
    %% but since the API supports it, so should the code
    gen_key_event:swap_handler(?SENSOR_EVENT, sample_forwarder(), [NewSupplyId, NewReturnId], sample_forwarder()),
    {ok,
     lists:flatten(Actions),
     State#state{
        mode = NewMode,
        supply_sensor = NewSupplyId,
        return_sensor = NewReturnId,
        manual_output = NewMO
      }}.

handle_cast({new_sample, Sample}, State=#state{supply_sensor=SupplyId,return_sensor=ReturnId}) ->
    case sample:id(Sample) of
        SupplyId -> {noreply, [], State#state{supply_sample = Sample}};
        ReturnId -> {noreply, [], State#state{return_sample = Sample}};
        _ -> {noreply, [], State}
    end.

terminate(Reason, _State) ->
    gen_key_event:remove_handler(?SENSOR_EVENT, sample_forwarder()),
    Reason.

%%
%% internal functions
%%
sample_forwarder() ->
    Self = self(),
    fun(_Key, Sample) -> ?MODULE:new_sample(Self, Sample) end.

new_sample(Pid, Sample) ->
    gen_3stage_strategy:cast(Pid, {new_sample, Sample}).

crah_cooling_control(K) ->
    DriverKey = driver_lib:with_type(K, driver),
    #crah_driver{cooling_control = CoolingControl} = driver_lib:call(DriverKey, get_config, []),
    CoolingControl.

crah_capacity(K) ->
    DriverKey = driver_lib:with_type(K, driver),
    #crah_status{capacity = Capacity} = driver_lib:call(DriverKey, get_status, []),
    Capacity.

-spec required_sensor_data_present(Supply :: sample:sample(),
                                   Return :: sample:sample(),
                                   CoolingControl :: cooling_control()) ->
    boolean().
required_sensor_data_present(_, _, supply) ->
    true;
required_sensor_data_present(Supply, Return, return) ->
    current_sample(Supply) andalso current_sample(Return).

current_sample(undefined) -> false;
current_sample(S) -> shadoww_lib:millisecs_now() - sample:timestamp(S) < ?MAX_SAMPLE_AGE.

