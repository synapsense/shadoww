%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(geom).

-compile({parse_transform, runtime_types}).

-export([
         point/2,
         segment/2,
         rect/2,
         rect/3,
         ellipse_from_rect/1,
         poly/1,
         ellipse/3,

         % geometric tests
         contains/2,
         coincident/2,
         colinear/3,
         smaller/2,
         intersects/2,

         smallest/1,
         smallest_containing_point/2,
         distance/1,
         distance/2,
         shortest_distance_around/3,
         walk_distance/3,
         centroid/1,

         segments/1,

         % mostly internal things
         floateq/2,
         floateq/4,
         adjacent_vertices/3,
         findhits/3,
         map_n/3,
         print/1,
         mxb/1
        ]).

% Our good friend ε, used for absolute comparison of floating point numbers
-define(EPSILON, 1.0e-8).
% Max relative error between 2 floating point numbers.  Comes into play when
% numbers very close to 0 are compared.
-define(MAX_RELATIVE_ERROR, 1.0e-5).
% Default precision for new points created through the public API.  Precision
% here means number of places after the decimal point.  This should be less
% than half of epsilon in powers of 10.  Some constructed points (intersections)
% use 2x this precision.  Care must be taken not to use constructed points in
% successive calculations.
-define(DEFAULT_PREC, 3).

-record(point, {
          x = 0.0 :: float(),
          y = 0.0 :: float()
         }).

-type point() :: #point{}.

-record(segment, {
          p1 = #point{} :: point(),
          p2 = #point{} :: point()
         }).
-type segment() :: #segment{}.


-record(rect, {
          top_left :: point(),
          bottom_right :: point()
         }).

-type rect() :: #rect{}.

% describe an ellipse in a structure that makes containment tests straightforward
-record(ellipse, {
          center :: point(), % h, k in standard ellipse formula
          width :: float(),
          height :: float()
         }).

-type ellipse() :: #ellipse{}.

-record(poly, {
          points :: [point()]
         }).
-type poly() :: #poly{}.

-export_type([point/0,segment/0,rect/0,poly/0,ellipse/0]).

-export_records([point,segment,rect,poly,ellipse]).

% Map 'N' elements at a time onto F where N is the arity of  F.
% Wrap controls how to handle the start/end of the list.
% If Wrap is 'open' there is no wrapping.  The Result list
% will have length(L) - (N-1) elements.
% If Wrap is 'closed' then the map wraps around the end,
% treating the list as a ring.  The Result list will
% have length(L) elements
map_n(F, L, open) ->
    {arity, N} = erlang:fun_info(F, arity),
    length(L) < N andalso error(list_to_short),
    {Start, End} = lists:split(N-1, L),
    {_, Mapped} = lists:foldl(
                    fun(I, {Args, Acc}) ->
                            NewArgs = Args ++ [I],
                            NAcc = [apply(F, NewArgs) | Acc],
                            {tl(NewArgs), NAcc}
                    end,
                    {Start, []},
                    End),
    lists:reverse(Mapped);
map_n(F, L, closed) ->
    {arity, N} = erlang:fun_info(F, arity),
    length(L) < N andalso error(list_to_short),
    {Start, End} = lists:split(N-1, L),
    {_, Mapped} = lists:foldl(
                    fun(I, {Args, Acc}) ->
                            NewArgs = Args ++ [I],
                            NAcc = [apply(F, NewArgs) | Acc],
                            {tl(NewArgs), NAcc}
                    end,
                    {Start, []},
                    End++Start),
    lists:reverse(Mapped).

filter_n(F, L, open) ->
    {arity, N} = erlang:fun_info(F, arity),
    length(L) < N andalso error(list_to_short),
    {Start, End} = lists:split(N-1, L),
    {_, Filtered} = lists:foldl(
                      fun(I, {Args, Acc}) ->
                              NewArgs = Args ++ [I],
                              NAcc = case apply(F, NewArgs) of
                                         true -> [hd(NewArgs) | Acc];
                                         false -> Acc
                                     end,
                              {tl(NewArgs), NAcc}
                    end,
                    {Start, []},
                    End),
    lists:reverse(Filtered);
filter_n(F, L, closed) ->
    {arity, N} = erlang:fun_info(F, arity),
    length(L) < N andalso error(list_to_short),
    {Start, End} = lists:split(N-1, L),
    {_, Filtered} = lists:foldl(
                      fun(I, {Args, Acc}) ->
                              NewArgs = Args ++ [I],
                              NAcc = case apply(F, NewArgs) of
                                         true -> [hd(NewArgs) | Acc];
                                         false -> Acc
                                     end,
                            {tl(NewArgs), NAcc}
                    end,
                    {Start, []},
                    End++Start),
    lists:reverse(Filtered).


%% Assuming the broken coordinate system of PC graphics, where the top left of the screen is 0,0 and coordinates increase down and to the right

% Construct a 2D point from X and Y
-spec point(X :: float(), Y :: float()) -> point().
point(X,Y) when is_float(X), is_float(Y) ->
    point(X,Y,?DEFAULT_PREC).
point(X,Y,P) ->
    #point{x=with_prec(X, P),
           y=with_prec(Y, P)}.

-spec segment(P1 :: point(), P2 :: point()) -> segment().
segment(P1, P2) ->
    #segment{p1=P1,p2=P2}.

% Construct a rectangle from the top/left point and width & height
-spec rect(Tl :: point(), X :: float(), Y :: float()) -> rect().
rect(Tl = #point{x = X, y = Y}, W, H)
  when is_float(W), is_float(H), W > 0.0, H > 0.0 ->
    rect(Tl, point(X+W, Y+H)).

% Construct a rectangle from the top/left and bottom/right points
-spec rect(Tl :: point(), Br :: point()) -> rect().
rect(Tl = #point{x=X1,y=Y1}, Br = #point{x=X2,y=Y2})
  when X1 < X2, Y1 < Y2 ->
    #rect{top_left = Tl, bottom_right = Br}.

-spec ellipse_from_rect(Rect :: rect()) -> ellipse().
ellipse_from_rect(#rect{top_left = #point{x=X1,y=Y1}, bottom_right = #point{x=X2,y=Y2}}) ->
    #ellipse{
       center = point((X1+X2)/2, (Y1+Y2)/2),
       width = (X2 - X1)/2,
       height = (Y2 - Y1)/2
      }.

% Create a polygon from a list of points
% The points are processed to remove duplicate points,
% remove colinear points, check for intersecting segments.
-spec poly(Points :: [point()]) -> poly().
poly(Points) when is_list(Points) ->
    case catch fixpoint([fun filter_dup/1, fun filter_colinear/1], Points) of
        L when length(L) > 2 ->
            Poly = #poly{points = L},
            % also need to look for non-adjacent coincident points
            NonAdjacentCoincident = lists:any(
                                      fun(X) -> X end,
                                      lists:flatten(
                                        shadoww_lib:pairmap(
                                          fun coincident/2,
                                          L))),
            NonAdjacentCoincident andalso error(coincident_points),
            Intersections = lists:flatten(shadoww_lib:pairmap(fun nonendpoint_intersection/2, segments(Poly))),
            length(Intersections) > 0 andalso error({self_intersections, Intersections}),
            Poly;
        O -> error({insufficient_vertices, O})
    end.

fixpoint(Filters, Points) ->
    Filtered = lists:foldl(fun(F, P) -> F(P) end,
                           Points,
                           Filters),
    case length(Filtered) == length(Points) of
        true -> Points;
        false -> fixpoint(Filters, Filtered)
    end.

filter_dup(Points) when length(Points) < 2 -> Points;
filter_dup(Points) ->
    filter_n(fun(A,B) -> not coincident(A,B) end, Points, closed).

filter_colinear(Points) ->
    lists:flatten(map_n(fun(A,B,C) ->
                                case colinear(A,B,C) of
                                    true -> [];
                                    false -> [B]
                                end
                        end,
                        Points,
                        closed)).

% pow2 because "square" could be confused with the shape
-spec pow2(X :: number()) -> float().
pow2(X) -> math:pow(X,2.0).

-spec ellipse(Center :: point(),W :: float(),H :: float()) -> ellipse().
ellipse(Center,W,H) when is_float(W), is_float(H) ->
    #ellipse{center = Center,
             width = W,
             height = H
            }.

% contains(Shape1, Shape2) -> boolean().
% Returns true if Shape1 contains (encloses, totally surrounds) Shape2
% false otherwise
-spec contains
    (R1 :: rect(), R2 :: rect()) -> boolean();
    (R1 :: rect(), Pt1 :: point()) -> boolean();
    (P1 :: poly(), P2 :: poly()) -> boolean();
    (P1 :: poly(), Pt1 :: point()) -> boolean();
    (E1 :: ellipse(), Pt1 :: point()) -> boolean().
contains(Outer = #rect{}, #rect{top_left = Tl, bottom_right = Br}) ->
    contains(Outer, Tl) andalso contains(Outer, Br);
contains(#rect{top_left = #point{x=X1,y=Y1}, bottom_right = #point{x=X2,y=Y2}}, #point{x=X,y=Y})
  when X1 < X, X < X2, Y1 < Y, Y < Y2 ->
    true;
contains(#rect{}, #point{}) ->
    false;
contains(Outer = #poly{}, #poly{points = InnerPoints}) ->
    lists:all(fun(Pt) -> contains(Outer, Pt) end, InnerPoints);
contains(#poly{points = Points}, Pt = #point{}) ->
    Sum = lists:sum(map_n(fun(P1, P2) ->
                                    ray_crosses(P1, P2, Pt)
                            end,
                            Points,
                            closed)),
    case Sum rem 2 of
        0 -> false;
        1 -> true
    end;
contains(#ellipse{width=W,height=H,center=#point{x=CX,y=CY}}, #point{x=X,y=Y}) when W > H ->
    lte((pow2(X - CX)/pow2(W)) + (pow2(Y - CY)/pow2(H)), 1.0);
contains(#ellipse{width=W,height=H,center=#point{x=CX,y=CY}}, #point{x=X,y=Y}) -> % H >= W
    lte((pow2(X - CX)/pow2(H)) + (pow2(Y - CY)/pow2(W)), 1.0).

coincident(#point{x=X1,y=Y1}, #point{x=X2,y=Y2}) ->
    floateq(X1, X2) andalso floateq(Y1, Y2).

% Given 2 shapes, one within the other, return the smaller one
% Currently only works for 2 shapes of the same type
-spec smaller
    (R1 :: rect(), R2 :: rect()) -> rect();
    (P1 :: poly(), P2 :: poly()) -> poly().
smaller(R1 = #rect{}, R2 = #rect{}) ->
    case contains(R1, R2) of
        true -> R2;
        _ -> R1
    end;
smaller(P1 = #poly{}, P2 = #poly{}) ->
    case contains(P1, P2) of
        true -> P2;
        _ -> P1
    end.

-spec smallest(Shapes :: [rect()|poly()]) -> poly().
smallest(Shapes) ->
    [C1 | C] = [S || S <- Shapes],
    lists:foldl(fun smaller/2, C1, C).

% Look for the innermost shape containing the point P
% ie, the smallest rectangle/polygon that contains P
% At least 1 shape must contain P.  It is an error if none of the shapes contains P.
-spec smallest_containing_point(Shapes :: [rect()|poly()], P :: point()) -> poly().
smallest_containing_point(Shapes, P = #point{}) ->
    [C1 | C] = [S || S <- Shapes, contains(S, P)],
    lists:foldl(fun smaller/2, C1, C).


% translated from C example at http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
% test if an infinite horizontal ray from Point crosses the line V1 - V2
ray_crosses(_Vertex1 = #point{x=X1,y=Y1}, _Vertex2 = #point{x=X2,y=Y2}, _Point = #point{x=X,y=Y})
  when (((Y2>Y) /= (Y1>Y)) andalso (X < (X1 - X2) * (Y - Y2) / (Y1 - Y2) + X2)) -> 1;
ray_crosses(#point{}, #point{}, #point{}) -> 0.

-spec distance(P1 :: point(), P2 :: point()) -> float().
distance(#point{x = X1, y = Y1}, #point{x = X2, y = Y2}) ->
    math:sqrt(math:pow((X1 - X2),2) + math:pow((Y1 -Y2),2)).

-spec distance(S1 :: segment()) -> float().
distance(#segment{p1=P1,p2=P2}) ->
    distance(P1,P2).

% Calculate the shortest distance from P1 to P2,
% navigating around the Poly.
-spec shortest_distance_around(Poly :: poly(), P1 :: point(), P2 :: point()) -> float() .
shortest_distance_around(#poly{points = Pts}, P1, P2) ->
    % start counting distance until either I1 or I2 is encountered.
    % Then count the distance until the other is encountered.
    % Then count the distance back to the first vertex.
    % min(Beginning+End, Middle)
    LoopedPoints = Pts ++ [hd(Pts)],
    {BeginPts, RemainingPts} = r_dist_around(LoopedPoints, P1, P2, []),
    % know that the first pt of RemainingPts intersects P1->P2, so remove it
    {Path1, EndPts} = r_dist_around(tl(RemainingPts), P1, P2, [hd(RemainingPts)]),
    Dist1 = path_distance(Path1, P1, P2),
    Dist2 = path_distance(EndPts ++ tl(BeginPts), P1, P2),
    min(Dist1, Dist2).

path_distance(Path, P1, P2) ->
    F = hd(Path),
    L = lists:last(Path),
    lists:sum(map_n(fun(A,B) -> distance(A,B) end, Path, open))
    + min(
        distance(P1, F) + distance(P2, L),
        distance(P2, F) + distance(P1, L)).

r_dist_around([_], _Start, _End, ProcessedPoints) -> {lists:reverse(ProcessedPoints), []};
r_dist_around([PP1 | Pts], Start, End, ProcessedPoints) ->
    Hpt = hd(Pts),
    case intersects(segment(PP1, Hpt), segment(Start,End)) of
        [] -> r_dist_around(Pts, Start, End, [PP1 | ProcessedPoints]);
        % need to handle case when P is a vertex.
        [P] ->
            case {coincident(P, PP1), coincident(P, Hpt)} of
                {true, false} ->
                    {lists:reverse([PP1 | ProcessedPoints]), [PP1 | Pts]};
                {false, true} ->
                    {lists:reverse([Hpt, PP1 | ProcessedPoints]), Pts};
                {false, false} ->
                    {lists:reverse([P, PP1 | ProcessedPoints]), [P | Pts]}
            end
    end.

-spec centroid([point()]) -> point() .
centroid(Points) ->
    Xs = [X || #point{x=X} <- Points],
    Ys = [Y || #point{y=Y} <- Points],
    point(lists:sum(Xs) / length(Points), lists:sum(Ys) / length(Points), 2*?DEFAULT_PREC).

-spec segments(P1 :: poly()) -> [segment()] .
segments(#poly{points = Pts}) ->
    map_n(fun segment/2, Pts, closed).

% algorithm for direction and segment intersection from:
% http://ptspts.blogspot.com/2010/06/how-to-determine-if-two-line-segments.html
direction(#point{x=Ax,y=Ay}, #point{x=Bx,y=By}, #point{x=Cx,y=Cy}) ->
    A = (Cx-Ax) * (By - Ay),
    B = (Bx-Ax) * (Cy-Ay),
    if
        A < B -> -1;
        A > B -> 1;
        A == B -> 0
    end.

nonendpoint_intersection(Seg1 = #segment{p1=P1, p2=P2}, Seg2 = #segment{}) ->
    case intersects(Seg1, Seg2) of
        [P] ->
            case coincident(P, P1) or coincident(P, P2) of
                true -> [];
                false -> [P]
            end;
        [] -> []
    end.

% find intersections between a line segment and a line segment, rectangle or polygon.
% Intersections are returned as a list of points; an empty list means no intersections.
-spec intersects(S1 :: segment(), S2 :: point() | segment() | poly()) -> [point()] .
intersects(Seg1 = #segment{p1=P1,p2=P2}, Seg2 = #segment{p1=P3,p2=P4}) ->
    D1 = direction(P3, P4, P1),
    D2 = direction(P3, P4, P2),
    D3 = direction(P1, P2, P3),
    D4 = direction(P1, P2, P4),
    I1 = intersects(Seg2, P1) /= [],
    I2 = intersects(Seg2, P2) /= [],
    I3 = intersects(Seg1, P3) /= [],
    I4 = intersects(Seg1, P4) /= [],
    if
        (((D1 > 0 andalso D2 < 0) orelse (D1 < 0 andalso D2 > 0)) andalso
         ((D3 > 0 andalso D4 < 0) orelse (D3 < 0 andalso D4 > 0))) orelse
        (D1 == 0 andalso I1) orelse
        (D2 == 0 andalso I2) orelse
        (D3 == 0 andalso I3) orelse
        (D4 == 0 andalso I4) ->
            {M1, B1} = mxb(Seg1),
            {M2, B2} = mxb(Seg2),
            case {M1, M2} of
                % both lines are vertical.  there is either no intersection,
                % a segment of intersection (not a single point), or the 2
                % segments share an endpoint.
                {undefined, undefined} ->
                    single_endpoint_intersection(P1, P2, P3, P4);
                % Seg1 is vertical, X coord of intersection is on Seg1
                {undefined, _} ->
                    X = P1#point.x,
                    Y = M2*X + B2,
                    [point(X, Y, 2*?DEFAULT_PREC)];
                {_, undefined} -> % Seg2 is vertical, X coord of intersection is on Seg2
                    X = P3#point.x,
                    Y = M1*X + B1,
                    [point(X, Y, 2*?DEFAULT_PREC)];
                {_, _} ->
                    case floateq(M1, M2) of
                        % same slope, same lines?
                        true ->
                            single_endpoint_intersection(P1, P2, P3, P4);
                        false ->
                            X = (B2-B1) / (M1-M2),
                            Y = M1*X + B1,
                            [point(X,Y, 2*?DEFAULT_PREC)]
                    end
            end;
        true ->
            []
    end;
intersects(Segment = #segment{}, Poly = #poly{}) ->
    Intersections = lists:flatten([intersects(Segment, PolySegment) || PolySegment <- segments(Poly)]),
    case length(Intersections) of
        L when L < 2 -> Intersections;
        L when L >= 2 ->
            % If coincident vertices are found, remove one of them and leave the
            % other
            Filter1 = uniq(fun(A,B) -> coincident(A,B) end, Intersections),
            case length(Filter1) of
                FL when FL < 2 -> Filter1;
                FL when FL >= 2 ->
                    % If adjacent vertices are found, remove them both.
                    filter_n(
                      fun(A,B) -> not adjacent_vertices(A,B,Poly) end,
                      Filter1,
                      closed)
            end
    end;
intersects(_Segment = #segment{p1=#point{x=Ax,y=Ay},p2=#point{x=Bx,y=By}}, Point = #point{x=X,y=Y}) ->
    case
        (lte(Ax, X) orelse lte(Bx, X)) andalso (lte(X, Ax) orelse lte(X, Bx)) andalso
        (lte(Ay, Y) orelse lte(By, Y)) andalso (lte(Y, Ay) orelse lte(Y, By)) of
        true -> [Point];
        false -> []
    end.

% Makes L uniq by removing the first of any 2 elements for which F returns true.
% The list is otherwise unchanged (not sorted).
uniq(F, L) ->
    r_uniq(F, L, []).

r_uniq(_F, [], Accum) -> lists:reverse(Accum);
r_uniq(F, [H | L], Accum) ->
    case lists:any(fun(I) -> F(H, I) end, L) of
        true -> r_uniq(F, L, Accum);
        false -> r_uniq(F, L, [H | Accum])
    end.

adjacent_vertices(A, B, Poly) ->
    case find_vertex(A, Poly) of
        not_found -> false;
        {Prev, Next} -> coincident(B, Prev) orelse coincident(B, Next)
    end.

% Find the vertex V in Poly.  If found, return the prior and successive
% vertices.  If not found, return not_found.
find_vertex(V = #point{}, #poly{points = Pts}) ->
    case lists:flatten(
           map_n(
             fun(A,B,C) ->
                     case coincident(B, V) of
                         true -> {A,C};
                         false -> []
                     end
             end,
             Pts,
             closed)) of
        [] -> not_found;
        [{A,C}] -> {A,C}
    end.

% P1-P2 is a segment, P3-P4 is a segment.
% If they share a single point, return it.
% If they are partially colinear, return nothing.
single_endpoint_intersection(P1, P2, P3, P4) ->
    S1 = segment(P1, P2),
    S2 = segment(P3, P4),

    I1 = intersects(S1, P3) ++ intersects(S1, P4),
    I2 = intersects(S2, P1) ++ intersects(S2, P2),

    case {I1, I2} of
        {[Pt1], [Pt2]} ->
            case coincident(Pt1, Pt2) of
                true -> [Pt1];
                false -> []
            end;
        _ -> []
    end.

% Test whether the 3 points lie on the same line.
colinear(P1, P2, P3) ->
    {M1,B1} = mxb(segment(P1, P2)),
    {M2,B2} = mxb(segment(P2, P3)),
    floateq_or_undef(M1,M2) andalso floateq_or_undef(B1,B2).

lte(F1, F2) ->
    F1 < F2 orelse floateq(F1, F2).

floateq_or_undef(undefined, undefined) ->
    true;
floateq_or_undef(F1, F2) when is_float(F1), is_float(F2) ->
    floateq(F1, F2);
floateq_or_undef(_, _) ->
    false.

floateq(A, B) ->
    floateq(A, B, ?EPSILON, ?MAX_RELATIVE_ERROR).

floateq(A, B, E, RE) ->
    D = abs(A-B),
	(A =:= B)
    orelse (D < E)
    orelse ((D / max(abs(A), abs(B))) < RE).

with_prec(F, P) when is_float(F), is_integer(P), P >= 0 ->
    D = math:pow(10, P),
    round(F * D) / D.

% calculate the slope & intercept of the line
mxb(#segment{p1=#point{x=Ax,y=_Ay},p2=#point{x=Bx,y=_By}}) when Bx == Ax ->
    % vertical line, slope is undefined, no y intercept
    {undefined, undefined};
mxb(#segment{p1=#point{x=Ax,y=Ay},p2=#point{x=Bx,y=By}}) ->
    Slope = (By-Ay) / (Bx-Ax),
    Intercept = By - Slope * Bx,
    {Slope, Intercept}.

% Find the distance from P1 to P2, avoiding (going around) any obstructions in between
% Actually, there should never be more than a single obstruction in between.
% Modeled, this means that at most 1 polygon in Obstructions should have intersections
% with the line segment P1,P2 and there should be exactly 2 intersections between
% the line segment and the polygon.  If there are more than 2 intersections with any
% polygon in Obstructions, distance is not calculated and 'max_dist' is returned.
-spec walk_distance(Start :: point(), End :: point(), Obstructions :: [poly()]) -> float() | max_dist .
walk_distance(P1, P2, Obstructions) ->
    case findhits(segment(P1,P2), Obstructions, []) of
        {multihit, O} ->
            lager:warning("Hit a single polygon too many times: ~p", [O]),
            max_dist;
        [] ->
            geom:distance(P1, P2);
        [{O, _I1, _I2}] ->
            shortest_distance_around(O, P1, P2);
        L when is_list(L) ->
            lager:warning("Hit too many polygons: ~p", [L]),
            max_dist
    end.

findhits(_Segment, [], Collisions) ->
    Collisions;
findhits(Segment, [O | Obstructions], Collisions) ->
    case intersects(Segment, O) of
        [] -> findhits(Segment, Obstructions, Collisions);
        %single intersection means we grazed a vertex of the poly.
        %No need to worry about it.
        [_H] -> findhits(Segment, Obstructions, Collisions);
        [I1,I2] -> findhits(Segment, Obstructions, [{O, I1, I2}|Collisions]);
        Multi when length(Multi) > 2 -> {multihit, O}
    end.

print(#segment{p1=P1, p2=P2}) ->
    io_lib:format("~s, ~s", [print(P1), print(P2)]);
print(#point{x=X,y=Y}) ->
    io_lib:format("(~p, ~p)", [X, Y]).

