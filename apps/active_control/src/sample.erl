
-module(sample).

-export([
	new/3,
	id/1,
	value/1,
	timestamp/1
	]).

-record(sample, {
		id :: term(),
		value :: float(),
		timestamp :: integer()
	}).
-type sample() :: #sample{}.

-export_type([sample/0]).

new(Id, Value, TS) when is_float(Value), is_integer(TS) ->
	#sample{id = Id, value = Value, timestamp = TS}
	.

id(#sample{id = Id}) -> Id.
value(#sample{value = V}) -> V.
timestamp(#sample{timestamp = TS}) -> TS.

