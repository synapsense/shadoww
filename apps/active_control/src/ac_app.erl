
-module(ac_app).

-behaviour(application).

-export([start/2, stop/1, config_change/3]).

start(normal, StartArgs) ->
	lager:info("ActiveControl application starting. start args: ~p", [StartArgs]),
	ac_supervisor:start_link()
	.

stop(State) ->
	lager:info("ActiveControl application stopping. state: ~p", [State])
	.

config_change(Changed, New, Removed) ->
	lager:info("ActiveControl config change. changed: ~p, new: ~p, removed: ~p", [Changed, New, Removed]),
	ok
	.


