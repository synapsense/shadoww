

%% basic idea: gen_event work well for a small number of handlers, many events, many event types
%% this process is meant for dealing with many handlers, many events of a single type, and selective dispatching to handlers based on a key
%% Each handler can be notified of events for many different keys.  A handler is not restricted to events with a single key.
%% A key can be any term.  Handlers are much simpler than gen_event.  A handler is only a single fun of arity 2, fun(Key, Event).


-module(gen_key_event).


-behaviour(gen_server).


-export([start/0, start/1, start_link/0, start_link/1]).

-export([notify/3, sync_notify/3, add_handler/3, add_sup_handler/3, add_all_key_handler/2, add_all_key_sup_handler/2, remove_handler/2, swap_handler/4, add_keys/3, list_keys/1, stop/1]).

-export([init/1, handle_call/3]).
-export([handle_cast/2, handle_info/2, terminate/2, code_change/3]).


-define(STATE, gen_key_event_state).


-type evt_fun() :: fun((any(), any()) -> any()) .

-record(?STATE, {
		handlers :: gb_trees:tree(), % map a key to a list of {pid, fun}
		all_key_handlers :: [{pid(), evt_fun()}], % A list of handlers {pid, fun} to call for any key
		registered :: [{undefined | reference(), pid(), evt_fun(), [any()] | all_keys}] % All registered handlers and the ref we are monitoring with
	}).


start() -> gen_server:start(?MODULE, noargs, []) .
start(Name) -> gen_server:start(Name, ?MODULE, noargs, []) .

start_link() -> gen_server:start_link(?MODULE, noargs, []) .
start_link(Name) -> gen_server:start_link(Name, ?MODULE, noargs, []) .


%% API functions

notify(N, Key, Event) ->
	gen_server:cast(N, {notify, Key, Event})
	.

sync_notify(N, Key, Event) ->
	gen_server:call(N, {notify, Key, Event})
	.

-spec add_all_key_handler(term(), fun((term(), term()) -> ignored)) -> ok.
add_all_key_handler(N, Fun) ->
	gen_server:call(N, {add_handler, all_keys, Fun, self(), false})
	.

-spec add_all_key_sup_handler(term(), fun((term(), term()) -> ignored)) -> ok.
% @doc
%  The handler is supervised.  If the caller exits, the handler is removed.
%  If the handler exits, the caller will be notified with a {gen_key_event_EXIT, Fun, Reason}
% Unlike gen_event, the gen_key_event_EXIT message will not be delivered when the handler
% is removed via remove_handler or changed with swap_handler
% Also, no EXIT message will be sent when the event manager exits.  If the caller desires
% this functionality, the event manager can be monitored.
add_all_key_sup_handler(N, Fun) ->
	gen_server:call(N, {add_handler, all_keys, Fun, self(), true})
	.

-spec add_handler(Name :: term(), Keys :: [term()], EvtFun :: fun((term(), term()) -> ignored)) -> ok | {error, already_exists}.
add_handler(N, Keys, Fun) ->
	gen_server:call(N, {add_handler, Keys, Fun, self(), false})
	.

-spec add_sup_handler(term(), [term()], fun((term(), term()) -> ignored)) -> ok | {error, already_exists}.
% @doc
%  The handler is supervised.  If the caller exits, the handler is removed.
%  If the handler exits, the caller will be notified with a {gen_key_event_EXIT, Fun, Reason}
% Unlike gen_event, the gen_key_event_EXIT message will not be delivered when the handler
% is removed via remove_handler or changed with swap_handler
% Also, no EXIT message will be sent when the event manager exits.  If the caller desires
% this functionality, the event manager can be monitored.
add_sup_handler(N, Keys, Fun) ->
	gen_server:call(N, {add_handler, Keys, Fun, self(), true})
	.

-spec remove_handler(term(), fun((term(), term()) -> ignored)) -> ok.
remove_handler(N, Fun) ->
	gen_server:call(N, {remove_handler, Fun, self()})
	.

-spec swap_handler(term(), fun((term(), term()) -> ignored), [term()], fun((term(), term()) -> ignored)) -> ok.
swap_handler(N, OldFun, Keys, NewFun) ->
	gen_server:call(N, {swap_handler, OldFun, Keys, NewFun, self()})
	.

add_keys(N, Keys, Fun) ->
	gen_server:call(N, {add_keys, Keys, Fun, self()}).

list_keys(N) ->
	gen_server:call(N, list_keys)
	.

stop(N) ->
	gen_server:call(N, stop)
	.

%% Behavior callbacks

init(_) ->
	{ok, #?STATE{handlers = gb_trees:empty(), all_key_handlers = [], registered = []}}
	.


handle_call({notify, Key, Event}, _From, State = #?STATE{all_key_handlers = AllKey, handlers = Handlers, registered = Reg}) ->
	KeyHandlers = case gb_trees:lookup(Key, Handlers) of none -> []; {value, H} -> H end,
	{NHandlers, Reg1} = case r_notify(KeyHandlers, Key, Event) of
		[] -> {Handlers, Reg};
		ToRemove ->
			lists:foldl(
				fun({Pid, Fun, Reason}, {H, R}) ->
					[{MRef, _, _, Keys}] = [Rec] = [E || E = {_, RPid, RFun, _} <- R, RPid == Pid, RFun == Fun],
					if MRef /= undefined -> Pid ! {gen_key_event_EXIT, Fun, Reason}; true -> ok end,
					do_demonitor(MRef),
					NH = map_remove_handlers(Keys, {Pid, Fun}, H),
					NR = lists:delete(Rec, R),
					{NH, NR}
				end,
				{Handlers, Reg},
				ToRemove)
	end,
	{NAllKey, Reg2} = case r_notify(AllKey, Key, Event) of
		[] -> {AllKey, Reg1};
		AllKeyToRemove ->
			lists:foldl(
				fun({Pid, Fun, Reason}, {A, R}) ->
					[{MRef, _, _, all_keys}] = [Rec] = [E || E = {_, RPid, RFun, _} <- R, RPid == Pid, RFun == Fun],
					if MRef /= undefined -> Pid ! {gen_key_event_EXIT, Fun, Reason}; true -> ok end,
					do_demonitor(MRef),
					NA = lists:delete({Pid, Fun}, A),
					NR = lists:delete(Rec, R),
					{NA, NR}
				end,
				{AllKey, Reg1},
				AllKeyToRemove)
	end,
	{reply, ok, State#?STATE{handlers = NHandlers, all_key_handlers = NAllKey, registered = Reg2}}
	;

handle_call({add_handler, all_keys, Fun, Pid, Sup}, _From, State = #?STATE{all_key_handlers = AllKey, registered = Reg}) ->
	case lists:member({Pid, Fun}, AllKey) of
		true ->
			{reply, {error, already_exists}, State};
		false ->
			NAllKey = [{Pid, Fun} | AllKey],
			NReg = case Sup of
				true -> [{monitor(process, Pid), Pid, Fun, all_keys} | Reg];
				false -> [{undefined, Pid, Fun, all_keys} | Reg]
			end,
			{reply, ok, State#?STATE{all_key_handlers = NAllKey, registered = NReg}}
	end
	;

handle_call({add_handler, Keys, Fun, Pid, Sup}, _From, State = #?STATE{handlers = Handlers, registered = Reg}) ->
	Exists = [E || E = {_, RPid, RFun, _} <- Reg, RPid == Pid, RFun == Fun],
	case Exists of
		[] ->
			NHandlers = map_add_handlers(Keys, {Pid, Fun}, Handlers),
			NReg = case Sup of
				true -> [{monitor(process, Pid), Pid, Fun, Keys} | Reg];
				false -> [{undefined, Pid, Fun, Keys} | Reg]
			end,
			{reply, ok, State#?STATE{handlers = NHandlers, registered = NReg}}
			;
		_ ->
			{reply, {error, already_exists}, State}
	end
	;

handle_call({add_keys, Keys, Fun, Pid}, _From, State = #?STATE{handlers = Handlers, registered = Reg}) ->
	Rec = [R || R = {_, RPid, RFun, _} <- Reg, RPid == Pid, RFun == Fun],
	case Rec of
		[] ->
			{reply, {error, no_handler}, State};
		[{MRef, _, _, OldKeys}] ->
			AddKeys = Keys -- OldKeys,
			AllKeys = OldKeys ++ AddKeys,
			lager:debug("KEYS TO ADD: ~p", [AddKeys]),
			Handlers1 = map_add_handlers(AddKeys, {Pid, Fun}, Handlers), % order important, since the tree is rebalanced on insert, not delete
			NReg = [{MRef, Pid, Fun, AllKeys} | lists:delete(hd(Rec), Reg)],
			{reply, ok, State#?STATE{handlers = Handlers1, registered = NReg}}
	end;

handle_call({remove_handler, Fun, Pid}, _From, State = #?STATE{handlers = Handlers, registered = Reg, all_key_handlers = AllKey}) ->
	Rec = [R || R = {_, RPid, RFun, _} <- Reg, RPid == Pid, RFun == Fun],
	case Rec of
		[] ->
			{reply, {error, no_handler}, State};
		[{MRef, _, _, all_keys}] ->
			do_demonitor(MRef),
			NAllKey = lists:delete({Pid, Fun}, AllKey),
			NReg = lists:delete(hd(Rec), Reg),
			{reply, ok, State#?STATE{all_key_handlers = NAllKey, registered = NReg}};
		[{MRef, _, _, Keys}] ->
			do_demonitor(MRef),
			NHandlers = map_remove_handlers(Keys, {Pid, Fun}, Handlers),
			NReg = lists:delete(hd(Rec), Reg),
			{reply, ok, State#?STATE{handlers = NHandlers, registered = NReg}}
	end
	;

handle_call({swap_handler, OldFun, all_keys, Fun, Pid}, _From, State = #?STATE{all_key_handlers = AllKey, registered = Reg}) ->
	Rec = [R || R = {_, RPid, RFun, all_keys} <- Reg, RPid == Pid, RFun == OldFun],
	case Rec of
		[] ->
			{reply, {error, no_handler}, State};
		[{MRef, _, _, all_keys}] ->
			NAllKey = [{Pid, Fun} | lists:delete({Pid, OldFun}, AllKey)],
			NReg = [{MRef, Pid, Fun, all_keys} | lists:delete(hd(Rec), Reg)],
			{reply, ok, State#?STATE{all_key_handlers = NAllKey, registered = NReg}}
	end
	;

% just updating the keys that Fun should be called for
handle_call({swap_handler, OldFun, Keys, Fun, Pid}, _From,
	    State = #?STATE{handlers = Handlers, registered = Reg})
    when OldFun == Fun ->
	Rec = [R || R = {_, RPid, RFun, _} <- Reg, RPid == Pid, RFun == Fun],
	case Rec of
		[] ->
			{reply, {error, no_handler}, State};
		[{MRef, _, _, OldKeys}] ->
			AddKeys = Keys -- OldKeys,
			lager:debug("KEYS TO ADD: ~p", [AddKeys]),
			RemoveKeys = OldKeys -- Keys,
			lager:debug("KEYS TO REMOVE: ~p", [RemoveKeys]),
			Handlers1 = map_remove_handlers(RemoveKeys, {Pid, Fun}, Handlers),
			Handlers2 = map_add_handlers(AddKeys, {Pid, Fun}, Handlers1), % order important, since the tree is rebalanced on insert, not delete
			NReg = [{MRef, Pid, Fun, Keys} | lists:delete(hd(Rec), Reg)],
			{reply, ok, State#?STATE{handlers = Handlers2, registered = NReg}}
	end
	;

% Changing the event handler, and possibly the key list as well
handle_call({swap_handler, OldFun, Keys, Fun, Pid}, _From, State = #?STATE{handlers = Handlers, registered = Reg}) ->
	Rec = [R || R = {_, RPid, RFun, _} <- Reg, RPid == Pid, RFun == OldFun],
	case Rec of
		[] ->
			{reply, {error, no_handler}, State};
		[{MRef, _, _, OldKeys}] ->
			Handlers1 = map_remove_handlers(OldKeys, {Pid, OldFun}, Handlers),
			Handlers2 = map_add_handlers(Keys, {Pid, Fun}, Handlers1), % order important, since the tree is rebalanced on insert, not delete
			NReg = [{MRef, Pid, Fun, Keys} | lists:delete(hd(Rec), Reg)],
			{reply, ok, State#?STATE{handlers = Handlers2, registered = NReg}}
	end
	;

handle_call(list_keys, _From, State = #?STATE{handlers = Handlers}) ->
	Reply = gb_trees:keys(Handlers),
	{reply, Reply, State}
	;

handle_call(stop, _From, State) ->
	{stop, shutdown, stopping, State}
	;

handle_call(Message, From, State) ->
	lager:error("Unexpected handle_call. message: ~p, from: ~p, state: ~p", [Message, From, State]),
	{reply, huh, State}
	.


handle_cast({notify, Key, Event}, State = #?STATE{handlers = Handlers, all_key_handlers = AllKey, registered = Reg}) ->
	KeyHandlers = case gb_trees:lookup(Key, Handlers) of none -> []; {value, H} -> H end,
	{NHandlers, Reg1} = case r_notify(KeyHandlers, Key, Event) of
		[] -> {Handlers, Reg};
		ToRemove ->
			lists:foldl(
				fun({Pid, Fun, Reason}, {H, R}) ->
					[{MRef, _, _, Keys}] = [Rec] = [E || E = {_, RPid, RFun, _} <- R, RPid == Pid, RFun == Fun],
					if MRef /= undefined -> Pid ! {gen_key_event_EXIT, Fun, Reason}; true -> ok end,
					do_demonitor(MRef),
					NH = map_remove_handlers(Keys, {Pid, Fun}, H),
					NR = lists:delete(Rec, R),
					{NH, NR}
				end,
				{Handlers, Reg},
				ToRemove)
	end,
	{NAllKey, Reg2} = case r_notify(AllKey, Key, Event) of
		[] -> {AllKey, Reg1};
		AllKeyToRemove ->
			lists:foldl(
				fun({Pid, Fun, Reason}, {A, R}) ->
					[{MRef, _, _, all_keys}] = [Rec] = [E || E = {_, RPid, RFun, _} <- R, RPid == Pid, RFun == Fun],
					if MRef /= undefined -> Pid ! {gen_key_event_EXIT, Fun, Reason}; true -> ok end,
					do_demonitor(MRef),
					NA = lists:delete({Pid, Fun}, A),
					NR = lists:delete(Rec, R),
					{NA, NR}
				end,
				{AllKey, Reg1},
				AllKeyToRemove)
	end,
	{noreply, State#?STATE{handlers = NHandlers, all_key_handlers = NAllKey, registered = Reg2}}
	;

handle_cast(Request, State) ->
	lager:error("Unexpected handle_cast. request: ~p, state: ~p", [Request, State]),
	{noreply, State}
	.


handle_info({'DOWN', MRef, process, Pid, _}, State = #?STATE{handlers = Handlers, all_key_handlers = AllKey, registered = Reg}) ->
	Rec = lists:keyfind(MRef, 1, Reg),
	case Rec of
		{MRef, _, Fun, all_keys} ->
			do_demonitor(MRef),
			NAllKey = lists:delete({Pid, Fun}, AllKey),
			NReg = lists:delete(Rec, Reg),
			{noreply, State#?STATE{all_key_handlers = NAllKey, registered = NReg}}
			;
		{MRef, _, Fun, Keys} ->
			do_demonitor(MRef),
			NHandlers = map_remove_handlers(Keys, {Pid, Fun}, Handlers),
			NReg = lists:delete(Rec, Reg),
			{noreply, State#?STATE{handlers = NHandlers, registered = NReg}}
	end
	;
handle_info(Info, State) ->
	lager:error("Unexpected handle_info. info: ~p, state: ~p", [Info, State]),
	{noreply, State}
	.


terminate(shutdown, _State) -> ok ;
terminate(Reason, _State) -> lager:info("terminating. reason: ~p", [Reason]) .

code_change(_OldVsn, State, _Extra) ->
	{ok, State}
	.


%% internal utility functions


map_add_handlers([], _, Handlers) ->
	Handlers
	;
map_add_handlers([Key | Keys], H, Handlers) ->
	map_add_handlers(Keys, H, map_add_handler(Key, H, Handlers))
	.

map_add_handler(Key, H, Handlers) ->
	case gb_trees:lookup(Key, Handlers) of
		none -> gb_trees:insert(Key, [H], Handlers);
		{value, V} -> gb_trees:update(Key, [H | V], Handlers)
	end
	.

map_remove_handlers([], _, Handlers) ->
	Handlers
	;
map_remove_handlers([Key | Keys], H, Handlers) ->
	map_remove_handlers(Keys, H, map_remove_handler(Key, H, Handlers))
	.

map_remove_handler(Key, H, Handlers) ->
	case gb_trees:get(Key, Handlers) of
		[H] -> gb_trees:delete(Key, Handlers);
		L -> gb_trees:update(Key, lists:delete(H, L), Handlers)
	end
	.

-spec r_notify([{pid(), evt_fun()}], any(), any()) -> [{pid(), evt_fun(), any()}].
r_notify([], _, _) ->
	[]
	;
r_notify([{Pid, Fun} | Hs], Key, Event) ->
	try
		Fun(Key, Event),
		r_notify(Hs, Key, Event)
	catch
		E:R -> [{Pid, Fun, {E, R}} | r_notify(Hs, Key, Event)]
	end
	.

do_demonitor(undefined) -> ok ;
do_demonitor(R) when is_reference(R) -> demonitor(R, [flush]) .

