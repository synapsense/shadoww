
-module(input_supervisor).


-behaviour(supervisor).


-export([start_link/0]).
-export([init/1]).


-define(SERVER, ?MODULE).


start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, [])
	.


init(_Args) ->
	RestartStrategy		= rest_for_one,	% any process dies, restart it and all after it
	MaxRestarts		= 2,		% number of restarts allowed in
	MaxTimeBetRestarts	= 3600,		% this number of seconds

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	{ok, DmHost} = application:get_env(dm_host),
	{ok, DmPort} = application:get_env(dm_port),

	ChildSpecs = [{
		sensor_event,
		{gen_key_event, start_link, [{local, sensor_event}]},
		permanent,
		1000,
		worker,
		[gen_key_event]
	}, {
		sensor_data_feeder,				% name of the process
		{sensor_data_feeder, start_link, [DmHost, DmPort]},	% module, function, args
		permanent,					% death of the process is never expected, so always restart
		1000,						% how many ms the process is given to stop gracefully before being killed
		worker,						% this process is doing useful work, not supervising other processes
		[sensor_data_feeder]				% module dependencies
	}],

	{ok, {SupFlags, ChildSpecs}}
	.

