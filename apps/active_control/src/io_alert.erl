
-module(io_alert).

-export([raise_connect_alert/2]).

raise_connect_alert({eval_error, {_Point, _Expr, Bindings}, econnrefused}, Id) ->
	Ip = proplists:get_value('IP', Bindings),
	control_alarm:gateway_unreachable(Ip, Id);
raise_connect_alert({eval_error, {_Point, _Expr, Bindings}, timeout}, Id) ->
	Ip = proplists:get_value('IP', Bindings),
	control_alarm:gateway_not_responding(Ip, Id);
raise_connect_alert({eval_error, {_Point, _Expr, Bindings}, {modbus_error, ErrNo, _Details}}, Id) ->
	Ip = proplists:get_value('IP', Bindings),
	Devid = proplists:get_value('Device', Bindings),
	control_alarm:modbus_gw_error(Ip, Devid, ErrNo, Id);
raise_connect_alert({eval_error, {_Point, _Expr, Bindings}, {bad_packet, Req, Resp}}, Id) ->
	Ip = proplists:get_value('IP', Bindings),
	Devid = proplists:get_value('Device', Bindings),
	control_alarm:modbus_data_corruption(Ip, Devid, Req, Resp, Id);
raise_connect_alert({eval_error, {Point, Expr, _Bindings}, {expression_abort, AbortMsg}}, Id) ->
	control_alarm:io_expression_failed(Point, Expr, AbortMsg, Id);
raise_connect_alert({eval_error, {Point, Expr, _Bindings}, {error, badarith}}, Id) ->
	control_alarm:io_expression_failed(Point, Expr, "arithmetic error", Id);
raise_connect_alert({eval_error, {Point, Expr, _Bindings}, {unexpected_failure, F}}, Id) ->
	control_alarm:io_expression_failed(Point, Expr, F, Id);
raise_connect_alert({eval_error, {Point, Expr, _Bindings}, {bad_output, ErrMsg}}, Id) ->
	control_alarm:io_expression_failed(Point, Expr, ErrMsg, Id);
raise_connect_alert({eval_error, {Point, Expr, _Bindings}, {register_overflow, Value}}, Id) ->
	ErrMsg = f("Register overflow.  Value: ~p", [Value]),
	control_alarm:io_expression_failed(Point, Expr, ErrMsg, Id);
raise_connect_alert({eval_error, {Point, Expr, Bindings}, no_reply}, Id) ->
	Ip = proplists:get_value('IP', Bindings),
	Devid = proplists:get_value('Device', Bindings),
	control_alarm:device_not_responding(Point, Expr, Ip, Devid, Id);
raise_connect_alert({eval_error, {Point, Expr, _Bindings}, {parse_error, {_, Msg}}}, Id) ->
	ErrMsg = f("Invalid IO Expression '~p'.  Details: ~p", [Expr, Msg]),
	control_alarm:io_expression_failed(Point, Expr, ErrMsg, Id);

raise_connect_alert({eval_error, {Point, Expr, _Bindings}, Err} = F, Id) ->
	lager:error([{id, Id}], "Unhandled I/O transaction failure: ~p", [F]),
	ErrMsg = f("I/O failure: ~p", [Err]),
	control_alarm:io_expression_failed(Point, Expr, ErrMsg, Id).

f(M, A) -> lists:flatten(io_lib:format(M, A)).

