
-ifndef(ac_constants).
-define(ac_constants, true).

% values for the "unitstatus" field
-define(ONLINE, "text/online").
-define(STANDBY, "text/standby").
-define(DISCONNECTED, "text/disconnected").
-define(OVERRIDE, "text/local_override").

% values for the "engaged" field
-define(ENGAGED, 1).
-define(DISENGAGED, 0).

% values for the "valveControl" field
-define(CONTROL_RETURN, 1).
-define(CONTROL_SUPPLY, 2).

% activity module parameters for the control system
-define(ACTIVITY_USER, "ControlSystem").
-define(ACTIVITY_MODULE, "ActiveControl").
-define(ACTIVITY_ACTION, "SystemControlParameterChan").

% constants for the "status" field of wsnsensor and controlpoint_singlecompare
-define(STATUS_NOREPORT, 0).
-define(STATUS_OK, 1).
-define(STATUS_DISABLED, 2).

%Activity messages
-define(TO_FAILSAFE_AVG_4,"$localize(activity_to_failsafe_avg_4,\"~ts\",\"~s\",\"~.2f\",\"~.3f\")$").
-define(TO_FAILSAFE_INPUT_4,"$localize(activity_to_failsafe_input_4,\"~ts\",\"~s\",\"~.2f\",\"~ts\")$").
-define(TO_FAILSAFE_FELL_3,"$localize(activity_to_failsafe_3,\"~ts\",\"~s\",\"~.2f\")$").
-define(TO_FAILSAFE_FELL_4,"$localize(activity_to_failsafe_4,\"~ts\",\"~s\",\"~.2f\",\"~.2f\")$").
-define(TO_FAILSAFE_FELL_5,"$localize(activity_to_failsafe_fell_5,\"~ts\",\"~s\",\"~.2f\",\"~p\",\"~p\")$").
-define(MODE_CHANGE_4,"$localize(activity_mode_change_4,\"~ts\",\"~s\",\"~s\",\"~s\")$").
-define(FROM_TO_4,"$localize(activity_from_to_4,\"~ts\",\"~s\",\"~.2f\",\"~.2f\")$").
-define(FANINC_FLUCT,"$localize(activity_fan_inc_fluct_3,\"~ts\",\"~s\",\"~.2f\")$").
-define(FANINC_LOWTEMP,"$localize(activity_fan_inc_low_temp_4,\"~ts\",\"~s\",\"~.2f\",\"~.2f\")$").
-define(FANHELD_FLUCT,"$localize(activity_fan_held_fluct_3,\"~ts\",\"~s\",\"~.2f\")$").

-define(UNKNOWN_COMPR, "unknown").
-define(BOTH_COMPR, "1 and 2").
-define(COMPR_1, "1").
-define(COMPR_2, "2").

-endif.

