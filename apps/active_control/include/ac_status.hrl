
-ifndef(ac_status_hrl).
-define(ac_status_hrl, true).

-include_lib("active_control/include/ac_api.hrl").

%% Status records
-record(raised_floor_room_status, {
		id :: id(),
		temperature_balancer_status = null :: raised_floor_balancer_strategy_status() | null,
		pressure_balancer_status :: raised_floor_balancer_strategy_status(),
		crahs_status :: [raised_floor_crah_status()]
	}).
-type raised_floor_room_status() :: #raised_floor_room_status{} .

-record(remote_control_room_status, {
		id :: id(),
		crahs_status :: [remote_control_crah_status()]
	 }).
-type remote_control_room_status() :: #remote_control_room_status{} .

-record(raised_floor_balancer_strategy_status, {
		id :: id(),
		resource :: controlled_resource(),
		aggressiveness :: aggressiveness(),
		highest_capacity :: {Value :: float(), Id :: id()} | null,
		lowest_capacity :: {Value :: float(), Id :: id()} | null,
		inputs_status :: [input_status()]
	}).
-type raised_floor_balancer_strategy_status() :: #raised_floor_balancer_strategy_status{} .

-record(remote_control_strategy_status, {
		id :: id(),
		resource :: controlled_resource(),
		status :: control_status(),
		user_setpoint :: float()
	}).
-type remote_control_strategy_status() :: #remote_control_strategy_status{} .

-record(raised_floor_crah_status, {
		id :: id(),
		temperature_driver_status = null :: crah_status() | null,
		temperature_strategy_status = null :: raised_floor_strategy_status() | null,
		pressure_driver_status :: vfd_status(),
		pressure_strategy_status :: raised_floor_strategy_status(),
		mode :: control_mode()
	}).
-type raised_floor_crah_status() :: #raised_floor_crah_status{} .

-record(remote_control_crac_status, {
	id :: id(),
	temperature_driver_status = null :: crac_status() | null,
	temperature_strategy_status = null :: remote_control_strategy_status() | null,
	pressure_driver_status = null :: vfd_status() | null,
	pressure_strategy_status = null :: remote_control_strategy_status() | null,
	mode :: control_mode()
}).
-type remote_control_crac_status() :: #remote_control_crac_status{} .

-record(remote_control_crah_status, {
	id :: id(),
	temperature_driver_status = null :: crah_status() | null,
	temperature_strategy_status = null :: remote_control_strategy_status() | null,
	pressure_driver_status = null :: vfd_status() | null,
	pressure_strategy_status = null :: remote_control_strategy_status() | null,
	mode :: control_mode()
}).
-type remote_control_crah_status() :: #remote_control_crah_status{} .

-record(raised_floor_strategy_status, {
		id :: id(),
		resource :: controlled_resource(),
		location :: geom:point(),
		status :: control_status(),
		failsafe :: float() | null,
		stage2 :: float() | null,
		user_setpoint :: float(),
		last_adjustment :: {Value :: float(), Timestamp :: integer()} | null,
		aoe_target :: float() | null,
		aoe_actual :: float() | null,
		inputs_status :: [input_status()]
	}).
-type raised_floor_strategy_status() :: #raised_floor_strategy_status{} .

-record(raised_floor_tdx_strategy_status, {
		id :: id(),
		resource :: controlled_resource(),
		location :: geom:point(),
		status :: control_status(),
		failsafe :: float() | null,
		stage2 :: float() | null,
		user_setpoint :: float(),
		last_adjustment :: {Value :: float(), Timestamp :: integer()} | null,
		aoe_target :: float() | null,
		aoe_actual :: float() | null,
		inputs_status :: [input_status()]
	}).
-type raised_floor_tdx_strategy_status() :: #raised_floor_tdx_strategy_status{} .

-record(crah_status, {
		id :: id(),
		resource :: controlled_resource(),
		status :: device_status(),
		setpoint :: float() | null,
		rat :: float() | null,
		sat :: float() | null,
		capacity :: float() | null
	}).
-type crah_status() :: #crah_status{} .

-record(crac_status, {
		id :: id(),
		resource :: controlled_resource(),
		status :: device_status(),
		setpoint :: float() | null,
		capacity :: float() | null,
		stages :: integer() | null,
		rat :: float() | null,
		sat :: float() | null
	}).
-type crac_status() :: #crac_status{} .

-record(vfd_status, {
		id :: id(),
		resource :: controlled_resource(),
		status :: device_status(),
		setpoint :: float() | null,
		cfm :: float() | null,
		kw :: float() | null
	}).
-type vfd_status() :: #vfd_status{} .

-record(input_status, {
		id :: id(),
		resource :: controlled_resource(),
		sensor_id :: id(),
		location :: geom:point(),
		target :: float(),
		deviation :: float() | null,
		actual :: float() | null,
		enabled :: boolean()
	}).
-type input_status() :: #input_status{} .


-endif.

