
-ifndef(ac_api_hrl).
-define(ac_api_hrl, true).

-type id() :: binary() .

-type controlled_resource() :: temperature | pressure .

% set at the strategy, per {id, resource} combo
-type control_mode() :: disengaged | manual | automatic .
-type control_status() :: disengaged | manual | automatic | failsafe .

-type device_status() :: disengaged | online | disconnected | standby | local_override.

-type cooling_control() :: supply | return .

-type aggressiveness() :: high | med | low .

-record(single_input, {
		id :: id(),
		resource :: controlled_resource(),
		sensor_id :: id(),
		location :: geom:point(),
		target :: float(),
		enabled :: boolean()
	}).
-type single_input() :: #single_input{} .

-record(modbus_bindings, {
		ip :: string(),
		device_id :: integer()
	}).
-type modbus_bindings() :: #modbus_bindings{} .

-record(bacnet_bindings, {
		ip :: string(),
		network :: integer(),
		device :: integer()
	}).
-type bacnet_bindings() :: #bacnet_bindings{} .

-record(generic_crah, {
		setpoint_write :: string(),
		setpoint_read :: string(),
		valve_read :: string(),
		rat_read :: string(),
		sat_read :: string(),
		standby_read :: string(),
		comm_read :: string(),
		change_filters_alarm_read :: string(),
		general_alarm_read :: string(),
		clear_alarm_write :: string()
	}).
-type generic_crah() :: #generic_crah{} .

-record(liebert_ahu, {
		sitelink_port :: integer()
	}).
-type liebert_ahu() :: #liebert_ahu{} .

-record(generic_crac, {
		setpoint_write :: string(),
		setpoint_read :: string(),
		rat_read :: string(),
		sat_read :: string(),
		compressor_read :: string(),
		capacity_read :: string(),
		standby_read :: string(),
		comm_read :: string(),
		high_head_pressure_alarm_read :: string(),
		low_suction_pressure_alarm_read :: string(),
		compressor_overload_alarm_read :: string(),
		short_cycle_alarm_read :: string(),
		change_filters_alarm_read :: string(),
		general_alarm_read :: string(),
		clear_alarm_write :: string()
	}).
-type generic_crac() :: #generic_crac{} .

-record(generic_vfd, {
		setpoint_write :: string(),
		setpoint_read :: string(),
		power_read :: string(),
		local_override_read :: string(),
		comm_read :: string(),
		over_current_alarm_read :: string(),
		over_voltage_alarm_read :: string(),
		under_voltage_alarm_read :: string(),
		over_temperature_alarm_read :: string(),
		general_alarm_read :: string(),
		clear_alarm_write :: string()
	}).
-type generic_vfd() :: #generic_vfd{} .

-record(ach550, {}).
-type ach550() :: #ach550{} .

-record(crah_driver, {
		id :: id(),
		resource :: controlled_resource(),
		pull_time :: integer(),
		push_time :: integer(),
		cooling_control :: cooling_control(),
		cascade_standby :: boolean(),
		protocol_bindings :: modbus_bindings() | bacnet_bindings(),
		driver_config :: generic_crah() | liebert_ahu()
	}).
-type crah_driver() :: #crah_driver{} .

-record(crac_driver, {
		id :: id(),
		resource :: controlled_resource(),
		pull_time :: integer(),
		push_time :: integer(),
		cooling_control :: cooling_control(),
		max_stages :: integer(),
		cascade_standby :: boolean(),
		protocol_bindings :: modbus_bindings() | bacnet_bindings(),
		driver_config :: generic_crac() | liebert_ahu()
	}).
-type crac_driver() :: #crac_driver{} .

-record(vfd_driver, {
		id :: id(),
		resource :: controlled_resource(),
		pull_time :: integer(),
		push_time :: integer(),
		cfm_table :: [{float(), float()}],
		protocol_bindings :: modbus_bindings() | bacnet_bindings(),
		driver_config :: generic_vfd() | ach550()
	}).
-type vfd_driver() :: #vfd_driver{} .

-record(raised_floor_crah_strategy, {
		id :: id(),
		resource :: controlled_resource(),
		manual_output :: float(),
		supply_sensor :: id(),
		return_sensor :: id(),
		mode :: control_mode()
	}).
-type raised_floor_crah_strategy() :: #raised_floor_crah_strategy{} .

-record(raised_floor_vfd_strategy, {
		id :: id(),
		resource :: controlled_resource(),
		manual_output :: float(),
		mode :: control_mode()
	}).
-type raised_floor_vfd_strategy() :: #raised_floor_vfd_strategy{} .

-record(remote_control_strategy, {
		id :: id(),
		resource :: controlled_resource(),
		manual_output :: float(),
		mode :: control_mode()
	}).
-type remote_control_strategy() :: #remote_control_strategy{} .

-record(raised_floor_balancer_strategy, {
		id :: id(),
		resource :: controlled_resource(),
		aggressiveness :: aggressiveness()
	}).
-type raised_floor_balancer_strategy() :: #raised_floor_balancer_strategy{} .

-record(raised_floor_crah, {
		id :: id(),
		location :: geom:point(),
		temperature_driver = none :: crah_driver() | none,
		temperature_strategy = none :: raised_floor_crah_strategy() | none,
		pressure_driver :: vfd_driver(),
		pressure_strategy :: raised_floor_vfd_strategy()
	}).
-type raised_floor_crah() :: #raised_floor_crah{} .

-record(raised_floor_room, {
		id :: id(),
		bounds :: geom:poly(),
		contained_rooms = [] :: [geom:poly()],
		heartbeat_manager :: heartbeat_manager() | none,
		temperature_inputs = [] :: [single_input()],
		pressure_inputs = [] :: [single_input()],
		crahs = [] :: [raised_floor_crah()],
		temperature_balancer = none :: raised_floor_balancer_strategy() | none,
		pressure_balancer :: raised_floor_balancer_strategy()
	}).
-type raised_floor_room() :: #raised_floor_room{} .

-record(remote_control_crah, {
		id :: id(),
		temperature_driver = none :: crah_driver() | none,
		temperature_strategy = none :: remote_control_strategy() | none,
		airflow_driver = none :: vfd_driver() | none,
		airflow_strategy = none :: remote_control_strategy() | none
	}).
-type remote_control_crah() :: #remote_control_crah{} .

-record(remote_control_crac, {
		id :: id(),
		temperature_driver = none :: crac_driver() | none,
		temperature_strategy = none :: remote_control_strategy() | none,
		airflow_driver = none :: vfd_driver() | none,
		airflow_strategy = none :: remote_control_strategy() | none
	}).
-type remote_control_crac() :: #remote_control_crac{} .

% This is to handle utility rooms, overhead ducted rooms, slab floor rooms, etc.
% ie, rooms that do not have a subfloor plenum or airflow patterns inferrable by
% an automated system.  All units are remote controlled, no automated control.
-record(remote_control_room, {
		id :: id(),
		heartbeat_manager :: heartbeat_manager() | none,
		crahs = [] :: [remote_control_crah() | remote_control_crac()]
	}).
-type remote_control_room() :: #remote_control_room{} .

-record(heartbeat_manager, {
		interval :: integer(),
		heartbeat_write :: string(),
		protocol_bindings :: bacnet_bindings() | modbus_bindings()
	 }).
-type heartbeat_manager() :: #heartbeat_manager{} .

-type room_types() :: raised_floor_room() | remote_control_room() .
-type crah_types() :: raised_floor_crah() | remote_control_crac() | remote_control_crah() .

-endif.
