
-module(vfd_fsm_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-include("../include/ac_constants.hrl").
-include("../src/drv_handler.hrl").

-compile(export_all).

-define(PULL_TIMER, 0.1).  % value in seconds
-define(EPSILON, 23).     % sleep value in ms

-define(DRV_MOD, ach550_vfd).

-import(test_lib, [until_equal/2]).

%% common test callbacks
suite() ->
	[
		{timetrap, {seconds, 10}}
	].

init_per_suite(Config) ->
	{ok, Started} = application:ensure_all_started(meck),
	[{started, Started} | Config].

end_per_suite(Config) ->
	meck:unload(),
	[application:stop(App) || App <- lists:reverse(?config(started, Config))].

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	meck:new(dumbtable, [no_link]),
	meck:expect(dumbtable, async_put, fun(_, _, _) -> ok end),
	meck:expect(dumbtable, get, fun(_, _) -> none end),
	meck:expect(dumbtable, remove, fun(_, _) -> ok end),

	meck:new(gproc, [no_link]),
	meck:expect(gproc, reg, fun(_) -> ok end),
	meck:expect(gproc, reg, fun(_,_) -> ok end),
	meck:expect(gproc, mreg, fun(_,_,_) -> ok end),
	meck:expect(gproc, lookup_pids, fun(_) -> [] end),
	meck:expect(gproc, unreg, fun(_) -> ok end),
	meck:expect(gproc, munreg, fun(_, _, _) -> ok end),
	meck:expect(gproc, goodbye, fun() -> ok end),

	meck:new(room_event, [no_link]),
	meck:expect(room_event, notify_device_status_change, fun(_, _, _) -> ok end),

	meck:new(control_alarm, [no_link]),

	Config.

end_per_testcase(_TestCase, _Config) ->
	meck:unload(),
	ok.

groups() ->
	[]
	.

all() ->
	[online_to_disengaged, online_to_disconnected, online_to_standby, online_to_reconnecting, online_to_override,
	 standby_to_online, standby_to_disengaged,
	 reconnecting_to_disengaged, reconnecting_to_disconnected, reconnecting_to_standby, reconnecting_to_online, reconnecting_to_override,
	 disconnected_to_disengaged, disconnected_to_online, disconnected_to_standby, disconnected_to_override,
	 disengaged_to_standby, disengaged_to_online, disengaged_to_disconnected, disengaged_to_override,
	 override_to_disengaged, override_to_disconnected, override_to_online, override_to_standby, override_to_reconnecting
	]
	.

%%
%% Test cases
%%

online_to_reconnecting(_Config) ->
	reconnecting(),
	meck:expect(control_alarm, io_expression_failed, fun("vfd.test", "test()", _, _) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, io_expression_failed, '_'),
	stop(Pid).

reconnecting_to_disconnected(_Config) ->
	%the same as 'online_to_reconnecting'
	reconnecting(),
	meck:expect(control_alarm, io_expression_failed, fun("vfd.test", "test()", _, _) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, io_expression_failed, '_'),
	stop(Pid).

online_to_disengaged(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:disengage(Pid),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid).

online_to_disconnected(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

online_to_standby(_Config) ->
	standby(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

online_to_override(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

reconnecting_to_disengaged(_Config) ->
	reconnecting(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid).

reconnecting_to_standby(_Config)  ->
	reconnecting(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

reconnecting_to_online(_Config) ->
	reconnecting(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end),
	mock_vfd_override([{read_standby, fun(_) -> false end}]),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

reconnecting_to_override(_Config) ->
	reconnecting(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_abort, TxRef, apply(F, A)} end),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end),
	meck:expect(?DRV_MOD, read_override, fun(_) -> true end),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

standby_to_disengaged(_Config) ->
	standby(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid).

standby_to_online(_Config) ->
	standby(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	vfd_fsm:set_status(Pid, ?ONLINE),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

disconnected_to_disengaged(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	until_equal({"state name", disengaged}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disconnected_to_standby(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disconnected_to_online(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_standby, fun(_) -> false end}]),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disconnected_to_override(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disengaged_to_standby(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:engage(Pid),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

disengaged_to_online(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid).

disengaged_to_disconnected(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disengaged_to_override(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

override_to_disengaged(_Config) ->
	online(),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	until_equal({"state name", disengaged}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

override_to_disconnected(_Config) ->
	online(),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_vfd_override([{read_comm, fun(_) -> false end}, {read_override, fun(_) -> false end}]),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

override_to_online(_Config) ->
	online(),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> false end}]),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

override_to_standby(_Config) ->
	online(),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	vfd_fsm:set_status(Pid, ?STANDBY),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

override_to_reconnecting(_Config) ->
	online(),
	mock_vfd_override([{read_comm, fun(_) -> true end}, {read_override, fun(_) -> true end}]),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", override}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_abort, TxRef, apply(F, A)} end),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

%%
%% Utility functions
%%

mock_vfd_testdriver() ->
	meck:new(?DRV_MOD, [non_strict]),
	do_mock(mock_vfd_values()).

mock_vfd_testdriver(Overrides) ->
	meck:new(?DRV_MOD, [non_strict]),
	do_mock(lists:foldl(fun({Name,Value}, List) -> lists:keyreplace(Name, 1, List, {Name,Value}) end, mock_vfd_values(), Overrides)).

mock_vfd_override(Overrides) ->
	do_mock(lists:foldl(fun({Name,Value}, List) -> lists:keyreplace(Name, 1, List, {Name,Value}) end, mock_vfd_values(), Overrides)).

do_mock(Values) ->
	lists:foreach(fun({Callback,Value}) -> meck:expect(?DRV_MOD, Callback, Value) end, Values).


mock_vfd_values() ->
	[
		{properties, fun() -> [] end},
		{read_comm, fun(_) -> true end},
                {read_override, fun(_) -> false end},
		{read_kw, fun(_) -> 1.0 end},
		{read_setpoint, fun(_) -> 34.5 end},
		{write_setpoint, fun(_,_) -> ok end},
		{read_over_current, fun(_) -> false end},
		{read_over_voltage, fun(_) -> false end},
		{read_under_voltage, fun(_) -> false end},
		{read_over_temperature, fun(_) -> false end},
		{read_general, fun(_) -> false end},
		{write_clear_alarm, fun(_) -> false end}
	].

online() ->
	mock_ioeval(connect),
	mock_vfd_testdriver(),
        meck:expect(?DRV_MOD, read_standby, fun(_) -> false end).


disconnected() ->
	mock_ioeval(connect),
	mock_vfd_testdriver([{read_comm, fun(_) -> false end}]).
	%meck:expect(?DRV_MOD, read_comm, fun(_) -> false end).

standby() ->
	mock_ioeval(connect),
	mock_vfd_testdriver([{read_comm, fun(_) -> true end}]).
	%meck:expect(?DRV_MOD, read_comm, fun(_) -> true end),
	%meck:expect(?DRV_MOD, read_standby, fun(_) -> true end).

reconnecting() ->
	mock_ioeval(disconnect),
	mock_vfd_testdriver().
	%meck:expect(io_eval, run_async, fun(TxRef, _, _) -> self() ! {txn_abort, TxRef, test_reason} end),
	%meck:validate(io_eval).

stop(Pid) ->
	unlink(Pid),
	base_driver:stop_driver(Pid),
	?assertEqual(undefined, process_info(Pid)),
	unmock().

unmock() ->
	 meck:unload(io_eval),
	 meck:unload(?DRV_MOD).

driver_rec() ->
	Config = room_builder:ach550(),
	Bindings = protocol_bindings:modbus_bindings(
		     "172.30.201.102",
		     1),
	room_builder:vfd_driver(
	  <<"CRAC:1">>,
	  pressure,
	  ?PULL_TIMER,
	  0.1,
	  [{20.0, 2800},{100.0, 14000}],
	  Bindings,
	  Config
	 ).

mock_ioeval(connect) ->
	meck:new(io_eval),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end);
mock_ioeval(disconnect) ->
	meck:new(io_eval),
	meck:expect(io_eval, run_async, fun(TxRef, _F, _A) -> self() ! {txn_abort, TxRef, {eval_error, {"vfd.test", "test()", []}, test_reason}} end).

