-module(crah_fsm_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-include("../include/ac_constants.hrl").
-include("../src/drv_handler.hrl").

-define(EPSILON, 23).
-define(PULL_TIMER, 0.1).

-define(DRV_MOD, liebert_modbus_crah).

-import(test_lib, [until_equal/2]).

-compile(export_all).

suite() ->
	[
		{timetrap, {seconds, 10}}
	].

init_per_suite(Config) ->
	{ok, Started} = application:ensure_all_started(meck),
	[{started, Started} | Config].

end_per_suite(Config) ->
	meck:unload(),
	[application:stop(App) || App <- lists:reverse(?config(started, Config))].

init_per_group(_G, Config) ->
	Config
	.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	meck:new(dumbtable, [no_link]),
	meck:expect(dumbtable, async_put, fun(_, _, _) -> ok end),
	meck:expect(dumbtable, get, fun(_, _) -> none end),
	meck:expect(dumbtable, remove, fun(_, _) -> ok end),

	meck:new(gproc, [no_link]),
	meck:expect(gproc, reg, fun(_) -> ok end),
	meck:expect(gproc, reg, fun(_,_) -> ok end),
	meck:expect(gproc, mreg, fun(_,_,_) -> ok end),
	meck:expect(gproc, lookup_pids, fun(_) -> [] end),
	meck:expect(gproc, unreg, fun(_) -> ok end),
	meck:expect(gproc, munreg, fun(_, _, _) -> ok end),
	meck:expect(gproc, goodbye, fun() -> ok end),

	meck:new(room_event, [no_link]),
	meck:expect(room_event, notify_device_status_change, fun(_, _, _) -> ok end),

	meck:new(control_alarm, [no_link]),

	Config.

end_per_testcase(_TestCase, _Config) ->
	meck:unload(),
	ok.

groups() ->
	[].

all() -> [online_to_disengaged, online_to_disconnected, online_to_standby, online_to_reconnecting,
	  standby_to_disconnected, standby_to_online, standby_to_disengaged, standby_to_reconnecting,
	  reconnecting_to_disengaged, reconnecting_to_disconnected, reconnecting_to_standby, reconnecting_to_online,
	  disconnected_to_disengaged, disconnected_to_online, disconnected_to_standby,
	  disengaged_to_standby, disengaged_to_online, disengaged_to_disconnected].

online_to_disengaged(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:disengage(Pid),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid)
	.

online_to_disconnected(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

online_to_standby(_Config) ->
	standby(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

online_to_reconnecting(_Config) ->
	reconnecting(),
	meck:expect(control_alarm, io_expression_failed, fun("crah.test", "test()", _, _) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, io_expression_failed, '_'),
	stop(Pid).

reconnecting_to_disengaged(_Config) ->
	reconnecting(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid).

reconnecting_to_disconnected(_Config) ->
	%the same as 'online_to_reconnecting'
	reconnecting(),
	meck:expect(control_alarm, io_expression_failed, fun("crah.test", "test()", _, _) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, io_expression_failed, '_'),
	stop(Pid).

reconnecting_to_standby(_Config)  ->
	reconnecting(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end),
	meck:expect(?DRV_MOD, read_standby, fun(_) -> true end),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

reconnecting_to_online(_Config) ->
	reconnecting(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	crah_fsm:set_property(Pid, "unitstatus", ?ONLINE),
	ct:log("Drv_state: ~p",[base_driver:drv_state(Pid)]),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end),
	mock_crah_override([{read_standby, fun(_) -> false end}]),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	stop(Pid).

standby_to_disengaged(_Config) ->
	standby(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

standby_to_disconnected(_Config) ->
	standby(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_crah_override([{read_comm, fun(_) -> false end}]),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

standby_to_online(_Config) ->
	standby(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_crah_override([{read_comm, fun(_) -> true end}, {read_standby, fun(_) -> false end}]),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

standby_to_reconnecting(_Config) ->
	standby(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:expect(io_eval, run_async, fun(TxRef, _F, _A) -> self() ! {txn_abort, TxRef, {eval_error, {"crah.test", "test()", []}, test_reason}} end),
	until_equal({"state name", reconnecting}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

disconnected_to_disengaged(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	base_driver:disengage(Pid),
	until_equal({"state name", disengaged}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disconnected_to_standby(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_crah_override([{read_comm, fun(_) -> true end}, {read_standby, fun(_) -> true end}]),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disconnected_to_online(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	mock_crah_override([{read_comm, fun(_) -> true end}, {read_standby, fun(_) -> false end}]),
	until_equal({"state name", online}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

disengaged_to_standby(_Config) ->
	online(),
	meck:expect(control_alarm, device_in_standby, fun(_) -> ok end),
	mock_crah_override([{read_standby, fun(_) -> true end}]),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:engage(Pid),
	until_equal({"state name", standby}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, device_in_standby, '_'),
	stop(Pid).

disengaged_to_online(_Config) ->
	online(),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:engage(Pid),
	?assertEqual({"state name", online}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	stop(Pid).

disengaged_to_disconnected(_Config) ->
	disconnected(),
	meck:expect(control_alarm, comm_broken, fun(_) -> ok end),
	{ok, Pid} = base_driver:start_link(<<"ROOM">>, driver_rec()),
	?assertEqual({"state name", disengaged}, lists:keyfind("state name", 1, base_driver:drv_state(Pid))),
	base_driver:engage(Pid),
	until_equal({"state name", disconnected}, fun() -> lists:keyfind("state name", 1, base_driver:drv_state(Pid)) end),
	meck:validate(control_alarm),
	true = meck:called(control_alarm, comm_broken, '_'),
	stop(Pid).

driver_rec() ->
	Config = room_builder:liebert_ahu(1),
	Bindings = protocol_bindings:modbus_bindings(
		     "172.30.201.102",
		     1),
	room_builder:crah_driver(
	  <<"CRAC:1">>,
	  temperature,
	  ?PULL_TIMER,
	  0.1,
	  return,
	  false,
	  Bindings,
	  Config
	 ).


mock_crah_testdriver() ->
	meck:new(?DRV_MOD, [non_strict]),
	do_mock(mock_crah_values()).
mock_crah_testdriver(Overrides) ->
	meck:new(?DRV_MOD, [non_strict]),
	do_mock(lists:foldl(fun({Name,Value}, List) -> lists:keyreplace(Name, 1, List, {Name,Value}) end, mock_crah_values(), Overrides)).

mock_crah_override(Overrides) ->
	do_mock(lists:foldl(fun({Name,Value}, List) -> lists:keyreplace(Name, 1, List, {Name,Value}) end, mock_crah_values(), Overrides)).

do_mock(Values) ->
	lists:foreach(fun({Callback,Value}) -> meck:expect(?DRV_MOD, Callback, Value) end, Values).

mock_crah_values() ->
	[
		{properties, fun() -> [] end},
		{read_setpoint, fun(_) -> true end},
		{read_comm, fun(_) -> true end},
		{read_standby, fun(_) -> true end},
		{read_valve, fun(_) -> 1.0 end},
		{read_return, fun(_) -> 34.5 end},
		{read_supply, fun(_) -> 34.5 end},
		{write_setpoint, fun(_, _) -> ok end},
		{read_change_filters, fun(_) -> false end},
		{read_general, fun(_) -> false end},
		{write_clear_alarm, fun(_) -> ok end}
	].

online() ->
	mock_ioeval(connect),
	mock_crah_testdriver(),
	meck:expect(?DRV_MOD, read_standby, fun(_) -> false end).

disconnected() ->
	mock_ioeval(connect),
	mock_crah_testdriver([{read_comm, fun(_) -> false end}]).
	%meck:expect(?DRV_MOD, read_comm, fun(_) -> false end).

standby() ->
	mock_ioeval(connect),
	mock_crah_testdriver([{read_comm, fun(_) -> true end}, {read_standby, fun(_) -> true end}]).
	%meck:expect(?DRV_MOD, read_comm, fun(_) -> true end),
	%meck:expect(?DRV_MOD, read_standby, fun(_) -> true end).

reconnecting() ->
	mock_ioeval(disconnect),
	mock_crah_testdriver().
	%meck:expect(io_eval, run_async, fun(TxRef, _, _) -> self() ! {txn_abort, TxRef, test_reason} end),
	%meck:validate(io_eval).

stop(Pid) ->
	unlink(Pid),
	base_driver:stop_driver(Pid),
	?assertEqual(undefined, process_info(Pid)),
	unmock().

unmock() ->
	 meck:unload(io_eval),
	 meck:unload(?DRV_MOD).

mock_ioeval(connect) ->
	meck:new(io_eval),
	meck:expect(io_eval, run_async, fun(TxRef, F, A) -> self() ! {txn_result, TxRef, apply(F, A)} end);
mock_ioeval(disconnect) ->
	meck:new(io_eval),
	meck:expect(io_eval, run_async, fun(TxRef, _F, _A) -> self() ! {txn_abort, TxRef, {eval_error, {"crah.test", "test()", []}, test_reason}} end).

