
-module(rf_balancer_SUITE).

%% API
-export([
         all/0,
         suite/0,
         groups/0,
         init_per_suite/1,
         end_per_suite/1,
         init_per_group/2,
         end_per_group/2,
         init_per_testcase/2,
         end_per_testcase/2
        ]).

%% Test cases
-export([
         identify_clusters_test/1,
         cluster_properties_test/1,
         do_rebalance_test/1,
         random_rebalance_test/1,
         one_cluster_rebalance_test/1,
         two_cluster_rebalance_test/1,
         redistribute_temp_on_online_test/1,
         redistribute_pressure_on_online_test/1,
         redistribute_temp_on_standby_test/1,
         redistribute_pressure_on_standby_test/1
        ]).

-include_lib("active_control/include/ac_status.hrl").

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

%% copy of records from rf_balancer_strategy.erl
-record(tracker, {
          id :: id(),
          strategy_status :: control_status(),
          device_status :: device_status(),
          location :: geom:point(),
          neighbors :: [id()],
          stage2 :: float()
         }).

%%%===================================================================
%%% Common Test callbacks
%%%===================================================================

all() ->
    [
     {group, one_cluster_g},
     {group, two_cluster_g},
     {group, random_rebalance_g},
     {group, rebalance_tests},
     {group, redistribute_tests}
    ].

suite() ->
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:add_config(ct_config_plain, TestCfg),
    [
     {timetrap, {seconds, 120}},
     {require, env}
    ].

% idea: create test groups based on number of clusters, ie, single_cluster, double_cluster, ...
% then run all tests in each group.  Test would need to query number of clusters from Config
% and react accordingly
% Also, repeat tests and have the test-case init bump the neighborhood size each time
groups() ->
    [
     {one_cluster_g, [parallel], [
                identify_clusters_test,
                do_rebalance_test
                ]},
     {two_cluster_g, [parallel], [
                identify_clusters_test,
                cluster_properties_test,
                do_rebalance_test
                ]},
     {random_rebalance_g, [{repeat, 5}], [
                random_rebalance_test,
                random_rebalance_test,
                random_rebalance_test,
                random_rebalance_test
                ]},
     {rebalance_tests, [parallel], [
                one_cluster_rebalance_test,
                two_cluster_rebalance_test
                ]},
     {redistribute_tests, [parallel], [
                redistribute_temp_on_standby_test,
                redistribute_pressure_on_standby_test,
                redistribute_temp_on_online_test,
                redistribute_pressure_on_online_test
                ]}
    ].

init_per_suite(Config) ->
    Env = ct:get_config(env),
    ct:pal("app loading results: ~p", [[{AppName, application:load(AppName)} || {AppName, _} <- Env, AppName =:= lager]]),
    [[application:set_env(AppName, K, V) || {K,V} <- Tuples] || {AppName, Tuples} <- Env, AppName =:= lager],
    {ok, Started1} = application:ensure_all_started(lager),
    {ok, Started2} = application:ensure_all_started(meck),

    TablePid = ets_table_proc(),

    [{started1, Started1},
     {started2, Started2},
     {table_pid, TablePid}
     | Config].

end_per_suite(Config) ->
    ?config(table_pid, Config) ! stop,
    [ok = application:stop(S) || S <- lists:reverse(?config(started2, Config))],
    [ok = application:stop(S) || S <- lists:reverse(?config(started1, Config))],
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:remove_config(ct_config_plain, TestCfg).

init_per_group(one_cluster_g, Config) ->
    Trackers = one_cluster_trackers(),
    CrahLocations = [{Id, Loc} || #tracker{id=Id,location=Loc} <- Trackers],
    RackLocations = get_random_locations(length(CrahLocations) * 20),
    PressureLocations = get_random_locations(length(CrahLocations) * 8),

    meck:unload(), % why is this needed?
    meck:new(driver_lib, [passthrough]),
    meck:expect(driver_lib, reg,
                fun
                    ({_, _, _}) -> ok
                end),
    {ok, Pid} = rf_geom:start_link(<<"">>, CrahLocations, RackLocations, PressureLocations, []),

    [
        {crahs, CrahLocations},
        {trackers, Trackers},
        {geom_pid, Pid},
        {n_clusters, 1},
        {neighborhood_size, 3},
        {variance_threshold, 4.0},
        {walls, []}
        | Config
    ];
init_per_group(two_cluster_g, Config) ->
    Trackers = two_cluster_trackers(),
    CrahLocations = [{Id, Loc} || #tracker{id=Id,location=Loc} <- Trackers],
    RackLocations = get_random_locations(length(CrahLocations) * 20),
    PressureLocations = get_random_locations(length(CrahLocations) * 8),

    meck:unload(), % why is this needed?
    meck:new(driver_lib, [passthrough]),
    meck:expect(driver_lib, reg,
                fun
                    ({_, _, _}) -> ok
                end),
    {ok, Pid} = rf_geom:start_link(<<"">>, CrahLocations, RackLocations, PressureLocations, []),

    [
        {crahs, CrahLocations},
        {trackers, Trackers},
        {geom_pid, Pid},
        {n_clusters, 2},
        {neighborhood_size, 3},
        {variance_threshold, 4.0},
        {walls, []}
        | Config
    ];
init_per_group(random_rebalance_g, Config) ->
    meck:unload(), % why is this needed?
    meck:new(driver_lib, [passthrough, no_link]),
    meck:expect(driver_lib, reg,
                fun
                    ({_, _, _}) -> ok
                end),
    Config;
init_per_group(_GroupName, Config) ->
    Config.

end_per_group(single_cluster_g, Config) ->
    ?config(geom_pid, Config) ! stop,
    meck:unload();
end_per_group(_GroupName, _Config) ->
    meck:unload().

init_per_testcase(random_rebalance_test, Config) ->
    [{seed, os:timestamp()} | Config];
init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

%%%===================================================================
%%% Test cases
%%%===================================================================

identify_clusters_test(Config) ->
    Trackers = ?config(trackers, Config),
    NClusters = ?config(n_clusters, Config),

    ?assertEqual(NClusters, length(rf_balancer_strategy:neighborhood_clusters(Trackers, 2))),
    ?assertEqual(NClusters, length(rf_balancer_strategy:neighborhood_clusters(Trackers, 3))),
    ?assertEqual(NClusters, length(rf_balancer_strategy:neighborhood_clusters(Trackers, 4))).

cluster_properties_test(Config) ->
    Trackers = ?config(trackers, Config),
    NeighborhoodSize = ?config(neighborhood_size, Config),
    VarianceThreshold = ?config(variance_threshold, Config),
    Walls = ?config(walls, Config),

    Clusters = rf_balancer_strategy:neighborhood_clusters(Trackers, NeighborhoodSize),
    ?assertEqual(length(Trackers), length(lists:flatten(Clusters))),

    ClusterTrackers = rf_balancer_strategy:build_cluster_trackers(Clusters, Trackers, Walls),
    ?assertEqual(length(Clusters), length(ClusterTrackers)),

    CBT = rf_balancer_strategy:balance_neighborhood_clusters(
            ClusterTrackers, Trackers, VarianceThreshold),
    ?assertEqual(length(Trackers), length(CBT)),

    TotalLoadBefore = lists:sum([S2 || #tracker{stage2=S2} <- Trackers]),
    TotalLoadAfter = lists:sum([S2 || #tracker{stage2=S2} <- CBT]),

    ct:pal("load before/after cluster balancing (neglecting per-CRAH balancing) ~f/~f",
           [TotalLoadBefore, TotalLoadAfter]).

do_rebalance_test(Config) ->
    Trackers = ?config(trackers, Config),
    NeighborhoodSize = ?config(neighborhood_size, Config),
    VarianceThreshold = ?config(variance_threshold, Config),
    Walls = ?config(walls, Config),

    Updates = rf_balancer_strategy:do_rebalance(Trackers, VarianceThreshold, NeighborhoodSize, Walls),
    ?assert(is_list(Updates)).

get_random_locations(N) ->
    MaxX = 1000,
    MaxY = 1000,
    IDs = lists:seq(1,N),
    [{Id, geom:point(rand:uniform() * MaxX, rand:uniform() * MaxY)} || Id <- IDs].

random_rebalance_test(Config) ->
    Seed = ?config(seed, Config),

    rand:seed(exsplus, Seed),

    NCrahs = rand:uniform(200) + 50,
    VarianceThreshold = rand:uniform() * 5,
    NeighborhoodSize = rand:uniform(4) + 1,
    Walls = [],

    ct:pal("Ncrahs: ~p", [NCrahs]),
    ct:pal("Neighborhood size: ~p", [NeighborhoodSize]),
    ct:pal("random seed: ~p", [Seed]),

    CrahLocations = get_random_locations(NCrahs),
    IDs = [Id || {Id, _} <- CrahLocations],

    RackLocations = get_random_locations(NCrahs * 20),
    PressureLocations = get_random_locations(NCrahs * 8),

    {ok, Pid} = rf_geom:start_link(<<"">>, CrahLocations, RackLocations, PressureLocations, Walls),

    % need to move the random call outside, since it will be run from a diff proc w/ a diff seed?
    % change this to a sequence meck
    meck:expect(driver_lib, call,
                fun
                    ({_, _, driver}, get_status, _) ->
                        #crah_status{status=ok};
                    ({_, _, strategy}, get_status_info, _) ->
                        [{status,automatic},{stage2,rand:uniform()*100}]
                end),
    meck:expect(driver_lib, lookup, fun({<<"">>, none, geom}) -> Pid end),

    Trackers = [rf_balancer_strategy:device_tracker(Id, <<"">>, temperature) || Id <- IDs],

    Updates = rf_balancer_strategy:do_rebalance(Trackers, VarianceThreshold, NeighborhoodSize, Walls),

    ?assert(is_list(Updates)),

    unlink(Pid),
    rf_geom:stop(Pid).

one_cluster_rebalance_test(_Config) ->
    Trackers = one_cluster_trackers(),

    Checker = fun({Id,Value}, Ts) ->
            #tracker{stage2=Tv} = lists:keyfind(Id, #tracker.id, Ts),
            case Value == Tv of
                true -> ok;
                false ->
                    erlang:error({wrong_value, [{id, Id}, {expected, Value}, {actual, Tv}]})
            end
    end,

    BalancedValues2_09 = [
            {<<"CRAH:1">>, (55.5 + (55.5+56.5+67.5)/3)/2},
            {<<"CRAH:2">>, (56.5 + (55.5+56.5+67.5)/3)/2},
            {<<"CRAH:3">>, (67.5 + (56.5+67.5+58.5)/3)/2},
            {<<"CRAH:4">>, (58.5 + (67.5+58.5+59.5)/3)/2},
            {<<"CRAH:5">>, (59.5 + (67.5+58.5+59.5)/3)/2}
            ],

    Balanced2_09 = [rf_balancer_strategy:balanced_stage2(Tracker, Trackers, 0.9, 2) || Tracker <- Trackers],
    [Checker(BV, Balanced2_09) || BV <- BalancedValues2_09],

    BalancedValues2_20 = [
            {<<"CRAH:1">>, 55.5},
            {<<"CRAH:2">>, 56.5},
            {<<"CRAH:3">>, (67.5 + (56.5+67.5)/2)/2},
            {<<"CRAH:4">>, (58.5 + (67.5+58.5)/2)/2},
            {<<"CRAH:5">>, 59.5}
            ],

    Balanced2_20 = [rf_balancer_strategy:balanced_stage2(Tracker, Trackers, 2.0, 1) || Tracker <- Trackers],
    [Checker(BV, Balanced2_20) || BV <- BalancedValues2_20].

two_cluster_rebalance_test(_Config) ->
    Trackers = two_cluster_trackers(),

    Checker = fun({Id,Value}, Ts) ->
            #tracker{stage2=Tv} = lists:keyfind(Id, #tracker.id, Ts),
            case Value == Tv of
                true ->
                    ct:pal("id ~p : ~f OK!", [Id, Value]);
                false ->
                    erlang:error({wrong_value, [{id, Id}, {expected, Value}, {actual, Tv}]})
            end
    end,

    C1 = [
            {<<"CRAH:1">>, (55.5 + (55.5+56.5+67.5)/3)/2},
            {<<"CRAH:2">>, (56.5 + (55.5+56.5+67.5)/3)/2},
            {<<"CRAH:3">>, (67.5 + (56.5+67.5+58.5)/3)/2},
            {<<"CRAH:4">>, (58.5 + (67.5+58.5+59.5)/3)/2},
            {<<"CRAH:5">>, (59.5 + (67.5+58.5+59.5)/3)/2}
            ],
    C2 = [
            {<<"CRAH:6">>, (65.5 + (65.5+66.5+77.5)/3)/2},
            {<<"CRAH:7">>, (66.5 + (65.5+66.5+77.5)/3)/2},
            {<<"CRAH:8">>, (77.5 + (66.5+77.5+68.5)/3)/2},
            {<<"CRAH:9">>, (68.5 + (77.5+68.5+69.5)/3)/2},
            {<<"CRAH:10">>, (69.5 + (77.5+68.5+69.5)/3)/2}
            ],

    C1Avg = lists:sum([V || {_,V} <- C1]) / 5,
    C2Avg = lists:sum([V || {_,V} <- C2]) / 5,

    ct:pal("C1 average: ~f", [C1Avg]),
    ct:pal("C2 average: ~f", [C2Avg]),

    CNAvg = (C1Avg + C2Avg) / 2,

    C1Adjust = 0.5 * (CNAvg - C1Avg),
    C2Adjust = 0.5 * (CNAvg - C2Avg),

    ct:pal("adjusting cluster 1 by: ~f", [C1Adjust]),
    ct:pal("adjusting cluster 2 by: ~f", [C2Adjust]),

    NC1 = [{Id,V+C1Adjust} || {Id,V} <- C1],
    NC2 = [{Id,V+C2Adjust} || {Id,V} <- C2],

    BalancedValues2_09 = C1 ++ C2,
    NBalancedValues2_09 = NC1 ++ NC2,

    Balanced2_09 = [rf_balancer_strategy:balanced_stage2(Tracker, Trackers, 0.9, 2) || Tracker <- Trackers],
    [Checker(BV, Balanced2_09) || BV <- BalancedValues2_09],

    ClusterBalanced2_09 = rf_balancer_strategy:do_rebalance(Trackers, 0.9, 2, []),
    ?assertEqual(lists:sort(NBalancedValues2_09), lists:sort(ClusterBalanced2_09)).

redistribute_temp_on_standby_test(_Config) ->
    Trackers = one_cluster_trackers(),
    Id = <<"CRAH:3">>,
    Resource = temperature,

    Adjustments2 = rf_balancer_strategy:load_out(Id, Trackers, Resource, 2),
    ?assertEqual([{<<"CRAH:2">>, 56.5}, {<<"CRAH:4">>, 58.5}], Adjustments2),

    Adjustments3 = rf_balancer_strategy:load_out(Id, Trackers, Resource, 3),
    ?assertEqual([{<<"CRAH:1">>, 55.5}, {<<"CRAH:2">>, 56.5}, {<<"CRAH:4">>, 58.5}], Adjustments3),

    Adjustments4 = rf_balancer_strategy:load_out(Id, Trackers, Resource, 4),
    ?assertEqual([{<<"CRAH:1">>, 55.5}, {<<"CRAH:2">>, 56.5}, {<<"CRAH:4">>, 58.5}, {<<"CRAH:5">>, 59.5}], Adjustments4).

redistribute_pressure_on_standby_test(_Config) ->
    Trackers = one_cluster_trackers(),
    Id = <<"CRAH:3">>,
    Resource = pressure,

    Adjustments2 = rf_balancer_strategy:load_out(Id, Trackers, Resource, 2),
    ?assertEqual([{<<"CRAH:2">>, 90.25}, {<<"CRAH:4">>, 92.25}], Adjustments2),

    Adjustments3 = rf_balancer_strategy:load_out(Id, Trackers, Resource, 3),
    ?assertEqual([{<<"CRAH:1">>, 78.0}, {<<"CRAH:2">>, 79.0}, {<<"CRAH:4">>, 81.0}], Adjustments3),

    Adjustments4 = rf_balancer_strategy:load_out(Id, Trackers, Resource, 4),
    ?assertEqual([{<<"CRAH:1">>, 72.375}, {<<"CRAH:2">>, 73.375}, {<<"CRAH:4">>, 75.375}, {<<"CRAH:5">>, 76.375}], Adjustments4).

redistribute_temp_on_online_test(_Config) ->
    Trackers = one_cluster_trackers(),
    Id = <<"CRAH:1">>,
    Resource = temperature,

    Adjustments2 = rf_balancer_strategy:load_in(Id, Trackers, Resource, 2),
    ?assertEqual([{<<"CRAH:1">>, 67.5}], Adjustments2),

    Adjustments3 = rf_balancer_strategy:load_in(Id, Trackers, Resource, 3),
    ?assertEqual([{<<"CRAH:1">>, 67.5}], Adjustments3),

    Adjustments4 = rf_balancer_strategy:load_in(Id, Trackers, Resource, 4),
    ?assertEqual([{<<"CRAH:1">>, 67.5}], Adjustments4).

redistribute_pressure_on_online_test(_Config) ->
    Trackers = one_cluster_trackers(),
    Id = <<"CRAH:1">>,
    Resource = pressure,

    Adjustments2 = rf_balancer_strategy:load_in(Id, Trackers, Resource, 2),
    ?assertEqual([
                  {<<"CRAH:1">>, 41.33333333333333},
                  {<<"CRAH:2">>, 37.666666666666664},
                  {<<"CRAH:3">>, 45.0}], Adjustments2),

    Adjustments3 = rf_balancer_strategy:load_in(Id, Trackers, Resource, 3),
    ?assertEqual([
                  {<<"CRAH:1">>, 45.625},
                  {<<"CRAH:2">>, 42.375},
                  {<<"CRAH:3">>, 50.625},
                  {<<"CRAH:4">>, 43.875}], Adjustments3),

    Adjustments4 = rf_balancer_strategy:load_in(Id, Trackers, Resource, 4),
    ?assertEqual([
                  {<<"CRAH:1">>, 48.400000000000006},
                  {<<"CRAH:2">>, 45.2},
                  {<<"CRAH:3">>, 54.0},
                  {<<"CRAH:4">>, 46.800000000000004},
                  {<<"CRAH:5">>, 47.6}], Adjustments4).

%%% utility functions

ets_table_proc() ->
    Self = self(),
    TablePid = spawn(
        fun() ->
            room_distances = ets:new(room_distances, [public, {read_concurrency, true}, named_table]),
            room_locations = ets:new(room_locations, [public, {read_concurrency, true}, named_table]),
            Self ! ready,
            receive stop -> ok end
        end),
    receive
        ready -> ok
    end,
    TablePid.

one_cluster_trackers() ->
    [
        #tracker{
            id = <<"CRAH:1">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 55.5,
            location = geom:point(1.0, 1.0),
            neighbors = [<<"CRAH:2">>, <<"CRAH:3">>, <<"CRAH:4">>, <<"CRAH:5">>]
            },
        #tracker{
            id = <<"CRAH:2">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 56.5,
            location = geom:point(1.0, 2.0),
            neighbors = [<<"CRAH:1">>, <<"CRAH:3">>, <<"CRAH:4">>, <<"CRAH:5">>]
            },
        #tracker{
            id = <<"CRAH:3">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 67.5,
            location = geom:point(1.0, 3.0),
            neighbors = [<<"CRAH:2">>, <<"CRAH:4">>, <<"CRAH:1">>, <<"CRAH:5">>]
            },
        #tracker{
            id = <<"CRAH:4">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 58.5,
            location = geom:point(1.0, 4.0),
            neighbors = [<<"CRAH:3">>, <<"CRAH:5">>, <<"CRAH:2">>, <<"CRAH:1">>]
            },
        #tracker{
            id = <<"CRAH:5">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 59.5,
            location = geom:point(1.0, 5.0),
            neighbors = [<<"CRAH:4">>, <<"CRAH:3">>, <<"CRAH:2">>, <<"CRAH:1">>]
            }
        ].

two_cluster_trackers() ->
    [
        #tracker{
            id = <<"CRAH:1">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 55.5,
            location = geom:point(1.0, 1.0),
            neighbors = [<<"CRAH:2">>, <<"CRAH:3">>, <<"CRAH:4">>, <<"CRAH:5">>]
            },
        #tracker{
            id = <<"CRAH:2">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 56.5,
            location = geom:point(1.0, 2.0),
            neighbors = [<<"CRAH:1">>, <<"CRAH:3">>, <<"CRAH:4">>, <<"CRAH:5">>]
            },
        #tracker{
            id = <<"CRAH:3">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 67.5,
            location = geom:point(1.0, 3.0),
            neighbors = [<<"CRAH:2">>, <<"CRAH:4">>, <<"CRAH:1">>, <<"CRAH:5">>]
            },
        #tracker{
            id = <<"CRAH:4">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 58.5,
            location = geom:point(1.0, 4.0),
            neighbors = [<<"CRAH:3">>, <<"CRAH:5">>, <<"CRAH:2">>, <<"CRAH:1">>]
            },
        #tracker{
            id = <<"CRAH:5">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 59.5,
            location = geom:point(1.0, 5.0),
            neighbors = [<<"CRAH:4">>, <<"CRAH:3">>, <<"CRAH:2">>, <<"CRAH:1">>]
            },
        #tracker{
            id = <<"CRAH:6">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 65.5,
            location = geom:point(10.0, 1.0),
            neighbors = [<<"CRAH:7">>, <<"CRAH:8">>, <<"CRAH:9">>, <<"CRAH:10">>]
            },
        #tracker{
            id = <<"CRAH:7">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 66.5,
            location = geom:point(10.0, 2.0),
            neighbors = [<<"CRAH:6">>, <<"CRAH:8">>, <<"CRAH:9">>, <<"CRAH:10">>]
            },
        #tracker{
            id = <<"CRAH:8">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 77.5,
            location = geom:point(10.0, 3.0),
            neighbors = [<<"CRAH:7">>, <<"CRAH:9">>, <<"CRAH:6">>, <<"CRAH:10">>]
            },
        #tracker{
            id = <<"CRAH:9">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 68.5,
            location = geom:point(10.0, 4.0),
            neighbors = [<<"CRAH:8">>, <<"CRAH:10">>, <<"CRAH:7">>, <<"CRAH:6">>]
            },
        #tracker{
            id = <<"CRAH:10">>,
            strategy_status = automatic,
            device_status = online,
            stage2 = 69.5,
            location = geom:point(10.0, 5.0),
            neighbors = [<<"CRAH:9">>, <<"CRAH:8">>, <<"CRAH:7">>, <<"CRAH:6">>]
            }
        ].


