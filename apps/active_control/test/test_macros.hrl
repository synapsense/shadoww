
-define(assertWithin(Low, High, Expr),
        begin
            ((fun() ->
                      __E = (Expr),
                      if
                          __E < Low; __E > High ->
                              erlang:error({assertWithin_failed,
                                            [{module, ?MODULE},
                                             {line, ?LINE},
                                             {expression, (??Expr)},
                                             {value, __E},
                                             {low, (Low)},
                                             {high, (High)}]});
                          __E >= Low, __E =< High ->
                              ok
                      end
              end)())
        end).

