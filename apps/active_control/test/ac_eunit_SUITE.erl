
-module(ac_eunit_SUITE).

-include_lib("common_test/include/ct.hrl").

-compile(export_all).

%% common test callbacks

all() ->
    [
	datawindow,
	geom,
	lstsq,
	rf_alg
    ]
    .

init_per_testcase(_, C) -> C.
end_per_testcase(_, _) -> ok.

%%% ---
%%% Test Cases
%%% ---

datawindow(_) -> ok = eunit:test(datawindow, [verbose]) .

geom(_) -> ok = eunit:test(geom, [verbose]) .

lstsq(_) -> ok = eunit:test(lstsq, [verbose]) .

rf_alg(_) -> ok = eunit:test(rf_alg, [verbose]) .

