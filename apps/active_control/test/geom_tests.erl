%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(geom_tests).

-include_lib("proper/include/proper.hrl").
-include_lib("eunit/include/eunit.hrl").

-export([cuter_intersection/6, i_counterexample/0]).
-export([pairmap_nonadjacent/2]).

% R1 is the biggest rect containing all others.
% R2 is the biggest room in r1
% R3 and R4 are still smaller rooms in r2
% R5 is a small room in R1 but outside of r2
% R6 is in R5
r1() -> geom:rect(geom:point(1.0, 1.0), 100.0, 100.0) .
r2() -> geom:rect(geom:point(5.0, 5.0), 90.0, 90.0) .
r3() -> geom:rect(geom:point(10.0, 10.0), 80.0, 80.0) .
r4() -> geom:rect(geom:point(15.0, 15.0), 70.0, 70.0) .
r5() -> geom:rect(geom:point(2.0, 2.0), geom:point(4.0, 4.0)) .
r6() -> geom:rect(geom:point(2.5, 2.5), geom:point(3.5, 3.5)) .

% in room r4
p1() -> geom:point(20.0, 20.0) .
% in room r2
p2() -> geom:point(7.0, 7.0) .
% in room r6
p3() -> geom:point(3.0, 3.0) .


% Lists of rects are randomized to ensure correct results are not dependent on order

innermost_rect_returns_inner_test_() ->
    Rects = [r1(), r2(), r3(), r4(), r5(), r6()],
    [?_assertEqual(r4(), geom:smallest_containing_point(randomized_list(Rects), p1())) || _ <- lists:seq(0,10)]
    .

innermost_not_inner_test_() ->
    Rects = [r1(), r2(), r3(), r4(), r5(), r6()],
    [?_assertEqual(r2(), geom:smallest_containing_point(randomized_list(Rects), p2())) || _ <- lists:seq(0,10)]
    .

innermost_sideroom_test_() ->
    Rects = [r1(), r2(), r3(), r4(), r5(), r6()],
    [?_assertEqual(r6(), geom:smallest_containing_point(randomized_list(Rects), p3())) || _ <- lists:seq(0,10)]
    .

point_in_poly_test() ->
    P = fun(X,Y) -> geom:point(X,Y) end,
    Poly = geom:poly([P(1.0, 1.0), P(10.0, 1.0), P(5.0, 10.0)]),
    Pt1 = P(3.0, 3.0),
    Pt2 = P(7.0, 7.0),
    ?assertEqual(true, geom:contains(Poly, Pt1)),
    ?assertEqual(false, geom:contains(Poly, Pt2))
    .

point_in_poly_from_data_test_d() ->
    Filename = filename:join([code:lib_dir(active_control), "test", "polydata.dat"]),
    {ok, Testdata} = file:consult(Filename),
    [case catch geom:contains(geom:poly(Poly), Pt) of
         {'EXIT', R} ->
             ?debugFmt("create poly failed: ~p~n", [R]);
         Result -> ok;
         Wrong ->
             ?debugFmt("expected ~p got ~p: {~p,~p}", [Result, Wrong, Pt, Poly]),
             MinPoly = minimize_failing_case(Result, Pt, Poly),
             ?debugFmt("minimized (shorter by ~b): ~p", [length(Poly) - length(MinPoly), MinPoly]),
             ?assertEqual(Result, Wrong)
     end || {Pt, Poly, Result} <- Testdata]
    .

point_in_ellipse_test() ->
    E5 = geom:ellipse_from_rect(r5()),
    ?assertEqual(true, geom:contains(E5, p3()))
    .

point_not_in_ellipse_test() ->
    E2 = geom:ellipse_from_rect(r2()),
    ?assertEqual(false, geom:contains(E2, p2()))
    .

minimize_failing_case(Expected, Pt, Poly) ->
    Min1 = remove_one(Expected, Pt, [], Poly),
    if
        length(Min1) == length(Poly) -> Min1;
        true -> minimize_failing_case(Expected, Pt, Min1)
    end.

remove_one(_Expected, _Pt, PolyHd, []) ->
    PolyHd;
remove_one(Expected, Pt, PolyHd, [T1 | PolyTl]) ->
    case geom:contains(PolyHd ++ PolyTl, Pt) of
        Expected -> remove_one(Expected, Pt, PolyHd ++ [T1], PolyTl);
        _ -> remove_one(Expected, Pt, PolyHd, PolyTl)
    end.

map_n_test() ->
    L = lists:seq(1, 5),
    ?assertEqual([2, 4, 6, 8, 10],
                 geom:map_n(fun(A) -> 2*A end, L, open)),
    ?assertEqual([2, 4, 6, 8, 10],
                 geom:map_n(fun(A) -> 2*A end, L, closed)),
    ?assertEqual([3, 5, 7, 9],
                 geom:map_n(fun(A,B) -> A+B end, L, open)),
    ?assertEqual([3, 5, 7, 9, 6],
                 geom:map_n(fun(A,B) -> A+B end, L, closed)),
    ?assertEqual([6, 9, 12],
                 geom:map_n(fun(A,B,C) -> A+B+C end, L, open)),
    ?assertEqual([6, 9, 12, 10, 8],
                 geom:map_n(fun(A,B,C) -> A+B+C end, L, closed)),
    ?assertEqual([10, 14],
                 geom:map_n(fun(A,B,C,D) -> A+B+C+D end, L, open)),
    ?assertEqual([10, 14, 13, 12, 11],
                 geom:map_n(fun(A,B,C,D) -> A+B+C+D end, L, closed)).

% L with the order of elements randomized
randomized_list(L) ->
    [I || {_R, I} <- lists:sort([{rand:uniform(), I} || I <- L])].

common_intersection([], []) -> true;
common_intersection([], [X]) -> X;
common_intersection([X], []) -> X;
common_intersection([P1],[P2]) ->
    geom:coincident(P1, P2).

line_intersects_point_test() ->
    S = geom:segment(geom:point(3.0, 1.0), geom:point(1.0, 3.0)),
    P = geom:point(2.0, 2.0),
    ?assertMatch([P], geom:intersects(S, P)).

line_shouldnt_intersect_line_test() ->
    S1 = geom:segment(geom:point(0.0, 0.0), geom:point(5.0, 0.0)),
    S2 = geom:segment(geom:point(1.0, 1.0), geom:point(1.0, 4.0)),
    ?assertMatch([], geom:intersects(S1, S2)).

line_intersects_line_test() ->
    S1 = geom:segment(geom:point(1.1, 1.1), geom:point(3.3, 3.3)),
    S2 = geom:segment(geom:point(3.0, 1.0), geom:point(1.0, 3.0)),
    P = geom:point(2.0, 2.0),
    ?assertMatch([P], geom:intersects(S1, S2)),
    ?assertMatch([P], geom:intersects(S2, S1)).

line_intersects_poly_test() ->
    S = geom:segment(geom:point(1.1, 1.1), geom:point(3.3, 3.3)),
    Py = geom:poly([geom:point(0.0, 0.0),
                    geom:point(1.1, 3.3),
                    geom:point(3.3, 1.1),
                    geom:point(3.3, 0.0)]),
    P = geom:point(2.2, 2.2),
    ?assertMatch([P], geom:intersects(S, Py)).

point() ->
    ?LET({X, Y, A, B},
         {integer(), integer(), integer(), integer()},
         geom:point(X + A / 100.0, Y + B / 100.0)).

segment() ->
    ?LET({P1, P2},
         ?SUCHTHAT({P1, P2},
                   {point(), point()},
                   not geom:coincident(P1, P2)),
         geom:segment(P1, P2)).

intersecting_segments() ->
    ?SUCHTHAT({S1, S2},
              {segment(), segment()},
              geom:intersects(S1, S2) =/= []).

% Think of the list L as items in a ring.
% Call F on all non-adjacent combinations of points in the ring.
pairmap_nonadjacent(F, [H|L]) when L > 2 ->
    lists:foldl(fun(I, A) -> [F(H,I) | A] end, [], tl(lists:reverse(tl(L))))
    ++ lists:reverse(r_pairmap_nonadjacent(F, L, [])).
r_pairmap_nonadjacent(_F, [_H, _L], A) -> A;
r_pairmap_nonadjacent(F, [H|L], Acc) -> r_pairmap_nonadjacent(F, L, lists:foldl(fun(I, A) -> [F(H,I) | A] end, Acc, tl(L))).

prop_segment_intersection_doesnt_fail() ->
    ?FORALL({S1, S2},
            {segment(), segment()},
            ?WHENFAIL(
               io:fwrite("S1: ~p~nS2: ~p~n", [S1, S2]),
               is_list(geom:intersects(S1, S2)))).
segment_intersection_doesnt_fail_test_() ->
    {timeout, 30,
     ?_assertEqual(true,
                   proper:quickcheck(prop_segment_intersection_doesnt_fail(),
                                     [verbose, {numtests, 10000}]))}.

gp([]) -> "[]";
gp([I]) -> lists:flatten(geom:print(I));
gp(I) -> lists:flatten(geom:print(I)).

prop_segments_intersect_both_ways() ->
    ?FORALL({S1, S2},
            {segment(), segment()},
            ?WHENFAIL(
               begin
                   io:fwrite("intersects(S1, S2): ~s~n",
                             [gp(geom:intersects(S1, S2))]),
                   io:fwrite("intersects(S2, S1): ~s~n",
                             [gp(geom:intersects(S2, S1))])
               end,
               common_intersection(
                 geom:intersects(S1, S2),
                 geom:intersects(S2, S1)))).
segments_intersect_both_ways_test() ->
    ?assertEqual(true,
                 proper:quickcheck(prop_segments_intersect_both_ways(),
                                   [verbose, {numtests, 1000}])).

prop_intersecting_segments_intersect_both_ways() ->
    ?FORALL({P1, P2, P3},
            {point(), point(), point()},
            common_intersection(
              geom:intersects(geom:segment(P1, P2), geom:segment(P2, P3)),
              geom:intersects(geom:segment(P2, P3), geom:segment(P1, P2)))
            andalso
            common_intersection(
              geom:intersects(geom:segment(P1, P3), geom:segment(P2, P3)),
              geom:intersects(geom:segment(P2, P3), geom:segment(P1, P3)))
           ).
intersecting_segments_intersect_both_ways_test() ->
    ?assertEqual(true,
                 proper:quickcheck(prop_intersecting_segments_intersect_both_ways(),
                                   [verbose, {numtests, 2000}])).

cuter_intersection(X1, Y1, X2, Y2, X3, Y3) when
      is_float(X1), is_float(Y1),
      is_float(X2), is_float(Y2),
      is_float(X3), is_float(Y3) ->
    P1 = geom:point(X1, Y1),
    P2 = geom:point(X2, Y2),
    P3 = geom:point(X3, Y3),
    A = geom:intersects(geom:segment(P1, P2), geom:segment(P2, P3)),
    B = geom:intersects(geom:segment(P2, P3), geom:segment(P1, P2)),
    C = geom:intersects(geom:segment(P1, P3), geom:segment(P2, P3)),
    D = geom:intersects(geom:segment(P2, P3), geom:segment(P1, P3)),
    case common_intersection(A,B) andalso common_intersection(C,D) of
        true -> ok;
        false -> throw({A,B,C,D})
    end;
cuter_intersection(_, _, _, _, _, _) -> ok.

i_counterexample() ->
    catch cuter_intersection(1572.992, 1145.5017,
                             804.9918, 1145.5017,
                             1573.0, 284.4983).

prop_segment_intersection_on_both_segments() ->
    ?FORALL({S1, S2},
            intersecting_segments(),
            ?WHENFAIL(
               begin
                   io:fwrite("S1: ~s~nS2: ~s~n",
                             [gp(S1), gp(S2)])
               end,
               begin
                   [P] = geom:intersects(S1, S2),
                   common_intersection(
                     geom:intersects(S1, P),
                     geom:intersects(S2, P))
               end)).
segment_intersection_on_both_segments_test() ->
    ?assertEqual(true,
                 proper:quickcheck(prop_segment_intersection_on_both_segments(),
                                   [verbose,
                                    {numtests, 1000},
                                    {constraint_tries, 100}
                                   ])).

colinear_triple() ->
    ?LET({M, B, X1, X2, X3},
         ?SUCHTHAT({M,B,X1,X2,X3},
                   {float(), float(), float(), float(), float()},
                   begin
                       P1 = geom:point(X1, M*X1+B),
                       P2 = geom:point(X2, M*X2+B),
                       P3 = geom:point(X3, M*X3+B),
                       false == (geom:coincident(P1, P2)
                                 orelse geom:coincident(P2, P3)
                                 orelse geom:coincident(P1, P3))
                   end),
         {geom:point(X1, M*X1+B),
          geom:point(X2, M*X2+B),
          geom:point(X3, M*X3+B)}).

prop_colinear_point_on_line() ->
    ?FORALL({P1, P2, P3},
            colinear_triple(),
            [false, false, true] ==
            lists:sort([geom:intersects(geom:segment(P1, P2), P3) /= [],
                        geom:intersects(geom:segment(P1, P3), P2) /= [],
                        geom:intersects(geom:segment(P3, P2), P1) /= []])).
colinear_point_on_line_test() ->
    ?assertEqual(true, proper:quickcheck(prop_colinear_point_on_line(),
                                         [verbose, {numtests, 1000}])).

prop_segments_intersect_at_endpoint() ->
    T = fun(_P, []) -> [];
           (P, [P2]) -> [geom:coincident(P,P2)]
        end,
    ?FORALL({P1, P2, P3},
            colinear_triple(),
            ?WHENFAIL(
               begin
                   io:fwrite("P2 ~p: ~p~n", [P2, gp(geom:intersects(
                                                      geom:segment(P1, P2),
                                                      geom:segment(P2, P3)))]),
                   io:fwrite("P1 ~p: ~p~n", [P1, gp(geom:intersects(
                                                      geom:segment(P2, P1),
                                                      geom:segment(P1, P3)))]),
                   io:fwrite("P3 ~p: ~p~n", [P3, gp(geom:intersects(
                                                      geom:segment(P1, P3),
                                                      geom:segment(P3, P2)))])
               end,
               ?assertMatch([true],
                            T(P2,
                              geom:intersects(
                                geom:segment(P1, P2),
                                geom:segment(P2, P3)))
                            ++
                            T(P1,
                              geom:intersects(
                                geom:segment(P2, P1),
                                geom:segment(P1, P3)))
                            ++
                            T(P3,
                              geom:intersects(
                                geom:segment(P1, P3),
                                geom:segment(P3, P2)))
                           ))).
segments_intersect_at_endpoint_test_d() ->
    % near impossible to make this test work under the given rules.
    % The rounding done on points means that what was 3 colinear points at
    % high precision are no longer colinear after rounding.
    % Because of that, the 2 lines are not recognized as colinear but rather
    % coincident at a point (one of the endpoints).
    ?assertEqual(true, proper:quickcheck(prop_segments_intersect_at_endpoint(),
                                         [verbose, {numtests, 1000}])).

line_intersect_poly_at_vertex_test() ->
    Poly = geom:poly([geom:point(1.0, 1.0),
                      geom:point(5.0, 2.0),
                      geom:point(3.0, 3.0)]),
    P1 = geom:point(0.0, 1.0),
    P2 = geom:point(1.0, 1.0),
    ?assertEqual(geom:distance(P1, P2),
                 geom:walk_distance(P1, P2, [Poly])).

v_line_point_intersect_at_origin_test() ->
    O = geom:point(0.0, 0.0),
    S = geom:segment(O, geom:point(0.0, 0.1)),
    ?assert(common_intersection(
              [O],
              geom:intersects(S, O))).

v_lines_intersect_at_origin_test() ->
    O = geom:point(0.0, 0.0),
    S1 = geom:segment(O, geom:point(0.0, -0.1)),
    S2 = geom:segment(O, geom:point(0.0, 0.1)),
    try
        [I] = geom:intersects(S1, S2),
        ?assert(geom:coincident(O, I))
    catch
        E:R ->
            ?debugFmt("~p:~p~n~p", [E, R, erlang:get_stacktrace()]),
            error({E,R})
    end.

simple_walk_distance_test() ->
    P = fun geom:point/2,
    W = geom:poly([
                   P(3.0, 3.0),
                   P(17.0, 3.0),
                   P(17.0, 7.0),
                   P(3.0, 7.0)
                  ]),

    ?assertEqual([P(10.0,3.0), P(10.0,7.0)], geom:intersects(geom:segment(P(10.0, 1.0), P(10.0, 10.0)), W)),
    ?assertEqual(23.0,
                 geom:walk_distance(P(10.0, 1.0), P(10.0, 10.0), [W])),
    ?assertEqual(21.0,
                 geom:walk_distance(P(11.0, 1.0), P(11.0, 10.0), [W])),
    ?assertEqual(9.0,
                 geom:walk_distance(P(18.0, 1.0), P(18.0, 10.0), [W])),
    ?assertEqual(9.0,
                 geom:walk_distance(P(17.0, 1.0), P(17.0, 10.0), [W])),
    ?assert(geom:distance(P(16.0, 1.0), P(18.0, 5.0))
            ==
            geom:walk_distance(P(16.0, 1.0), P(18.0, 5.0), [W])),
    ?assert(geom:distance(P(16.0, 1.0), P(18.0, 6.0))
            <
            geom:walk_distance(P(16.0, 1.0), P(18.0, 6.0), [W])),
    ok.

walk_distance_horiz_line_test() ->
    Wall = geom:poly([
                      geom:point(1296.0, 593.0),
                      geom:point(661.0, 593.0),
                      geom:point(659.0, 874.0)
                     ]),
    P1 = geom:point(980.0, 350.0),
    P2 = geom:point(1193.0, 1059.0),
    F = geom:walk_distance(P1, P2, [Wall]),
    ?assert(is_float(F)).

crosses_near_vertex_test() ->
    Wall =  geom:poly([
                       geom:point(60.0, 722.0),
                       geom:point(497.49988, 721.9977),
                       geom:point(496.0, 426.0)
                      ]),
    P1 = geom:point(702.0, 226.0),
    P2 = geom:point(441.0, 858.0),
    F = geom:walk_distance(P1, P2, [Wall]),
    %?debugFmt("~nD: ~p, WD: ~p~n", [geom:distance(P1, P2), F]),
    ?assert(is_float(F)),
    ?assert(F > geom:distance(P1, P2)).

line_colinear_with_edge_test() ->
    Wall = geom:poly([
                      geom:point(3.0, 3.0),
                      geom:point(4.0, 4.0),
                      geom:point(5.0, 3.0)
                     ]),
    P1 = geom:point(2.0, 2.0),
    P2 = geom:point(5.0, 5.0),
    D = geom:distance(P1, P2),
    WD = geom:walk_distance(P1, P2, [Wall]),
    ?assertMatch([], geom:intersects(geom:segment(P1, P2), Wall)),
    ?assertMatch(D, WD).

line_intersects_vertex_test() ->
    CoPoint = geom:point(4.0, 4.0),
    Wall = geom:poly([
                      geom:point(3.0, 3.0),
                      CoPoint,
                      geom:point(5.0, 3.0)
                     ]),
    P1 = geom:point(2.0, 5.0),
    P2 = geom:point(6.0, 3.0),
    D = geom:distance(P1, P2),
    WD = geom:walk_distance(P1, P2, [Wall]),
    ?assertMatch(false, geom:adjacent_vertices(CoPoint, CoPoint, Wall)),
    [IPoint] = geom:intersects(geom:segment(P1, P2), Wall),
    ?assert(geom:coincident(CoPoint, IPoint)),
    ?assert(D == WD).

self_intersecting_poly_fails_test() ->
    ?assertError({self_intersections, _},
                 geom:poly([geom:point(0.0,0.0),
                            geom:point(2.0, 2.0),
                            geom:point(2.0, 0.0),
                            geom:point(0.0, 2.0)])).

dup_points_eliminated_test() ->
    ?assertError({insufficient_vertices, _},
                 geom:poly([geom:point(1.0,1.0),
                            geom:point(2.0, 2.0),
                            geom:point(1.0, 1.0)])).

colinear_triples_reduced_test() ->
    ?assertError({insufficient_vertices, _},
                 geom:poly([geom:point(1.0, 1.0),
                            geom:point(2.0, 2.0),
                            geom:point(3.0, 3.0)])),
    P = geom:poly([geom:point(1.0, 1.0),
                   geom:point(2.0, 2.0),
                   geom:point(3.0, 3.0),
                   geom:point(1.0, 2.0)]),
    ?assertEqual(3, length(geom:segments(P))).

line_intersects_nonadjacent_vertex_test() ->
    Wall1 = geom:poly([
                      geom:point(3.0, 3.0),
                      geom:point(4.0, 4.0),
                      geom:point(5.0, 3.0)
                     ]),
    Wall2 = geom:poly([
                      geom:point(4.0, 4.0),
                      geom:point(5.0, 3.0),
                      geom:point(3.0, 3.0)
                     ]),
    Wall3 = geom:poly([
                      geom:point(5.0, 3.0),
                      geom:point(3.0, 3.0),
                      geom:point(4.0, 4.0)
                     ]),

    P1 = geom:point(4.0, 1.0),
    P2 = geom:point(4.0, 6.0),

    P3 = geom:point(2.0, 3.0),
    P4 = geom:point(6.0, 3.0),

    ?assertEqual(5.0, geom:distance(P1, P2)),

    ?assertEqual([geom:point(4.0, 3.0), geom:point(4.0, 4.0)],
                 lists:sort(geom:intersects(geom:segment(P1, P2), Wall1))),
    ?assertEqual([geom:point(4.0, 3.0), geom:point(4.0, 4.0)],
                 lists:sort(geom:intersects(geom:segment(P1, P2), Wall2))),
    ?assertEqual([geom:point(4.0, 3.0), geom:point(4.0, 4.0)],
                 lists:sort(geom:intersects(geom:segment(P1, P2), Wall3))),

    ?assertEqual(5.0 + math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall1])),
    ?assertEqual(5.0 + math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall2])),
    ?assertEqual(5.0 + math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall3])),

    ?assertEqual(4.0, geom:distance(P3, P4)),
    ?assertEqual(4.0, geom:walk_distance(P3, P4, [Wall1])),
    ?assertEqual(4.0, geom:walk_distance(P3, P4, [Wall2])),
    ?assertEqual(4.0, geom:walk_distance(P3, P4, [Wall3])),

    ?assertEqual([], lists:sort(geom:intersects(geom:segment(P3, P4), Wall1))),
    ?assertEqual([], lists:sort(geom:intersects(geom:segment(P3, P4), Wall2))),
    ?assertEqual([], lists:sort(geom:intersects(geom:segment(P3, P4), Wall3))).

line_intersects_nonadjacent_vertices_test() ->
    Wall1 = geom:poly([
                      geom:point(3.0, 3.0),
                      geom:point(4.0, 4.0),
                      geom:point(5.0, 3.0),
                      geom:point(4.0, 2.0)
                     ]),
    Wall2 = geom:poly([
                      geom:point(4.0, 4.0),
                      geom:point(5.0, 3.0),
                      geom:point(4.0, 2.0),
                      geom:point(3.0, 3.0)
                     ]),
    Wall3 = geom:poly([
                      geom:point(5.0, 3.0),
                      geom:point(4.0, 2.0),
                      geom:point(3.0, 3.0),
                      geom:point(4.0, 4.0)
                     ]),
    Wall4 = geom:poly([
                      geom:point(4.0, 2.0),
                      geom:point(3.0, 3.0),
                      geom:point(4.0, 4.0),
                      geom:point(5.0, 3.0)
                     ]),

    P1 = geom:point(4.0, 1.0),
    P2 = geom:point(4.0, 6.0),

    P3 = geom:point(2.0, 3.0),
    P4 = geom:point(6.0, 3.0),

    ?assertEqual(5.0, geom:distance(P1, P2)),
    ?assertEqual(3.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall1])),
    ?assertEqual(3.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall2])),
    ?assertEqual(3.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall3])),
    ?assertEqual(3.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P1, P2, [Wall4])),

    ?assertEqual(4.0, geom:distance(P3, P4)),
    ?assertEqual(2.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P3, P4, [Wall1])),
    ?assertEqual(2.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P3, P4, [Wall2])),
    ?assertEqual(2.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P3, P4, [Wall3])),
    ?assertEqual(2.0 + 2.0 * math:sqrt(2.0),
                 geom:walk_distance(P3, P4, [Wall4])).

