
-module(datawindow_tests).

-include_lib("eunit/include/eunit.hrl").

min_test() ->
    Items = lists:zip([1.0 * X || X <- lists:seq(1,10)], lists:seq(1,10)),
    DW = lists:foldl(fun datawindow:push/2, datawindow:new(min, 5), Items),
    ?assertEqual(6.0, datawindow:value(DW))
    .

max_test() ->
    Items = lists:zip([1.0 * X || X <- lists:seq(10,1,-1)], lists:seq(1,10)),
    DW = lists:foldl(fun datawindow:push/2, datawindow:new(max, 5), Items),
    ?assertEqual(5.0, datawindow:value(DW))
    .

random_min_test_() ->
    N = 1000,
    WindowSz = 100,
    [?_test(begin
	Values = [rand:uniform() || _ <- lists:seq(1,N)],
	Items = lists:zip(Values, lists:seq(1,N)),
	DW = lists:foldl(fun datawindow:push/2, datawindow:new(min, WindowSz), Items),
	Min = lists:min(lists:nthtail(N-WindowSz, Values)),
	?assertEqual(Min, datawindow:value(DW))
	    end) || _ <- lists:seq(1,10)]
    .

random_max_test_() ->
    N = 1000,
    WindowSz = 100,
    [?_test(begin
	Values = [rand:uniform() || _ <- lists:seq(1,N)],
	Items = lists:zip(Values, lists:seq(1,N)),
	DW = lists:foldl(fun datawindow:push/2, datawindow:new(max, WindowSz), Items),
	Max = lists:max(lists:nthtail(N-WindowSz, Values)),
	?assertEqual(Max, datawindow:value(DW))
	    end) || _ <- lists:seq(1,10)]
    .

