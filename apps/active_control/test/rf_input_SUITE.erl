%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(rf_input_SUITE).

%% API
-export([all/0,
         suite/0,
         groups/0,
         init_per_suite/1,
         end_per_suite/1,
         group/1,
         init_per_group/2,
         end_per_group/2,
         init_per_testcase/2,
         end_per_testcase/2]).

%% Test cases
-export([
         new_aoe/1,
         update_inputs/1,
         set_input_config/1,
         set_input_target/1,
         get_all_input_config/1,
         get_aoe_status/1
        ]).

-define(N_CRAHS, 5).
-define(N_INPUTS, 50).
-define(CRAHS_PER_SENSOR, 2).
-define(ROOM_ID, <<"ROOM:1">>).
-define(RESOURCE, temperature).
-define(AGGRESSIVENESS, med).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").
-include_lib("active_control/test/test_macros.hrl").

-include_lib("active_control/include/ac_status.hrl").

%%%===================================================================
%%% Common Test callbacks
%%%===================================================================

all() ->
    [
     new_aoe,
     update_inputs,
     set_input_config,
     set_input_target,
     get_all_input_config,
     get_aoe_status
    ].

suite() ->
    [{timetrap, {seconds, 30}}].

groups() -> [].

init_per_suite(Config) ->
    {ok, Started1} = application:ensure_all_started(meck),
    {ok, Started2} = application:ensure_all_started(dumbtable),
    ct:pal("started: ~p ~p", [Started1, Started2]),
    dumbtable:create_local_copy(state_cache),

    meck:new(driver_lib, [passthrough, no_link]),
    meck:expect(driver_lib, reg, fun({Id, R, T}) -> {Id, R, T} end),

    {ok, _EvtPid} = gen_key_event:start({local, sensor_event}),

    Inputs = inputs(?N_INPUTS),
    {ok, Pid} = rf_input:start_link(?ROOM_ID, ?RESOURCE, ?AGGRESSIVENESS, Inputs),
    unlink(Pid),
    meck:expect(driver_lib, lookup, fun
                                        ({?ROOM_ID, ?RESOURCE, _}) -> Pid;
                                        ({?ROOM_ID, none, _}) -> Pid
                                    end),
    meck:expect(driver_lib, call, fun(_K, _F, _A) -> ok end),

    [{started1, Started1}, {started2, Started2}, {input_pid, Pid} | Config].

end_per_suite(Config) ->
    exit(?config(input_pid, Config), shutdown),

    meck:unload(),
    gen_key_event:stop(sensor_event),
    dumbtable:delete_local_copy(state_cache),
    [ok = application:stop(A) || A <- lists:reverse(?config(started2, Config))],
    [ok = application:stop(A) || A <- lists:reverse(?config(started1, Config))].

group(_GroupName) ->
    [].

init_per_group(_GroupName, Config) ->
    Config.

end_per_group(_GroupName, _Config) ->
    ok.

init_per_testcase(_Testcase, Config) ->
    meck:new(strategy_param),
    meck:expect(strategy_param, find,
                [{[max_sample_age, ?RESOURCE, ?AGGRESSIVENESS, ?ROOM_ID], 1000},
                {[quorum, ?RESOURCE, ?AGGRESSIVENESS, ?ROOM_ID], {1.0, 2}}]),
    meck:expect(strategy_param, find,
                [mean_shift, ?RESOURCE, ?AGGRESSIVENESS, ?ROOM_ID, '_'], 1.0),
    Config.

end_per_testcase(_Testcase, _Config) ->
    true = meck:validate(strategy_param),
    meck:unload(strategy_param).

%%%===================================================================
%%% Test cases
%%%===================================================================

new_aoe(_Config) ->
    AoE = gen_aoes(?CRAHS_PER_SENSOR, ?N_CRAHS, ?N_INPUTS),
    ok = rf_input:new_aoe(?ROOM_ID, ?RESOURCE, AoE),
    Statuses = rf_input:get_all_input_status(?ROOM_ID, ?RESOURCE),
    [?assertEqual(null, D) || #input_status{deviation=D} <- Statuses].

update_inputs(_Config) ->
    [gen_key_event:notify(sensor_event, test_lib:sensor_id(IdNo), test_lib:random_sample(IdNo))
     || IdNo <- lists:seq(1, ?N_INPUTS)],
    timer:sleep(100),
    Statuses = rf_input:get_all_input_status(?ROOM_ID, ?RESOURCE),
    [?assertNotEqual(null, D) || #input_status{deviation=D} <- Statuses],
    ?assertEqual(?N_CRAHS, meck:num_calls(driver_lib, call, '_')).

set_input_config(_Config) ->
    IdNo = rand:uniform(?N_INPUTS),
    Id = test_lib:input_id(IdNo),
    [Old] = rf_input:get_input_config(?ROOM_ID, ?RESOURCE, [Id]),
    ?assertEqual(Id, Old#single_input.id),
    New = input(IdNo),
    rf_input:set_input_config(?ROOM_ID, New),
    ?assertEqual(New, rf_input:get_input_config(?ROOM_ID, ?RESOURCE, Id)).

set_input_target(_Config) ->
    IdNo = rand:uniform(?N_INPUTS),
    Id = test_lib:input_id(IdNo),
    [Old] = rf_input:get_input_config(?ROOM_ID, ?RESOURCE, [Id]),
    ?assertEqual(Id, Old#single_input.id),
    NewTarget = test_lib:random_temp(),
    rf_input:set_input_target(?ROOM_ID, Id, ?RESOURCE, NewTarget),
    New = rf_input:get_input_config(?ROOM_ID, ?RESOURCE, Id),
    ?assertEqual(NewTarget, New#single_input.target).

get_all_input_config(_Config) ->
    InputConfig = rf_input:get_all_input_config(?ROOM_ID, ?RESOURCE),
    ?assertEqual(?N_INPUTS, length(InputConfig)),
    InputStatus = rf_input:get_all_input_status(?ROOM_ID, ?RESOURCE),
    ?assertEqual(?N_INPUTS, length(InputStatus)).

get_aoe_status(_Config) ->
    TStatusLen = fun(Id) ->
                         PL = rf_input:get_aoe_status(?ROOM_ID,
                                                      ?RESOURCE,
                                                      test_lib:crah_id(Id)),
                         length(proplists:get_value(inputs_status, PL))
                 end,
    NStatuses = [TStatusLen(Id) || Id <- lists:seq(1,?N_CRAHS)],

    InputStatus = fun(Id) ->
                          PL = rf_input:get_aoe_status(?ROOM_ID,
                                                       ?RESOURCE,
                                                       test_lib:crah_id(Id)),
                          proplists:get_value(inputs_status, PL)
                  end,
    IIDs = [[IID || #input_status{id=IID} <- InputStatus(Id)]
            || Id <- lists:seq(1, ?N_CRAHS)],

    UniqueIIDs = lists:usort(lists:flatten(IIDs)),
    ?assertEqual(?N_INPUTS, length(UniqueIIDs)),
    AvgSensorsPerCrah = lists:sum(NStatuses) / length(NStatuses),
    CalcAvg = ?N_INPUTS / ?N_CRAHS * ?CRAHS_PER_SENSOR,
    ?assertEqual(CalcAvg, AvgSensorsPerCrah).

%% support functions
gen_aoes(SPerC, NCrahs, NInputs) ->
    lists:flatten([[{test_lib:crah_id(CrahNo), test_lib:input_id(I)}
                    || CrahNo <- uniform_distinct(SPerC, NCrahs)]
                   || I <- lists:seq(1,NInputs)]).

% generate NN distinct random integers,
% distributed uniformly between 1 and N (inclusive)
% NN must be << N, enforced as NN < N/2
% The larger NN is relative to N, the longer
% this function will take
uniform_distinct(NN, N) when NN < (N/2), NN > 0 ->
    uniform_distinct(NN-1, N, [rand:uniform(N)]).

uniform_distinct(0, _N, Acc) -> Acc;
uniform_distinct(NN, N, Acc) ->
    X = rand:uniform(N),
    case lists:member(X, Acc) of
        true -> uniform_distinct(NN, N, Acc);
        false -> uniform_distinct(NN-1, N, [X | Acc])
    end.

inputs(N) ->
    [input(I) || I <- lists:seq(1,N)].

random_point() -> geom:point(100*rand:uniform(), 100*rand:uniform()).

input(IdNo) ->
    room_builder:single_input(test_lib:input_id(IdNo),
                              test_lib:sensor_id(IdNo),
                              random_point(),
                              test_lib:random_temp(),
                              true,
                              ?RESOURCE).

