
import random
import time

import numpy as np

def main():
	samples = 5
	testsets = 1000

	random.seed()

	now = time.time()

	# generate some random data that is somewhat like real data might be:
	#   timestamps that are very large integers
	#   multiple sets of sample data:
	#     sample data that is centered around 45 degrees +/- 10
	#     sample data that varies very little: +/- 0.1
	#
	sample_timestamps = np.random.randint(1000, size=(testsets, samples)) + int(now)
	sample_data1 = np.random.normal(loc=45.0, scale=10.0, size=(testsets, samples))
	sample_data2 = np.random.normal(loc=45.0, scale=0.1, size=(testsets, samples))

	residuals1 = [[ssr(xs, ys)] for (xs, ys) in zip(sample_timestamps, sample_data1)]
	residuals2 = [[ssr(xs, ys)] for (xs, ys) in zip(sample_timestamps, sample_data2)]

	sample_timestamps = np.concatenate([sample_timestamps, sample_timestamps], 0)
	sample_data = np.concatenate([sample_data1, sample_data2], 0)
	residuals = np.concatenate([residuals1, residuals2], 0)

	with open("sample_timestamps.dat", 'w') as f:
		for row in sample_timestamps:
			f.write(" ".join(["%i" % i for i in row]))
			f.write("\n")
	write_array("sample_data.dat", sample_data)
	write_array("sumsq_residuals.dat", residuals)

def ssr(xs, ys):
	xs = [x - min(xs) for x in xs]
	A = np.vstack([xs, np.ones(len(xs))]).T
	((m,c), residues, rank, s) = np.linalg.lstsq(A, ys)
	return residues[0]

def write_array(fname, array):
	with open(fname, 'w') as f:
		for row in array:
			f.write(" ".join(["%.9e" % i for i in row]))
			f.write("\n")

if __name__ == "__main__":
	main()

