-module(sensor_data_feeder_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

-define(ADDRESS, "127.0.0.1").
-define(PORT, 6238).

-define(SENSOR_EVT_MGR, sensor_event).

suite() ->
	[
		{timetrap, {seconds, 30}}
	]
	.

all() ->
	[
		send_wsn_sensor_data_triggers_gen_event
	]
	.

init_per_suite(Config) ->
	{ok, Started} = application:ensure_all_started(meck),
	test_lib:mock_folsom(),
	[{started, Started} | Config].

end_per_suite(Config) ->
	meck:unload(),
	[ok = application:stop(A) || A <- lists:reverse(?config(started, Config))].

init_per_group(_G, Config) ->
	Config
	.

end_per_group(_G, _Config) ->
	ok
	.

init_per_testcase(_TestCase, Config) ->
	Config
	.

end_per_testcase(_TestCase, _Config) ->
	ok
	.


send_wsn_sensor_data_triggers_gen_event(_Config) ->
	{ok, EvPid} = gen_key_event:start({local, ?SENSOR_EVT_MGR}),
	% register for events
	Self = self(),
	gen_key_event:add_handler(EvPid, [<<"WSNSENSOR:1">>], fun(_Id, Ev) -> Self ! Ev end),
	{ok, LS} = gen_tcp:listen(?PORT, [binary, {packet, 0}, {reuseaddr, true}]),
	{ok, _Pid} = sensor_data_feeder:start_link(?ADDRESS, ?PORT),
	{ok, Sock} = gen_tcp:accept(LS),
	Data = "{\"type\":\"WSN_SENSOR_DATA\",\"DATA\":73.5,\"TIMESTAMP\":12345,\"to\":\"WSNSENSOR:1\"}\r\n",
	ok = gen_tcp:send(Sock, Data),
	Msg = receive
		M -> M
		after 1000 -> no_msg
	end,
	gen_key_event:stop(EvPid),
	gen_tcp:close(Sock),
	?assertEqual(sample:new(<<"WSNSENSOR:1">>, 73.5, 12345), Msg)
	.
