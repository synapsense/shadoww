
-module(room_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

-define(TIMER, 500).

%% common test callbacks
suite() ->
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:add_config(ct_config_plain, TestCfg),
    [
     {timetrap, {minutes, 1}},
     {require, env}
    ]
    .

init_per_suite(Config) ->
    {ok, Started} = test_lib:start_ac(),
    room_manager:isolated(false),
    process_cache:isolated(false),
    [{started, Started} | Config].

end_per_suite(Config) ->
    test_lib:stop_ac(?config(started, Config)),
    dumbtable:delete_local_copy(room_manager),
    dumbtable:delete_local_copy(state_cache),
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:remove_config(ct_config_plain, TestCfg).

init_per_group(driver_tests, _Config) ->
    ok;

init_per_group(_G, Config) ->
    Config.

end_per_group(driver_tests, _Config) ->
    ok;
end_per_group(_G, _Config) ->
    ok.

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

groups() ->
    [
     {
      lifecycle,
      [sequence],
      [
       start_rc_room,
       room_status,
       stop_room,
       all_clean
      ]
     }
    ]
    .

all() ->
    [
     {group, lifecycle}
    ]
    .

%%% ---
%%% Test Cases
%%% ---

start_rc_room(_) ->
    ct:pal("lager level: ~p", [lager:get_loglevel(lager_console_backend)]),
    RoomId = <<"ROOM:1">>,
    Model = rc_model(RoomId),
    ok = remote_control_room:start(Model).

room_status(_) ->
    RoomId = <<"ROOM:1">>,
    Status = room_manager:room_status(RoomId),
    ExitReason = room_manager:exit_reason(RoomId),
    ?assertEqual({running, undefined}, {Status, ExitReason})
    .

stop_room(_) ->
    % TODO: put room id in config, along with a "starting number" so that
    % tests can be parameterized and multiple run in parallel
    RoomId = <<"ROOM:1">>,
    ?assert(length(dumbtable:keys(state_cache)) > 1),
    ?assertEqual(running, room_manager:room_status(RoomId)),
    ?assertEqual(stopped, room_manager:stop_room(RoomId)),
    ?assertEqual({error, not_started}, room_manager:room_status(RoomId)),
    ?assertEqual([], dumbtable:keys(state_cache))
    .

all_clean(_) ->
    Rooms = dumbtable:keys(room_manager),
    ?assertEqual([], Rooms)
    .

rc_model(RoomId) ->
    room_builder:remote_control_room(
      RoomId,
      room_builder:heartbeat_manager(5, "Value", protocol_bindings:modbus_bindings("127.0.0.1", 1)),
      [room_builder:remote_control_crah(
         Id,
         room_builder:crah_driver(
           Id, temperature, 10.0, 10.0, rat, true,
           protocol_bindings:modbus_bindings("127.0.0.1", 1),
           room_builder:generic_crah("Value", "77.0", "75.0", "87.0", "72.0", "false", "true", "false", "false", "1")
          ),
         room_builder:remote_control_strategy(Id, temperature, 75.0, manual),
         room_builder:vfd_driver(
           Id, pressure, 10.0, 10.0,
           [{20.0,2800},{100.0,14000}],
           protocol_bindings:modbus_bindings("127.0.0.1", 1),
           room_builder:generic_vfd("Value", "85.0", "7.5", "false", "true", "false", "false", "false", "false", "false", "1")
          ),
         room_builder:remote_control_strategy(Id, pressure, 80.0, manual)
        ) || Id <- id_seq("CRAH:",3)]
     )
    .

id_seq(Prefix, N) ->
    [list_to_binary(io_lib:format("~s~B", [Prefix, I])) || I <- lists:seq(1,N)]
    .

