%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(test_lib).

-compile(export_all).

-define(EPSILON, 23).

floateq(A, B, L) ->
	A =:= B orelse abs(A-B) < L.

start_ac() ->
    {ok, _} = net_kernel:start([test, longnames]),

	Env = ct:get_config(env),

	ct:pal("app loading results: ~p", [[{AppName, application:load(AppName)} || {AppName, _} <- Env]]),
	[[application:set_env(AppName, K, V) || {K,V} <- Tuples] || {AppName, Tuples} <- Env],

	LibDir = code:lib_dir(active_control),
	DummyNode = list_to_atom("dummy@" ++ net_adm:localhost()),

	application:set_env(active_control, loadbalance_nodes, [node(), DummyNode]),

	Defaults = filename:join([LibDir, "test", "cluster_conf.def"]),
	Over = filename:join([LibDir, "test", "cluster_conf.over"]),
	application:set_env(cluster_config, nodes, [node(), DummyNode]),
	application:set_env(cluster_config, defaults_file, Defaults),
	application:set_env(cluster_config, overrides_file, Over),

	YawsConf = filename:join([LibDir, "test", "yaws.conf"]),
	application:set_env(yaws, conf, YawsConf),

	[ct:pal("~p env: ~p", [AppName, application:get_all_env(AppName)]) || {AppName, _} <- Env],

	application:ensure_all_started(active_control)
	.

stop_ac(Started) ->
	[application:stop(App) || App <- lists:reverse(Started)],
    net_kernel:stop().

while_false(Fun) ->
	case Fun() of
		false -> timer:sleep(?EPSILON), while_false(Fun);
		V -> V
	end.

until_equal(Value, Fun) ->
	case Value == Fun() of
		true -> Value;
		false -> timer:sleep(?EPSILON), until_equal(Value, Fun)
	end
	.

until_valid(Mod, Fun, Args) ->
	case meck:called(Mod, Fun, Args) of
		false -> timer:sleep(?EPSILON), until_valid(Mod, Fun, Args);
		true -> ok
	end
	.

set_properties(Properties, [{PName,Val}|T]) ->
    NProperties = lists:keyreplace(PName, 1, Properties, {PName, Val}),
    set_properties(NProperties, T);
set_properties(P,[]) ->
    P.

mock_process_cache() ->
    meck:new(process_cache, [no_link, passthrough]),
    meck:expect(process_cache, load_properties, fun(_) -> {error, no_data} end),
    meck:expect(process_cache, delete_properties, fun(_) -> ok end),
    meck:expect(process_cache, update_properties, fun(_,_) -> ok end)
    .

mock_folsom() ->
	meck:new(folsom_metrics, [no_link])
	, meck:expect(folsom_metrics, notify, fun(_) -> ok end)
	, meck:expect(folsom_metrics, notify, fun(_, _) -> ok end)
	, meck:expect(folsom_metrics, new_meter, fun(_) -> ok end)
	, meck:expect(folsom_metrics, new_history, fun(_) -> ok end)
	, meck:expect(folsom_metrics, tag_metric, fun(_, _) -> ok end)
	.

firstelem(_Fun, []) ->
	none;
firstelem(Fun, [I | L]) ->
	case catch Fun(I) of
		true -> I;
		_ -> firstelem(Fun, L)
	end
	.

random_sample(IdNo) when is_integer(IdNo) ->
    sample:new(sensor_id(IdNo), random_temp(), shadoww_lib:millisecs_now());
random_sample(Id) when is_binary(Id) ->
    sample:new(Id, random_temp(), shadoww_lib:millisecs_now()).

random_temp() -> 75.0 + 10.0*rand:uniform().

crah_id(IdNo) -> bin_id("CRAH", IdNo).
input_id(IdNo) -> bin_id("INPUT", IdNo).
sensor_id(IdNo) -> bin_id("SENSOR", IdNo).
bin_id(T,N) -> list_to_binary(io_lib:format("~s:~p", [T, N])).

