
-module(gen_key_event_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	[
		{timetrap, {minutes, 1}}
	].

init_per_suite(Config) ->
	Config.

end_per_suite(_Config) ->
	ok.

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(_TestCase, _Config) ->
	ok.

groups() -> [].

all() ->
	[add_handler, notify, swap_handler, remove_handler].

%% test cases

add_handler() -> [].

add_handler(_Config) ->
	{ok, Pid} = gen_key_event:start(),

	F = fun handler_1/3,
	gen_key_event:add_handler(Pid, [key1, key2], F),

	Handlers = gen_key_event:list_keys(Pid),
	?assertMatch([key1, key2], Handlers),

	stopping = gen_key_event:stop(Pid)
	.


notify() -> [].

notify(_Config) ->
	CollectorPid = spawn(fun() -> collector([]) end),

	{ok, Pid} = gen_key_event:start(),

	gen_key_event:add_handler(Pid, [key1, key2], fun(K,E) -> CollectorPid ! {K, E} end),

	gen_key_event:sync_notify(Pid, key1, event1),
	gen_key_event:sync_notify(Pid, key2, event2),
	gen_key_event:sync_notify(Pid, key3, event3),

	CollectorPid ! {deliver, self()},

	List = receive
		{CollectorPid, L} -> L
	end,

	?assertMatch([{key2, event2}, {key1, event1}], List),

	stopping = gen_key_event:stop(Pid)
	.


swap_handler() -> [].

swap_handler(_Config) ->
	CollectorPid = spawn(fun() -> collector([]) end),

	{ok, Pid} = gen_key_event:start(),

	F = fun(K,E) -> CollectorPid ! {K, E} end,

	gen_key_event:add_handler(Pid, [k1, k2], F),

	gen_key_event:sync_notify(Pid, k2, ev1),

	gen_key_event:swap_handler(Pid, F, [k1, k3], F),

	gen_key_event:sync_notify(Pid, k1, ev2),
	gen_key_event:sync_notify(Pid, k2, ev3), % this should be ignored
	gen_key_event:sync_notify(Pid, k3, ev4),

	gen_key_event:swap_handler(Pid, F, [k1, k2], F),

	gen_key_event:sync_notify(Pid, k2, ev5),

	CollectorPid ! {deliver, self()},

	List = receive
		{CollectorPid, L} -> L
	end,

	stopping = gen_key_event:stop(Pid),

	?assertMatch([{k2, ev5}, {k3, ev4}, {k1, ev2}, {k2, ev1}], List)
	.


remove_handler() -> [].

remove_handler(_Config) ->
	{ok, Pid} = gen_key_event:start(),

	F = fun handler_1/3,
	gen_key_event:add_handler(Pid, [key1, key2], F),
	gen_key_event:remove_handler(Pid, F),

	Handlers = gen_key_event:list_keys(Pid),
	?assertMatch([], Handlers),

	stopping = gen_key_event:stop(Pid)
	.


%% utility functions

handler_1(R, key1, Ev) ->
	R ! {key1, Ev}
	;
handler_1(R, key2, Ev) ->
	R ! {key2, Ev}
	.

bad_handler(K, E) ->
	ct:pal("bad_handler(~p, ~p)~n", [K, E]),
	exit(E)
	.

collector(L) ->
	receive
		{deliver, Pid} -> Pid ! {self(), L};
		M -> collector([M | L])
	end
	.

