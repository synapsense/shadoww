%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(rf_crah_strategy_SUITE).

%% API
-export([all/0,
         suite/0,
         groups/0,
         init_per_suite/1,
         end_per_suite/1,
         group/1,
         init_per_group/2,
         end_per_group/2,
         init_per_testcase/2,
         end_per_testcase/2]).

%% Test cases
-export([
         strategy_starts/1,
         status_info_returns_disengaged/1,
         set_stage1_stored_in_disengaged/1,
         disengaged_to_manual_succeeds/1,
         manual_to_automatic_sets_stage2/1,
         manual_to_automatic_failsafe_with_no_sensor_data/1,
         set_stage1_handled_in_automatic/1,
         set_stage1_triggers_new_output/1,
         no_sensor_triggers_failsafe/1
        ]).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-include_lib("active_control/include/ac_api.hrl").
-include_lib("active_control/include/ac_status.hrl").

-define(ROOM_ID, <<"ROOM:1">>).
-define(CRAH_ID, <<"CRAH:1">>).
-define(RESOURCE, temperature).
-define(SUPPLY_ID, <<"SENSOR:2">>).
-define(RETURN_ID, <<"SENSOR:3">>).

%%%===================================================================
%%% Common Test callbacks
%%%===================================================================

all() ->
    [
     strategy_starts,
     status_info_returns_disengaged,
     set_stage1_stored_in_disengaged,
     disengaged_to_manual_succeeds,
     manual_to_automatic_sets_stage2,
     manual_to_automatic_failsafe_with_no_sensor_data,
     set_stage1_handled_in_automatic,
     set_stage1_triggers_new_output,
     no_sensor_triggers_failsafe
    ].

suite() ->
    [{timetrap, {seconds, 30}}].

groups() -> [].

init_per_suite(Config) ->
    {ok, Started} = application:ensure_all_started(meck),
    {ok, _EvtPid} = gen_key_event:start({local, sensor_event}),
    [{started, Started} | Config].

end_per_suite(Config) ->
    gen_key_event:stop(sensor_event),
    meck:unload(),
    Started = ?config(started, Config),
    [application:stop(A) || A <- lists:reverse(Started)].

group(_GroupName) ->
    [].

init_per_group(_GroupName, Config) ->
    Config.

end_per_group(_GroupName, _Config) ->
    ok.

init_per_testcase(TestCase, Config) ->
    CoolingControl = case TestCase of
                         set_stage1_triggers_new_output -> supply;
                         _ -> return
                     end,
    mock_driver_lib(CoolingControl),
    mock_process_cache(),
    Strategy = room_builder:raised_floor_crah_strategy(
                 ?CRAH_ID,
                 ?RESOURCE,
                 75.0,
                 disengaged,
                 ?SUPPLY_ID,
                 ?RETURN_ID
                ),
    {ok, Pid} = rf_crah_strategy:start_link(?ROOM_ID, Strategy),
    unlink(Pid),
    [{pid, Pid} | Config].

end_per_testcase(_TestCase, Config) ->
    Pid = ?config(pid, Config),
    stop = rf_crah_strategy:stop(Pid),
    meck:unload([process_cache, driver_lib]),
    ok.

%%%===================================================================
%%% Test cases
%%%===================================================================

strategy_starts(Config) ->
    Pid = ?config(pid, Config),
    ?assertNotEqual(undefined, erlang:process_info(Pid)).

status_info_returns_disengaged(Config) ->
    Pid = ?config(pid, Config),
    Status = gen_3stage_strategy:get_status_info(Pid),
    ControlStatus = proplists:get_value(status, Status),
    ?assertEqual(disengaged, ControlStatus).

set_stage1_stored_in_disengaged(Config) ->
    Pid = ?config(pid, Config),
    Stage1 = 77.3,
    gen_3stage_strategy:set_stage1(Pid, Stage1, med),
    Status = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(disengaged, proplists:get_value(status, Status)),
    StatusStage1 = proplists:get_value(stage1, Status),
    StatusStage2 = proplists:get_value(stage2, Status),
    ?assertEqual(Stage1, StatusStage1),
    ?assertEqual(undefined, StatusStage2).

disengaged_to_manual_succeeds(Config) ->
    mock_base_driver(),
    mock_room_event(),
    mock_strategy_param(),

    Pid = ?config(pid, Config),
    ok = gen_3stage_strategy:set_mode(Pid, manual),
    Status1 = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(manual, proplists:get_value(status, Status1)),

    meck:validate([strategy_param, room_event, base_driver]),
    meck:unload([strategy_param, room_event, base_driver]).

manual_to_automatic_failsafe_with_no_sensor_data(Config) ->
    mock_base_driver(),
    mock_room_event(),
    mock_strategy_param(),

    Pid = ?config(pid, Config),
    ok = gen_3stage_strategy:set_mode(Pid, manual),
    ManualStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(manual, proplists:get_value(status, ManualStatus)),

    ok = gen_3stage_strategy:set_mode(Pid, automatic),

    test_lib:while_false(fun() ->
                                 Stat = gen_3stage_strategy:get_status_info(Pid),
                                 proplists:get_value(status, Stat) =/= automatic
                         end),

    FailsafeStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(failsafe, proplists:get_value(status, FailsafeStatus)),

    meck:validate([strategy_param, room_event, base_driver]),
    meck:unload([strategy_param, room_event, base_driver]).

manual_to_automatic_sets_stage2(Config) ->
    mock_base_driver(),
    mock_room_event(),
    mock_strategy_param(),

    Pid = ?config(pid, Config),
    ok = gen_3stage_strategy:set_mode(Pid, manual),
    ManualStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(manual, proplists:get_value(status, ManualStatus)),
    ?assertEqual(undefined, proplists:get_value(stage2, ManualStatus)),

    gen_key_event:notify(sensor_event, ?RETURN_ID, test_lib:random_sample(?RETURN_ID)),
    gen_key_event:notify(sensor_event, ?SUPPLY_ID, test_lib:random_sample(?SUPPLY_ID)),

    test_lib:while_false(fun() ->
                                 SampleStatus = gen_3stage_strategy:get_status_info(Pid),
                                 proplists:get_value(supply_sample, SampleStatus) =/= undefined
                         end),

    SampleStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertNotEqual(undefined, proplists:get_value(supply_sample, SampleStatus)),
    ?assertNotEqual(undefined, proplists:get_value(return_sample, SampleStatus)),

    ok = gen_3stage_strategy:set_mode(Pid, automatic),
    AutoStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(automatic, proplists:get_value(status, AutoStatus)),
    ?assertNotEqual(undefined, proplists:get_value(stage2, AutoStatus)),

    meck:validate([strategy_param, room_event, base_driver]),
    meck:unload([strategy_param, room_event, base_driver]).

set_stage1_handled_in_automatic(Config) ->
    mock_base_driver(),
    mock_room_event(),
    mock_strategy_param(),

    Pid = ?config(pid, Config),

    gen_key_event:notify(sensor_event, ?RETURN_ID, test_lib:random_sample(?RETURN_ID)),
    gen_key_event:notify(sensor_event, ?SUPPLY_ID, test_lib:random_sample(?SUPPLY_ID)),

    test_lib:while_false(fun() ->
                                 SampleStatus = gen_3stage_strategy:get_status_info(Pid),
                                 proplists:get_value(supply_sample, SampleStatus) =/= undefined
                         end),

    ok = gen_3stage_strategy:set_mode(Pid, manual),
    ok = gen_3stage_strategy:set_mode(Pid, automatic),
    AutomaticStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(automatic, proplists:get_value(status, AutomaticStatus)),

    Stage1 = 77.3,
    ok = gen_3stage_strategy:set_stage1(Pid, Stage1, med),
    IntakeStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(Stage1, proplists:get_value(stage1, IntakeStatus)),
    ?assert(is_float(proplists:get_value(stage2, IntakeStatus))),

    meck:validate([strategy_param, room_event, base_driver]),
    meck:unload([strategy_param, room_event, base_driver]).

set_stage1_triggers_new_output(Config) ->
    mock_base_driver(),
    mock_room_event(),
    mock_strategy_param(),

    Pid = ?config(pid, Config),

    gen_key_event:notify(sensor_event, ?RETURN_ID, test_lib:random_sample(?RETURN_ID)),
    gen_key_event:notify(sensor_event, ?SUPPLY_ID, test_lib:random_sample(?SUPPLY_ID)),

    test_lib:while_false(fun() ->
                                 SampleStatus = gen_3stage_strategy:get_status_info(Pid),
                                 proplists:get_value(supply_sample, SampleStatus) =/= undefined
                         end),

    ok = gen_3stage_strategy:set_mode(Pid, manual),
    ok = gen_3stage_strategy:set_mode(Pid, automatic),
    AutomaticStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(automatic, proplists:get_value(status, AutomaticStatus)),

    Self = self(),
    meck:expect(base_driver, set_output, fun(fake_pid, V) -> Self ! {set_output, V} end),

    Stage1 = 77.3,
    ok = gen_3stage_strategy:set_stage1(Pid, Stage1, med),

    receive
        {set_output, Value} -> Value
    end,

    FinalStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(automatic, proplists:get_value(status, FinalStatus)),

    meck:validate([strategy_param, room_event, base_driver]),
    meck:unload([strategy_param, room_event, base_driver]).

no_sensor_triggers_failsafe(Config) ->
    mock_base_driver(),
    mock_room_event(),
    mock_strategy_param(),

    Pid = ?config(pid, Config),

    meck:expect(strategy_param, find, fun
                                          (min_s2, temperature, _, _) -> 45.0;
                                          (min_s2, pressure, _, _) -> 20.0;
                                          (max_s2, temperature, _, _) -> 85.0;
                                          (max_s2, pressure, _, _) -> 100.0;
                                          (failsafe_timeout, _, _, _) -> 1000;
                                          (stage3_time, _, _, _) -> 20000;
                                          (propagation_delay, _, _, _) -> 1200000
                                      end),

    gen_key_event:notify(sensor_event, ?RETURN_ID, test_lib:random_sample(?RETURN_ID)),
    gen_key_event:notify(sensor_event, ?SUPPLY_ID, test_lib:random_sample(?SUPPLY_ID)),

    test_lib:while_false(fun() ->
                                 SampleStatus = gen_3stage_strategy:get_status_info(Pid),
                                 proplists:get_value(supply_sample, SampleStatus) =/= undefined
                         end),

    ok = gen_3stage_strategy:set_mode(Pid, manual),
    ok = gen_3stage_strategy:set_mode(Pid, automatic),
    AutomaticStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(automatic, proplists:get_value(status, AutomaticStatus)),

    Self = self(),
    meck:expect(room_event, notify_strategy_status_change,
                fun(?ROOM_ID, {?CRAH_ID, ?RESOURCE, strategy}, Mode) ->
                        Self ! {status, Mode}
                end),

    Stage1 = 77.3,
    ok = gen_3stage_strategy:set_stage1(Pid, Stage1, med),

    Status = receive
               {status, S} -> S
           end,
    ?assertEqual(failsafe, Status),

    FinalStatus = gen_3stage_strategy:get_status_info(Pid),
    ?assertEqual(failsafe, proplists:get_value(status, FinalStatus)),

    meck:validate([strategy_param, room_event, base_driver]),
    meck:unload([strategy_param, room_event, base_driver]).

%% support functions

mock_driver_lib(CrahControl) ->
    meck:new(driver_lib, [passthrough, no_link]),
    meck:expect(driver_lib, call,
                fun
                    (_, get_config, []) ->
                        #crah_driver{cooling_control = CrahControl};
                    (_, get_status, []) ->
                        #crah_status{capacity = 50.0}
                end),
    meck:expect(driver_lib, register_strategy,
                fun(Id, Resource) -> {Id, Resource, strategy} end),
    meck:expect(driver_lib, register_callback,
                fun(gen_3stage_strategy) -> ok end),
    meck:expect(driver_lib, lookup, fun(_) -> fake_pid end).

mock_process_cache() ->
    meck:new(process_cache, [no_link]),
    meck:expect(process_cache, load_properties,
                fun(_) -> {error, no_data} end),
    meck:expect(process_cache, update_properties,
                fun(_, _) -> ok end),
    meck:expect(process_cache, delete_properties,
                fun(_) -> ok end).

mock_base_driver() ->
    meck:new(base_driver),
    meck:expect(base_driver, engage, fun(fake_pid) -> ok end),
    meck:expect(base_driver, set_output, fun(fake_pid, Output) ->
                                                 ct:pal("set driver output: ~p", [Output])
                                         end).

mock_room_event() ->
    meck:new(room_event),
    meck:expect(room_event, notify_strategy_status_change,
                fun(?ROOM_ID, {?CRAH_ID, ?RESOURCE, strategy}, Mode) ->
                        ct:pal("strategy status change: ~p", [Mode])
                end),
    meck:expect(room_event, notify_stage2_change,
                fun(?ROOM_ID, {?CRAH_ID, ?RESOURCE, strategy}, Value) ->
                        ct:pal("stage2 change: ~p", [Value])
                end).

mock_strategy_param() ->
    meck:new(strategy_param),
    meck:expect(strategy_param, find, fun
                                          (min_s2, temperature, _, _) -> 45.0;
                                          (min_s2, pressure, _, _) -> 20.0;
                                          (max_s2, temperature, _, _) -> 85.0;
                                          (max_s2, pressure, _, _) -> 100.0;
                                          (failsafe_timeout, _, _, _) -> 60000;
                                          (stage3_time, _, _, _) -> 2000;
                                          (propagation_delay, _, _, _) -> 1200000
                                      end),
    meck:expect(strategy_param, find, fun
                                          (early_adjust_threshold, _, _, _, _) -> 10.0;
                                          (error_scale, _, _, _, _) -> 1.0;
                                          (deadband, _, _, _, _) -> 1.0
                                      end).

