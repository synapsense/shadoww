
-module(rf_aoe_manager_SUITE).

%% API
-export([
        all/0,
        suite/0,
        groups/0,
        init_per_suite/1,
        end_per_suite/1,
        group/1,
        init_per_group/2,
        end_per_group/2,
        init_per_testcase/2,
        end_per_testcase/2
        ]).

%% Test cases
-export([
         recalc_on_startup/1,
         status_change_triggers_recalc/1,
         setpoint_change_triggers_recalc/1,
         status_change_extends_recalc_timer/1,
         input_change_extends_recalc_timer/1
        ]).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-include_lib("active_control/include/ac_status.hrl").

%%%===================================================================
%%% Common Test callbacks
%%%===================================================================

all() ->
    [
     {group, temperature},
     {group, pressure}
    ].

suite() ->
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:add_config(ct_config_plain, TestCfg),
    [
     {timetrap, {seconds, 30}},
     {require, env}
    ].

groups() ->
    [
     {temperature, [sequence], [
                                recalc_on_startup,
                                status_change_triggers_recalc,
                                setpoint_change_triggers_recalc,
                                status_change_extends_recalc_timer,
                                input_change_extends_recalc_timer]},
     {pressure, [sequence], [
                             recalc_on_startup,
                             status_change_triggers_recalc,
                             status_change_extends_recalc_timer,
                             input_change_extends_recalc_timer]}
    ].

init_per_suite(Config) ->
    Env = ct:get_config(env),
    ct:pal("app loading results: ~p", [[{AppName, application:load(AppName)} || {AppName, _} <- Env, AppName =:= lager]]),
    [[application:set_env(AppName, K, V) || {K,V} <- Tuples] || {AppName, Tuples} <- Env, AppName =:= lager],
    {ok, Started1} = application:ensure_all_started(lager),
    {ok, Started2} = application:ensure_all_started(meck),
    [{started1, Started1}, {started2, Started2} | Config].

end_per_suite(Config) ->
    [ok = application:stop(S) || S <- lists:reverse(?config(started2, Config))],
    [ok = application:stop(S) || S <- lists:reverse(?config(started1, Config))],
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:remove_config(ct_config_plain, TestCfg).

group(_GroupName) ->
    [].

init_per_group(temperature, Config) ->
    Resource = temperature,
    [
     {resource, Resource}
     | Config
    ];
init_per_group(pressure, Config) ->
    Resource = pressure,
    [
     {resource, Resource}
     | Config
    ].

end_per_group(_GroupName, _Config) ->
    ok.

init_per_testcase(_TC, Config) ->
    RoomId = <<"ROOM:1">>,
    Resource = ?config(resource, Config),

    CrahLocations = [
                     {<<"CRAH:1">>, geom:point(0.0, 0.0)},
                     {<<"CRAH:2">>, geom:point(30.0, 0.0)},
                     {<<"CRAH:3">>, geom:point(30.0, 30.0)},
                     {<<"CRAH:4">>, geom:point(0.0, 30.0)},
                     {<<"CRAH:5">>, geom:point(14.0, 16.0)}
                    ],

    CrahIds = [Id || {Id, _} <- CrahLocations],

    TempInputs = temp_inputs(),
    RackLocations = [{Id, Location} || #single_input{id=Id,location=Location} <- TempInputs],

    PressureInputs = pressure_inputs(),
    PressureLocations = [{Id, Location} || #single_input{id=Id,location=Location} <- PressureInputs],

    CooldownTime = 200,

    meck:new(rf_input),
    meck:expect(rf_input, get_all_input_config,
                fun
                    (_RoomId, temperature) ->
                        TempInputs;
                    (_RoomId, pressure) ->
                        PressureInputs
                end),
    meck:expect(rf_input, get_all_input_status,
                fun
                    (_RoomId, temperature) ->
                        [#input_status{id=Id,resource=R,target=T,actual=79.3,deviation=T-79.3,enabled=E}
                        || #single_input{id=Id,resource=R,enabled=E,target=T} <- TempInputs];
                    (_RoomId, pressure) ->
                        [#input_status{id=Id,resource=R,target=T,actual=0.031,deviation=T-0.031,enabled=E}
                        || #single_input{id=Id,resource=R,enabled=E,target=T} <- PressureInputs]
                end),
    meck:expect(rf_input, new_aoe, fun(_, _, _) -> ok end),

    meck:new(driver_lib, [passthrough, no_link]),
    meck:expect(driver_lib, reg,
                fun
                    ({_, _, _}) -> ok
                end),
    meck:expect(driver_lib, call,
                fun
                    ({_Id,pressure,driver}, get_property, ["cfm_table"]) ->
                       [{20.0, 2800}, {100.0, 14000}];
                    ({Id,pressure,driver},get_status,_Args) ->
                        #vfd_status{id=Id, status=online, setpoint=75.0};
                    ({Id,temperature,driver},get_status,_Args) ->
                        #crah_status{id=Id, status=online, setpoint=75.0};
                    ({_Id,pressure,driver},speed_to_flow,[Speed]) ->
                        Speed * 450.0
                end),

    meck:new(folsom_metrics, [no_link]),
    meck:expect(folsom_metrics, new_history, fun(_) -> ok end),
    meck:expect(folsom_metrics, tag_metric, fun(_, _) -> ok end),
    meck:expect(folsom_metrics, new_histogram, fun(_) -> ok end),
    meck:expect(folsom_metrics, notify, fun(_, _) -> ok end),
    meck:expect(folsom_metrics, histogram_timed_begin, fun(_) -> ok end),
    meck:expect(folsom_metrics, histogram_timed_notify, fun(_) -> ok end),

    Walls = [],
    {ok, GeomPid} = rf_geom:start_link(RoomId, CrahLocations, RackLocations, PressureLocations, Walls),
    unlink(GeomPid),

    meck:expect(driver_lib, lookup,
                fun
                    ({Id, none, geom}) when Id =:= RoomId -> GeomPid;
                    (K) -> K
                end),

    meck:new(room_event, [no_link]),
    meck:expect(room_event, relay_events, fun(_, _) -> ok end),

    Self = self(),
    PcachePid = spawn(
        fun() ->
            true = mock_pcache:start(),
            Self ! ready,
            receive stop -> ok end
        end),
    receive
        ready -> ok
    end,

    meck:new(process_cache, [no_link]),
    meck:expect(process_cache, initial_config, fun(K, V) -> mock_pcache:initial_config(K,V) end),
    meck:expect(process_cache, load_properties, fun(K) -> mock_pcache:load_properties(K) end),
    meck:expect(process_cache, update_properties, fun(K,V) -> mock_pcache:update_properties(K,V) end),
    meck:expect(process_cache, delete_properties, fun(K) -> mock_pcache:delete_properties(K) end),

    meck:new(strategy_param, [no_link]),
    meck:expect(strategy_param, find,
                fun
                    (aoe_cooldown_time, _, _) -> CooldownTime;
                    (fanspeed_delta_trigger, _, _) -> 5.0
                end),

    meck:new(glpsol, [no_link]),
    meck:expect(glpsol, solve, fun(_, _, _, _) -> {solution, []} end),

    [
        {room_id, RoomId},
        {resource, Resource},
        {aggressiveness, low},
        {t_inputs, TempInputs},
        {p_inputs, PressureInputs},
        {crah_ids, CrahIds},
        {cooldown_time, CooldownTime},
        {pcache_pid, PcachePid},
        {geom_pid, GeomPid}
        | Config
    ].

end_per_testcase(_TC, Config) ->
    rf_geom:stop(?config(geom_pid, Config)),
    % TODO: check that table is empty before deleting?
    ?config(pcache_pid, Config) ! stop,
    ValidateModules = [driver_lib, process_cache, strategy_param, glpsol, room_event],
    ValidateModule = fun(Mod) ->
                             case meck:validate([Mod]) of
                                 true -> ok;
                                 false -> ct:pal("'~p' failed validation.  History:~n~p", [Mod, meck:history(Mod)])
                             end
                     end,
    [ValidateModule(M) || M <- ValidateModules],
    meck:unload().

%%%===================================================================
%%% Test cases
%%%===================================================================

recalc_on_startup(Config) ->
    CrahIds = ?config(crah_ids, Config),

    {ok, Pid} = rf_aoe_manager:start_link(
            ?config(room_id, Config),
            ?config(resource, Config),
            ?config(aggressiveness, Config),
            CrahIds
           ),
    sleep(round(1.5 * ?config(cooldown_time, Config))),
    rf_aoe_manager:stop(Pid),
    ?assertEqual(1, meck:num_calls(glpsol, solve, '_')).

status_change_triggers_recalc(Config) ->

    RoomId = ?config(room_id, Config),
    CrahIds = ?config(crah_ids, Config),
    Resource = ?config(resource, Config),
    CooldownTime = ?config(cooldown_time, Config),

    {ok, Pid} = rf_aoe_manager:start_link(
            RoomId,
            Resource,
            ?config(aggressiveness, Config),
            CrahIds
           ),
    sleep(round(1.5 * CooldownTime)),
    ?assertEqual(1, meck:num_calls(glpsol, solve, '_')),

    CrahId = hd(CrahIds),
    Id = driver_lib:driver_key(CrahId, Resource),
    %room_event:notify_device_status_change(RoomId, Id, standby),
    send_room_event(Pid, Id, device_status, standby, round(1.5 * CooldownTime)),
    rf_aoe_manager:stop(Pid),
    ?assertEqual(2, meck:num_calls(glpsol, solve, '_')).

setpoint_change_triggers_recalc(Config) ->

    CrahIds = ?config(crah_ids, Config),
    Resource = ?config(resource, Config),
    CooldownTime = ?config(cooldown_time, Config),

    {ok, Pid} = rf_aoe_manager:start_link(
            ?config(room_id, Config),
            Resource,
            ?config(aggressiveness, Config),
            CrahIds
           ),
    sleep(round(1.5 * CooldownTime)),
    ?assertEqual(1, meck:num_calls(glpsol, solve, '_')),

    CrahId = hd(CrahIds),
    Id = driver_lib:driver_key(CrahId, Resource),
    send_room_event(Pid, Id, device_setpoint, 95.0, round(1.5 * CooldownTime)),
    rf_aoe_manager:stop(Pid),
    ?assertEqual(2, meck:num_calls(glpsol, solve, '_')).

status_change_extends_recalc_timer(Config) ->

    CrahIds = ?config(crah_ids, Config),
    Resource = ?config(resource, Config),
    CooldownTime = ?config(cooldown_time, Config),

    {ok, Pid} = rf_aoe_manager:start_link(
            ?config(room_id, Config),
            Resource,
            ?config(aggressiveness, Config),
            CrahIds
           ),
    sleep(round(1.5 * ?config(cooldown_time, Config))),
    ?assertEqual(1, meck:num_calls(glpsol, solve, '_')),

    [send_room_event(Pid, driver_lib:driver_key(C, Resource), device_status, standby, round(0.6 * CooldownTime)) || C <- CrahIds],
    [send_room_event(Pid, driver_lib:driver_key(C, Resource), device_status, online, round(0.6 * CooldownTime)) || C <- CrahIds],
    sleep(round(1.5 * CooldownTime)),

    rf_aoe_manager:stop(Pid),
    ?assertEqual(2, meck:num_calls(glpsol, solve, '_')).

send_room_event(Pid, Id, EventType, Status, CooldownTime) ->
    gen_fsm:send_event(Pid, {Id, {EventType, Status}}),
    sleep(CooldownTime).

input_change_extends_recalc_timer(Config) ->
    Resource = ?config(resource, Config),
    InputVar = case Resource of
                   pressure -> p_inputs;
                   temperature -> t_inputs
               end,
    Inputs = ?config(InputVar, Config),
    CrahIds = ?config(crah_ids, Config),

    InputIds = [Id || #single_input{id=Id} <- Inputs],

    CooldownTime = ?config(cooldown_time, Config),

    {ok, Pid} = rf_aoe_manager:start_link(
            ?config(room_id, Config),
            Resource,
            ?config(aggressiveness, Config),
            CrahIds
           ),
    sleep(round(1.5 * ?config(cooldown_time, Config))),
    ?assertEqual(1, meck:num_calls(glpsol, solve, '_')),

    [send_room_event(Pid, driver_lib:proc_key(I, Resource, input), input_status, false, round(0.6 * CooldownTime)) || I <- InputIds],
    [send_room_event(Pid, driver_lib:proc_key(I, Resource, input), input_status, true, round(0.6 * CooldownTime)) || I <- InputIds],
    sleep(round(1.5 * CooldownTime)),

    rf_aoe_manager:stop(Pid),
    ?assertEqual(2, meck:num_calls(glpsol, solve, '_')).

%%% Utility & helper functions

sleep(Ms) ->
    receive
        unreceiveable -> error(unreceiveable)
    after
        Ms -> ok
    end.

temp_inputs() ->
    [
     room_builder:single_input(
       <<"INPUT:1">>,
       <<"SENSOR:1">>,
       geom:point(10.0, 10.0),
       78.0,
       true,
       temperature
      ),
     room_builder:single_input(
       <<"INPUT:2">>,
       <<"SENSOR:2">>,
       geom:point(20.0, 10.0),
       78.0,
       true,
       temperature
      ),
     room_builder:single_input(
       <<"INPUT:3">>,
       <<"SENSOR:3">>,
       geom:point(20.0, 20.0),
       78.0,
       true,
       temperature
      ),
     room_builder:single_input(
       <<"INPUT:4">>,
       <<"SENSOR:4">>,
       geom:point(10.0, 20.0),
       78.0,
       true,
       temperature
      )
    ].

pressure_inputs() ->
    [
     room_builder:single_input(
       <<"INPUT:5">>,
       <<"SENSOR:5">>,
       geom:point(10.0, 15.0),
       0.28,
       true,
       pressure
      ),
     room_builder:single_input(
       <<"INPUT:6">>,
       <<"SENSOR:6">>,
       geom:point(20.0, 15.0),
       0.03,
       true,
       pressure
      ),
     room_builder:single_input(
       <<"INPUT:7">>,
       <<"SENSOR:7">>,
       geom:point(15.0, 15.0),
       0.03,
       true,
       pressure
      ),
     room_builder:single_input(
       <<"INPUT:8">>,
       <<"SENSOR:8">>,
       geom:point(10.0, 20.0),
       0.03,
       true,
       pressure
      )
    ].

