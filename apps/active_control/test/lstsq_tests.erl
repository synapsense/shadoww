
-module(lstsq_tests).

-include_lib("eunit/include/eunit.hrl").

-define(EPSILON, 1.0e-6).

random_data_test() ->
	TestDir = filename:join(code:lib_dir(active_control), "test"),
	Data = parse_file(filename:join(TestDir, "sample_data.dat")),
	Timestamp = parse_file(filename:join(TestDir, "sample_timestamps.dat")),
	Residuals = [R || [R] <- parse_file(filename:join(TestDir, "sumsq_residuals.dat"))],
	CalcedResiduals = [lstsq:ssr(Xs, Ys) || {Xs, Ys} <- lists:zip(Timestamp, Data)],
	{Pass, Fail} = lists:partition(fun({R, C}) -> feq(R, C) end, lists:zip(Residuals, CalcedResiduals)),
	io:format(user, "~B/~B ", [length(Pass), length(Residuals)]),
	?assertEqual(0, length(Fail))
	.

horizontal_line_test() ->
	Xs = [0, 1, 2, 3],
	Ys = [1, 3, 3, 1],
	?assertEqual(4.0, lstsq:ssr(Xs, Ys))
	.

duplicate_sample_test() ->
	Xs = [0, 1, 2, 3, 3],
	Ys = [1, 3, 3, 1, 1],
	lstsq:ssr(Xs, Ys)
	.

duplicate_timestamp_test() ->
	Xs = [1, 3, 3, 4],
	Ys = [0, 2, 0, 2],
	lstsq:ssr(Xs, Ys)
	.

perfect_line_test() ->
	Xs = [1, 2, 3, 4],
	Ys = [4, 3, 2, 1],
	?assertEqual(0.0, lstsq:ssr(Xs, Ys))
	.

two_sample_test() ->
	Xs = [1, 2],
	Ys = [1, 2],
	?assertEqual(0.0, lstsq:ssr(Xs, Ys))
	.

one_sample_test() ->
	Xs = [0],
	Ys = [0],
	?assertEqual(0.0, lstsq:ssr(Xs, Ys))
	.

%% Test whether 2 floating-point numbers are within epsilon of each other, where epsilon is very small
%% Required to account for small floating-point calculation errors
feq(F1, F2) ->
	abs(F1 - F2) < ?EPSILON
	.

% Parse the file containing random test data
% The file is returned as a list of lines
% Each line of the file is a list of numbers
% The list of numbers are separated by spaces
parse_file(Fname) ->
	[parse_floats(L) || L <- lines(Fname)]
	.

parse_floats(Line) ->
	[list_to_number(F) || F <- re:split(Line, " |\n", [{return, list}, trim]), F /= []]
	.

lines(Fname) ->
	{ok, FH} = file:open(Fname, [read, raw, read_ahead]),
	Lines = lines0(FH),
	file:close(FH),
	Lines
	.
lines0(Dev) ->
	case file:read_line(Dev) of
		eof -> [];
		{ok, L} -> [L | lines0(Dev)]
	end
	.

% because list_to_float("1") fails...
list_to_number(L) ->
	try list_to_float(L) catch _:_ -> list_to_integer(L) end.

