
#include <iostream>
#include <random>

using namespace std;

int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy);

/*
 * Generates test case data for the pnpoly function translated into Erlang
 *
 * pnpoly came from http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
 */

int main() {

	float x,y;

	float xs[20];
	float ys[20];

	uniform_int_distribution<> polysize_d(3,20);
	uniform_int_distribution<> polypoint_d(0,100);
	mt19937 random;

	cout.precision(5);

	int examples = 1000;
	cerr << "Generating " << examples << " examples" << endl;
	for(int example=0; example<examples; example++) {
		x = polypoint_d(random) / 10.0;
		y = polypoint_d(random) / 10.0;
		int points = polysize_d(random);
		xs[0] = polypoint_d(random) / 10.0;
		ys[0] = polypoint_d(random) / 10.0;
		cout << "{{point," << x << "," << y << "},[{point," << xs[0] << "," << ys[0] << "}";
		for(int i=1; i<points; i++) {
			xs[i] = polypoint_d(random) / 10.0;
			ys[i] = polypoint_d(random) / 10.0;
			cout << ",{point," << xs[i] << "," << ys[i] << "}";
		}
		bool inside = pnpoly(points, xs, ys, x, y) == 1;

		cout << "]," << boolalpha << inside << "} ." << endl;
	}
}

int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
{
	int i, j, c = 0;
	for (i = 0, j = nvert-1; i < nvert; j = i++) {
		if ( ((verty[i]>testy) != (verty[j]>testy)) &&
				(testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
			c = !c;
	}
	return c;
}

