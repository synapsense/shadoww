-module(evaps_SUITE).

-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

%% common test callbacks
suite() ->
	[].

init_per_suite(Config) ->
	Config.

end_per_suite(_Config) ->
	ok.

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(_TestCase, _Config) ->
	ok.

groups() -> [].

all() ->
	[
		init_evaps_using_ids,

		evap_value_arrived,
		evap_value_arrived_oldest_removed,
		calculate_new_residual_error,
		calculate_new_residual_updates_only_one_evaporator,
		calculate_new_residual_error_trims_to_capacity,

		update_action_stay_all_accepted,
		update_action_reject_decrease,
		update_action_raise,
		update_action_raise_minabstemp,

		check_values_get_sorted_by_timestamp_before_trim
	].

%% test cases

init_evaps_using_ids(_Config) ->
	Evaps = evaps:new(e1, e2),

	% check that all default conditions are correct
	accept_all = evaps:action(Evaps),
	{all_below, all_below} = evaps:conditions(Evaps),
	{[], []} = evaps:values(Evaps),
	{[], []} = evaps:residuals(Evaps)
	.


evap_value_arrived(_Config) ->

	RegressionWindow = 10,
	ResidualWindow = 10,
	ResidualThreshold = 100,
	MinAbsTemp = 0,

	Evaps = evaps:new("e1", "e2"),

	Evaps1 = evaps:add_value({"e1", 70.5, 1}, Evaps, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),
	Evaps2 = evaps:add_value({"e2", 71.0, 2}, Evaps1, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),
	Evaps3 = evaps:add_value({"e1", 72.0, 3}, Evaps2, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	accept_all = evaps:action(Evaps3),
	{all_below, all_below} = evaps:conditions(Evaps3),
	{[72.0, 70.5], [71.0]} = evaps:values(Evaps3),
	{[], []} = evaps:residuals(Evaps)
	.

evap_value_arrived_oldest_removed(_Config) ->
	RegressionWindow = 4,
	ResidualWindow = 5,
	ResidualThreshold = 20,
	MinAbsTemp = 38,

	Evaps = add_values(
		[{"e1", 60.0, 0}, {"e1", 61.0, 1},{"e1", 62.0, 2},{"e1", 63.0, 3},{"e1", 64.0, 4}, {"e1", 65.0, 5}, {"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	{[65.0, 64.0, 63.0, 62.0], [60.0]} = evaps:values(Evaps)
	.

calculate_new_residual_error(_Config) ->
	RegressionWindow = 6,
	ResidualWindow = 5,
	ResidualThreshold = 20,
	MinAbsTemp = 38,

	Evaps = add_values(
		[{"e1", 60.0, 0}, {"e1", 61.0, 1},{"e1", 62.0, 2},{"e1", 63.0, 3},{"e1", 64.0, 4}, {"e1", 65.0, 5}, {"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	CheckResidual = lstsq:ssr([0,1,2,3,4,5], [60.0, 61.0, 62.0, 63.0, 64.0, 65.0]),

	{[CheckResidual], []} = evaps:residuals(Evaps)
	.

calculate_new_residual_updates_only_one_evaporator(_Config) ->
	RegressionWindow = 6,
	ResidualWindow = 2,
	ResidualThreshold = 20,
	MinAbsTemp = 38,

	Evaps = add_values(
		[
			{"e1", 60.0, 0}, {"e1", 61.0, 1}, {"e1", 62.0, 2}, {"e1", 63.0, 3}, {"e1", 64.0, 4},{"e1", 65.0, 5},
			{"e2", 60.0, 1}, {"e2", 76.0, 2}, {"e2", 80.0, 3}, {"e2", 90.0, 4}, {"e2", 50.0, 5}
		],		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	Evaps1 = evaps:add_value({"e2", 60.0, 6}, Evaps, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	CheckResidual = lstsq:ssr([1,2,3,4,5,6], [60.0,50.0,90.0,80.0,76.0,60.0]),

	{[_SingleValue], [CheckResidual]} = evaps:residuals(Evaps1)
	.

calculate_new_residual_error_trims_to_capacity(_Config) ->
	MinAbsTemp = 38,
	RegressionWindow = 6,
	ResidualWindow = 10,
	ResidualThreshold = 20,

	Evaps = add_values(
		[{"e1", 61.0, 1},{"e1", 62.0, 2},{"e1", 63.0, 3},{"e1", 64.0, 4}, {"e1", 65.0, 5}, {"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	NewValue = {"e1", 45.0, 6},
	Evaps1 = evaps:add_value(NewValue, Evaps, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),
	NewValue1 = {"e1", 120.0, 7},
	Evaps2 = evaps:add_value(NewValue1, Evaps1, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	CheckResidual1 = lstsq:ssr([1,2,3,4,5,6], [61.0, 62.0, 63.0, 64.0, 65.0, 45.0]),
	CheckResidual2 = lstsq:ssr([2,3,4,5,6,7], [62.0, 63.0, 64.0, 65.0, 45.0, 120.0]),

	{[CheckResidual2, CheckResidual1], []} = evaps:residuals(Evaps2)
	.

update_action_stay_all_accepted(_Config) ->
	RegressionWindow = 2,
	MinAbsTemp = 38,
	ResidualWindow = 4,
	ResidualThreshold = 20,

	Evaps = add_values(
		[{"e1", 61.0, 1},{"e1", 62.0, 2},{"e1", 63.0, 3},{"e1", 64.0, 4}, {"e1", 65.0, 5}, {"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	{all_below, all_below} = evaps:conditions(Evaps),
	accept_all = evaps:action(Evaps)
	.

update_action_reject_decrease(_Config) ->
	RegressionWindow = 4,
	MinAbsTemp = 0,
	ResidualWindow = 2,
	ResidualThreshold = 20,

	Evaps = add_values(
		[{"e1", 61.0, 1},{"e1", 62.0, 2},{"e1", 63.0, 3},{"e1", 64.0, 4}, {"e1", 1200.0, 5}, {"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	{above_below, all_below} = evaps:conditions(Evaps),
	reject_decrease = evaps:action(Evaps)
	.

update_action_raise(_Config) ->
	RegressionWindow = 3,
	MinAbsTemp = 0,
	ResidualWindow = 2,
	ResidualThreshold = 1,

	Evaps = add_values(
		[{"e1", 10.0, 1},{"e1", 200.0, 2},{"e1", 50.0, 3},{"e1", 300.0, 4}, {"e1", 40.0, 5}, {"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	{all_above, all_below} = evaps:conditions(Evaps),
	raise_now = evaps:action(Evaps)
	.

update_action_raise_minabstemp(_Config) ->
	RegressionWindow = 3,
	MinAbsTemp = 38.0,
	ResidualWindow = 3,
	ResidualThreshold = 1.0,

	Evaps = add_values(
		[{"e1", 60.0, 1},{"e1", 61.0, 2},{"e1", 62.0, 3},{"e1", 63.0, 4},{"e1", 35.0, 5},{"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),
	raise_now_low_temp = evaps:action(Evaps)
	.

check_values_get_sorted_by_timestamp_before_trim(_Config) ->
	RegressionWindow = 3,
	MinAbsTemp = 38.0,
	ResidualWindow = 3,
	ResidualThreshold = 1.0,

	Evaps = add_values(
		[{"e1", 60.0, 10},{"e1", 61.0, 8},{"e1", 62.0, 11},{"e1", 63.0, 20},{"e1", 50.0, 1},{"e2", 60.0, 1}],
		evaps:new("e1", "e2"), RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp),

	{[63.0, 62.0, 60.0],[60.0]} = evaps:values(Evaps)
	.

add_values(Values, Evaps0, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp) ->
	lists:foldl(
		fun(Value, Evaps) ->
			evaps:add_value(Value, Evaps, RegressionWindow, ResidualWindow, ResidualThreshold, MinAbsTemp) end,
		Evaps0, Values)
	.