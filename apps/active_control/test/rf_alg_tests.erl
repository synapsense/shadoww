%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(rf_alg_tests).

-include_lib("eunit/include/eunit.hrl").

% General reminder: [1,2] == lists:seq(1,2)
%                     [1] /= lists:seq(1,2)

calc_stage1_no_quorum_test() ->
    % half the deltas are too old, so quorum not met
    NDeltas = 5,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+NDeltas-1)]
             ++ [{0.0, undefined} || _ <- lists:seq(StartTime+NDeltas,
                                                    StartTime+2*NDeltas-1)],
    ?assertEqual(no_quorum,
                 rf_alg:calc_stage1(Deltas, StartTime, {1.0, 2*NDeltas}, 1.0)).

calc_stage1_quorum_test() ->
    % all deltas recent enough, quorum present
    NDeltas = 8,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+NDeltas-1)],
    ?assertEqual(0.0,
                 rf_alg:calc_stage1(Deltas, StartTime, {1.0, NDeltas}, 1.0)).

calc_stage1_quorum_pct_test() ->
    % Half of the deltas are recent enough to be in the quorum
    % Test that the percent limit works by setting the count limit to 0
    NDeltas = 5,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+(2*NDeltas)-1)],
    ?assertEqual(no_quorum,
                 rf_alg:calc_stage1(Deltas, StartTime+NDeltas, {0.6, 0}, 1.0)),
    ?assertEqual(0.0,
                 rf_alg:calc_stage1(Deltas, StartTime+NDeltas, {0.5, 0}, 1.0)).

calc_stage1_quorum_nlim_test() ->
    % Half of the deltas are recent enough to be in the quorum
    % Test that the count limit works by setting the percent limit to 0
    NDeltas = 5,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+(2*NDeltas)-1)],
    ?assertEqual(no_quorum,
                 rf_alg:calc_stage1(Deltas, StartTime+NDeltas, {0.0, NDeltas+1}, 1.0)),
    ?assertEqual(0.0,
                 rf_alg:calc_stage1(Deltas, StartTime+NDeltas, {0.0, NDeltas}, 1.0)).

calc_stage1_no_quorum_aged_out_test() ->
    % 1 delta aged out, quorum not met
    NDeltas = 8,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+NDeltas-1)],
    ?assertEqual(no_quorum,
                 rf_alg:calc_stage1(Deltas, StartTime+1, {1.0, NDeltas}, 1.0)).

calc_stage1_mean_test() ->
    NDeltas = 5,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+NDeltas-1)]
             ++ [{1.0, N} || N <- lists:seq(StartTime+NDeltas,
                                            StartTime+2*NDeltas-1)],
    ?assertEqual(0.5,
                 rf_alg:calc_stage1(Deltas, StartTime, {1.0, NDeltas}, 0.0)).

calc_stage1_meanshift_test() ->
    NDeltas = 5,
    StartTime = 1000,
    Deltas = [{0.0, N} || N <- lists:seq(StartTime, StartTime+NDeltas-1)]
             ++ [{1.0, N} || N <- lists:seq(StartTime+NDeltas,
                                            StartTime+2*NDeltas-1)],
    NegShift = rf_alg:calc_stage1(Deltas, StartTime, {1.0, NDeltas}, -0.1),
    NoShift = rf_alg:calc_stage1(Deltas, StartTime, {1.0, NDeltas}, 0.0),
    SmallShift = rf_alg:calc_stage1(Deltas, StartTime, {1.0, NDeltas}, 0.1),
    LargeShift = rf_alg:calc_stage1(Deltas, StartTime, {1.0, NDeltas}, 0.2),
    ?assert(NegShift < NoShift),
    ?assert(NoShift < SmallShift),
    ?assert(SmallShift < LargeShift),
    ?assert(test_lib:floateq(NoShift - NegShift, SmallShift - NoShift, 1.0e-8)).

calc_stage2_hold_propagating_test() ->
    % PresentMean > Deadband, but < EarlyAdjustThreshold, so no adjustment b/c
    % Propagating
    PresentMean = 0.2,
    ErrorScale = 1.0,
    Deadband = 0.1,
    TargetSat = 65.0,
    RespondingTo = 0.0,
    EarlyAdjustThreshold = 0.5,
    Propagating = true,
    ?assertMatch({new_target, _},
                 rf_alg:calc_stage2(PresentMean,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    RespondingTo,
                                    EarlyAdjustThreshold,
                                    not Propagating)),
    ?assertEqual(hold,
                 rf_alg:calc_stage2(PresentMean,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    RespondingTo,
                                    EarlyAdjustThreshold,
                                    Propagating)).

calc_stage2_propagating_early_adjust_test() ->
    % Override "Propagating" when PresentMean > EarlyAdjust
    PresentMean = 0.6,
    ErrorScale = 1.0,
    Deadband = 0.3,
    TargetSat = 65.0,
    RespondingTo = 0.0,
    EarlyAdjustThreshold = 0.5,
    Propagating = true,
    ?assertEqual(hold,
                 rf_alg:calc_stage2(PresentMean-0.2,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    RespondingTo,
                                    EarlyAdjustThreshold,
                                    Propagating)),

    ?assertEqual({new_target, TargetSat+PresentMean},
                 rf_alg:calc_stage2(PresentMean,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    RespondingTo,
                                    EarlyAdjustThreshold,
                                    Propagating)),

    % When overriding Propagating, only part of the PresentMean is applied
    NewResponding = 0.2,
    ?assertEqual({new_target, TargetSat+(PresentMean-NewResponding)},
                 rf_alg:calc_stage2(PresentMean,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    NewResponding,
                                    EarlyAdjustThreshold,
                                    Propagating)).

calc_stage2_adjust_test() ->
    % Test the formula of a normal and early adjustment
    PresentMean = 1.0,
    ErrorScale = 1.0,
    Deadband = 0.3,
    TargetSat = 65.0,
    RespondingTo = 0.2,
    EarlyAdjustThreshold = 0.5,
    Propagating = false,
    ?assertEqual({new_target, TargetSat+ErrorScale*PresentMean},
                 rf_alg:calc_stage2(PresentMean,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    RespondingTo,
                                    EarlyAdjustThreshold,
                                    Propagating)),

    ?assertEqual({new_target, TargetSat+ErrorScale*(PresentMean-RespondingTo)},
                 rf_alg:calc_stage2(PresentMean,
                                    ErrorScale,
                                    Deadband,
                                    TargetSat,
                                    RespondingTo,
                                    EarlyAdjustThreshold,
                                    not Propagating)).

calc_stage3_supply_test() ->
    TargetSat = 65.0,
    PresentSat = 64.0,
    PresentRat = 80.0,
    ValveControl = supply,
    ValvePosition = 50.0,
    ?assertEqual(65.0, rf_alg:calc_stage3(TargetSat,
                                          PresentSat,
                                          PresentRat,
                                          ValveControl,
                                          ValvePosition)).

calc_stage3_return_test() ->
    TargetSat = 65.0,
    PresentSat = 64.0,
    PresentRat = 80.0,
    ValveControl = return,
    ValvePosition = 50.0,
    ?assertEqual(81.0, rf_alg:calc_stage3(TargetSat,
                                          PresentSat,
                                          PresentRat,
                                          ValveControl,
                                          ValvePosition)).

