%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(all_drivers_SUITE).

%% API
-export([all/0,
         suite/0,
         groups/0,
         init_per_suite/1,
         end_per_suite/1,
         group/1,
         init_per_group/2,
         end_per_group/2,
         init_per_testcase/2,
         end_per_testcase/2]).

%% Test cases
-export([start_driver/1,
         status_is_disengaged/1,
         engage_driver/1,
         set_output/1,
         pull_executed/1,
         pull_not_executed/1,
         push_executed/1,
         push_not_executed/1,
         disengage_driver/1,
         stop_driver/1]).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").
-include_lib("active_control/test/test_macros.hrl").

-include_lib("active_control/include/ac_status.hrl").

-define(PULL_MS, 300).
-define(PUSH_MS, 300).
-define(PULL_TIME, ?PULL_MS / 1000).
-define(PUSH_TIME, ?PUSH_MS / 1000).

wait_called(Timeout) ->
    receive
        called -> erlang:monotonic_time();
        Other -> error({unexpected_message, Other})
    after
        Timeout -> error({timeout, Timeout})
    end.


%%%===================================================================
%%% Common Test callbacks
%%%===================================================================

all() ->
    [
     {group, generic_modbus_crah},
     {group, generic_bacnet_crah},
     {group, generic_modbus_crac},
     {group, generic_bacnet_crac},
     {group, generic_modbus_vfd},
     {group, generic_bacnet_vfd}
    ].

suite() ->
    [{timetrap, {seconds, 30}}].

groups() ->
    [
     {generic_modbus_crah, [sequence],
      [start_driver,
       status_is_disengaged,
       pull_not_executed,
       push_not_executed,
       engage_driver,
       push_not_executed,
       set_output,
       pull_executed,
       push_executed,
       disengage_driver,
       stop_driver
      ]},
     {generic_bacnet_crah, [sequence],
      [start_driver,
       status_is_disengaged,
       pull_not_executed,
       push_not_executed,
       engage_driver,
       push_not_executed,
       set_output,
       pull_executed,
       push_executed,
       disengage_driver,
       stop_driver]},
     {generic_modbus_crac, [sequence],
      [start_driver,
       status_is_disengaged,
       pull_not_executed,
       push_not_executed,
       engage_driver,
       push_not_executed,
       set_output,
       pull_executed,
       push_executed,
       disengage_driver,
       stop_driver
      ]},
     {generic_bacnet_crac, [sequence],
      [start_driver,
       status_is_disengaged,
       pull_not_executed,
       push_not_executed,
       engage_driver,
       push_not_executed,
       set_output,
       pull_executed,
       push_executed,
       disengage_driver,
       stop_driver
      ]},
     {generic_modbus_vfd, [sequence],
      [start_driver,
       status_is_disengaged,
       pull_not_executed,
       push_not_executed,
       engage_driver,
       push_not_executed,
       set_output,
       pull_executed,
       push_executed,
       disengage_driver,
       stop_driver]},
     {generic_bacnet_vfd, [sequence],
      [start_driver,
       status_is_disengaged,
       pull_not_executed,
       push_not_executed,
       engage_driver,
       push_not_executed,
       set_output,
       pull_executed,
       push_executed,
       disengage_driver,
       stop_driver]}
    ].

init_per_suite(Config) ->
    {ok, Started} = application:ensure_all_started(meck),

    meck:new(folsom_metrics, [no_link, stub_all, no_history]),
    meck:expect(folsom_metrics, notify, fun(_) -> ok end),
    meck:expect(folsom_metrics, notify, fun(_,_) -> ok end),

    meck:new(room_event, [no_link]),
    meck:expect(room_event, notify_device_status_change, fun(_, _, _) -> ok end),
    meck:expect(room_event, notify_device_setpoint_change, fun(_, _, _) -> ok end),

    meck:new(process_cache, [no_link, no_history]),
    meck:expect(process_cache, load_properties, fun(_) -> {error, no_data} end),
    meck:expect(process_cache, update_properties, fun(_, _) -> ok end),
    meck:expect(process_cache, delete_properties, fun(_) -> ok end),

    [{started, Started} | Config].

end_per_suite(Config) ->
    meck:unload(),
	[application:stop(App) || App <- lists:reverse(?config(started, Config))].

group(_GroupName) ->
    [].

init_per_group(Group=generic_modbus_crah, Config) ->
    ct:pal("Starting test group ~p", [Group]),
    Id = <<"CRAH:1">>,
    Rec = room_builder:crah_driver(
            Id, temperature,
            ?PULL_TIME, ?PUSH_TIME,
            return, false,
            modbus_bindings(),
            generic_crah()
           ),
    [{record, Rec}, {protocol_mod, Group} | Config];
init_per_group(Group=generic_bacnet_crah, Config) ->
    ct:pal("Starting test group ~p", [Group]),
    Id = <<"CRAH:1">>,
    Rec = room_builder:crah_driver(
            Id, temperature,
            ?PULL_TIME, ?PUSH_TIME,
            return, false,
            bacnet_bindings(),
            generic_crah()
           ),
    [{record, Rec}, {protocol_mod, Group} | Config];
init_per_group(Group=generic_modbus_crac, Config) ->
    ct:pal("Starting test group ~p", [Group]),
    Id = <<"CRAH:1">>,
    Rec = room_builder:crac_driver(
            Id, temperature,
            ?PULL_TIME, ?PUSH_TIME,
            return, 4, false,
            modbus_bindings(),
            generic_crac()
           ),
    [{record, Rec}, {protocol_mod, Group} | Config];
init_per_group(Group=generic_bacnet_crac, Config) ->
    ct:pal("Starting test group ~p", [Group]),
    Id = <<"CRAH:1">>,
    Rec = room_builder:crac_driver(
            Id, temperature,
            ?PULL_TIME, ?PUSH_TIME,
            return, 4, false,
            bacnet_bindings(),
            generic_crac()
           ),
    [{record, Rec}, {protocol_mod, Group} | Config];
init_per_group(Group=generic_modbus_vfd, Config) ->
    ct:pal("Starting test group ~p", [Group]),
    Id = <<"CRAH:1">>,
    Rec = room_builder:vfd_driver(
            Id, pressure,
            ?PULL_TIME, ?PUSH_TIME,
            [{20.0, 2800.0}, {100.0, 14000.0}],
            modbus_bindings(),
            generic_vfd()
           ),
    [{record, Rec}, {protocol_mod, Group} | Config];
init_per_group(Group=generic_bacnet_vfd, Config) ->
    ct:pal("Starting test group ~p", [Group]),
    Id = <<"CRAH:1">>,
    Rec = room_builder:vfd_driver(
            Id, pressure,
            ?PULL_TIME, ?PUSH_TIME,
            [{20.0, 2800.0}, {100.0, 14000.0}],
            bacnet_bindings(),
            generic_vfd()
           ),
    [{record, Rec}, {protocol_mod, Group} | Config];
init_per_group(_GroupName, Config) ->
    Config.

end_per_group(_GroupName, _Config) ->
    ok.

init_per_testcase(start_driver, Config) ->
    meck:new(driver_lib, [no_link, passthrough]),
    meck:expect(driver_lib, register_driver, fun(Id, R) -> {Id, R, driver} end),
    meck:expect(driver_lib, proc_key, fun(Id, R, T) -> {Id, R, T} end),
    meck:expect(driver_lib, register_callback, fun(_) -> ok end),
    meck:expect(driver_lib, driver_running, fun(_, _) -> false end),

    meck:reset([process_cache]),
    Config;
init_per_testcase(stop_driver, Config) ->
    meck:new(driver_lib, [no_link, passthrough]),
    meck:expect(driver_lib, unreg, fun(_) -> ok end),

    meck:reset([process_cache]),
    Config;
init_per_testcase(_TestCase, Config) ->
    meck:reset([process_cache]),
    Config.

end_per_testcase(start_driver, _Config) ->
    ?assert(meck:validate(driver_lib)),
    true = meck:called(driver_lib, register_driver, '_'),
    true = meck:called(driver_lib, register_callback, '_'),

    ?assert(meck:validate(process_cache)),
    true = meck:called(process_cache, load_properties, '_'),
    true = meck:called(process_cache, update_properties, '_'),

    meck:unload([driver_lib]);
end_per_testcase(engage_driver, _Config) ->
    ?assert(meck:validate(room_event)),
    true = meck:called(room_event, notify_device_status_change, '_'),

    ?assert(meck:validate(process_cache)),
    true = meck:called(process_cache, update_properties, '_');
end_per_testcase(disengage_driver, _Config) ->
    ?assert(meck:validate(room_event)),
    true = meck:called(room_event, notify_device_status_change, '_'),

    ?assert(meck:validate(process_cache)),
    true = meck:called(process_cache, update_properties, '_');
end_per_testcase(stop_driver, _Config) ->
    ?assert(meck:validate(driver_lib)),
    true = meck:called(driver_lib, unreg, '_'),
    true = meck:called(process_cache, delete_properties, '_'),
    meck:unload([driver_lib]);
end_per_testcase(_TestCase, _Config) ->
    ok.

%%%===================================================================
%%% Test cases
%%%===================================================================

start_driver(Config) ->
    DriverConfig = ?config(record, Config),
    RoomId = ?config(room_id, Config),
    {ok, Pid} = base_driver:start_link(RoomId, DriverConfig),
    unlink(Pid),
    ct:pal("Driver ~p~nrunning at pid: ~p", [DriverConfig, Pid]),
    {save_config, [{driver_pid, Pid}]}.

status_is_disengaged(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    DriverPid = ?config(driver_pid, StartConfig),
    S = base_driver:get_status(DriverPid),
    ?assertEqual(disengaged, driver_status(S)),
    {save_config, StartConfig}.

engage_driver(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    DriverPid = ?config(driver_pid, StartConfig),
    base_driver:engage(DriverPid),
    S = base_driver:get_status(DriverPid),
    ?assertEqual(online, driver_status(S)),
    {save_config, StartConfig}.

push_not_executed(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    ProtocolMod = ?config(protocol_mod, Config),
    meck:new(ProtocolMod, [passthrough]),
    meck:expect(ProtocolMod, write_setpoint, fun(_, S) -> S end),
    timer:sleep(round(5 * ?PUSH_MS)),
    ?assertEqual(0, meck:num_calls(ProtocolMod, write_setpoint, '_')),
    meck:unload(ProtocolMod),
    {save_config, StartConfig}.

set_output(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    DriverPid = ?config(driver_pid, StartConfig),
    Value = 70.0,
    {accepted, Value} = base_driver:set_output(DriverPid, Value),
    {save_config, StartConfig}.

pull_executed(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    ProtocolMod = ?config(protocol_mod, Config),
    meck:new(ProtocolMod, [passthrough]),
    Self = self(),
    meck:expect(ProtocolMod, read_setpoint, fun(_) -> Self ! called, 75 end),
    T1 = wait_called(round(10 * ?PULL_MS)),
    T2 = wait_called(round(10 * ?PULL_MS)),
    meck:unload(ProtocolMod),
    T = erlang:convert_time_unit(T2 - T1, native, milli_seconds),
    ?assertWithin(?PULL_MS, round(1.5*?PULL_MS), T),
    {save_config, StartConfig}.

pull_not_executed(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    ProtocolMod = ?config(protocol_mod, Config),
    meck:new(ProtocolMod, [passthrough]),
    meck:expect(ProtocolMod, read_setpoint, fun(_) -> 75 end),
    timer:sleep(round(5 * ?PULL_MS)),
    ?assertEqual(0, meck:num_calls(ProtocolMod, read_setpoint, '_')),
    meck:unload(ProtocolMod),
    {save_config, StartConfig}.

push_executed(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    ProtocolMod = ?config(protocol_mod, Config),
    meck:new(ProtocolMod, [passthrough]),
    Self = self(),
    meck:expect(ProtocolMod, write_setpoint, fun(_, S) -> Self ! called, S end),
    T1 = wait_called(infinity),
    T2 = wait_called(round(1000 * ?PUSH_MS)),
    meck:unload(ProtocolMod),
    T = erlang:convert_time_unit(T2 - T1, native, milli_seconds),
    ?assertWithin(?PUSH_MS, round(1.5*?PUSH_MS), T),
    {save_config, StartConfig}.

disengage_driver(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    DriverPid = ?config(driver_pid, StartConfig),
    base_driver:disengage(DriverPid),
    S = base_driver:get_status(DriverPid),
    ?assertEqual(disengaged, driver_status(S)),
    {save_config, StartConfig}.

stop_driver(Config) ->
    {_, StartConfig} = ?config(saved_config, Config),
    DriverPid = ?config(driver_pid, StartConfig),
    MRef = monitor(process, DriverPid),
    base_driver:stop(DriverPid),
    receive
        {_, MRef, process, DriverPid, normal} ->
            ct:pal("Driver ~p exited normally", [DriverPid])
    end.

%%%===================================================================
%%% Utility functions
%%%===================================================================

modbus_bindings() ->
  protocol_bindings:modbus_bindings(
    "172.30.101.103",
    1
   ).

bacnet_bindings() ->
  protocol_bindings:bacnet_bindings(
    "172.30.101.103",
    1,
    1
   ).

generic_crah() ->
    room_builder:generic_crah(
      "78.0",
      "78.0",
      "90.0",
      "80.0",
      "65.0",
      "false",
      "true",
      "false",
      "false",
      "true"
     ).

generic_crac() ->
    room_builder:generic_crac(
      "78.0",
      "78.0",
      "90.0",
      "80.0",
      "2",
      "50.0",
      "false",
      "true",
      "false",
      "false",
      "false",
      "false",
      "false",
      "false",
      "true"
     ).

generic_vfd() ->
    room_builder:generic_vfd(
      "100.0",
      "100.0",
      "4.5",
      "false",
      "true",
      "false",
      "false",
      "false",
      "false",
      "false",
      "true"
     ).

driver_status(#crah_status{status = S}) -> S;
driver_status(#crac_status{status = S}) -> S;
driver_status(#vfd_status{status = S}) -> S.

