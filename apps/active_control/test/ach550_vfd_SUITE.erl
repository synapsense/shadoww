
-module(ach550_vfd_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

-import(test_lib, [set_properties/2, mock_ioeval/1, until_equal/2]).

%% common test callbacks
suite() ->
	[
		{timetrap, {minutes, 1}}
	].

init_per_suite(Config) ->
	{ok, Started} = application:ensure_all_started(meck),
	[{started, Started} | Config].

end_per_suite(Config) ->
	meck:unload(),
	[application:stop(A) || A <- lists:reverse(?config(started, Config))].

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(read_comm, _Config) ->
	ok;
end_per_testcase(read_override, _Config) ->
	ok;
end_per_testcase(_, _Config) ->
	meck:unload(mbgw),
	ok.

groups() ->
	[]
	.

all() ->
	[
		read_comm,
		read_override,
		read_kw,
		read_setpoint,
		write_setpoint,
		read_over_current,
		read_over_voltage,
		read_under_voltage,
		read_over_temperature,
		read_general,
		write_clear_alarm
	]
	.

%%
%% Test cases
%%

-define(IP, "127.0.0.1").
-define(DEVID, 2).

read_comm(_Config) ->
	?assertEqual(true, ach550_vfd:read_comm(props()))
	.

read_override(_Config) ->
	?assertEqual(false, ach550_vfd:read_override(props()))
	.

read_kw(_Config) ->
	ok = meck:new(mbgw),
	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40007) -> 745 end),
	?assertEqual(74.5, ach550_vfd:read_kw(props())),
	true = meck:validate(mbgw)
	.

read_setpoint(_Config) ->
	ok = meck:new(mbgw),
	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40005) -> 540 end),
	?assertEqual(90.0, ach550_vfd:read_setpoint(props())),
	true = meck:validate(mbgw)
	.

write_setpoint(_Config) ->
	ok = meck:new(mbgw),
	ok = meck:expect(mbgw, write_register16, fun(?IP, ?DEVID, 40002, X) -> X end),
	?assertEqual(18000, ach550_vfd:write_setpoint(props(), 90.0)),
	true = meck:validate(mbgw)
	.

read_over_current(_Config) ->
	ok = meck:new(mbgw),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000001 end),
	?assertEqual(true, ach550_vfd:read_over_current(props())),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000000 end),
	?assertEqual(false, ach550_vfd:read_over_current(props())),

	true = meck:validate(mbgw)
	.

read_over_voltage(_Config) ->
	ok = meck:new(mbgw),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000010 end),
	?assertEqual(true, ach550_vfd:read_over_voltage(props())),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000000 end),
	?assertEqual(false, ach550_vfd:read_over_voltage(props())),

	true = meck:validate(mbgw)
	.

read_under_voltage(_Config) ->
	ok = meck:new(mbgw),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000100 end),
	?assertEqual(true, ach550_vfd:read_under_voltage(props())),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000000 end),
	?assertEqual(false, ach550_vfd:read_under_voltage(props())),

	true = meck:validate(mbgw)
	.

read_over_temperature(_Config) ->
	ok = meck:new(mbgw),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000100000000 end),
	?assertEqual(true, ach550_vfd:read_over_temperature(props())),

	ok = meck:expect(mbgw, read_uint16, fun(?IP, ?DEVID, 40308) -> 2#0000000000000000 end),
	?assertEqual(false, ach550_vfd:read_over_temperature(props())),

	true = meck:validate(mbgw)
	.

read_general(_Config) ->
	ok = meck:new(mbgw),

	ok = meck:expect(mbgw, read_uint16, fun
			(?IP, ?DEVID, 40305) -> 2#0000000000000000;
			(?IP, ?DEVID, 40306) -> 2#0000000000000000;
			(?IP, ?DEVID, 40308) -> 2#0000000000000000;
			(?IP, ?DEVID, 40309) -> 2#0000000000000000 end),

	?assertEqual(false, ach550_vfd:read_general(props())),

	ok = meck:expect(mbgw, read_uint16, fun
			(?IP, ?DEVID, 40305) -> 2#0000100000000000;
			(?IP, ?DEVID, 40306) -> 2#0010000000000000;
			(?IP, ?DEVID, 40308) -> 2#0000100000000000;
			(?IP, ?DEVID, 40309) -> 2#0010000000000000 end),

	?assertEqual(true, ach550_vfd:read_general(props())),

	true = meck:validate(mbgw)
	.

write_clear_alarm(_Config) ->
	ok = meck:new(mbgw),

	ok = meck:expect(mbgw, write_register16, fun
			(?IP, ?DEVID, 40305, X) -> X;
			(?IP, ?DEVID, 40306, X) -> X;
			(?IP, ?DEVID, 40308, X) -> X;
			(?IP, ?DEVID, 40309, X) -> X end),

	ach550_vfd:write_clear_alarm(props()),

	true = meck:validate(mbgw)
	.


%% Utility functions


props() -> [{"ip", ?IP}, {"devid", ?DEVID}] .

