%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(ac_api_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:add_config(ct_config_plain, TestCfg),
    [
     {timetrap, {minutes, 1}},
     {require, env}
    ].

init_per_suite(Config) ->
    {ok, Started} = test_lib:start_ac(),
    room_manager:isolated(false),
    process_cache:isolated(false),
    [{started, Started} | Config].

end_per_suite(Config) ->
    test_lib:stop_ac(?config(started, Config)),
    TestCfg = filename:join([code:lib_dir(active_control), "test", "app_env.config"]),
    ct:remove_config(ct_config_plain, TestCfg).

init_per_group(_G, Config) ->
    Config.

end_per_group(_G, _Config) ->
    ok.

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

groups() -> [].

all() ->
    [
	].

%% test cases


