
-module(mock_pcache).

-export([
    load_properties/1,
    initial_config/2,
    delete_properties/1,
    update_properties/2,
    start/0,
    stop/0,
    isolated/1
    ]).

load_properties(Id) ->
    case ets:lookup(?MODULE, Id) of
        [] -> {error, no_data};
        [Val] -> {ok, Val}
    end.

initial_config(Id, Config) ->
    case ets:lookup(?MODULE, Id) of
        [] ->
            ets:insert(?MODULE, {Id, Config}),
            Config;
        [Cached] ->
            Cached
    end.

delete_properties(Id) ->
    ets:delete(?MODULE, Id).

update_properties(Id, Properties) ->
    ets:insert(?MODULE, {Id, Properties}).

start() ->
    ?MODULE =:= ets:new(?MODULE, [set, named_table, public]).

stop() ->
    ets:delete(?MODULE).

isolated(_Isolated) ->
    ok.
