%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(json_rpc).

-compile({parse_transform, runtime_types}).

-behaviour(gen_server).

%% API functions
-export([
         start_link/0,
         register_method/2,
         json_call/1,
         handle_rpc/3,
         dispatch_request/2
        ]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {
          callbacks = #{} % {MethodName :: binary(), Module :: atom()}
         }).

-record(json_rpc_request, {
          jsonrpc = <<"2.0">> :: binary(),
          method :: binary(),
          params :: [aeon:json_terms()],
          id :: integer()
         }).
-type json_rpc_request() :: #json_rpc_request{}.

-record(json_rpc_result, {
          jsonrpc = <<"2.0">> :: binary(),
          result :: json_rpc_error_detail() | binary() | aeon:json_terms(),
          id :: integer()
         }).
-type json_rpc_result() :: #json_rpc_result{}.

-record(json_rpc_error_detail, {
          code :: integer(),
          message :: binary()
         }).
-type json_rpc_error_detail() :: #json_rpc_error_detail{}.

-record(json_rpc_error, {
          jsonrpc = <<"2.0">> :: binary(),
          error :: json_rpc_error_detail(),
          id :: integer() | null
         }).
-type json_rpc_error() :: #json_rpc_error{}.

-record(batched_rpc_wrapper, {batch :: [json_rpc_request()
                                        | json_rpc_result()
                                        | json_rpc_error()],
                              id :: integer()}).
-type batched_rpc_wrapper() :: #batched_rpc_wrapper{}.

-type maybe_batched_request() :: json_rpc_request() | batched_rpc_wrapper().
-type maybe_batched_response() :: json_rpc_result()
                                | json_rpc_error()
                                | batched_rpc_wrapper().

-export_type([maybe_batched_request/0,
              maybe_batched_response/0,
              batched_rpc_wrapper/0,
              json_rpc_request/0,
              json_rpc_result/0,
              json_rpc_error_detail/0,
              json_rpc_error/0
             ]).

-export_records([
                 batched_rpc_wrapper,
                 json_rpc_request,
                 json_rpc_result,
                 json_rpc_error_detail,
                 json_rpc_error
                ]).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec register_method(binary(), module()) -> ok.
register_method(MethodName, CallbackModule) ->
    gen_server:call(?MODULE, {reg, MethodName, CallbackModule}).

json_call(JsonRpcRequest) ->
    gen_server:call(?MODULE, {call, JsonRpcRequest}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({reg, MethodName, CallbackModule}, _From, State=#state{callbacks=CB}) ->
    folsom_metrics:new_histogram({jsonrpc, MethodName}),
    folsom_metrics:tag_metric({jsonrpc, MethodName}, jsonrpc),
    {reply, ok, State#state{callbacks = maps:put(MethodName, CallbackModule, CB)}};
handle_call({call, JsonRpcRequest}, From, State=#state{callbacks=CB}) ->
    spawn(?MODULE, handle_rpc, [JsonRpcRequest, CB, From]),
    {noreply, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_rpc(JsonRpcString, CbMap, From) ->
    try
        Req = parse_request(JsonRpcString),
        Resp = dispatch_request(Req, CbMap),
        JsonRespString = encode_response(Resp),
        gen_server:reply(From, JsonRespString)
    catch
        E:R ->
            gen_server:reply(From, {error, {E,R}})
    end.

parse_request(JsonRpcRequestString) ->
    JsonRpcRequest = jsx:decode(JsonRpcRequestString),
    aeon:to_type(JsonRpcRequest, ?MODULE, maybe_batched_request).

-spec dispatch_request(maybe_batched_request(), #{}) -> maybe_batched_response().
dispatch_request(#batched_rpc_wrapper{batch = Calls, id = Id}, CbMap) ->
    case shadoww_lib:pmap_lim(5, {?MODULE, dispatch_request}, [CbMap], Calls) of
        {error, R} ->
            lager:error("Batched RPC error: ~p", [R]),
            #json_rpc_error{
               error = #json_rpc_error_detail{
                          code = -32700,
                          message = <<"Invalid request">>},
               id = null};
        Resp ->
            #batched_rpc_wrapper{batch = Resp, id = Id}
    end;
dispatch_request(Req = #json_rpc_request{method=M,
                                       params=P,
                                       id=Id},
               CbMap) ->
    lager:debug("RPC request: ~p", [Req]),
    try
        CbMod = maps:get(M, CbMap),
        % ensure that only registered functions get tracked.  prevent DoS
        Begin = folsom_metrics:histogram_timed_begin({jsonrpc, M}),
        Fun = binary_to_existing_atom(M, utf8),
        lager:debug("~p:~p(~p)", [CbMod, Fun, P]),
        R = case apply(CbMod, Fun, P) of
                ok ->
                    rpc_result(<<"ok">>, Id);
                {ok, Reply} ->
                    lager:debug("RPC reply: ~p", [Reply]),
                    rpc_result(Reply, Id);
                {error, Reason} ->
                    lager:warning("RPC error: ~p", [Reason]),
                    #json_rpc_error{
                       error = #json_rpc_error_detail{
                                  code = -32700,
                                  message = <<"Internal server error">>},
                       id = null}
            end,
        folsom_metrics:histogram_timed_notify(Begin), % exceptions won't get tracked
        R
    catch
        Einternal:Rinternal ->
            lager:error("Internal error: {~p,~p} ~p",
                        [Einternal,Rinternal,erlang:get_stacktrace()]),
            #json_rpc_error{
               error = #json_rpc_error_detail{
                          code = -32700,
                          message = <<"Internal server error">>},
               id = null}
    end.

rpc_result(R, Id) -> #json_rpc_result{result = R, id = Id} .

-spec encode_response(maybe_batched_response()) -> binary().
encode_response(MaybeBatchedResponse) ->
    try
        lager:debug("Encoding response: ~p", [MaybeBatchedResponse]),
        JsonErl = aeon:type_to_jsx(MaybeBatchedResponse, ?MODULE, maybe_batched_response),
        lager:debug("Response terms ~p", [JsonErl]),
        jsx:encode(JsonErl)
    catch
        Eencoding:Rencoding ->
            lager:error("Response encoding error: {~p,~p} ~p",
                        [Eencoding,Rencoding,erlang:get_stacktrace()]),
            jsx:encode(aeon:record_to_jsx(
                         #json_rpc_error{
                            error = #json_rpc_error_detail{
                                       code = -32700,
                                       message = <<"Error encoding response">>},
                            id = null},
                         ?MODULE))
    end.

