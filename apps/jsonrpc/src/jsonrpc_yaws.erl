%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=4 sw=4 tw=80 cc=80 :

-module(jsonrpc_yaws).

-include_lib("yaws/include/yaws_api.hrl").

%% API
-export([out/1]).

out(A) ->
    RpcRequest = A#arg.clidata,
    Reply = json_rpc:json_call(RpcRequest),
	{content, "application/x-javascript", Reply}.

