%%%-------------------------------------------------------------------
%% @doc jsonrpc top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module('jsonrpc_sup').

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Childspec = {
      json_rpc,
      {json_rpc, start_link, []},
      permanent,
      infinity,
      worker,
      [json_rpc]
     },
    {ok, { {one_for_all, 0, 1}, [Childspec]} }.

%%====================================================================
%% Internal functions
%%====================================================================
