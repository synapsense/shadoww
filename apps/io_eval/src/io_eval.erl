
-module(io_eval).

-export([
	run_async/3,
	run_async/4,
	eval/3,
	async_proc/5,
	timeout_reply/3
]).


-define(MAX_TXN_DURATION, 5 * 60 * 1000). % 5 minutes

% This should only be called from inside an I/O transaction.
% Either returns the value of the expression, or throws an error.
% Thrown errors are meant to be caught by the transaction wrapper, not user code.
% The thrown error will eventually be returned to client code with a {txn_abort, Ref, Reason} message.

-spec eval(string(), string(), [proplists:property()]) -> any().
eval(Pointname, Expr, BindingList) ->
	try
		InputValidator = io_validators:in_validator(Pointname),

		InValue = proplists:get_value('Value', BindingList, undefined),
		case io_validators:validate(InputValidator, InValue) of
			true -> ok;
			false ->
				throw({bad_input, io_validators:validate_msg(InputValidator, InValue)})
		end,

		{ok, Tokens, _} = erl_scan:string(Expr ++ " ."),
		{ok, Forms} = erl_parse:parse_exprs(Tokens),
		Bindings = list_to_bindings(BindingList),

		lager:debug("recursively evaluating forms: ~p, binding: ~p", [Forms, Bindings]),
		Value = rec_eval(Forms, Bindings),
		Validator = io_validators:out_validator(Pointname),
		case io_validators:validate(Validator, Value) of
			true -> Value;
			false -> throw({bad_output, io_validators:validate_msg(Validator, Value)})
		end
	catch
		error:{badmatch, {error, {N, erl_parse, Details}}} ->
			lager:debug("Parse error: {~p, ~p} on expression: ~p", [N, Details, Expr]),
			throw({eval_error, {Pointname, Expr, BindingList}, {parse_error, {N, Details}}});
		E:R ->
			lager:debug("exception abort {~p, ~p}. stacktrace: ~p", [E, R, erlang:get_stacktrace()]),
			throw({eval_error, {Pointname, Expr, BindingList}, R})
	end
	.


b_to_i(true) -> 1;
b_to_i(false) -> 0;
b_to_i(X) -> X.

%% @spec run_async(TxRef::term(), Fun::function(), Args::[term()]) -> IoPid::pid()
%% @doc spawns a process that executes the transaction function Fun with the supplied Args.
%% The return value of Fun is sent to the calling process as {@type {txn_result, TxRef, Return@}}.
%% If Fun does not return normally (an I/O function returns an error code, or times out) then the
%% calling process will receive the message {@type {txn_abort, TxRef, Reason@}}
%%
%% {@type Reason} can be either an error term returned from a function like {@type mbgw:read_uint16()}
%% Possibilities are: {@type timeout | gw_not_connected | {bad_packet, Request, Resp@} | {modbus_error, ErrorCode, ErrorString@}}
%% or a {@type {Error, Reason@}} tuple from a <code>catch E:R</code> block where Error is {@type throw | exit | error}.
run_async(TxRef, Fun, Args) when is_function(Fun), is_list(Args) ->
	run_async(TxRef, Fun, Args, ?MAX_TXN_DURATION)
	.

run_async(TxRef, Fun, Args, Timeout) when is_function(Fun), is_list(Args) ->
	folsom_metrics:notify({run_async, 1}),
	CallerPid = self(),
	ChildPid = proc_lib:spawn(io_eval, async_proc, [CallerPid, TxRef, Fun, Args, Timeout]),
	spawn(?MODULE, timeout_reply, [CallerPid, ChildPid, TxRef]),
	ChildPid
	.

timeout_reply(ReplyTo, MonitorPid, TxRef) ->
	Mref = monitor(process, MonitorPid),
	receive
		{'DOWN', Mref, process, MonitorPid, timeout} ->
			erlang:send(ReplyTo, {txn_abort, TxRef, timeout});
		{'DOWN', Mref, process, MonitorPid, Info}
				when Info == normal; Info == noproc ->
			ok; % async_proc took care of the reply, or it died before we installed the monitor
		{'DOWN', Mref, process, MonitorPid, Info} ->
			erlang:send(ReplyTo, {txn_abort, TxRef, {exit, Info}})
	end
	.

-spec async_proc(pid(), reference(), fun(), list(), integer()) -> {'txn_abort', reference(), any()} | {'txn_result', reference(), any()}.
async_proc(ReplyTo, TxRef, Fun, Args, infinity) ->
	Reply = try
		Result = apply(Fun, Args),
		{txn_result, TxRef, Result}
	catch
		throw:Thrown ->
			lager:debug("throw executing transaction: ~p, stacktrace: ~p", [Thrown, erlang:get_stacktrace()]),
			{txn_abort, TxRef, Thrown};
		E:R ->
			lager:error("exception executing transaction: {~p, ~p}, stacktrace: ~p", [E, R, erlang:get_stacktrace()]),
			{txn_abort, TxRef, {E, R}}
	end,
	erlang:send(ReplyTo, Reply)
	;
async_proc(ReplyTo, TxRef, Fun, Args, Timeout) ->
	try
		{ok, TRef} = timer:exit_after(Timeout, timeout),
		Reply = try
			Result = apply(Fun, Args),
			{txn_result, TxRef, Result}
		catch
			throw:Thrown ->
				lager:debug("throw executing transaction: ~p, stacktrace: ~p", [Thrown, erlang:get_stacktrace()]),
				{txn_abort, TxRef, Thrown};
			E:R ->
				lager:error("exception executing transaction: {~p, ~p}, stacktrace: ~p", [E, R, erlang:get_stacktrace()]),
				{txn_abort, TxRef, {E, R}}
		end,
		timer:cancel(TRef),
		erlang:send(ReplyTo, Reply)
	catch
		E1:R1 ->
			lager:error("Unexpected I/O transaction failure: {~p, ~p}, stacktrace: ~p", [E1, R1, erlang:get_stacktrace()]),
			erlang:send(ReplyTo, {txn_abort, TxRef, {unexpected_failure, {E1, R1}}})
	end
	.


rec_eval([Form], Bindings) ->
	{value, V, _NewBindings} = erl_eval:expr(Form, Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	V
	;
rec_eval([Form | Forms], Bindings) ->
	{value, _V, NewBindings} = erl_eval:expr(Form, Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	LastV = rec_eval(Forms, NewBindings),
	LastV
	.


list_to_bindings(L) -> list_to_bindings(L, erl_eval:new_bindings()).

list_to_bindings([], B) -> B;
list_to_bindings([{Var, Val} | Vs], B) -> list_to_bindings(Vs, erl_eval:add_binding(Var, Val, B)).


extcall({erlang, Function}, Args) ->
	apply(erlang, Function, Args)
	;
extcall(Fun, Args) ->
	exit({not_allowed, Fun, Args})
	.

get_binding(Name, Bindings) ->
	case erl_eval:binding(Name, Bindings) of
		unbound -> error(no_binding, Name);
		{value, V} -> V
	end
	.

localcall_e(Fun, [RegE], Bindings) when
		Fun == read_int16; Fun == read_uint16;
		Fun == read_int32; Fun == read_uint32;
		Fun == read_coil
		->
	lager:debug("entering eval [RegE]. fun: ~p, bindings: ~p", [Fun, Bindings]),
	{value, Reg, NewBindings} = erl_eval:expr(RegE, Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, mbgw:Fun(get_binding('IP', Bindings), get_binding('Device', Bindings), Reg), NewBindings}
	;

localcall_e(Fun, [DeviceE, RegE], Bindings) when
		Fun == read_int16; Fun == read_uint16;
		Fun == read_int32; Fun == read_uint32;
		Fun == read_coil
		->
	lager:debug("entering eval [DeviceE, RegE]. fun: ~p, bindings: ~p", [Fun, Bindings]),
	{[Device, Reg], NewBindings} = erl_eval:expr_list([DeviceE, RegE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, mbgw:Fun(get_binding('IP', Bindings), Device, Reg), NewBindings}
	;

localcall_e(Fun, [IPE, DeviceE, RegE], Bindings) when
		Fun == read_int16; Fun == read_uint16;
		Fun == read_int32; Fun == read_uint32;
		Fun == read_coil
		->
	lager:debug("entering eval [IPE, DeviceE, RegE]. fun: ~p, bindings: ~p", [Fun, Bindings]),
	{[IP, Device, Reg], NewBindings} = erl_eval:expr_list([IPE, DeviceE, RegE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, mbgw:Fun(IP, Device, Reg), NewBindings}
	;

% TODO: remove the round() call at a later date.  It should be explicit
localcall_e(Fun, [RegE, ValueE], Bindings) when
		Fun == write_register16; Fun == write_register32; Fun == write_coil
		->
	{[Reg, Value], NewBindings} = erl_eval:expr_list([RegE, ValueE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, mbgw:Fun(get_binding('IP', Bindings), get_binding('Device', Bindings), Reg, round(b_to_i(Value))), NewBindings}
	;

% TODO: remove the round() call at a later date.  It should be explicit
localcall_e(Fun, [DeviceE, RegE, ValueE], Bindings) when
		Fun == write_register16; Fun == write_register32; Fun == write_coil
		->
	{[Device, Reg, Value], NewBindings} = erl_eval:expr_list([DeviceE, RegE, ValueE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, mbgw:Fun(get_binding('IP', Bindings), Device, Reg, round(b_to_i(Value))), NewBindings}
	;

% TODO: remove the round() call at a later date.  It should be explicit
localcall_e(Fun, [IPE, DeviceE, RegE, ValueE], Bindings) when
		Fun == write_register16; Fun == write_register32; Fun == write_coil
		->
	{[IP, Device, Reg, Value], NewBindings} = erl_eval:expr_list([IPE, DeviceE, RegE, ValueE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, mbgw:Fun(IP, Device, Reg, round(b_to_i(Value))), NewBindings}
	;

localcall_e(Fun, [IpE, NetE, DevE, ObjE, IdE], Bindings) when
		Fun == read_ai; Fun == read_ao;
		Fun == read_av; Fun == read_bi;
		Fun == read_bo; Fun == read_bv;
		Fun == read_mi; Fun == read_mo;
		Fun == read_mv
		->
	{[Ip, Net, Dev, Obj, Id], NewBindings} = erl_eval:expr_list([IpE, NetE, DevE, ObjE, IdE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, bacnet:Fun(Ip, Net, Dev, Obj, Id), NewBindings}
	;

localcall_e(Fun, [ObjE, IdE], Bindings) when
		Fun == read_ai; Fun == read_ao;
		Fun == read_av; Fun == read_bi;
		Fun == read_bo; Fun == read_bv;
		Fun == read_mi; Fun == read_mo;
		Fun == read_mv
		->
	{[Obj, Id], NewBindings} = erl_eval:expr_list([ObjE, IdE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, bacnet:Fun(get_binding('IP', Bindings), get_binding('Net', Bindings), get_binding('Device', Bindings), Obj, Id), NewBindings}
	;

localcall_e(Fun, [IpE, NetE, DevE, ObjE, IdE, ValueE], Bindings) when
		Fun == write_ao; Fun == write_av; 
		Fun == write_bo; Fun == write_bv; 
		Fun == write_mo; Fun == write_mv
		->
	{[Ip, Net, Dev, Obj, Id, Value], NewBindings} = erl_eval:expr_list([IpE, NetE, DevE, ObjE, IdE, ValueE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, bacnet:Fun(Ip, Net, Dev, Obj, Id, b_to_i(Value)), NewBindings}
	;

localcall_e(Fun, [ObjE, IdE], Bindings) when
		Fun == write_ao; Fun == write_av; 
		Fun == write_bo; Fun == write_bv; 
		Fun == write_mo; Fun == write_mv
		->
	{[Obj, Id], NewBindings} = erl_eval:expr_list([ObjE, IdE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, bacnet:Fun(get_binding('IP', Bindings), get_binding('Net', Bindings), get_binding('Device', Bindings), Obj, Id, b_to_i(get_binding('Value', Bindings))), NewBindings}
	;
localcall_e(Fun, [ObjE, IdE, ValueE], Bindings) when
		Fun == write_ao; Fun == write_av; 
		Fun == write_bo; Fun == write_bv; 
		Fun == write_mo; Fun == write_mv
		->
	{[Obj, Id, Value], NewBindings} = erl_eval:expr_list([ObjE, IdE, ValueE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, bacnet:Fun(get_binding('IP', Bindings), get_binding('Net', Bindings), get_binding('Device', Bindings), Obj, Id, b_to_i(Value)), NewBindings}
	;

%%%%% general utility functions for calling within an expression
localcall_e(b_to_i, [ValueE], Bindings) ->
	{value, Value, NewBindings} = erl_eval:expr(ValueE, Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, b_to_i(Value), NewBindings}
	;
localcall_e(bit, [BitE, ValueE], Bindings) ->
	{[Bit, Value], NewBindings} = erl_eval:expr_list([BitE, ValueE], Bindings, {eval, fun localcall_e/3}, {value, fun extcall/2}),
	{value, io_expr_util:bit(Bit, Value), NewBindings}
	;

localcall_e(Fun, Args, _Bindings) ->
	error({not_allowed, Fun, Args})
	.


