
-module(io_validators).

-export([in_validator/1, out_validator/1, validate/2, validate_msg/2, pointnames/0]).


-type validator() :: 'true' | {'enum', list(boolean() | integer() | float())} | {'range', number(), number()} | {bad_pointname, string()}.

validators() ->
	[
		%% {point name, in(read) validator, out(write) validator}
		{"crah.setpointwrite",		{range, 35.0, 120.0},	true},
		{"crah.setpointread",		true,			{range, 35.0, 120.0}},
		{"crah.valveread",		true,			{range, 0.0, 100.0}},
		{"crah.ratread",		true,			{range, 35.0, 120.0}},
		{"crah.satread",		true,			{range, 35.0, 120.0}},
		{"crah.standbyread",		true,			{enum, [true, false, 0, 1]}},
		{"crah.commread",		true,			{enum, [true, false, 0, 1]}},
		{"crah.changefiltersalarmread",	true,			{enum, [true, false, 0, 1]}},
		{"crah.generalalarmread",	true,			{enum, [true, false, 0, 1]}},
		{"crah.clearalarmwrite",	true,			true},

		{"crac.setpointwrite",		{range, 35.0, 120.0},	true},
		{"crac.setpointread",		true,			{range, 35.0, 120.0}},
		{"crac.compressorread",		true,			{range, 0.0, 10.0}},
		{"crac.capacityread",		true,			{range, 0.0, 100.0}},
		{"crac.ratread",		true,			{range, 35.0, 120.0}},
		{"crac.satread",		true,			{range, 35.0, 120.0}},
		{"crac.standbyread",		true,			{enum, [true, false, 0, 1]}},
		{"crac.commread",		true,			{enum, [true, false, 0, 1]}},
		{"crac.highheadpressurealarmread",true,			{enum, [true, false, 0, 1]}},
		{"crac.lowsuctionpressurealarmread",true,		{enum, [true, false, 0, 1]}},
		{"crac.compressoroverloadalarmread",true,		{enum, [true, false, 0, 1]}},
		{"crac.shortcyclealarmread",	true,			{enum, [true, false, 0, 1]}},
		{"crac.changefiltersalarmread",	true,			{enum, [true, false, 0, 1]}},
		{"crac.generalalarmread",	true,			{enum, [true, false, 0, 1]}},
		{"crac.clearalarmwrite",	true,			true},

		{"vfd.setpointwrite",		{range, 0.0, 100.0},	true},
		{"vfd.setpointread",		true,			{range, 0.0, 100.0}},
		{"vfd.powerread",		true,			{range, 0.0, 50.0}},
		{"vfd.localoverrideread",	true,			{enum, [true, false, 0, 1]}},
		{"vfd.commread",		true,			{enum, [true, false, 0, 1]}},
		{"vfd.overcurrentalarmread",	true,			{enum, [true, false, 0, 1]}},
		{"vfd.overvoltagealarmread",	true,			{enum, [true, false, 0, 1]}},
		{"vfd.undervoltagealarmread",	true,			{enum, [true, false, 0, 1]}},
		{"vfd.overtemperaturealarmread",true,			{enum, [true, false, 0, 1]}},
		{"vfd.generalalarmread",	true,			{enum, [true, false, 0, 1]}},
		{"vfd.clearalarmwrite",		true,			true},

		{"heartbeatmanager.heartbeatwrite",		{enum, [0,1]},		true},

		{"test.true",			true,			true},	% for testing
		{"test.false",			false,			false}	% for testing
	]
	.

pointnames() ->
	[N || {N, _, _} <- validators()]
	.

% Return a tuple that specifies how the tag should be validated
% This function returns a validator for parameters going into an
% expression that must be written to a device
-spec in_validator(string()) -> validator().

in_validator(Point) ->
	case lists:keyfind(Point, 1, validators()) of
		false -> {bad_pointname, Point};
		{Point, V, _} -> V
	end
	.


% This function returns a validator for return values of an expression
-spec out_validator(string()) -> validator().

out_validator(Point) ->
	case lists:keyfind(Point, 1, validators()) of
		false -> {bad_pointname, Point};
		{Point, _, V} -> V
	end
	.

% Match a validator description against a value
-spec validate(validator(), any()) -> boolean().
validate({range, Low, High}, Value) -> (Value >= Low) and (Value =< High) ;
validate({enum, Items}, Value) -> lists:member(Value, Items) ;
validate(true, _Value) -> true ;
validate(false, _Value) -> false ;
validate({bad_pointname, P}, _) -> throw({bad_pointname, P}) .


% If a validator fails, this formats an error message to describe why
-spec validate_msg(validator(), any()) -> string().

validate_msg(false, _Value) ->
	"Validator always fails"
	;
validate_msg({range, Low, High}, Value) ->
	f("~p is not within the required range: (~p - ~p).", [Value, Low, High])
	;
validate_msg({enum, Items}, Value) ->
	f("~p must be one of: ~p.", [Value, Items])
	.

f(A, B) ->
	lists:flatten(io_lib:format(A, B))
	.

