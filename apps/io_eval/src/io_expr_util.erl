
-module(io_expr_util).

-export([bit/2]).


-spec bit(N :: 0..63, Int :: integer()) -> 0..1 .
bit(N, Int) when is_integer(Int), 0 =< N, N < 64 ->
	EndBits = 63 - N,
	<<_:EndBits, Bit:1, _:N>> = <<Int:64/unsigned-integer>>,
	Bit
	.

