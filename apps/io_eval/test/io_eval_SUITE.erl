
-module(io_eval_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	TestCfg = filename:join([code:lib_dir(io_eval), "test", "io_eval_test.config"]),
	ct:add_config(ct_config_plain, TestCfg),
	[
		{timetrap, {minutes, 5}},
		{require, crah_ip},
		{require, crah_id},
		{require, no_modbus_ip},
		{require, unreachable_ip},
		{require, crah_read_bindings},
		{require, crah_write_bindings}
	].

init_per_suite(Config) ->
	Defaults = filename:join([code:lib_dir(mbgw), "test", "cluster_conf.def"]),
	Over = filename:join([code:lib_dir(mbgw), "test", "cluster_conf.over"]),
	application:load(cluster_config),
	application:set_env(cluster_config, defaults_file, Defaults),
	application:set_env(cluster_config, overrides_file, Over),
	{ok, Started} = application:ensure_all_started(io_eval),
	[{started, Started} | Config].

end_per_suite(Config) ->
	TestCfg = filename:join([code:lib_dir(io_eval), "test", "io_eval_test.config"]),
	ct:remove_config(ct_config_plain, TestCfg),
	Started = ?config(started, Config),
	[application:stop(App) || App <- lists:reverse(Started)].

init_per_group(_GroupName, Config) ->
	Config.

end_per_group(_GroupName, _Config) ->
	ok.

init_per_testcase(TestCase, Config) ->
	Config.

end_per_testcase(TestCase, Config) ->
	ok.

groups() ->
	[].

all() ->
	[
		connect_to_no_modbus_ip_fails_with_econnrefused,
		connect_to_unreachable_ip_fails_with_timeout,
		read_out_of_range_value_fails_with_bad_output,
		write_out_of_range_value_fails_with_bad_input,
		successful_write_returns_value_written,
		successful_read_returns_value_read,
		undefined_function_fails_with_not_allowed,
		nonexistent_point_name_fails_with_bad_pointname,
		syntax_error_fails_with_parse_error,
		unbound_variable_fails_with_unbound,
		exit_call_returns,
		timeout_returns_timeout,
		add_to_atom_fails_with_badarith,
		run_async_captures_badarith
	].

%% test cases

connect_to_no_modbus_ip_fails_with_econnrefused(_Config) ->
	NoModbusIp = ct:get_config(no_modbus_ip),
	CrahId = ct:get_config(crah_id),
	?assertMatch({eval_error, _, econnrefused},
		eval_io_expr("crah.setpointread", "read_int16(40001)",
			     [{'IP', NoModbusIp}, {'Device', CrahId}, {'Value', undefined}]))
	.


connect_to_unreachable_ip_fails_with_timeout(_Config) ->
	UnreachableIp = ct:get_config(unreachable_ip),
	CrahId = ct:get_config(crah_id),
	?assertMatch({eval_error, _, timeout},
		eval_io_expr("crah.setpointread", "read_int16(40001)",
			     [{'IP', UnreachableIp}, {'Device', CrahId}, {'Value', undefined}]))
	.

read_out_of_range_value_fails_with_bad_output(Config) ->
	% this test failing could be the result of a badly configured test rig.
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, {bad_output, _}},
		eval_io_expr("crah.setpointread", "read_int16(40001)", CrahBindings))
	.

write_out_of_range_value_fails_with_bad_input(_Config) ->
	CrahIp = ct:get_config(crah_ip),
	CrahId = ct:get_config(crah_id),
	?assertMatch({eval_error, _, {bad_input, _}},
		eval_io_expr("crah.setpointwrite", "write_register16(40005, Value)", [{'IP', CrahIp}, {'Device', CrahId}, {'Value', 0.0}]))
	.

successful_write_returns_value_written(_Config) ->
	CrahIp = ct:get_config(crah_ip),
	CrahId = ct:get_config(crah_id),
	?assertEqual(40,
		eval_io_expr("crah.setpointwrite", "write_register16(40005, Value)", [{'IP', CrahIp}, {'Device', CrahId}, {'Value', 40.0}]))
	.

successful_read_returns_value_read(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertEqual(40,
		eval_io_expr("crah.setpointread", "read_int16(40005)", CrahBindings))
	.

undefined_function_fails_with_not_allowed(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, {not_allowed, _, _}},
		eval_io_expr("crah.setpointread", "read_blah(40001)", CrahBindings))
	.

nonexistent_point_name_fails_with_bad_pointname(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, {bad_pointname, _}}, eval_io_expr("crah.setpointr", "read_blah(1)", CrahBindings))
	.

syntax_error_fails_with_parse_error(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, {parse_error, _}},
		eval_io_expr("crah.setpointread", "a read_blah(1)", CrahBindings))
	.

unbound_variable_fails_with_unbound(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, {unbound, _}},
		eval_io_expr("crah.setpointread", "write_register16(40001, UndefVariable)", CrahBindings))
	.

exit_call_returns(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, simulated_exit},
		eval_io_expr("crah.setpointread", "exit(simulated_exit)", CrahBindings))
	.

timeout_returns_timeout(_Config) ->
	Epsilon = 20,
	io_eval:run_async(ref, fun() -> timer:sleep(Epsilon * 2) end, [], Epsilon),
	{TxnAbort, TxnResult} = receive
		{TxnA, ref, TxnR} -> {TxnA, TxnR}
	after
		Epsilon * 3 -> {no_return, no_return}
	end,
	?assertEqual(txn_abort, TxnAbort),
	?assertEqual(timeout, TxnResult)
	.

add_to_atom_fails_with_badarith(_Config) ->
	CrahBindings = ct:get_config(crah_read_bindings),
	?assertMatch({eval_error, _, badarith},
		eval_io_expr("crah.setpointread", "1 + a", CrahBindings))
	.

run_async_captures_badarith(_Config) ->
	Ref = make_ref(),
	io_eval:run_async(Ref, fun err_txn/0, []),
	Result = receive
		{_, Ref, _} = R -> R
	after
		10 * 1000 ->
			timeout
	end,
	?assertEqual({txn_abort, Ref, {error, badarith}}, Result)
	.

err_txn() ->
	1 + a
	.

%% utility functions

eval_io_expr(Point, Expr, Bindings) ->

	% purge any responses from a previous timeout
	receive X -> ok after 0 -> ok end,

	TxnRef = make_ref(),
	io_eval:run_async(TxnRef, fun io_eval:eval/3, [Point, Expr, Bindings]),

	Wait = 30,

	Return = receive
		{txn_result, TxnRef, Result} ->
			Result
			;
		{txn_abort, TxnRef, Reason} ->
			Reason
			;
		R ->
			R
	after
		Wait * 1000 ->
			timeout
	end,
	Return
	.


