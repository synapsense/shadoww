
-module(glpsol_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	[
		{timetrap, {minutes, 1}}
	].

init_per_suite(Config) ->
	application:set_env(glpsol, temp_dir, "."),
	{ok, Started} = application:ensure_all_started(glpsol),
	[{started, Started} | Config].

end_per_suite(Config) ->
	% TODO: clean up the priv dir, since each bad model leaves files around
	[application:stop(App) || App <- lists:reverse(?config(started, Config))],
	ok.

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(_TestCase, _Config) ->
	ok.

groups() ->
	[
		{all_tests, [parallel], [small_model,
					 large_model,
					 bad_model,
					 glpsol_run]}
	].

all() ->
	[
		{group, all_tests},
		glpsol_model_tests
	]
	.

%% test cases

small_model(_) ->
	Crahs = [
			{<<"1">>, 10000.0},
			{<<"2">>, 10000.0},
			{<<"3">>, 10000.0}
		],
	Racks = [
			{<<"1">>, 0.02},
			{<<"2">>, 0.02},
			{<<"3">>, 0.02}
		],
	Distances = [
			{<<"1">>, <<"1">>, 1.0},
			{<<"1">>, <<"2">>, 2.0},
			{<<"1">>, <<"3">>, 2.0},
			{<<"2">>, <<"1">>, 2.0},
			{<<"2">>, <<"2">>, 1.0},
			{<<"2">>, <<"3">>, 2.0},
			{<<"3">>, <<"1">>, 2.0},
			{<<"3">>, <<"2">>, 2.0},
			{<<"3">>, <<"3">>, 1.0}
		],

	{solution, Flows} = glpsol:solve("model", Crahs, Racks, Distances),

	?assert(length(Flows) == length(Distances))
	.

large_model(_) ->
	Crahs = [
			{<<"1">>, 100.0},
			{<<"2">>, 90.0},
			{<<"3">>, 80.0}
		],
	Racks = [
			{<<"1">>, 0.021},
			{<<"2">>, 0.022},
			{<<"3">>, 0.023},
			{<<"4">>, 0.024}
		],
	Distances = [
			{<<"1">>, <<"1">>, 2.0},
			{<<"1">>, <<"2">>, 3.0},
			{<<"1">>, <<"3">>, 4.0},
			{<<"1">>, <<"4">>, 5.0},
			{<<"2">>, <<"1">>, 3.0},
			{<<"2">>, <<"2">>, 2.0},
			{<<"2">>, <<"3">>, 3.0},
			{<<"2">>, <<"4">>, 4.0},
			{<<"3">>, <<"1">>, 4.0},
			{<<"3">>, <<"2">>, 3.0},
			{<<"3">>, <<"3">>, 2.0},
			{<<"3">>, <<"4">>, 3.0}
		],

	{solution, Flows} = glpsol:solve("model", Crahs, Racks, Distances),

	?assert(length(Flows) == length(Distances))
	.

bad_model(_) ->
	Crahs = [
			{<<"1">>, 10.0},
			{<<"2">>, 10.0}
		],
	Racks = [
			{<<"1">>, 0.0},
			{<<"2">>, 0.0}
		],
	Distances = [
			{<<"1">>, <<"1">>, 1.0},
			{<<"1">>, <<"2">>, 2.0},
			{<<"2">>, <<"1">>, 3.0},
			{<<"2">>, <<"2">>, 4.0}
		],

	?assertMatch({error, 1, _ST}, glpsol:solve("model", Crahs, Racks, Distances))
	.

glpsol_run(_) ->
	ok = eunit:test(glpsol_run, [verbose]) .

glpsol_model_tests(_) ->
	ok = eunit:test(glpsol_model_tests, [verbose]) .

