%% coding: utf-8
%% vim: set et ft=erlang ff=unix ts=8 sw=4 tw=80 cc=80 :

-module(glpsol_model_tests).

-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

crah_id(N) -> list_to_binary(io_lib:format("C:~B", [N])).
pressure_id(N) -> list_to_binary(io_lib:format("P:~B", [N])).
rack_id(N) -> list_to_binary(io_lib:format("R:~B", [N])).

point() -> geom:point(10.0 * rand:uniform(), 10.0 * rand:uniform()).

random_pressure() -> 0.01 + rand:uniform() * 0.04.
random_cfm() -> 2000 + rand:uniform() * 12000.

test_pmodel(Ncrah, Npressure) ->
    Crahs = [{crah_id(N), 1.0, point()} || N <- lists:seq(1,Ncrah)],
    Pressures = [{pressure_id(N), random_pressure(), point()} || N <- lists:seq(1,Npressure)],
    Distances = lists:flatten(
                  [[{CrahId, PressureId, geom:distance(CrahPoint, PressurePoint)}
                    || {CrahId, _, CrahPoint} <- Crahs]
                   || {PressureId, _, PressurePoint} <- Pressures]
                 ),

    CrahFlows = [{Id, Flow} || {Id, Flow, _} <- Crahs],
    PressurePressures = [{Id, Pressure} || {Id, Pressure, _} <- Pressures],
    case glpsol:solve("pmodel", CrahFlows, PressurePressures, Distances) of
        {solution, Flows} ->
            %?debugFmt("~n~B crahs, ~B pressures (ok)~n", [Ncrah, Npressure]),
            length(Distances) =:= length(Flows);
        no_solution ->
            ?debugFmt("~n~B crahs, ~B pressures (failed)~n~p~n~p", [Ncrah, Npressure, Crahs, Pressures]),
            false
    end.

test_tmodel(Ncrah, Nrack) ->
    Crahs = [{crah_id(N), random_cfm(), point()} || N <- lists:seq(1,Ncrah)],
    Racks = [{rack_id(N), random_pressure(), point()} || N <- lists:seq(1,Nrack)],
    Distances = lists:flatten(
                  [[{CrahId, RackId, geom:distance(CrahPoint, RackPoint)}
                    || {CrahId, _, CrahPoint} <- Crahs]
                   || {RackId, _, RackPoint} <- Racks]
                 ),
    CrahFlows = [{Id, Flow} || {Id, Flow, _} <- Crahs],
    RackPressures = [{Id, Pressure} || {Id, Pressure, _} <- Racks],
    case glpsol:solve("model", CrahFlows, RackPressures, Distances) of
        {solution, Flows} ->
            %?debugFmt("~n~B crahs, ~B racks (ok)~n", [Ncrah, Nrack]),
            length(Distances) =:= length(Flows);
        no_solution ->
            ?debugFmt("~n~B crahs, ~B racks (failed)~n~p~n~p", [Ncrah, Nrack, Crahs, Racks]),
            false
    end.

start_glpsol() ->
    application:load(sasl),
    application:set_env(sasl, sasl_error_logger, false),
    {ok, Started} = application:ensure_all_started(glpsol),
    application:set_env(glpsol, temp_dir, "."),
    lists:reverse(Started).

stop_glpsol(S) ->
    [ok = application:stop(A) || A <- S].

lazy_gen(F, C, P, N) ->
    {generator,
     fun() ->
             case N of
                 0 -> [];
                 N -> [?_assert(F(C, P)) | lazy_gen(F, C, P, N-1)]
             end
     end}.

pmodel_test_() ->
    F = fun test_pmodel/2,
    N = 20,
    {setup,
     fun start_glpsol/0,
     fun stop_glpsol/1,
     [lazy_gen(F, C, P, N) || C <- lists:seq(4,8), P <- lists:seq(4,8)]
    }.

tmodel_test_() ->
    F = fun test_tmodel/2,
    N = 20,
    {setup,
     fun start_glpsol/0,
     fun stop_glpsol/1,
     [lazy_gen(F, C, P, N) || C <- lists:seq(4,8), P <- lists:seq(4,8)]
    }.

