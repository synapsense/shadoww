
-module(glpsol).

-export([
	solve/4
	]).

-spec solve(
		Model :: string(),
		CrahFlows :: [{CrahId :: integer(), Flow :: float()}],
		RackPressures :: [{RackId :: integer(), Pressure :: float()}],
		Distances :: [{CrahId :: integer(), RackId :: integer(), Distance :: float()}]
		) ->
	{error, Reason :: any(), Stacktrace :: any()}
	| {solution, Flows :: [{CrahId :: integer(), RackId :: integer(), Flow :: float()}]}
	| no_solution .

solve(Model, CrahFlows, RackPressures, Distances) ->
	{ok, ChildPid} = glpsol_supervisor:start_child(Model, CrahFlows, RackPressures, Distances),

	receive
		{ChildPid, R} -> R
	after
		10 * 60 * 1000 ->
			glpsol_supervisor:terminate_child(ChildPid),
			error(timeout)
	end
	.


