
-module(glpsol_run).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

-export([
	start_link/5,
	run/4
	]).

%% for testing
-export([
	parse_flows/1,
	parse_flow_line/1
	]).

start_link(Model, CrahFlows, RackPressures, Distances, ReplyTo) ->
	ChildPid = spawn_link(fun() ->
				try
					ReplyTo ! {self(), run(Model, CrahFlows, RackPressures, Distances)}
				catch
					error:R ->
						lager:alert("Solver aborted"),
						ReplyTo ! {self(), {error, R, erlang:get_stacktrace()}}
				end
		end),
	{ok, ChildPid}
	.

-spec run(Model :: string(),
	  CrahFlows :: [{binary(), float()}],
	  RackPressures :: [{binary(), float()}],
	  Distances :: [{binary(), binary(), float()}]
	 ) -> no_solution | {solution, [{binary(), binary(), float()}]}.
run(Model, CrahFlows, RackPressures, Distances) ->
	% Create a temp dir
	% Create temp files in dir
	% Write model to dir
	% Start glpsol
	% Wait for it to finish
	% Read in flows
	% Return flows
	{ok, MyApp} = application:get_application(),
	PrivDir = code:priv_dir(MyApp),
	{ok, TempDir} = application:get_env(temp_dir),
	SolveDir = filename:join(TempDir, shadoww_lib:temp_name()),
	ok = file:make_dir(SolveDir),

	lager:debug("Setting up solver data in ~s", [SolveDir]),

	CrahFlowsIodata = mk_csv(CrahFlows, "CRAH,flow", "'~s',~f"),
	file:write_file(filename:join(SolveDir, "crah_flow.csv"), CrahFlowsIodata),

	RackPressuresIodata = mk_csv(RackPressures, "RACK,pressure", "'~s',~f"),
	file:write_file(filename:join(SolveDir, "racks.csv"), RackPressuresIodata),

	DistancesIodata = mk_csv(Distances, "CRAH,RACK,distance", "'~s','~s',~f"),
	file:write_file(filename:join(SolveDir, "distance.csv"), DistancesIodata),

	SolverPath = path_to_glpsol(PrivDir),
	ModelPath = filename:join(PrivDir, Model ++ ".dat"),

	Port = start_solver(SolveDir, SolverPath, ModelPath),

	lager:debug("Solver running at port ~p", [Port]),

	% glpsol note: '0' exit status doesn't mean a solution was found,
	% it just means the solver had no internal errors.
	% If no solution is found, the 'flows.csv' won't exist.
	receive
		{Port, {exit_status, 0}} -> ok;
		{Port, {exit_status, N}} ->
			lager:alert("Solver exited with (~p). Leaving files in place for analysis: ~s", [N, SolverPath]),
			error(N)
	end,

	lager:debug("Solver finished"),

	case file:read_file(filename:join(SolveDir, "flows.csv")) of
		{ok, FlowTxt} ->
			Flows = parse_flows(FlowTxt),
			rm_rf(SolveDir),
			lager:debug("Output parsed, returning flows"),
			{solution, Flows};
		{error, enoent} ->
			% log some serious error here
			lager:alert("No solution! Preserving temp dir (~s) for analysis.", [SolveDir]),
			no_solution
	end
	.

start_solver(CWD, SolverPath, ModelPath) ->
	open_port({spawn_executable, SolverPath}, [
			{cd, CWD},
			{args, ["--math", ModelPath, "-y", "info.txt"]},
			hide,
			exit_status
		])
	.

path_to_glpsol(PrivDir) ->
	ExeName = case os:type() of
		{unix, _} -> "glpsol";
		{win32, _} -> "glpsol.exe"
	end,
	case os:find_executable(ExeName) of
		false ->
			filename:join(PrivDir, ExeName)
			;
		FullPath ->
			FullPath
	end
	.

mk_csv(Lines, Header, FmtStr) ->
	string:join([Header | [io_lib:format(FmtStr, tuple_to_list(L)) || L <- Lines]], "\n")
	.

parse_flow_line(Line) ->
	[<<>>, Crah, Rack, Flow] = binary:split(Line,
						[<<$">>,
						 <<$",$,,$">>,
						 <<$",$,>>],
						[global]),
	{binary:copy(Crah), binary:copy(Rack), b_to_f(Flow)}
	.

b_to_f(N) ->
	try
		binary_to_float(N)
	catch
		_:_ -> 1.0 * binary_to_integer(N)
	end
	.

parse_flows(BinData) ->
	Lines = binary:split(BinData,
			     [<<"\r\n">>, <<"\n">>, <<"\r">>],
			     [global, trim]),
	% TODO: make Concurrency a configurable?
	Concurrency = max(erlang:system_info(schedulers_online) - 1, 1),
	shadoww_lib:pmap_lim(Concurrency, {?MODULE, parse_flow_line}, [], tl(Lines))
	.

-ifdef(EUNIT).
parse_flows_unix_le_test() ->
	TxtFlows = <<"CRAH,RACK,flow\n\"1\",\"1\",1\n\"1\",\"2\",1.0\n\"2\",\"1\",10000\n\"2\",\"2\",999999.943\n">>,
	Flows = parse_flows(TxtFlows),
	?assertEqual([{<<"1">>,<<"1">>,1.0},
		      {<<"1">>,<<"2">>,1.0},
		      {<<"2">>,<<"1">>,10000.0},
		      {<<"2">>,<<"2">>,999999.943}], Flows)
	.

parse_flows_win_le_test() ->
	TxtFlows = <<"CRAH,RACK,flow\r\n\"1\",\"1\",1\r\n\"1\",\"2\",1.0\r\n\"2\",\"1\",10000\r\n\"2\",\"2\",999999.943\r\n">>,
	Flows = parse_flows(TxtFlows),
	?assertEqual([{<<"1">>,<<"1">>,1.0},
		      {<<"1">>,<<"2">>,1.0},
		      {<<"2">>,<<"1">>,10000.0},
		      {<<"2">>,<<"2">>,999999.943}], Flows)
	.

parse_flows_mac_le_test() ->
	TxtFlows = <<"CRAH,RACK,flow\r\"1\",\"1\",1\r\"1\",\"2\",1.0\r\"2\",\"1\",10000\r\"2\",\"2\",999999.943\r">>,
	Flows = parse_flows(TxtFlows),
	?assertEqual([{<<"1">>,<<"1">>,1.0},
		      {<<"1">>,<<"2">>,1.0},
		      {<<"2">>,<<"1">>,10000.0},
		      {<<"2">>,<<"2">>,999999.943}], Flows)
	.

-endif.

rm_rf(DirName) ->
	{ok, Files} = file:list_dir(DirName),
	[ok = file:delete(DirName ++ "/" ++ F) || F <- Files],
	ok = file:del_dir(DirName)
	.

