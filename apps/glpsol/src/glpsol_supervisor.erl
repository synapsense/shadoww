
-module(glpsol_supervisor).

-behaviour(supervisor).

-export([
	start_link/0,
	init/1,
	start_child/4,
	terminate_child/1
	]).

-define(SERVER, ?MODULE).


start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, [])
	.

init([]) ->
	RestartStrategy		= simple_one_for_one,
	MaxRestarts		= 1,
	MaxTimeBetRestarts	= 1,

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpec = [{
		glpsol_run,
		{glpsol_run, start_link, []},
		transient,
		2 * 60 * 1000,
		worker,
		[glpsol_run]
	}],

	{ok, {SupFlags, ChildSpec}}
	.

start_child(Model, CrahFlows, RackPressures, Distances) ->
	supervisor:start_child(?SERVER, [Model, CrahFlows, RackPressures, Distances, self()])
	.

terminate_child(ChildPid) ->
	supervisor:terminate_child(?SERVER, ChildPid)
	.

