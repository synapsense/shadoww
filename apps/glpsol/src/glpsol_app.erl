
-module(glpsol_app).

-behaviour(application).

-export([start/2, stop/1, config_change/3]).

start(normal, _StartArgs) ->
	Sup = glpsol_supervisor:start_link(),
	lager:info("glpsol supervisor started: ~p", [Sup]),
	Sup
	.

stop(_State) -> ok .

config_change(Changed, New, Removed) ->
	lager:info("glpsol config change. changed: ~p, new: ~p, removed: ~p", [Changed, New, Removed])
	.


