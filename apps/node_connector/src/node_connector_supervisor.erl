
-module(node_connector_supervisor).

-behaviour(supervisor).

-export([
	start_link/0,
	init/1
	]).

-define(SERVER, ?MODULE).


start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, [])
	.

init([]) ->
	RestartStrategy		= one_for_one,
	MaxRestarts		= 1,
	MaxTimeBetRestarts	= 60 * 60,

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpec = [{
		node_connector,
		{node_connector, start_link, []},
		permanent,
		1000,
		worker,
		[node_connector]
	}],

	{ok, {SupFlags, ChildSpec}}
	.

