
-module(node_connector).

-behaviour(gen_server).

%% API
-export([
         start_link/0,
         start_connect/1,
         stop_connect/1,
         list_connect/0
        ]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {
          reconnect_delay :: integer(),
          nodes :: [node()],
          down :: [node()]
         }).

-define(SERVER, ?MODULE).
-define(RECONNECT, reconnect).

%%%===================================================================
%%% API
%%%===================================================================
%%%
%%% NOTE: monitor_node() can take a very long time, on the order of 10sec,
%%% which is why the timeout on all calls is infinity.
%%%
%%% Time to call one of these functions could be much longer than 10sec
%%% if many nodes are disconnected.

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
-spec start_link() -> {ok, pid()}.
start_link() ->
    NodeList = node_list(),
    {ok, ReconnectDelay} = application:get_env(reconnect_delay),
    gen_server:start_link({local, ?SERVER}, ?MODULE, {NodeList, ReconnectDelay}, []).

-spec start_connect(node()) -> ok | already_monitoring.
start_connect(NodeName) when is_atom(NodeName) ->
    gen_server:call(?SERVER, {start, NodeName}, infinity).

-spec stop_connect(node()) -> ok | not_monitoring.
stop_connect(NodeName) when is_atom(NodeName) ->
    gen_server:call(?SERVER, {stop, NodeName}, infinity).

-spec list_connect() -> {Up :: [node()], Down :: [node()]}.
list_connect() ->
    gen_server:call(?SERVER, list, infinity).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init({NodeList, ReconnectDelay}) ->
    {ok, #state{reconnect_delay=ReconnectDelay,nodes=NodeList,down=[]}, 0}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({start, Node}, _From, State=#state{nodes=Nodes}) ->
    case lists:member(Node, Nodes) of
        true ->
            {reply, already_monitoring, State};
        false ->
            monitor_node(Node, true),
            {reply, ok, State#state{nodes=[Node|Nodes]}}
    end;
handle_call({stop, Node}, _From, State=#state{nodes=Nodes,down=Down}) ->
    case lists:member(Node, Nodes) of
        true ->
            monitor_node(Node, false),
            {reply, ok, State#state{nodes=Nodes--[Node], down=Down--[Node]}};
        false ->
            {reply, not_monitoring, State}
    end;
handle_call(list, _From, State=#state{down=Down,nodes=Nodes}) ->
    Up = Nodes -- Down,
    {reply, {Up, Down}, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(timeout, State=#state{reconnect_delay=Delay,nodes=Nodes}) ->
    [reconnect_node(Node) || Node <- Nodes],
    erlang:send_after(Delay, self(), ?RECONNECT),
    {noreply, State};
handle_info(?RECONNECT,
            State=#state{reconnect_delay=Delay,down=Down,nodes=Nodes}) ->
    [reconnect_node(D) || D <- Down, lists:member(D, Nodes)],
    erlang:send_after(Delay, self(), ?RECONNECT),
    {noreply, State#state{down=[]}};
handle_info({nodedown, DownNode}, State=#state{nodes=Nodes,down=Down}) ->
    case lists:member(DownNode, Nodes) of
        true -> {noreply, State#state{down=[DownNode|Down]}};
        false -> {noreply, State}
    end;
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

node_list() ->
    {ok, Nodes} = application:get_env(nodes),
    Nodes.

reconnect_node(Node) ->
    net_kernel:connect_node(Node),
    erlang:monitor_node(Node, true).

