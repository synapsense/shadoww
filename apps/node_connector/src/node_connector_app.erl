
-module(node_connector_app).

-behaviour(application).

-export([start/2, stop/1, config_change/3]).

start(normal, _StartArgs) ->
	Sup = node_connector_supervisor:start_link(),
	lager:info("node_connector supervisor started: ~p", [Sup]),
	Sup.

stop(_State) -> ok.

config_change(Changed, New, Removed) ->
	lager:info("node_connector config change. changed: ~p, new: ~p, removed: ~p", [Changed, New, Removed]).


