
-module(shadoww_lib).

-compile(export_all).

-include_lib("et/include/et.hrl").

% like rpc:pmap/3 but all processes run on a single, specified node
-spec pmap(Node :: node(),
           MF :: {module(), atom()},
           ExtraArgs :: [any()],
           List1 :: [any()]) ->
    List2 :: [any()].
pmap(Node, {M,F}, ExtraArgs, List1) ->
    Keys = [rpc:async_call(Node, M, F, [Arg|ExtraArgs]) || Arg <- List1],
    pmap_check([rpc:yield(Key) || Key <- Keys], []).

pmap_check([{badrpc, X}|_], _) -> exit({badrpc, X});
pmap_check([X|Xs], Acc) -> pmap_check(Xs, [X|Acc]);
pmap_check([], Acc) -> lists:reverse(Acc).

% like rpc:pmap/3
% but limit the number of processes running concurrently
% and only spawn them on the current node
% and bail immediately if one fails
-spec pmap_lim(Limit :: non_neg_integer(),
               MF :: {module(), atom()},
               ExtraArgs :: [any()],
               List1 :: [any()]) ->
    List2 :: [any()] | {error, any()}.
pmap_lim(Lim, {_M,_F}=MF, ExtraArgs, List1) when Lim > 0  ->
    NProcs = length(List1),
    ArgsList = [[L|ExtraArgs] || L <- List1],
    NumberedArgs = lists:zip(lists:seq(0, NProcs-1), ArgsList),
    {Running, Waiting} = lists:split(min(NProcs,Lim), NumberedArgs),
    [spawn(?MODULE, pmap_lim_run, [MF, Args, self()]) || Args <- Running],
    pmap_lim1(MF, Waiting, array:new(NProcs), min(NProcs,Lim)).

pmap_lim1(_MF, [], Results, 0) ->
    array:to_list(Results);
pmap_lim1(MF, [], Results, NRunning) ->
    receive
        {_N, {error, {_Err, _Reason}} = Failure} ->
            % running procs get abandoned
            Failure;
        {N, {ok, R}} ->
            pmap_lim1(MF, [], array:set(N, R, Results), NRunning - 1)
    end;
pmap_lim1(MF, [Next | Waiting], Results, NRunning) ->
    receive
        {_N, {error, {_Err, _Reason}} = Failure} ->
            % running procs get abandoned
            Failure;
        {N, {ok, R}} ->
            spawn(?MODULE, pmap_lim_run, [MF, Next, self()]),
            pmap_lim1(MF, Waiting, array:set(N, R, Results), NRunning)
    end.

pmap_lim_run({M,F}, {N, Args}, ReplyTo) ->
    Result = try
                 apply(M, F, Args)
             catch
                 Err:Reason -> {error, {Err, Reason}}
             end,
    ReplyTo ! {N, {ok, Result}}.

zipX(ListOLists) -> zipX_r(ListOLists, []).

zipX_r([], Accum) -> lists:reverse(Accum);
zipX_r(ListOLists, Accum) ->
    Hs = [hd(L) || L <- ListOLists],
    Ts = [tl(L) || L <- ListOLists],
    zipX_r(Ts, [list_to_tuple(Hs) | Accum]).

% like the lists:key* functions, operate on a list of tuples
% pull all tuples from TupleList with a key in Keys
keyfilter(Keys, N, TupleList) ->
	lists:foldl(
		fun(Elem, Acc) ->
			case lists:keyfind(Elem, N, TupleList) of
				false -> Acc;
				Tup -> Acc ++ [Tup]
			end
		end, [], Keys)
	.

% Union of two tuple lists, by key N
% If a key in L1 compares equal to a key in L2, only the tuple from L1 is included
keyunion(N, TupleList1, TupleList2) ->
	Keys1 = [element(N, T) || T <- TupleList1],
	TupleList1 ++ [T || T <- TupleList2, not lists:member(element(N, T), Keys1)]
	.

% Like lists:keyfind(Key, N, TupleList) -> Tuple | false.
% except that this will return all tuples, not just the first, with a matching key.
-spec keyfindall(term(), integer(), [tuple()]) -> [tuple()].
keyfindall(_, _, []) ->
	[];
keyfindall(Key, N, [T | TupleList]) when element(N, T) == Key ->
	[T | keyfindall(Key, N, TupleList)];
keyfindall(Key, N, TupleList) ->
	keyfindall(Key, N, tl(TupleList)).

-define(MEGA, 1000000000).

millisecs_now() -> now_to_millisecs(os:timestamp()) .

now_to_millisecs({Mega, Secs, Micro}) ->
	(Mega * ?MEGA) + (1000 * Secs) + (Micro div 1000)
	.

millisecs_to_now(UnixTime) ->
	Mega = UnixTime div ?MEGA,
	MinusMega = UnixTime - (Mega * ?MEGA),
	Secs = MinusMega div 1000,
	MinusSecs = MinusMega - (Secs * 1000),
	Micro = MinusSecs * 1000,
	Now = {Mega, Secs, Micro},
	Now
	.

dotted_to_tuple(Ip) when is_binary(Ip) ->
    dotted_to_tuple(binary_to_list(Ip));
dotted_to_tuple(Ip) ->
	[A,B,C,D] = lists:map(fun(E) -> {Int, []} = string:to_integer(E), Int end, string:tokens(Ip, ".")),
	{A,B,C,D}.

tuple_to_dotted({A,B,C,D}) ->
        lists:flatten(io_lib:format("~B.~B.~B.~B", [A,B,C,D])).

% Map a value from one linear scale to another linear scale
% The input value will be clamped to the Amin and Amax parameters,
% so the return is always a value between Bmin and Bmax inclusive.
linear_map(Aval, Amin, Amax, Bmin, Bmax) ->
	Val = erlang:min(erlang:max(Aval, Amin), Amax),
	Percent = (Val - Amin) / (Amax - Amin),
	BVal = Bmin + (Percent * (Bmax - Bmin)),
	BVal
	.

% return a count of items in the list for which Pred is true
-spec count_true(fun((term()) -> boolean), [term()]) -> non_neg_integer().
count_true(_, []) -> 0;
count_true(Pred, [I | List]) ->
	case Pred(I) of
		true -> 1;
		false -> 0
	end + count_true(Pred, List)
	.

% Takes a list of tuples and groups them by a specific key in the tuple.
% Return a list of lists, each list containing all tuples with the same value of the key.
% The return value is a list of tuple lists [TupleList1, TupleList2, ...]
% Example: keypivot(1, [{a, 1}, {a, 2}, {b, 3}, {c, 4}]) -> [[{a, 1}, {a, 2}], [{b, 3}], [{c, 4}]].
-spec keygroup(non_neg_integer(), [tuple()]) -> [[tuple()]].
keygroup(N, TupleList) ->
	r_keygroup(N, TupleList, [])
	.

r_keygroup(_N, [], Results) ->
	Results;
r_keygroup(N, Remaining, Results) ->
	Key = element(N, hd(Remaining)),
	{Matching, NextRemaining} = keytakeall(Key, N, Remaining),
	r_keygroup(N, NextRemaining, [Matching | Results])
	.

pairmap(F, L) -> r_pairmap(F, L, []).

r_pairmap(F, [H, L], Accum) -> Accum ++ [F(H, L)];
r_pairmap(F, [H | L], Accum) -> r_pairmap(F, L, Accum ++ [F(H, I) || I <- L]).

-spec keytakeall(term(), non_neg_integer(), [tuple()]) -> {[tuple()], [tuple()]}.
keytakeall(Key, N, TupleList) ->
    r_keytakeall(Key, N, TupleList, []).

r_keytakeall(K, N, TupleList, Taken) ->
    case lists:keytake(K, N, TupleList) of
	false -> {Taken, TupleList};
	{value, V, Remain} -> r_keytakeall(K, N, Remain, [V | Taken])
    end.

getprop(PName,Properties)->
    case proplists:lookup(PName, Properties) of
	none -> error(badarg, [PName]);
	{PName, Val} -> Val
    end.

setprop({PName, Val}, Properties) ->
    Old = case proplists:lookup(PName, Properties) of
	      none -> error(badarg, [PName, Val]);
	      {PName, OldVal} -> OldVal
	  end,
    {Old,lists:keyreplace(PName, 1, Properties, {PName, Val})}.


go_et() ->
	et_viewer:start([{trace_global, true}, {trace_pattern, {et, max}}, {max_actors, 10}, {active_filter, pretty_to}, {dict_insert, {filter, pretty_to}, fun pretty_to/1}]),
	dbg:p(all, call),
	dbg:tpl(et, trace_me, 5, []),
	dbg:tpl(et, trace_me, 4, [])
	.

pretty_to(E) when is_record(E, event) ->
	Prettify = fun(Id) ->
			case Id of
				{to, _} -> {esapi:object_type(Id), esapi:object_instance(Id)};
				Id -> Id
			end
	end,
	{true, E#event{from = Prettify(E#event.from), to = Prettify(E#event.to)}}
	.

%% Return the first element in L for which F(Elem) returns true
%% fails with badarg if no element matches
firstmatch(F, [H|T]) ->
	case F(H) of
		true -> H;
		false -> firstmatch(F, T)
	end
	.

temp_name() ->
	{Mega, Secs, Micro} = os:timestamp(),
	U = erlang:unique_integer([positive]),
	lists:flatten(io_lib:format("~B_~B_~B_~B", [Mega, Secs, Micro, U]))
	.

corralled(Min, Max, Value) ->
    max(Min, min(Max, Value)).

