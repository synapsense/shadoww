
-module(gen_event_fun).

% event handler callback module that takes a fun to process events.

-behaviour(gen_event).


% API
-export([
    add_handler/3,
    add_sup_handler/3,
    update_fun/3,
    delete_handler/2
    ]).

% module callbacks
-export([
    init/1,
    handle_event/2,
    handle_call/2,
    handle_info/2,
    terminate/2,
    code_change/3
    ]).


-record(state, {id, owner_pid, user_fun}).


% API methods
add_handler(EventManager, Id, Fun) ->
    gen_event:add_handler(EventManager, {?MODULE, Id}, {Id, Fun}).

add_sup_handler(EventManager, Id, Fun) ->
    gen_event:add_sup_handler(EventManager, {?MODULE, Id}, {Id, self(), Fun}).

update_fun(EventManager, Id, Fun) ->
    gen_event:call(EventManager, Id, Fun).

delete_handler(EventManager, Id) ->
    gen_event:delete_handler(EventManager, {?MODULE, Id}, normal).

%% Behavior callbacks

init({Id, Fun}) ->
    {ok, #state{id = Id, user_fun = Fun}};
init({Id, OwnerPid, Fun}) ->
    {ok, #state{id = Id, owner_pid = OwnerPid, user_fun = Fun}}.

handle_event(Event, S=#state{user_fun=F}) ->
    F(Event),
    {ok, S}.

handle_call(NewFun, S) ->
    {ok, ok, S#state{user_fun=NewFun}}.

handle_info({'EXIT', Pid, _Reason}, #state{owner_pid=Owner}) when Pid =:= Owner ->
    remove_handler;
handle_info(_Info, State) ->
    {ok, State}.

terminate(Reason, _State) ->
    Reason.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

