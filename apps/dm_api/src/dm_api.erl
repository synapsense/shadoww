-module(dm_api).

-behaviour(gen_server).

-export([start_link/0, stop/0, get_value/2, set_value/3]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).


-define(SERVER, ?MODULE).
-define(STATE, ?MODULE).
-define(TIMEOUT, 10 * 1000).  % 10 second timeout


-record(?STATE, {port_pid, waiting_replies = [], node_name}).

%% ====================================================================
%% External functions
%% ====================================================================

start_link() ->
	Return = gen_server:start_link({local, ?SERVER}, ?MODULE, #?STATE{}, []),
	lager:info("AC Plugin Adapter application started: ~p", [Return]),
	Return
	.

stop() ->
	gen_server:cast(?SERVER, stop)
	.

get_value(PluginName, ParamsList) ->
	call_port({get_value, PluginName, ParamsList}).

set_value(PluginName, ParamsList, NewValue) ->
	call_port({set_value, PluginName, ParamsList, NewValue}).

%% ====================================================================
%% Server functions
%% ====================================================================

%% Behavior callbacks
%% --------------------------------------------------------------------
%% Function: init/1
%% Description: Initiates the server
%% Returns: {ok, State}          |
%%          {ok, State, Timeout} |
%%          ignore               |
%%          {stop, Reason}
%% --------------------------------------------------------------------

init(Config) ->
	IsActive = application:get_env(active),
	if
		{ok, false} == IsActive ->
			lager:info("DM API is not starting"),
			{ok, Config}
			;
		({ok, true} == IsActive) or (undefined == IsActive) ->
			PortPid = create_port(),
			lager:info("DM API init complete"),
			{ok, Config#?STATE{port_pid = PortPid, node_name = construct_nodename()}}
	end
	.
%% --------------------------------------------------------------------
%% Function: handle_call/3
%% Description: Handling call messages
%% Returns: {reply, Reply, State}          |
%%          {reply, Reply, State, Timeout} |
%%          {noreply, State}               |
%%          {noreply, State, Timeout}      |
%%          {stop, Reason, Reply, State}   | (terminate/2 is called)
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------

handle_call(Message, From, State) ->
	lager:error("Unexpected handle_call. message: ~p, from: ~p, state: ~p", [Message, From, State]),
	{reply, huh, State}
	.
%% --------------------------------------------------------------------
%% Function: handle_cast/2
%% Description: Handling cast messages
%% Returns: {noreply, State}          |
%%          {noreply, State, Timeout} |
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------

handle_cast(stop, State = #?STATE{node_name = NodeName}) ->
	lager:info("Shutting down port"),
	{ok, MboxName} = application:get_env(dm_api, mbox_name),
	erlang:send({MboxName, NodeName}, shutdown),
	{noreply, State}
	;
handle_cast(Request, State) ->
	lager:error("Unexpected handle_cast. request: ~p, state: ~p", [Request, State]),
	{noreply, State}
	.

%% --------------------------------------------------------------------
%% Function: handle_info/2
%% Description: Handling all non call/cast messages
%% Returns: {noreply, State}          |
%%          {noreply, State, Timeout} |
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------
handle_info({nodedown, DownName}, State = #?STATE{node_name = NodeName})
		when DownName == NodeName ->
	lager:error("nodedown from AC Plugin adapter, gen_server exiting"),
	{stop, nodedown, State}
	;
handle_info({Port, {exit_status, Status}}, State = #?STATE{port_pid = PortPid})
		when Port == PortPid ->
	lager:error("AC Plugin adapter exited, gen_server exiting.  exit_status=~p", [Status]),
	{stop, nodedown, State}
	;
handle_info(Info, State) ->
	lager:error("Unexpected handle_info. info: ~p, state: ~p", [Info, State]),
	{noreply, State}
	.

%% --------------------------------------------------------------------
%% Function: terminate/2
%% Description: Shutdown the server
%% Returns: any (ignored by gen_server)
%% --------------------------------------------------------------------
terminate(nodedown, _State) ->
	lager:warning("Java node died, exiting..."),
	nodedown
	;
terminate(Reason, State = #?STATE{port_pid = Port, node_name = NodeName}) ->
	lager:warning("terminating. reason: ~p, state: ~p", [Reason, State]),
	{ok, MboxName} = application:get_env(dm_api, mbox_name),
	erlang:send({MboxName, NodeName}, shutdown),
	port_close(Port),
	receive
		{nodedown, NodeName} -> ok
	after
		5000 -> ok
	end,
	Reason
	.

%% --------------------------------------------------------------------
%% Func: code_change/3
%% Purpose: Convert process state when code is changed
%% Returns: {ok, NewState}
%% --------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
	{ok, State}
	.

%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------
wait_stop(NodeName) ->
	case net_kernel:connect_node(NodeName) of
		false ->
			lager:info("Java node not running"),
			pang;
		true ->
			lager:warning("Still running, try again in 5 seconds"),
			timer:sleep(5000),
			wait_stop(NodeName)
	end
	.

wait_start(_Port, _NodeName, 0) ->
	lager:error("Java node failed to start."),
	not_started
	;
wait_start(Port, NodeName, N) ->
	receive
		{Port, {exit_status, Status}} ->
			lager:error("Java node exited with status=~p", [Status]),
			not_started
	after
		0 ->
			ok
	end,
	case net_kernel:connect_node(NodeName) of
		false ->
			lager:warning("Java node not running, try again in 5 seconds. node_name: ~p, retries: ~p", [NodeName, N]),
			timer:sleep(5000),
			wait_start(Port, NodeName, N - 1);
		true ->
			lager:info("Java node running"),
			started
	end
	.

create_port() ->
	{ok, DriverCommand} = application:get_env(dm_api, driver_command),
	{ok, ArgsString} = application:get_env(dm_api, args),
	{ok, MboxName} = application:get_env(dm_api, mbox_name),
	{ok, DebugPort} = application:get_env(dm_api, debug_port),
	DebugArgs = case application:get_env(dm_api, debug_enabled) of
		{ok, true} ->
			{ok, DebugPort} = application:get_env(dm_api, debug_port),
			{ok, Suspend} = application:get_env(dm_api, suspend),
			["-Xdebug -Xrunjdwp:transport=dt_socket,address="
				++ DebugPort
				++ ",server=y,suspend="
				++ Suspend];
		_ ->
			[]
	end,

	NodeName = construct_nodename(),

	lager:info("Waiting for old Java node to stop before starting new one"),
	wait_stop(NodeName),

	PrivDirectory = code:priv_dir(dm_api),

	CommandList = [DriverCommand] ++ DebugArgs ++ [ArgsString,
						       atom_to_list(NodeName),
						       atom_to_list(MboxName),
						       atom_to_list(erlang:get_cookie())],
	CommandString = string:join(CommandList, " "),
	lager:info("Starting AC Plugin adapter. command=~s, dir=~s", [CommandString, PrivDirectory]),
	PortPid = open_port({spawn, CommandString}, [{cd, PrivDirectory}, stream, exit_status, hide]),
	lager:info("AC Plugin adapter starting (pid=~p)...", [PortPid]),

	lager:info("Waiting for Java node to start before setting monitor"),
	started = wait_start(PortPid, NodeName, 10),

	lager:info("Setting monitor on Java node ~p", [NodeName]),
	start_monitor(NodeName),
	erlang:send({MboxName, NodeName}, link),
	PortPid
	.

start_monitor(NodeName) ->
	erlang:monitor_node(NodeName, true),
	receive
		{nodedown, NodeName} ->
			lager:warning("Set monitor on node ~p failed, trying again in 100 ms", [NodeName]),
			timer:sleep(100),
			start_monitor(NodeName)
	after
		500 ->
			lager:info("Monitor established on node ~p", [NodeName])
	end
	.

% must be called by the caller's PID
call_port(Message) ->
	call_port(Message, ?TIMEOUT)
	.

call_port(Message, Timeout) ->
	NodeName = construct_nodename(),
	{ok, MboxName} = application:get_env(dm_api, mbox_name),
	Ref = make_ref(),
	erlang:send({MboxName, NodeName}, {Ref, Message}),
	receive
		{Ref, {error, Reason}} ->
			error(Reason);
		{Ref, Reply} ->
			Reply
	after
		Timeout ->
			lager:warning("Command (~p) timed out ~p", [Message, Timeout]),
			error(no_reply)
	end
	.

construct_nodename() ->
	[MyNode, MyHost] = binary:split(atom_to_binary(node(), latin1), <<"@">>),
	NodeName = <<MyNode/binary, "-plugadapter@", MyHost/binary>>,
	binary_to_atom(NodeName, latin1)
	.

