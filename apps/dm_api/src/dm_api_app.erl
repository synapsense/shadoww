
-module(dm_api_app).

-behaviour(application).

-export([start/2, stop/1]).

start(Type, StartArgs) ->
	lager:info("dm_api app starting type=~p, start_args=~p", [Type, StartArgs]),
	dm_api_sup:start_link(StartArgs)
	.

stop(State) ->
	lager:info("dm_api app stopping state=~p", [State])
	.

