%% Author: root
%% Created: Sep 1, 2011
%% Description: TODO: Add description to bacnet
-module(bacnet).

-export([read_ai/5, read_ao/5, read_av/5, read_bi/5, read_bo/5, read_bv/5, read_mi/5, read_mo/5, read_mv/5, write_ao/6, write_av/6, write_bo/6, write_bv/6, write_mo/6, write_mv/6]).

-define(PLUGIN, ?MODULE).

-type integer_bool() :: 0 | 1.

-spec read_ai(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: float().
%% Reads specified property of an BACNet object with Analog-Input type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_ai(Ip, Net, Dev, Obj, Id)->
	read("Analog-Input", Ip, Net, Dev, Obj, Id).

-spec read_ao(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: float().
%% Reads specified property of an BACNet object with Analog-Output type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_ao(Ip, Net, Dev, Obj, Id)->
	read("Analog-Output", Ip, Net, Dev, Obj, Id).

-spec read_av(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: float().
%% Reads specified property of an BACNet object with Analog-Value type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_av(Ip, Net, Dev, Obj, Id)->
	read("Analog-Value", Ip, Net, Dev, Obj, Id).

-spec read_bi(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: integer_bool().
%% Reads specified property of an BACNet object with Binary-Input type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_bi(Ip, Net, Dev, Obj, Id)->
	read("Binary-Input", Ip, Net, Dev, Obj, Id).

-spec read_bo(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: integer_bool().
%% Reads specified property of an BACNet object with Binary-Output type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_bo(Ip, Net, Dev, Obj, Id)->
	read("Binary-Output", Ip, Net, Dev, Obj, Id).

-spec read_bv(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: integer_bool().
%% Reads specified property of an BACNet object with Binary-Value type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_bv(Ip, Net, Dev, Obj, Id)->
	read("Binary-Value", Ip, Net, Dev, Obj, Id).

-spec read_mi(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: integer().
%% Reads specified property of an BACNet object with Multi-State-Input type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_mi(Ip, Net, Dev, Obj, Id)->
	read("Multy-State-Input", Ip, Net, Dev, Obj, Id).

-spec read_mo(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: integer().
%% Reads specified property of an BACNet object with Multi-State-Output type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_mo(Ip, Net, Dev, Obj, Id)->
	read("Multy-State-Output", Ip, Net, Dev, Obj, Id).

-spec read_mv(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer()) -> Value :: integer().
%% Reads specified property of an BACNet object with Multi-State-Value type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
read_mv(Ip, Net, Dev, Obj, Id)->
	read("Multy-State-Value", Ip, Net, Dev, Obj, Id).

-spec write_ao(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer(), Value :: float()) -> Value :: float().
%% Writes specified property of an BACNet object with Analog-Output type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
write_ao(Ip, Net, Dev, Obj, Id, Value)->
	write("Analog-Output", Ip, Net, Dev, Obj, Id, Value).

-spec write_av(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer(), Value :: float()) -> Value :: float().
%% Writes specified property of an BACNet object with Analog-Value type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
write_av(Ip, Net, Dev, Obj, Id, Value)->
	write("Analog-Value", Ip, Net, Dev, Obj, Id, Value).

-spec write_bo(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer(), Value :: integer_bool()) -> Value :: integer_bool().
%% Writes specified property of an BACNet object with Binary-Output type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
write_bo(Ip, Net, Dev, Obj, Id, Value)->
	write("Binary-Output", Ip, Net, Dev, Obj, Id, Value).

-spec write_bv(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer(), Value :: integer_bool()) -> Value :: integer_bool().
%% Writes specified property of an BACNet object with Binary-Value type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
write_bv(Ip, Net, Dev, Obj, Id, Value)->
	write("Binary-Value", Ip, Net, Dev, Obj, Id, Value).

-spec write_mo(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer(), Value :: number()) -> Value :: integer().
%% Writes specified property of an BACNet object with Multi_State-Output type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
write_mo(Ip, Net, Dev, Obj, Id, Value) when is_integer(Value)->
	write("Multy-State-Output", Ip, Net, Dev, Obj, Id, Value);
write_mo(Ip, Net, Dev, Obj, Id, Value)->
	write("Multy-State-Output", Ip, Net, Dev, Obj, Id, round(Value)).

-spec write_mv(Ip :: string(), Net :: integer(), Dev :: integer(), Obj :: integer(), Id :: integer(), Value :: number()) -> Value :: integer().
%% Writes specified property of an BACNet object with Multi_State-Value type.
%% Ip - Destination IP address.
%% Net - Destination BACNet Network number.
%% Dev - Destination BACNet Device instance.
%% Obj - BACNet Object identifier.
%% Id - BACnet Property identifier.
write_mv(Ip, Net, Dev, Obj, Id, Value) when is_integer(Value)->
	write("Multy-State-Value", Ip, Net, Dev, Obj, Id, Value);
write_mv(Ip, Net, Dev, Obj, Id, Value)->
	write("Multy-State-Value", Ip, Net, Dev, Obj, Id, round(Value)).

read(ObjType, Ip, Net, Dev, Obj, Id)->
	dm_api:get_value(?PLUGIN, pack_params(Ip, Net, Dev, Obj, ObjType, Id)).

write(ObjType, Ip, Net, Dev, Obj, Id, Value)->
	dm_api:set_value(?PLUGIN, pack_params(Ip, Net, Dev, Obj, ObjType, Id), Value).

pack_params(Ip, Net, Dev, Obj, ObjType, Id)->
	[{"ip", Ip}, {"net", Net},{"dev",Dev},{"obj", Obj},{"type", ObjType},{"id", Id}].
