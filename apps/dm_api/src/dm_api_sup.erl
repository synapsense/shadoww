
-module(dm_api_sup).

-behaviour(supervisor).

-export([start_link/1, init/1]).

-define(SERVER, ?MODULE).

start_link(_StartArgs) ->
	supervisor:start_link(?MODULE, [])
	.

init(_Args) ->
	RestartStrategy		= rest_for_one,	% each process that dies, resart it and all after it
	MaxRestarts		= 2,		% number of restarts allowed in
	MaxTimeBetRestarts	= 3600,		% this number of seconds

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpecs = [{
		dm_api,				% name of the process
		{dm_api, start_link, []},	% module, function, args
		permanent,			% death of the process is never expected, so always restart
		1000,				% how many ms the process is given to stop gracefully before being killed
		worker,				% this process is doing actual work
		[dm_api]			% module dependencies
	}],
	{ok, {SupFlags, ChildSpecs}}
	.

