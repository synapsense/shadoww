-module(ac_plugin_SUITE).

-compile(export_all).

%-export([suite/0, all/0,
% init_per_suite/1, end_per_suite/1, init_per_testcase/2, end_per_testcase/2]).

-include_lib("common_test/include/ct.hrl").

%%--------------------------------------------------------------------
%% COMMON TEST CALLBACK FUNCTIONS
%%--------------------------------------------------------------------
%%--------------------------------------------------------------------
%% Function: suite() -> Info
%%
%% Info = [tuple()]
%%   List of key/value pairs.
%%
%% Description: Returns list of tuples to set default properties
%%              for the suite.
%%--------------------------------------------------------------------
suite() ->
    [{timetrap,{minutes,1}}].

%%--------------------------------------------------------------------
%% Function: init_per_suite(Config0) -> Config1
%%
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%%
%% Description: Initialization before the suite.
%%--------------------------------------------------------------------
%init_per_suite(Config) ->
%    ok.
%%--------------------------------------------------------------------
%% Function: end_per_suite(Config) -> void()
%%
%% Config = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%%
%% Description: Cleanup after the suite.
%%--------------------------------------------------------------------
%end_per_suite(Config) ->
%    ok.
%%--------------------------------------------------------------------
%% Function: init_per_testcase(TestCase, Config0) -> Config1
%%
%% TestCase = atom()
%%   Name of the test case that is about to run.
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%%
%% Description: Initialization before each test case.
%%--------------------------------------------------------------------
%init_per_testcase(Case, Config) ->
%    ok.
%%--------------------------------------------------------------------
%% Function: end_per_testcase(TestCase, Config) -> void()
%%
%% TestCase = atom()
%%   Name of the test case that is finished.
%% Config = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%%
%% Description: Cleanup after each test case.
%%--------------------------------------------------------------------
%end_per_testcase(_Case, Config) ->
%    ok.
%%--------------------------------------------------------------------
%% Function: all() -> GroupsAndTestCases
%%
%% GroupsAndTestCases = [{group,GroupName} | TestCase]
%% GroupName = atom()
%%   Name of a test case group.
%% TestCase = atom()
%%   Name of a test case.
%%
%% Description: Returns the list of groups and test cases that
%%              are to be executed.
%%--------------------------------------------------------------------
all() ->
    [testcase1].
%%--------------------------------------------------------------------
%% TEST CASES
%%--------------------------------------------------------------------
testcase1(Config) ->
	Result = net_adm:ping('controller@n102250.merann.ru'),
	
	ct:pal("Ping result: ~p", [Result]),
	case Result of
		pong -> ct:pal("pong");
		pang -> ct:pal("ERRRR-pang")
	end,
	[MyNode, MyHost] = binary:split(atom_to_binary(node(), latin1), <<"@">>),
	%Node = node(),
	
	Res = rpc:call('controller@n102250.merann.ru', dm_api, get_value, ["bacnet",[{ip,"192.168.102.24"},{net,"2"},{dev,"77002"},{obj,"0"},{id, "85"}, {idx, "0"}]]),
	ct:pal("Result ~p", [Res]),
	ok.
