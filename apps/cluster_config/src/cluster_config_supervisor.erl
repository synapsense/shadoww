
-module(cluster_config_supervisor).

-behaviour(supervisor).

-export([
	start_link/0,
	init/1
	]).

-define(SERVER, ?MODULE).

start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
	{ok, DefaultsFile} = application:get_env(defaults_file),
	{ok, OverridesFile} = application:get_env(overrides_file),

	RestartStrategy		= one_for_one,
	MaxRestarts		= 1,
	MaxTimeBetRestarts	= 60 * 60,

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpec = [{
		cluster_config,
		{cluster_config, start_link, [DefaultsFile, OverridesFile]},
		permanent,
		1000,
		worker,
		[cluster_config]
	}],

	{ok, {SupFlags, ChildSpec}}.

