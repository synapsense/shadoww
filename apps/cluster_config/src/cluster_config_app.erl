
-module(cluster_config_app).

-behaviour(application).

-export([start/2, stop/1, config_change/3]).

start(normal, _StartArgs) ->
	Sup = cluster_config_supervisor:start_link(),
	lager:info("cluster_config supervisor started: ~p", [Sup]),
	Sup
	.

stop(_State) -> ok .

config_change(Changed, New, Removed) ->
	lager:info("cluster_config config change. changed: ~p, new: ~p, removed: ~p", [Changed, New, Removed])
	.


