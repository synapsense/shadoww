
-module(cluster_config).

-behaviour(locks_leader).

%% API
-export([
	start_link/2,
	has_key/2,
	get_value/2,
	get_value/3,
	set_override/3,
	remove_override/2,
	list_overrides/0
	]).

%% locks_leader callbacks
-export([
	init/1,
	elected/3,
	surrendered/3,
	handle_DOWN/3,
	handle_leader_call/4,
	handle_leader_cast/3,
	from_leader/3,
	handle_cast/3,
	handle_call/4,
	handle_info/3,
	terminate/2,
	code_change/4
	]).

-define(SERVER, ?MODULE).
-define(STATE, ?MODULE).

-define(DEFAULTS_FILE, "cluster_config.default").
-define(CACHE_FILE, "cluster_config.dat").

-record(?STATE,
	{
	 am_leader = false,
	 defaults :: pyramid:pyramid(),
	 overrides :: pyramid:pyramid(),
	 overrides_file :: file:filename_all()
	}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(DefaultsFile, OverridesFile) ->
	Defaults = pyramid:from_list(source_list(DefaultsFile)),
	Overrides = case catch source_list(OverridesFile) of
			    {error, enoent} ->
				    pyramid:new();
			    {'EXIT', Error} ->
				    lager:error("Error reading overrides.  Resetting to defaults: ~p", [Error]),
				    P = pyramid:new(),
				    write_overrides(OverridesFile, P),
				    P;
			    OverrideList ->
				    pyramid:from_list(OverrideList)
		    end,
	locks_leader:start_link(?SERVER, ?MODULE, {Defaults, Overrides, OverridesFile}, []).

%% Test whether the key exists
-spec has_key(pyramid:path(), pyramid:key()) -> boolean().
has_key(Path, Key) ->
	case get_value(Path, Key) of
		error -> false;
		{ok, _} -> true
	end.

%% Get the value associated with the key, or error if it does not exist.
-spec get_value(pyramid:path(), pyramid:key()) -> {ok, term()} | error.
get_value(Path, Key) ->
	locks_leader:call(?SERVER, {get_value, {Path, Key}}).

%% Get the value associated with the key, returning Default if the key does not exist.
-spec get_value(pyramid:path(), pyramid:key(), term()) -> {ok, term()}.
get_value(Path, Key, Default) ->
	case get_value(Path, Key) of
		error -> {ok , Default};
		V -> V
	end.

set_override(Path, Key, Value) ->
	locks_leader:leader_call(?SERVER, {set_override, {Path, Key, Value}}).

remove_override(Path, Key) ->
	locks_leader:leader_call(?SERVER, {remove_override, {Path, Key}}).

list_overrides() ->
	locks_leader:call(?SERVER, list_overrides).

%%%===================================================================
%%% locks_leader callbacks
%%%===================================================================

init({Defaults, Overrides, OverridesFile}) ->
	{ok, #?STATE{defaults = Defaults,
		     overrides = Overrides,
		     overrides_file = OverridesFile
		    }}.

elected(S = #?STATE{overrides=Over}, LdrInfo, undefined) ->
	case locks_leader:new_candidates(LdrInfo) of
		[] ->
			lager:info("elected as leader of cluster"),
			{ok, {sync, Over}, S#?STATE{am_leader = true}};
		_L ->
			lager:info("elected as leader of merged cluster"),
			%% Don't try to merge anything, just use my config
			%% as global truth
			{ok, {sync, Over}, S#?STATE{am_leader = true}}
	end;
%% new candidate joined our cluster
elected(State = #?STATE{overrides=Over}, _LdrInfo, NewCandidate) when is_pid(NewCandidate) ->
	lager:info("new candidate joined my cluster"),
	{reply, {sync, Over}, State#?STATE{am_leader = true}}.

surrendered(State = #?STATE{am_leader=AmLeader, overrides_file=OverFile}, {sync, Over}, _LdrInfo) ->
	lager:info("surrendered. was_leader = ~p", [AmLeader]),
	write_overrides(OverFile, Over),
	{ok, State#?STATE{overrides=Over, am_leader = false}}.

handle_DOWN(Pid, State, _LdrInfo) ->
	lager:info("handle_down() from node ~p", [node(Pid)]),
	{ok, State}.

handle_leader_call({set_override, {Path, Key, Value}},
		   _From,
		   State = #?STATE{overrides=Over,
				   overrides_file=OverFile},
		   _El) ->
	NewOver = pyramid:insert(Path, Key, Value, Over),
	write_overrides(OverFile, NewOver),
	{reply, ok, {sync, NewOver}, State#?STATE{overrides=NewOver}};
handle_leader_call({remove_override, {Path, Key}},
		   _From,
		   State = #?STATE{overrides=Over,
				   overrides_file=OverFile},
		   _El) ->
	NewOver = pyramid:delete(Path, Key, Over),
	write_overrides(OverFile, NewOver),
	{reply, ok, {sync, NewOver}, State#?STATE{overrides=NewOver}};
handle_leader_call(_Request, _From, State, _Election) ->
	{reply, ok, State}.

handle_leader_cast(_Request, State, _Election) ->
	{ok, State}.

from_leader({sync, Over}, State = #?STATE{overrides_file=OverFile}, _LdrInfo) ->
	lager:info("from_leader(~p)", [Over]),
	write_overrides(OverFile, Over),
	{ok, State#?STATE{overrides=Over}};
from_leader(LdrMsg, State, _LdrInfo) ->
	lager:warning("unexpected from_leader(~p)", [LdrMsg]),
	{ok, State}.

handle_call({get_value, {Path, Key}},
	    _From,
	    State = #?STATE{defaults=Def,overrides=Over},
	    _E) ->
	V = case pyramid:search(Path, Key, Over) of
		    error ->
			    pyramid:search(Path, Key, Def);
		    {ok, _} = R ->
			    R
	    end,
	{reply, V, State};
handle_call(list_overrides, _From, State = #?STATE{overrides=Over}, _El) ->
	{reply, pyramid:to_list(Over), State};
handle_call(Request, _From, State, _Election) ->
	lager:warning("unexpected handle_call(~p)", [Request]),
	{reply, ok, State}.

handle_cast(Msg, State, _Election) ->
	lager:warning("unexpected handle_cast(~p)", [Msg]),
	{ok, State}.

handle_info(Info, State, _Election) ->
	lager:warning("unexpected handle_info(~p)", [Info]),
	{ok, State}.

terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Election, _Extra) ->
	{ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================

% FileName is a file containing a list of {Key, Value} terms
source_list(FileName) ->
	case file:read_file(FileName) of
		{ok, BinData} ->
			from_binary(BinData);
		Error -> Error
	end.

write_overrides(File, Overrides) ->
	ok = file:write_file(File, to_binary(pyramid:to_list(Overrides))).

to_binary(Settings) -> list_to_binary(io_lib:format("~p.", [Settings])) .

%because file:consult can't handle expressions like "60 * 1000"
from_binary(String) ->
	{ok, Ts, _} = erl_scan:string(binary_to_list(String)),
	{ok, Exprs} = erl_parse:parse_exprs(Ts),
	{value, Proplist, _} = erl_eval:exprs(Exprs, []),
	Proplist.

