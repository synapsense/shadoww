# Cluster Config

Think app config but slightly more dynamic and changes are broadcast
across the cluster.

Config settings are saved to a local file in term format for easy
browsing and hand-editing if required.

File contains a version so that config keys can be added on
application upgrade.

File contains a timestamp of the last write so that the most
recent version of the DB can be determined across the cluster.

DB is initialized with a TS of 0, each write sets current time.

File can be saved and moved to port configuration settings to
a new instance ie for a hardware upgrade.

Uses locks_leader for synchronizing writes.

