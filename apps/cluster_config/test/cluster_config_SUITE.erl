
-module(cluster_config_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	[
		{timetrap, {minutes, 1}}
	].

init_per_suite(Config) ->
	{ok, Started} = application:ensure_all_started(cluster_config),
	[{started, Started} | Config].

end_per_suite(Config) ->
	[application:stop(App) || App <- lists:reverse(?config(started, Config))],
	ok.

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(_TestCase, _Config) ->
	ok.

groups() ->
	[
	].

all() ->
	[
	]
	.

%% test cases



