
-module(local_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	[{timetrap, {minutes, 5}}].

init_per_suite(Config) ->
	{ok, Started} = application:ensure_all_started(dumbtable),
	[{started, Started} | Config].

end_per_suite(Config) ->
	[application:stop(App) || App <- lists:reverse(?config(started, Config))],
	ok.

init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(write_a_value_and_read_it_back, Config) ->
	ok = dumbtable:create_local_copy(dt_test),
	Config
	;
init_per_testcase(read_non_existent_key_returns_none, Config) ->
	ok = dumbtable:create_local_copy(dt_test),
	Config
	;
init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(write_a_value_and_read_it_back, _Config) ->
	ok = dumbtable:delete_local_copy(dt_test)
	;
end_per_testcase(read_non_existent_key_returns_none, _Config) ->
	ok = dumbtable:delete_local_copy(dt_test)
	;
end_per_testcase(_TestCase, _Config) ->
	ok.

groups() ->
	[
		{create_delete_tables, [sequence], [
				create_delete_table_with_atom_name_successfully,
				create_delete_table_with_string_name_successfully,
				create_delete_table_with_tuple_name_successfully,
				list_tables_returns_correct_table_list
			]},
		{read_write_tests, [sequence], [
				write_a_value_and_read_it_back,
				read_non_existent_key_returns_none,
				read_non_existent_table_returns_badarg,
				delete_non_existent_table_returns_no_table
			]},
		{all, [sequence], [
				{group, create_delete_tables},
				{group, read_write_tests}
			]}
	].

all() ->
	[
		{group, all}
	]
	.

%% test cases

create_delete_table_with_atom_name_successfully() -> [].
create_delete_table_with_atom_name_successfully(_Config) -> cd_table(table).

create_delete_table_with_string_name_successfully() -> [].
create_delete_table_with_string_name_successfully(_Config) -> cd_table("table").

create_delete_table_with_tuple_name_successfully() -> [].
create_delete_table_with_tuple_name_successfully(_Config) -> cd_table({test, "table"}).

list_tables_returns_correct_table_list() -> [].
list_tables_returns_correct_table_list(_Config) ->
	ok = dumbtable:create_local_copy(table1),
	ok = dumbtable:create_local_copy(table2),
	ok = dumbtable:create_local_copy(table3),
	ok = dumbtable:delete_local_copy(table1),
	Tables = dumbtable:list_tables(),
	?assert(lists:member(table2, Tables)),
	?assert(lists:member(table3, Tables)),
	?assertEqual(2, length(Tables)).

write_a_value_and_read_it_back() -> [].
write_a_value_and_read_it_back(_Config) ->
	Table = dt_test,
	dumbtable:put(Table, k, v),
	{value, v} = dumbtable:get(Table, k)
	.

read_non_existent_key_returns_none() -> [].
read_non_existent_key_returns_none(_Config) ->
	Table = dt_test,
	none = dumbtable:get(Table, bad_key)
	.

read_non_existent_table_returns_badarg() -> [].
read_non_existent_table_returns_badarg(_Config) ->
	Table = i_dont_exist,
	{'EXIT', {badarg, _Blah}} = (catch dumbtable:get(Table, k)),
	ok
	.

delete_non_existent_table_returns_no_table() -> [].
delete_non_existent_table_returns_no_table(_Config) ->
	Table = i_dont_exist,
	{no_table, Table} = dumbtable:delete_local_copy(Table)
	.

cd_table(Tname) ->
	ct:pal("tables: ~p", [dumbtable:list_tables()]),
	?assertEqual(0, length(dumbtable:list_tables())),
	ok = dumbtable:create_local_copy(Tname),
	dumbtable:put(Tname, Tname, Tname),
	?assertEqual({value, Tname}, dumbtable:get(Tname, Tname)),
	ok = dumbtable:delete_local_copy(Tname),
	?assertEqual(0, length(dumbtable:list_tables())).

