
-module(distributed_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	[
		{timetrap, {minutes, 5}}
	].

init_per_suite(Config) ->
	{ok, _} = net_kernel:start([test,longnames]),

	Node1 = start_slave(dumbtable_test_1),
	Node2 = start_slave(dumbtable_test_2),

	ok = rpc:call(Node1, application, start, [bear]),
	ok = rpc:call(Node2, application, start, [bear]),

	ok = rpc:call(Node1, application, start, [folsom]),
	ok = rpc:call(Node2, application, start, [folsom]),

	ok = rpc:call(Node1, application, start, [dumbtable]),
	ok = rpc:call(Node2, application, start, [dumbtable]),

	pong = rpc:call(Node1, net_adm, ping, [Node2]),

	[{node1, Node1}, {node2, Node2} | Config].

end_per_suite(Config) ->
	{node1, Node1} = proplists:lookup(node1, Config),
	{node2, Node2} = proplists:lookup(node2, Config),
	ok = slave:stop(Node1),
	ok = slave:stop(Node2),
	net_kernel:stop(),
	ok.

init_per_group(setup_node1, Config) ->
	{node1, Node1} = proplists:lookup(node1, Config),
	[{node, Node1} | Config]
	;
init_per_group(read_node1_data_from_node2, Config) ->
	{node2, Node2} = proplists:lookup(node2, Config),
	[{node, Node2} | Config]
	;
init_per_group(read_node2_data_from_node1, Config) ->
	{node1, Node1} = proplists:lookup(node1, Config),
	[{node, Node1} | Config]
	;
init_per_group(_G, Config) ->
	Config.

end_per_group(_G, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(_TestCase, _Config) ->
	ok.

groups() ->
	[
		{all, [sequence], [
				{group, sync_tests},
				{group, async_tests}
			]},
		{sync_tests, [sequence], [
				print_mem_usage,
				{group, setup_node1},
				{group, read_node1_data_from_node2},
				{group, read_node2_data_from_node1},
				{group, write_tons_of_random_data},
				node1_and_node2_keys_match,
				print_mem_usage,
				delete_local_copies
			]},
		{async_tests, [sequence], [
				print_mem_usage,
				{group, setup_node1},
				{group, read_node1_data_from_node2},
				{group, read_node2_data_from_node1},
				{group, async_write_tons_of_random_data},
				node1_and_node2_keys_match,
				print_mem_usage,
				delete_local_copies
			]},
		{setup_node1, [sequence], [
				create_local_copy,
				put_key1_value1,
				get_key1_returns_value1
			]},
		{read_node1_data_from_node2, [sequence], [
				lget_fails_with_no_local_copy,
				get_key1_returns_value1,
				create_local_copy,
				put_key1_value2
			]},
		{read_node2_data_from_node1, [sequence], [
				get_key1_returns_value2
			]},
		{write_tons_of_random_data, [parallel], [
				write_tons_of_random_data_node1,
				write_tons_of_random_data_node2
			]},
		{async_write_tons_of_random_data, [parallel], [
				async_write_tons_of_random_data_node1,
				async_write_tons_of_random_data_node2
			]}
	].

all() ->
	[
		{group, all}
	].

%% test cases

create_local_copy() -> [].
create_local_copy(Config) ->
	Node = proplists:get_value(node, Config),
	ok = rpc:call(Node, dumbtable, create_local_copy, [dt_test])
	.

delete_local_copies(Config) ->
	Node1 = proplists:get_value(node1, Config),
	ok = rpc:call(Node1, dumbtable, delete_local_copy, [dt_test]),

	Node2 = proplists:get_value(node2, Config),
	ok = rpc:call(Node2, dumbtable, delete_local_copy, [dt_test])
	.

list_local_tables_returns_only_local() -> [].
list_local_tables_returns_only_local(_Config) ->
	ok
	.

create_table_copy_copies_data() -> [].
create_table_copy_copies_data(_Config) ->
	ok
	.

put_key1_value1() -> [].
put_key1_value1(Config) ->
	Node = proplists:get_value(node, Config),
	ok = rpc:call(Node, dumbtable, put, [dt_test, key1, value1])
	.

lget_fails_with_no_local_copy() -> [].
lget_fails_with_no_local_copy(Config) ->
	Node = proplists:get_value(node, Config),
	{badrpc, {'EXIT', {badarg, _Blah}}} = (catch rpc:call(Node, dumbtable, lget, [dt_test, key1])),
	ok
	.

put_key1_value2() -> [].
put_key1_value2(Config) ->
	Node = proplists:get_value(node, Config),
	ok = rpc:call(Node, dumbtable, put, [dt_test, key1, value2])
	.

get_key1_returns_value1() -> [].
get_key1_returns_value1(Config) ->
	Node = proplists:get_value(node, Config),
	{value, value1} = rpc:call(Node, dumbtable, get, [dt_test, key1])
	.

get_key1_returns_none() -> [].
get_key1_returns_none(Config) ->
	Node = proplists:get_value(node, Config),
	none = rpc:call(Node, dumbtable, get, [dt_test, key1])
	.

get_key1_returns_value2() -> [].
get_key1_returns_value2(Config) ->
	Node = proplists:get_value(node, Config),
	{value, value2} = rpc:call(Node, dumbtable, get, [dt_test, key1])
	.

write_tons_of_random_data_node1() -> [].
write_tons_of_random_data_node1(Config) ->
	Node = ?config(node1, Config),
	{Size, Keys} = rpc:call(Node, ?MODULE, do_massive_write, []),
	ct:pal("Wrote ~B bytes ~B times on node ~p", [Size, Keys, Node])
	.

async_write_tons_of_random_data_node1() -> [].
async_write_tons_of_random_data_node1(Config) ->
	Node = ?config(node1, Config),
	{Size, Keys} = rpc:call(Node, ?MODULE, do_massive_async_write, []),
	ct:pal("Wrote ~B bytes ~B times on node ~p", [Size, Keys, Node])
	.

write_tons_of_random_data_node2() -> [].
write_tons_of_random_data_node2(Config) ->
	Node = ?config(node2, Config),
	{Size, Keys} = rpc:call(Node, ?MODULE, do_massive_write, []),
	ct:pal("Wrote ~B bytes ~B times on node ~p", [Size, Keys, Node])
	.

async_write_tons_of_random_data_node2() -> [].
async_write_tons_of_random_data_node2(Config) ->
	Node = ?config(node2, Config),
	{Size, Keys} = rpc:call(Node, ?MODULE, do_massive_async_write, []),
	ct:pal("Wrote ~B bytes ~B times on node ~p", [Size, Keys, Node])
	.

node1_and_node2_keys_match() -> [].
node1_and_node2_keys_match(Config) ->
	Node1 = ?config(node1, Config),
	Node2 = ?config(node2, Config),
	ct:pal("Grabbing all keys from ~p and ~p", [Node1, Node2]),
	{Keys1, Keys2} = wait_equal_keys(3, Node1, Node2),
	ct:pal("Comparing ~p keys: ~p", [length(Keys1), Keys1]),
	true = lists:all(fun({A, B}) -> A == B end, lists:zip(Keys1, Keys2))
	.

wait_equal_keys(0, _, _) -> error(retry);
wait_equal_keys(N, Node1, Node2) ->
	Future1 = rpc:async_call(Node1, dumbtable, keys, [dt_test]),
	Future2 = rpc:async_call(Node2, dumbtable, keys, [dt_test]),
	Keys1 = rpc:yield(Future1),
	Keys2 = rpc:yield(Future2),
	case length(Keys1) == length(Keys2) of
		true -> {Keys1, Keys2};
		false ->
			ct:pal("keys unequal, trying again"),
			timer:sleep(1),
			wait_equal_keys(N-1, Node1, Node2)
	end
	.

print_mem_usage() -> [].
print_mem_usage(Config) ->
	Node1 = ?config(node1, Config),
	Node2 = ?config(node2, Config),
	Mem1 = rpc:call(Node1, erlang, memory, []),
	Mem2 = rpc:call(Node2, erlang, memory, []),
	ct:pal("Memory usage on ~p:~n~p~n", [Node1, Mem1]),
	ct:pal("Memory usage on ~p:~n~p~n", [Node2, Mem2]),
	ok
	.

%% utility functions

do_massive_async_write() ->
	Junk = random_numbers(round(math:pow(2, 16)), []),
	Processes = processes(),
	lists:foreach(fun(Pid) ->
			dumbtable:async_put(dt_test, Pid,
				{
					Pid,
					erlang:process_info(Pid, [current_function, dictionary, memory, status, registered_name]),
					Junk
				}
			)
		end,
		Processes
	),
	{size(term_to_binary(Junk)), length(Processes)}
	.

do_massive_write() ->
	Junk = random_numbers(round(math:pow(2, 16)), []),
	Processes = processes(),
	lists:foreach(fun(Pid) ->
			dumbtable:put(dt_test, Pid,
				{
					Pid,
					erlang:process_info(Pid, [current_function, dictionary, memory, status, registered_name]),
					Junk
				}
			)
		end,
		Processes
	),
	{size(term_to_binary(Junk)), length(Processes)}
	.

random_numbers(1, Acc) ->
	[rand:uniform() | Acc];
random_numbers(N, Acc) ->
	random_numbers(N-1, [rand:uniform() | Acc]).

start_slave(Name) ->
	Paths = [
			code:lib_dir(dumbtable, ebin),
			code:lib_dir(dumbtable, test),
			code:lib_dir(folsom, ebin),
			code:lib_dir(folsom, test),
			code:lib_dir(bear, ebin),
			code:lib_dir(bear, test)
		],

	SlavePath = "-pa " ++ string:join(Paths, " -pa "),

	[_NodeName, HostName] = string:tokens(atom_to_list(node()), "@"),

	ct:pal("emulator arguments: ~p", [init:get_arguments()]),
	ct:pal("node info: node(): ~p, localhost: ~p, longnames: ~p", [node(), net_adm:localhost(), net_kernel:longnames()]),
	ct:pal("Starting slave node from node ~p with host name \"~s\" with name '~p' and args '~s'", [node(), HostName, Name, SlavePath]),
	{ok, Node} = slave:start(HostName, Name, SlavePath),
	ct:pal("Slave node ~p started", [Node]),
	Node
	.

