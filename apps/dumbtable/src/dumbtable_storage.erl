
%% @author Garret Smith <gsmith@synapsense.com>
%% @copyright 2012 Garret Smith
%% @version 1.0.1
%% @doc
%% Dumbtable storage backend using gb_trees.
%% @end

-module(dumbtable_storage).

-behaviour(gen_server).

-compile({no_auto_import, [put/2]}).

-export([start_link/1, start_link/2]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([
		put/2,
		aput/2,
		get/2,
		get_all/2,
		remove/2,
		remove_all/2,
		has_key/2,
		keys/1,
		values/1,
		as_list/1,
		stop/1
	]).


-spec start_link(term()) -> ignore | {error, Reason :: term()} | {ok, pid()}.
start_link(TName) -> gen_server:start_link(?MODULE, TName, []).

-spec start_link(term(), copy) -> ignore | {error, Reason :: term()} | {ok, pid()}.
start_link(TName, copy) -> gen_server:start_link(?MODULE, {TName, copy}, []).

-spec put(pid(), [{term(), term()}]) -> ok.
put(Pid, Tuples) when is_list(Tuples) ->
	gen_server:call(Pid, {put, Tuples}).

-spec aput(pid(), [{term(), term()}]) -> ok.
aput(Pid, Tuples) when is_list(Tuples) ->
	gen_server:cast(Pid, {put, Tuples}).

-spec get(pid(), term()) -> none | {value, term()}.
get(Pid, Key) ->
	gen_server:call(Pid, {get, Key}).

-spec get_all(pid(), [term()]) -> [term()].
get_all(Pid, Keys) ->
	gen_server:call(Pid, {get_all, Keys}).

-spec remove(pid(), term()) -> ok.
remove(Pid, Key) ->
	gen_server:cast(Pid, {remove, Key}).

-spec remove_all(pid(), [term()]) -> ok.
remove_all(Pid, Keys) when is_list(Keys) ->
	gen_server:cast(Pid, {remove_all, Keys}).

-spec has_key(pid(), term()) -> boolean().
has_key(Pid, Key) ->
	gen_server:call(Pid, {has_key, Key}).

-spec keys(pid()) -> [term()].
keys(Pid) ->
	gen_server:call(Pid, keys).

-spec values(pid()) -> [term()].
values(Pid) ->
	gen_server:call(Pid, values).

-spec as_list(pid()) -> orddict:orddict().
as_list(Pid) ->
	gen_server:call(Pid, as_list).

-spec stop(pid()) -> normal.
stop(Pid) ->
	gen_server:call(Pid, stop).

%% behavior callbacks


init({TName, copy}) ->
	Data = case pg2:get_members(TName) of
		[] -> gb_trees:empty();
		[P | _] -> gb_trees:from_orddict(as_list(P))
	end,
	erlang:put(tname, TName), % don't want  to change state record.  ick
	folsom_metrics:new_meter({dumbtable_reads, TName}),
	folsom_metrics:new_meter({dumbtable_writes, TName}),
	pg2:join(TName, self()),
	{ok, Data};
init(TName) ->
	pg2:join(TName, self()),
	{ok, gb_trees:empty()}
	.


handle_call(as_list, _From, Data) ->
	inc_read(),
	{reply, gb_trees:to_list(Data), Data};
handle_call({get, Key}, _From, Data) ->
	inc_read(),
	{reply, gb_trees:lookup(Key, Data), Data};
handle_call({get_all, Keys}, _From, Data) ->
	inc_read(),
	{reply, [Value || {value, Value} <- [gb_trees:lookup(Key, Data) || Key <- Keys]], Data};
handle_call({put, Tuples}, _From, Data) ->
	inc_write(),
	{reply, ok, lists:foldl(fun({Key, Value}, T) -> gb_trees:enter(Key, Value, T) end, Data, Tuples)};
handle_call({has_key, Key}, _From, Data) ->
	inc_read(),
	{reply, gb_trees:is_defined(Key, Data), Data};
handle_call(keys, _From, Data) ->
	inc_read(),
	{reply, gb_trees:keys(Data), Data};
handle_call(values, _From, Data) ->
	inc_read(),
	{reply, gb_trees:values(Data), Data};
handle_call(stop, _From, Data) ->
	{stop, normal, normal, Data};
handle_call(_Message, _From, State) ->
	{reply, huh, State}
	.


handle_cast({put, Tuples}, Data) ->
	inc_write(),
	{noreply, lists:foldl(fun({Key, Value}, T) -> gb_trees:enter(Key, Value, T) end, Data, Tuples)};
handle_cast({remove, Key}, Data) ->
	inc_write(),
	{noreply, gb_trees:delete_any(Key, Data)};
handle_cast({remove_all, Keys}, Data) ->
	inc_write(),
	{noreply, lists:foldl(fun gb_trees:delete_any/2, Data, Keys)};
handle_cast(_Request, State) ->
	{noreply, State}
	.


handle_info(_Info, State) ->
	{noreply, State}
	.


terminate(normal, _State) ->
	pg2:leave(erlang:get(tname), self())
	.


code_change(_OldVsn, State, _Extra) ->
	{ok, State}
	.

inc_read() ->
	folsom_metrics:notify({{dumbtable_reads, erlang:get(tname)}, 1})
	.

inc_write() ->
	folsom_metrics:notify({{dumbtable_writes, erlang:get(tname)}, 1})
	.
