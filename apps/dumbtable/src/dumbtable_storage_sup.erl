
-module(dumbtable_storage_sup).

-behaviour(supervisor).

-export([start_link/0, init/1, start_storage/1, stop_storage/1]).

-define(SERVER, ?MODULE).


start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, [])
	.

init(_Args) ->
	RestartStrategy		= simple_one_for_one,	% Nothing should die here...
	MaxRestarts		= 5,		% number of restarts allowed in
	MaxTimeBetRestarts	= 3600,		% this number of seconds

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

	ChildSpecs = [{
		dumbtable_storage,
		{dumbtable_storage, start_link, []},
		transient,
		10000,
		worker,
		[dumbtable_storage]
	}],

	{ok, {SupFlags, ChildSpecs}}
	.

start_storage(TName) ->
	supervisor:start_child(?MODULE, [TName, copy])
	.

stop_storage(Pid) ->
	dumbtable_storage:stop(Pid).

