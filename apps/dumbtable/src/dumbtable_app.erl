
-module(dumbtable_app).

-behaviour(application).

-export([start/2, start_phase/3, prep_stop/1, stop/1, config_change/3]).

start(_Type, _StartArgs) ->
	{ok, Pid} = dumbtable_storage_sup:start_link(),
	case application:get_env(local_copies) of
		{ok, Tables} -> lists:foreach(fun(Table) -> dumbtable:create_local_copy(Table) end, Tables);
		undefined -> ok
	end,
	{ok, Pid}
	.

start_phase(_Phase, _StartType, _StartArgs) ->
	ok
	.

prep_stop(State) ->
	State
	.

stop(_State) ->
	ignored
	.

config_change(_Changed, _New, _Removed) ->
	ok
	.


