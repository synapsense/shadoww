
%% @author Garret Smith <gsmith@synapsense.com>
%% @copyright 2012 Garret Smith
%% @version 1.0.0
%% @doc
%% Public API for dumbtable.
%% @end

-module(dumbtable).

-export([
		list_tables/0,
		list_local_tables/0,
		table_info/0,
		table_info/1,
		list_copies/1,
		create_local_copy/1,
		delete_local_copy/1,
		get/2,
		get_all/2,
		lget/2,
		async_put/2,
		async_put/3,
		put/2,
		put/3,
		remove/2,
		remove_all/2,
		has_key/2,
		keys/1,
		values/1,
		as_list/1
	]).

-export([multi_apply/2, members_call/2]).

%% Public API

-spec list_tables() -> [term()].
%% @doc
%% Return a list of all distributed table names.
%% @end
list_tables() ->
	[Name || {dumbtable, Name} <- pg2:which_groups()]
	.

-spec list_local_tables() -> [term()].
%% @doc
%% Return a list of all distributed table names that have a local copy.
%% @end
list_local_tables() ->
	[Name || {dumbtable, Name} <- pg2:which_groups(), pg2:get_local_members({dumbtable, Name}) /= []]
	.

%% @doc
%% Return a few interesting data points about each local dumbtable storage processes running
%% @end
table_info() ->
	[{Name, table_info(Pid)}
		|| {Name, Pid} <- [{Name, hd(pg2:get_local_members({dumbtable, Name}))} || Name <- list_local_tables()]]
	.

%% @doc
%% Return a few interesting data points about the local dumbtable storage process
%% @end
table_info(Pid) when is_pid(Pid) ->
	process_info(Pid, [memory, message_queue_len, suspending, trace, reductions]);
table_info(TableName) ->
	table_info(hd(pg2:get_local_members({dumbtable, TableName})))
	.

-spec list_copies(term()) -> [node()].
%% @doc
%% Return a list of nodes containing a copy of the table.
%% @end
list_copies(Name) ->
	[node(Pid) || Pid <- pg2:get_members({dumbtable, Name})]
	.

local_copy_exists(Name) ->
	case pg2:get_local_members(Name) of
		[] -> false;
		[_] -> true;
		{error, {no_such_group, Name}} -> false
	end
	.

-spec create_local_copy(Name :: term()) -> ok | {error, already_started}.
%% @doc
%% Create a local copy of the replicated table 'Name'.
%%
%% The table will be initialized by copying all key/value pairs from another copy.
%% The copy is selected at random from all online copies.
%%
%% If no distributed copy is found, the table will be empty.
%%
%% Note: copying a large table can create a lot of network traffic.
%% @end
create_local_copy(Name) ->
	TName = {dumbtable, Name},
	case local_copy_exists(TName) of
		false ->
			pg2:create(TName),
			{ok, _Pid} = dumbtable_storage_sup:start_storage(TName),
			ok;
		true ->
			{error, already_started}
	end
	.

-spec delete_local_copy(Name :: term()) -> ok | {no_table, Name :: term()}.
%% @doc
%% Delete the local copy of a distributed table.
%% @end
delete_local_copy(Name) ->
	TName = {dumbtable, Name},
	R = case pg2:get_local_members(TName) of
		[] -> {no_table, Name};
		[Pid] -> normal = dumbtable_storage_sup:stop_storage(Pid), ok;
		{error, {no_such_group, TName}} -> {no_table, Name};
		Pids when is_list(Pids) ->
			lists:foreach(fun(P) -> normal = dumbtable_storage_sup:stop_storage(P) end, Pids),
			ok
	end,
	case pg2:get_members(TName) of
		[] -> pg2:delete(TName);
		_ -> ok
	end,
	R
	.

-spec get(Name :: term(), Key :: term()) -> none | {value, term()}.
%% @doc
%% Get the most recent value associated with 'Key' in table 'Name'.
%%
%% If a local copy of table 'Name' exists, retrieve the value from it.
%% If there is no local copy, retrieve the value from a online, remote copy.
%% @end
get(Name, Key) ->
	closest_call(Name, fun(Pid) -> dumbtable_storage:get(Pid, Key) end)
	.

-spec get_all(Name :: term(), Keys :: [Key :: term()]) -> [term()].
%% @doc
%% Get a list of the most recent values associated with each 'Key' in table 'Name'.
%% If a key is not present, no result will be included, so the length of the returned
%% list may be shorter than the length of the key list.
%%
%% If a local copy of table 'Name' exists, retrieve the value from it.
%% If there is no local copy, retrieve the values from a online, remote copy.
%% @end
get_all(Name, Keys) when is_list(Keys) ->
	closest_call(Name, fun(Pid) -> dumbtable_storage:get_all(Pid, Keys) end)
	.

-spec lget(Name :: term(), Key :: term()) -> none | {value, term()}.
%% @doc
%% Get the most recent value associated with 'Key' from local table 'Name'.
%%
%% If there is no local copy of table 'Name' fail with 'badarg'.
%% @end
lget(Name, Key) ->
	TName = {dumbtable, Name},
	case pg2:get_local_members(TName) of
		[Pid | _] ->
			dumbtable_storage:get(Pid, Key);
		[] ->
			error(badarg, Name);
		{error, {no_such_group, TName}} ->
			error(badarg, Name)
	end
	.

-spec async_put(Name :: term(), [{Key :: term(), Value :: term()}]) -> ok.
%% @doc
%% Insert or update multiple values simultaneously
%%
%% The function returns immediately, before any values are actually written.
%% Writes are cast'ed sequentially to each online copy of the table in a separate process.
%% The local copy is not necessarily first.
%% @end
async_put(Name, Tuples) when is_list(Tuples) ->
	members_call(Name,
		     fun(Pids) ->
				     {Local, Remote} = lists:partition(fun(I) -> node(I) =:= node() end, Pids),
				     multi_apply(Local, {dumbtable_storage, put, [Tuples]}),
				     spawn(?MODULE, multi_apply, [Remote, {dumbtable_storage, aput, [Tuples]}])
		     end),
	ok
	.

-spec async_put(Name :: term(), Key :: term(), Value :: term()) -> ok.
%% @doc
%% Insert or update the value associated with 'Key'.
%%
%% The function returns immediately, before any values are actually written.
%% Writes are cast'ed sequentially to each online copy of the table in a separate process.
%% The local copy is not necessarily first.
%% @end
async_put(Name, Key, Value) ->
	async_put(Name, [{Key, Value}])
	.

-spec put(Name :: term(), [{Key :: term(), Value :: term()}, ...]) -> ok.
%% @doc
%% Insert or update multiple values simultaneously
%%
%% When the function returns, all keys and values are updated in all online copies of the table.
%% @end
put(Name, Tuples) when is_list(Tuples) ->
	members_call(Name, fun(Pids) -> multi_apply(Pids, {dumbtable_storage, put, [Tuples]}) end),
	ok
	.

-spec put(Name :: term(), Key :: term(), Value :: term()) -> ok.
%% @doc
%% Insert or update the value associated with 'Key'.
%%
%% When the function returns, the value of the key is updated in all online copies of the table.
%% @end
put(Name, Key, Value) ->
	?MODULE:put(Name, [{Key, Value}])
	.

-spec remove(Name :: term(), Key :: any()) -> ok.
%% @doc
%% Remove the key 'Key' and associated value from the table.
%%
%% The remove operation may not be performed until some time after the function returns.
%% @end
remove(Name, Key) ->
	members_call(Name, fun(Pids) -> multi_apply(Pids, {dumbtable_storage, remove, [Key]}) end),
	ok
	.

-spec remove_all(Name :: term(), Keys :: [Key :: any()]) -> ok.
%% @doc
%% Remove all keys and associated values from the table.
%%
%% The remove operation may not be performed until some time after the function returns.
%% @end
remove_all(Name, Keys) ->
	members_call(Name, fun(Pids) -> multi_apply(Pids, {dumbtable_storage, remove_all, [Keys]}) end),
	ok
	.

-spec has_key(Name :: term(), Key :: any()) -> boolean().
%% @doc
%% Query to see if the Key exists in the table
%% @end
has_key(Name, Key) ->
	closest_call(Name, fun(Pid) -> dumbtable_storage:has_key(Pid, Key) end)
	.

-spec keys(Name :: term()) -> [term()].
%% @doc
%% List all keys in table 'Name'.
%%
%% If a local copy of the table exists, keys will be retrieved from there.
%% If no local copy exists, keys will be retrieved from an online remote copy.
%% @end
keys(Name) ->
	closest_call(Name, fun(Pid) -> dumbtable_storage:keys(Pid) end)
	.

-spec values(Name :: term()) -> [term()].
%% @doc
%% List all values in table 'Name'.
%%
%% If a local copy of the table exists, values will be retrieved from there.
%% If no local copy exists, values will be retrieved from an online remote copy.
%% @end
values(Name) ->
	closest_call(Name, fun(Pid) -> dumbtable_storage:values(Pid) end)
	.

-spec as_list(Name :: term()) -> [{term(), term()}].
%% @doc
%% Return all key/value pairs in the table.
%%
%% If a local copy of the table exists, data will be retrieved from there.
%% If no local copy exists, data will be retrieved from an online remote copy.
%% @end
as_list(Name) ->
	closest_call(Name, fun(Pid) -> dumbtable_storage:as_list(Pid) end)
	.


%% private utility functions

-spec multi_apply(Pids :: [pid()], MFA :: {module(), atom(), [term()]}) -> ok.
%% @private
multi_apply([], _) -> ok;
multi_apply([Pid | Pids], {M, F, A}) ->
	catch apply(M, F, [Pid | A]),
	multi_apply(Pids, {M, F, A})
	.

-spec members_call(Name :: term(), Fun :: fun(([pid()]) -> any())) -> any().
%% @private
members_call(Name, Fun) ->
	TName = {dumbtable, Name},
	case pg2:get_members(TName) of
		[] ->
			error(badarg, Name);
		Pids when is_list(Pids) ->
			Fun(Pids);
		{error, {no_such_group, TName}} ->
			error(badarg, Name)
	end
	.

-spec closest_call(Name :: term(), Fun :: fun((pid()) -> any())) -> any().
%% @private
closest_call(Name, Fun) ->
	TName = {dumbtable, Name},
	case pg2:get_closest_pid(TName) of
		Pid when is_pid(Pid) ->
			Fun(Pid);
		{error, {no_process, TName}} ->
			error(badarg, Name);
		{error, {no_such_group, TName}} ->
			error(badarg, Name)
	end
	.

