
-module(mbtcp_pipeline).


-behaviour(gen_server).


% api functions
-export([start_link/1]).
-export([read_coil/3, read_int16/3, read_uint16/3, read_int32/3, read_uint32/3]).
-export([write_register16/4, write_register32/4, write_coil/4]).
-export([is_connected/1, reconnect/1]).
-export([fc1/4, fc2/4, fc3/4, fc4/4, fc5/4, fc6/4, fc16/4]).
-export([get_error_string/1]).

% behaviour callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).


-include_lib("eunit/include/eunit.hrl").


-define(HIGHEST_SLAVEID, 247).
-define(MODBUS_TCP_PORT, 502).
-define(GW_MSG_TIMEOUT, 30 * 1000).
-define(RECONNECT_TIME, 10 * 1000).
-define(SEND_TIMEOUT, 500).

-type from() :: {pid(), term()}.

-record(statedata, {
		awaiting_replies :: [{integer(), from()}],
		pipeline_limit :: integer(),
		pending_sends :: [{from(), binary()}],
		socket :: port(),
		connect_error :: inet:posix(),
		address :: tuple(),
		txnid :: integer(),
		rx_timer :: reference() | none,
		rx_timeout :: integer(),
		inactivity_timeout :: integer(),
		inactivity_timer :: reference()
	}).


start_link(Address) when is_tuple(Address) ->
	PipelineLimit = int_config(Address, mbtcp_pipeline),
	RxTimeout = int_config(Address, mbtcp_rx_timeout),
	InactivityTimeout = int_config(Address, mbtcp_inactivity_timeout),
	gen_server:start_link(?MODULE, [Address, InactivityTimeout, PipelineLimit, RxTimeout], [])
	.

int_config(Address, Param) ->
	case cluster_config:get_value([mbgw, Address], Param) of
		{ok, Value} when is_integer(Value) -> Value;
		Err -> error({bad_config, Err})
	end.

read_coil(Pid, Device, Register) when 1 =< Register, Register =< 9999 ->
	case fc1(Pid, Device, Register - 1, 1) of
		{ok, <<_:7, Bit:1>>} -> Bit;
		{error, Reason} -> error(Reason)
	end
	;
read_coil(Pid, Device, Register) when 10001 =< Register, Register =< 19999 ->
	case fc2(Pid, Device, Register - 10001, 1) of
		{ok, <<_:7, Bit:1>>} -> Bit;
		{error, Reason} -> error(Reason)
	end
	.

write_coil(Pid, Device, Register, Value) when
		(1 =< Register) and (Register =< 9999)
		and ((Value == 0) or (Value == 1) or (Value == true) or (Value == false))
		->
	ToWrite = case Value of
		true  -> 16#FF00;
		false -> 16#0000;
		1 -> 16#FF00;
		0 -> 16#0000
	end,
	case fc5(Pid, Device, Register - 1, ToWrite) of
		{ok, _} -> Value;
		{error, Reason} -> error(Reason)
	end
	.

read_int16(Pid, Device, Register) when (Register >= 40001) and (Register =< 49999) ->
	case fc3(Pid, Device, Register - 40001, 1) of
		{ok, RespWord} ->
			<<Value:16/signed-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	;

read_int16(Pid, Device, Register) when (Register >= 30001) and (Register =< 39999) ->
	case fc4(Pid, Device, Register - 30001, 1) of
		{ok, RespWord} ->
			<<Value:16/signed-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	.


read_uint16(Pid, Device, Register) when (Register >= 40001) and (Register =< 49999) ->
	case fc3(Pid, Device, Register - 40001, 1) of
		{ok, RespWord} ->
			<<Value:16/unsigned-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	;

read_uint16(Pid, Device, Register) when (Register >= 30001) and (Register =< 39999) ->
	case fc4(Pid, Device, Register - 30001, 1) of
		{ok, RespWord} ->
			<<Value:16/unsigned-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	.


read_int32(Pid, Device, Register) when (Register >= 40001) and (Register =< 49999) ->
	case fc3(Pid, Device, Register - 40001, 2) of
		{ok, RespWord} ->
			<<Value:32/signed-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	;

read_int32(Pid, Device, Register) when (Register >= 30001) and (Register =< 39999) ->
	case fc4(Pid, Device, Register - 30001, 2) of
		{ok, RespWord} ->
			<<Value:32/signed-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	.


read_uint32(Pid, Device, Register) when (Register >= 40001) and (Register =< 49999) ->
	case fc3(Pid, Device, Register - 40001, 2) of
		{ok, RespWord} ->
			<<Value:32/unsigned-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	;

read_uint32(Pid, Device, Register) when (Register >= 30001) and (Register =< 39999) ->
	case fc4(Pid, Device, Register - 30001, 2) of
		{ok, RespWord} ->
			<<Value:32/unsigned-integer>> = RespWord,
			Value;
		{error, Reason} ->
			error(Reason)
	end
	.


write_register16(Pid, Device, Register, Value) when (Register >= 40001) and (Register =< 49999) and is_integer(Value) ->
	if
		Value < - 32768 -> error({register_overflow, Value});
		Value > 65535 -> error({register_overflow, Value});
		true -> ok
	end,
	case fc6(Pid, Device, Register - 40001, Value) of
		{ok, _Value} -> Value;
		{error, Reason} -> error(Reason)
	end
	.

write_register32(Pid, Device, Register, Value) when (Register >= 40001) and (Register =< 49999) and is_integer(Value) ->
	if

		Value < - 2147483648 -> error({register_overflow, Value});
		Value > 4294967295 -> error({register_overflow, Value});
		true -> ok
	end,
	<<H:16, L:16>> = <<Value:32>>,
	case fc16(Pid, Device, Register - 40001, [H, L]) of
		{ok, _Values} -> Value;
		{error, Reason} -> error(Reason)
	end
	.

%% Checks to see whether or not we are currently connected to the Modbus/TCP device
%% Also resets the inactivity timer, since if someone is asking whether or not I'm connected,
%% they care enough that I shouldn't jump off a ledge.
is_connected(Pid) ->
	try
		gen_server:call(Pid, is_connected)
	catch
		% mostly to catch timeouts.  If the driver is unresponsive to the query, it isn't really "connected" enough to handle more traffic.
		% TODO: log something about the I/O being really backed up?
		_:_ -> false
	end
	.

reconnect(Pid) ->
	Pid ! reconnect
	.

-spec fc1(Pid :: pid(), DeviceID :: 1..247, Offset :: 0..16#FFFF, Coils :: 1..2000) -> {'error', term()} | {'ok', binary()}.

fc1(Pid, Device, Offset, Coils) when
		1 =< Device, Device =< ?HIGHEST_SLAVEID,
		0 =< Offset, Offset =< 16#FFFF,
		1 =< Coils, Coils =< 2000 ->
	Function = 1,
	ErrorIndicator = Function + 16#80,
	ReadPacket = <<Device:8, Function:8, Offset:16, Coils:16>>,
	Return = case gw_message(Pid, ReadPacket) of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, RespBytes:8, RespData/binary>>} when RespBytes == size(RespData) ->
			{ok, RespData};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p, coil count: ~p", [ReadPacket, P, Device, Function, Offset, Coils]),
			{error, {bad_packet, ReadPacket, P}}
	end,
	Return
	.


-spec fc2(Pid :: pid(), DeviceID :: 1..247, Offset :: 0..16#FFFF, Coils :: 1..2000) -> {'error', term()} | {'ok', binary()}.

fc2(Pid, Device, Offset, Coils) when
		1 =< Device, Device =< ?HIGHEST_SLAVEID,
		0 =< Offset, Offset =< 16#FFFF,
		1 =< Coils, Coils =< 2000 ->
	Function = 2,
	ErrorIndicator = Function + 16#80,
	ReadPacket = <<Device:8, Function:8, Offset:16, Coils:16>>,
	Return = case gw_message(Pid, ReadPacket) of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, RespBytes:8, RespData/binary>>} when RespBytes == size(RespData) ->
			{ok, RespData};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p, coil count: ~p", [ReadPacket, P, Device, Function, Offset, Coils]),
			{error, {bad_packet, ReadPacket, P}}
	end,
	Return
	.


-spec fc3(Pid :: pid(), DeviceID :: 1..247, Offset :: 0..16#FFFF, WordCount :: 1..125) -> {'error', term()} | {'ok', binary()}.

fc3(Pid, Device, Offset, WordCount) when (Device >= 1) and (Device =< ?HIGHEST_SLAVEID) and (Offset >= 0) and (Offset =< 16#FFFF) and (WordCount >= 0) and (WordCount =< 125) ->
	Function = 3,
	ErrorIndicator = Function + 16#80,
	ReadPacket = <<Device:8, Function:8, Offset:16, WordCount:16>>,
	DataPacket = gw_message(Pid, ReadPacket),
	Return = case DataPacket of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, RespBytes:8, RespWords/binary>>} when RespBytes == size(RespWords) ->
			{ok, RespWords};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p, word count: ~p", [ReadPacket, DataPacket, Device, Function, Offset, WordCount]),
			{error, {bad_packet, ReadPacket, P}}
	end,
	Return
	.


fc4(Pid, Device, Offset, WordCount) when (Device >= 1) and (Device =< ?HIGHEST_SLAVEID) and (Offset >= 0) and (Offset =< 16#FFFF) and (WordCount >= 0) and (WordCount =< 125) ->
	Function = 4,
	ErrorIndicator = Function + 16#80,
	ReadPacket = <<Device:8, Function:8, Offset:16, WordCount:16>>,
	DataPacket = gw_message(Pid, ReadPacket),
	Return = case DataPacket of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, RespBytes:8, RespWords/binary>>} when RespBytes == size(RespWords) ->
			{ok, RespWords};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p, word count: ~p", [ReadPacket, DataPacket, Device, Function, Offset, WordCount]),
			{error, {bad_packet, ReadPacket, P}}
	end,
	Return
	.


fc5(Pid, Device, Offset, Value) when
		(Device >= 1) and (Device =< ?HIGHEST_SLAVEID)
		and (Offset >= 0) and (Offset =< 16#FFFF)
		and ((Value == 16#0000) or (Value == 16#FF00))
		->
	Function = 5,
	ErrorIndicator = Function + 16#80,
	WritePacket = <<Device:8, Function:8, Offset:16, Value:16>>,
	Return = case gw_message(Pid, WritePacket) of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, Offset:16, WValue:16>>} ->
			{ok, WValue};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p", [WritePacket, P, Device, Function, Offset]),
			{error, {bad_packet, WritePacket, P}}
	end,
	Return
	.


fc6(Pid, Device, Offset, Value) when (Device >= 1) and (Device =< ?HIGHEST_SLAVEID) and (Offset >= 0) and (Offset =< 16#FFFF) ->
	Function = 6,
	ErrorIndicator = Function + 16#80,
	WritePacket = <<Device:8, Function:8, Offset:16, Value:16>>,
	DataPacket = gw_message(Pid, WritePacket),
	Return = case DataPacket of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, Offset:16, WValue:16>>} ->
			{ok, WValue};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p", [WritePacket, DataPacket, Device, Function, Offset]),
			{error, {bad_packet, WritePacket, P}}
	end,
	Return
	.

fc16(Pid, Device, Offset, Values)
	when
		(Device >= 1)
		and
		(Device =< ?HIGHEST_SLAVEID)
		and
		(Offset >= 0)
		and
		(Offset =< 16#FFFF)
		and
		is_list(Values)
		and
		(length(Values) > 0)
		and
		(length(Values) =< 123) ->
	Function = 16,
	ErrorIndicator = Function + 16#80,
	NumRegisters = length(Values),
	BinWords = list_to_binary_words(Values),
	WritePacket = <<Device:8, Function:8, Offset:16, NumRegisters:16, (2 * NumRegisters):8, BinWords/binary>>,
	DataPacket = gw_message(Pid, WritePacket),
	Return = case DataPacket of
		{error, Reason} ->
			{error, Reason};
		{ok, <<Device:8, Function:8, Offset:16, NumRegisters:16>>} ->
			{ok, Values};
		{ok, <<Device:8, ErrorIndicator:8, ErrorCode:8>>} ->
			{error, {modbus_error, ErrorCode, get_error_string(ErrorCode)}};
		{ok, P} ->
			lager:warning("Unable to parse Modbus response. request: ~p, response: ~p, device: ~p, function: ~p, offset: ~p", [WritePacket, DataPacket, Device, Function, Offset]),
			{error, {bad_packet, WritePacket, P}}
	end,
	Return
	.

list_to_binary_words([]) ->
	<<>>
	;
list_to_binary_words([H | T]) ->
	<<H:16, (list_to_binary_words(T))/binary>>
	.

gw_message(Pid, Message) ->
	Begin = folsom_metrics:histogram_timed_begin({mbtcp_latency, gproc:get_value({p,l,address}, Pid)}),
	R = (catch gen_server:call(Pid, {gw_message, Message}, ?GW_MSG_TIMEOUT)),
	folsom_metrics:histogram_timed_notify(Begin),
	R
	.

% behaviour callbacks

init([Address, InactivityTimeout, PipelineLimit, RxTimeout]) ->
	State = #statedata{
		awaiting_replies = [],
		pipeline_limit = PipelineLimit,
		pending_sends = [],
		address = Address,
		txnid = 0,
		rx_timer = none,
		rx_timeout = RxTimeout,
		inactivity_timeout = InactivityTimeout,
		socket = undefined
	},
	lager:info("Starting Modbus/TCP connection to ~p with pipeline ~B, inactivity timeout ~B, receive timeout ~B", [Address, PipelineLimit, InactivityTimeout, RxTimeout]),

	folsom_metrics:new_meter({mbtcp_tx, Address}),
	folsom_metrics:tag_metric({mbtcp_tx, Address}, mbtcp),

	folsom_metrics:new_histogram({mbtcp_latency, Address}),
	folsom_metrics:tag_metric({mbtcp_latency, Address}, mbtcp),

	gproc:add_local_property(address, Address),
	gproc:add_local_name({mbtcp, Address}),
	put(address, Address),

	reconnect(self()),
	{ok, reset_timeout(State)}
	.

handle_call({gw_message, _Body}, _From, State = #statedata{socket = S, connect_error = Error})
		when S == undefined ->
	{reply, {error, Error}, reset_timeout(State)}
	;
handle_call({gw_message, Body}, From, State = #statedata{pending_sends = ToSend}) ->
	{noreply, reset_timeout(send_limit(State#statedata{pending_sends = push(ToSend, {From, Body})}))}
	;
handle_call(is_connected, _From, State = #statedata{socket = Socket}) ->
	{reply, is_port(Socket), reset_timeout(State)}
	;
handle_call(Request, From, State) ->
	lager:error("Unexpected handle_call. request: ~p, from: ~p, state: ~p", [Request, From, State]),
	{reply, huh, State}
	.

handle_cast(Request, State) ->
	lager:error("Unexpected handle_cast. request: ~p, state: ~p", [Request, State]),
	{noreply, State}
	.

handle_info(reconnect, State = #statedata{
				  socket = OldSocket,
				  address = Address,
				  inactivity_timeout = Timeout,
				  awaiting_replies = AR,
				  pending_sends = PS,
				  rx_timeout = RxTimeout}) ->
	close_socket(OldSocket),
	[send_noresponse(R) || R <- AR],
	[send_noresponse(R) || R <- PS],
	% Should probably send and error to everybody in pending_sends as well
	% since they could be waiting forever (and eventually time out) if the
	% reconnect doesn't succeed.
	lager:info(
	  "(Re)opening connection to: ~p, inactivity timeout: ~B, rx timeout: ~B",
	  [Address, Timeout, RxTimeout]),
	folsom_metrics:notify(mbtcp_connect, Address),
	NewState = case gen_tcp:connect(Address, ?MODBUS_TCP_PORT,
					[binary,
					 {packet, raw},
					 {active, once},
					 {nodelay, true},
					 {send_timeout, ?SEND_TIMEOUT},
					 {send_timeout_close, true}
					],
					5000) of
		{ok, NewSocket} ->
			send_limit(State#statedata{socket = NewSocket,
						   awaiting_replies = [],
						   pending_sends = [],
						   connect_error = undefined});
		{error, Reason} ->
			lager:warning("Modbus/TCP connection to ~p failed with ~p",
				      [Address, Reason]),
			erlang:send_after(?RECONNECT_TIME, self(), reconnect),
			State#statedata{socket = undefined,
					awaiting_replies = [],
					pending_sends = [],
					connect_error = Reason}
		end,
	{noreply, cancel_rx_timer(NewState)}
	;
handle_info({tcp, S, Data}, State = #statedata{socket = Socket, awaiting_replies = R})
		when S == Socket ->
	lager:debug("Received data packet: ~p", [Data]),
	Nr = send_replies(process_data(Data), R),
	NState = send_limit(State#statedata{awaiting_replies = Nr}),
	ok = inet:setopts(S, [{active, once}]),
	{noreply, check_update_cancel_rx_timer(NState)}
	;
handle_info({tcp_closed, S}, State = #statedata{socket = Socket})
		when S == Socket ->
	lager:warning("TCP socket closed, reconnecting..."),
	reconnect(self()),
	{noreply, State#statedata{socket = undefined, awaiting_replies = [], pending_sends = []}}
	;
handle_info({timeout, TRef, rx_timeout}, State = #statedata{rx_timer = RxTimer, rx_timeout = RxTimeout})
		when TRef == RxTimer ->
	lager:warning("Timeout (~B) waiting for response", [RxTimeout]),
	reconnect(self()),
	{noreply, State}
	;
handle_info(timeout, State = #statedata{socket = Socket, address = Address, inactivity_timeout = Timeout}) ->
	lager:warning("Modbus/TCP connection to ~p shutting down due to ~p seconds of inactivity", [Address, round(Timeout/1000)]),
	close_socket(Socket),
	{stop, normal, State}
	;
handle_info(Info, State) ->
	lager:error("unexpected handle_info. info: ~p, state: ~p", [Info, State]),
	{noreply, State}
	.


terminate(Reason, State = #statedata{socket = S}) ->
	lager:warning("terminating. reason: ~p, state: ~p", [Reason, State]),
	close_socket(S)
	.


code_change(_OldVsn, State, _Extra) ->
	{ok, State}
	.

% utility functions

% take a binary blob of data, split it into a list of packets
process_data(<<>>) -> [];
process_data(Data) ->
	case Data of
		<<Id:16, 0:16, 0:8, RespBytes:8, PayloadRemainder/binary>> ->
			<<Payload:RespBytes/binary, Remainder/binary>> = PayloadRemainder,
			[{Id, Payload} | process_data(Remainder)];
		_ ->
			reconnect(self()),
			[]
	end
	.

process_empty_packet_test() ->
	?assertEqual([], process_data(<<>>))
	.

process_data_single_packet_test() ->
	Id = 123, Data = 1234,
	Packet = <<Id:16, 0:16, 0:8, 2:8, Data:16>>,
	?assertEqual([{Id, <<Data:16/integer>>}], process_data(Packet))
	.

process_data_multi_packet_test() ->
	Packet1 = <<123:16, 0:16, 0:8, 2:8, 1234:16>>,
	Packet2 = <<124:16, 0:16, 0:8, 2:8, 4567:16>>,
	?assertEqual([{123, <<1234:16/integer>>}, {124, <<4567:16/integer>>}], process_data(<<Packet1/binary, Packet2/binary>>))
	.

process_data_bad_data_reconnect_test() ->
	[] = process_data(<<1:16, 2:16>>),
	receive
		reconnect -> ok
	after
		10 -> throw(no_reconnect)
	end
	.

-spec nextid(#statedata{}) -> {integer(), #statedata{}}.
nextid(State) ->
	Nextid = case State#statedata.txnid of
		65535 -> 0;
		X -> X+1
	end,
	NewState = State#statedata{txnid = Nextid},
	{Nextid, NewState}
	.

-spec get_error_string(integer()) -> string().
get_error_string(ErrorCode) ->
	ErrorString = case ErrorCode of
		1 -> "Function code not supported";
		2 -> "Bad register address";
		3 -> "Too many registers requested";
		4 -> "Register read failure";
		6 -> "Server busy";
		10 -> "No path available";
		11 -> "Target device failed to respond";
		X -> lists:flatten(io_lib:format("Error code ~p not understood", [X]))
	end,
	ErrorString
	.

send_replies([], Awaiting) -> Awaiting;
send_replies([{Id, BodyResp} | Replies], Awaiting) ->
	send_replies(Replies, do_reply(Id, BodyResp, Awaiting))
	.

%% Scan the Waiters list until we find a matching Id.
%% If a match is found, remove all waiters before it.
%% If no match is found, leave the waiters alone.
do_reply(Id, Data, [{Wid, Waiter} | Waiters]) when Id == Wid ->
	lager:debug("Replying with TXN ID ~B", [Id]),
	gen_server:reply(Waiter, {ok, Data}),
	Waiters
	;
do_reply(Id, Data, Waiters) ->
	case lists:splitwith(fun({Wid, _Waiter}) -> Wid /= Id end, Waiters) of
		{L, []} ->
			lager:warning("No waiting process for reply TXN ID ~B in waitlist ~p", [Id, Waiters]),
			reconnect(self()),
			L;
		{H, [{Id, Waiter} | L]} ->
			lager:warning("Purging head of wait list ~p for TXN ID ~B", [H, Id]),
			[send_noresponse(LostWaiter) || LostWaiter <- H],
			gen_server:reply(Waiter, {ok, Data}),
			L
	end
	.

send_noresponse({From, Packet}) when is_tuple(From) ->
	lager:warning("Pending send discarded: ~p", [Packet]),
	folsom_metrics:notify(mbtcp_lost, gproc:get_value({p,l,address})),
	gen_server:reply(From, {error, no_response})
	;
send_noresponse({Id, Waiter}) when is_tuple(Waiter) ->
	lager:warning("Response lost for TXN ID ~B", [Id]),
	folsom_metrics:notify(mbtcp_lost, gproc:get_value({p,l,address})),
	gen_server:reply(Waiter, {error, no_response})
	.

do_send(Body, From, State = #statedata{socket = Socket, awaiting_replies = R, address = A}) ->
	{Nextid, NewState} = nextid(State),
	PktSz = size(Body),
	Header = <<Nextid:16, 0:16, 0:8, PktSz:8>>,
	Packet = <<Header/binary, Body/binary>>,

	lager:debug("Sending packet with TXN ID ~B : ~p", [Nextid, Packet]),
	ok = gen_tcp:send(Socket, Packet),
	folsom_metrics:notify({mbtcp_tx, A}, 1),
	check_start_rx_timer(NewState#statedata{awaiting_replies = push(R, {Nextid, From})})
	.

send_limit(S = #statedata{awaiting_replies = R, pending_sends = P, pipeline_limit = L})
		when (length(P) > 0) andalso ((L == 0) orelse (length(R) < L)) ->
	[{From, Body} | Np] = P,
	send_limit(do_send(Body, From, S#statedata{pending_sends = Np}))
	;
send_limit(S) -> S .

% push an item onto the tail of the list, so that it can be popped with an [X|Xs] pattern match
% Could there be a more efficient way to do this?
% 'queue' module for FIFOs.  But that may not be more efficient for small queues...
push(L, I) -> lists:reverse(lists:reverse(L), [I]) .

reset_timeout(State = #statedata{inactivity_timer = ITimer, inactivity_timeout = Timeout}) ->
	catch erlang:cancel_timer(ITimer),
	State#statedata{inactivity_timer = erlang:send_after(Timeout, self(), timeout)}
	.

close_socket(undefined) -> ok;
close_socket(S) -> catch gen_tcp:close(S).

cancel_rx_timer(State = #statedata{rx_timer = RxTimer}) ->
	lager:debug("Canceling RX timer"),
	catch erlang:cancel_timer(RxTimer),
	State#statedata{rx_timer = none}
	.

restart_rx_timer(State = #statedata{rx_timeout = RxTimeout, rx_timer = RxTimer})
		when RxTimer =:= none ->
	lager:debug("Starting RX timer"),
	State#statedata{rx_timer = erlang:start_timer(RxTimeout, self(), rx_timeout)}
	;
restart_rx_timer(State = #statedata{rx_timeout = RxTimeout, rx_timer = RxTimer}) ->
	lager:debug("Re-starting RX timer"),
	catch erlang:cancel_timer(RxTimer),
	State#statedata{rx_timer = erlang:start_timer(RxTimeout, self(), rx_timeout)}
	.

check_start_rx_timer(State = #statedata{rx_timer = RxTimer, awaiting_replies = AR})
		when RxTimer =:= none, length(AR) > 0 ->
	restart_rx_timer(State)
	;
check_start_rx_timer(State) ->
	State
	.

check_update_cancel_rx_timer(State = #statedata{rx_timer = RxTimer, awaiting_replies = AR})
		when RxTimer =/= none, length(AR) =:= 0 ->
	cancel_rx_timer(State)
	;
check_update_cancel_rx_timer(State = #statedata{rx_timer = RxTimer, awaiting_replies = AR})
		when RxTimer =/= none, length(AR) > 0 ->
	restart_rx_timer(State)
	;
check_update_cancel_rx_timer(State) ->
	State
	.

