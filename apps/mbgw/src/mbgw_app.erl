
-module(mbgw_app).

-behaviour(application).

-export([start/2, stop/1, config_change/3]).

start(normal, StartArgs) ->
	lager:info("mbgw app starting. start args: ~p", [StartArgs]),
	mbgw:start_link()
	.

stop(State) ->
	lager:info("mbgw app stopping. state: ~p", [State])
	.

config_change(Changed, New, Removed) ->
	lager:info("mbgw config change. changed: ~p, new: ~p, removed: ~p", [Changed, New, Removed]),
	ok
	.

