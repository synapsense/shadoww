
-module(mbgw).


-behaviour(supervisor).


-export([start_link/0, init/1, connect/1, wait_connect/1, lookup_driver/1]).

-export([read_coil/3, read_int16/3, read_uint16/3, read_int32/3, read_uint32/3, write_coil/4, write_register16/4, write_register32/4]).


-define(SERVER, ?MODULE).
-define(PROTOMOD, mbtcp_pipeline).

% behavior interface

start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, [])
	.

init(_Args) ->
	RestartStrategy		= one_for_one,
	MaxRestarts		= 2,
	MaxTimeBetRestarts	= 3600,

	SupFlags	= {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},
	ChildSpecs 	= [],

	folsom_metrics:new_history(mbtcp_connect),
	folsom_metrics:tag_metric(mbtcp_connect, mbtcp),

	folsom_metrics:new_history(mbtcp_lost),
	folsom_metrics:tag_metric(mbtcp_lost, mbtcp),

	{ok, {SupFlags, ChildSpecs}}
	.


% public API

-spec connect(string() | tuple()) -> {'ok', 'connected'} | {'error', any()}.
connect(Address) when is_list(Address) ->
	connect(dotted_to_tuple(Address))
	;

connect(Address) when is_tuple(Address) ->
	ChildSpec = {
		Address,
		{?PROTOMOD, start_link, [Address]},
		temporary,
		1000,
		worker,
		[?PROTOMOD]
	},
	case supervisor:start_child(?SERVER, ChildSpec) of
		{ok, _Pid} ->
			{ok, connected};
		{error, already_present} ->
			case supervisor:delete_child(?SERVER, Address) of
				{error, running} -> {error, already_connected};
				{error, not_found} -> connect(Address);
				ok -> connect(Address)
			end;
		{error, {already_started, _Pid}} ->
			{error, already_connected};
		{error, Reason} ->
			lager:error("Unable to start Modbus/TCP connection. address: ~p, error: ~p", [Address, Reason]),
			{error, Reason}
	end
	.

wait_connect(Ip) -> wait_connect(Ip, 3) .

-spec wait_connect(string(), non_neg_integer()) -> pid().
wait_connect(Ip, 0) ->
	error({connect_fail, Ip})
	;
wait_connect(Ip, Attempts) when Attempts > 0 ->
	case lookup_driver(Ip) of
		{ok, GwPid} -> GwPid ;
		_Err ->
			connect(Ip),
			timer:sleep(2000),
			wait_connect(Ip, Attempts - 1)
	end
	.

-spec read_coil(string(), non_neg_integer(), non_neg_integer()) -> integer() .
read_coil(Gwid, Device, Register) -> ?PROTOMOD:read_coil(wait_connect(Gwid), Device, Register) .

-spec read_uint16(string(), non_neg_integer(), non_neg_integer()) -> integer() .
read_uint16(Gwid, Device, Register) -> ?PROTOMOD:read_uint16(wait_connect(Gwid), Device, Register) .

-spec read_int16(string(), non_neg_integer(), non_neg_integer()) -> integer() .
read_int16(Gwid, Device, Register) -> ?PROTOMOD:read_int16(wait_connect(Gwid), Device, Register) .

-spec read_uint32(string(), non_neg_integer(), non_neg_integer()) -> integer() .
read_uint32(Gwid, Device, Register) ->?PROTOMOD:read_uint32(wait_connect(Gwid), Device, Register) .

-spec read_int32(string(), non_neg_integer(), non_neg_integer()) -> integer() .
read_int32(Gwid, Device, Register) -> ?PROTOMOD:read_int32(wait_connect(Gwid), Device, Register) .

-spec write_coil(string(), non_neg_integer(), non_neg_integer(), 0..1 | boolean()) -> integer() .
write_coil(Gwid, Device, Register, Value) -> ?PROTOMOD:write_coil(wait_connect(Gwid), Device, Register, Value) .

-spec write_register16(string(), non_neg_integer(), non_neg_integer(), integer()) -> integer() .
write_register16(Gwid, Device, Register, Value) -> ?PROTOMOD:write_register16(wait_connect(Gwid), Device, Register, Value) .

-spec write_register32(string(), non_neg_integer(), non_neg_integer(), integer()) -> integer() .
write_register32(Gwid, Device, Register, Value) -> ?PROTOMOD:write_register32(wait_connect(Gwid), Device, Register, Value) .


% what about a race condition here?  Couldn't the child have changed state (or Pid) between the time we read the state (or Pid) and the caller uses it?
-spec lookup_driver(string() | tuple()) -> {'ok', pid()} | {'error', 'gw_not_configured'}.
lookup_driver(Address) when is_list(Address) ->
	lookup_driver(dotted_to_tuple(Address))
	;
lookup_driver(Address) when is_tuple(Address) ->
	Children = supervisor:which_children(?SERVER),
	case lists:keyfind(Address, 1, Children) of
		{Address, undefined, _Type, _Modules} -> {error, gw_not_running};
		{Address, Pid, _Type, _Modules} -> {ok, Pid};
		false -> {error, gw_not_configured}
	end
	.

dotted_to_tuple(Ip) ->
	list_to_tuple(lists:map(fun(E) -> {Int, []} = string:to_integer(E), Int end, string:tokens(Ip, ".")))
	.

