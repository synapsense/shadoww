
-module(mbgw_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile(export_all).


%% common test callbacks
suite() ->
	TestCfg = filename:join([code:lib_dir(mbgw), "test", "mbgw_test.config"]),
	ct:add_config(ct_config_plain, TestCfg),
	[
		{timetrap, {minutes, 1}},
		{require, mbtcp_server_ip},
		{require, device_id},
		{require, coil_device_id}
	].

init_per_suite(Config) ->
	Defaults = filename:join([code:lib_dir(mbgw), "test", "cluster_conf.def"]),
	Over = filename:join([code:lib_dir(mbgw), "test", "cluster_conf.over"]),
	application:load(cluster_config),
	application:set_env(cluster_config, defaults_file, Defaults),
	application:set_env(cluster_config, overrides_file, Over),
	{ok, Started} = application:ensure_all_started(mbgw),
	[{started, Started} | Config].

end_per_suite(Config) ->
	TestCfg = filename:join([code:lib_dir(mbgw), "test", "mbgw_test.config"]),
	ct:remove_config(ct_config_plain, TestCfg),
	[application:stop(App) || App <- lists:reverse(?config(started, Config))].

init_per_group(_GroupName, Config) ->
	Config.

end_per_group(_GroupName, _Config) ->
	ok.

init_per_testcase(_TestCase, Config) ->
	Config.

end_per_testcase(_TestCase, _Config) ->
	ok.

groups() ->
	[].

all() ->
	[int16, uint16, int32, uint32, read_write_coil].

%% test cases

int16() ->
	[].

int16(_Config) ->
	Ip = ct:get_config(mbtcp_server_ip),
	Device = ct:get_config(device_id),

	Start = 40001,

	rand:seed(exsplus, os:timestamp()),

	{W1, W2, W3} = {rand:uniform(32768), -rand:uniform(32768), rand:uniform(65536)},

	ct:pal("int16: W1: ~b, W2: ~b, W3: ~b", [W1, W2, W3]),
	ct:pal("writing to Device ~p at ~p", [Device, Ip]),

	mbgw:write_register16(Ip, Device, Start + 0, W1),
	mbgw:write_register16(Ip, Device, Start + 1, W2),
	mbgw:write_register16(Ip, Device, Start + 2, W3),

	R1 = mbgw:read_int16(Ip, Device, Start + 0),
	R2 = mbgw:read_int16(Ip, Device, Start + 1),
	R3 = mbgw:read_int16(Ip, Device, Start + 2),

	?assertEqual(W1, R1),
	?assertEqual(W2, R2),
	?assertEqual(unsigned_to_signed16(W3), R3)
	.

uint16() ->
	[].

uint16(_Config) ->
	Ip = ct:get_config(mbtcp_server_ip),
	Device = ct:get_config(device_id),

	Start = 40005,

	rand:seed(exsplus, os:timestamp()),

	{W1, W2, W3} = {rand:uniform(32768), -rand:uniform(32768), rand:uniform(65536)},

	ct:pal("uint16: W1: ~b, W2: ~b, W3: ~b", [W1, W2, W3]),

	mbgw:write_register16(Ip, Device, Start + 0, W1),
	mbgw:write_register16(Ip, Device, Start + 1, W2),
	mbgw:write_register16(Ip, Device, Start + 2, W3),

	R1 = mbgw:read_uint16(Ip, Device, Start + 0),
	R2 = mbgw:read_uint16(Ip, Device, Start + 1),
	R3 = mbgw:read_uint16(Ip, Device, Start + 2),

	?assertEqual(signed_to_unsigned16(W1), R1),
	?assertEqual(signed_to_unsigned16(W2), R2),
	?assertEqual(signed_to_unsigned16(W3), R3)
	.


int32() ->
	[].

int32(_Config) ->
	Ip = ct:get_config(mbtcp_server_ip),
	Device = ct:get_config(device_id),

	Start = 40010,

	rand:seed(exsplus, os:timestamp()),

	{W1, W2, W3} = {rand:uniform(2147483648), -rand:uniform(2147483648), rand:uniform(4294967296)},

	ct:pal("int32: W1: ~b, W2: ~b, W3: ~b", [W1, W2, W3]),

	mbgw:write_register32(Ip, Device, Start + 0, W1),
	mbgw:write_register32(Ip, Device, Start + 2, W2),
	mbgw:write_register32(Ip, Device, Start + 4, W3),

	R1 = mbgw:read_int32(Ip, Device, Start + 0),
	R2 = mbgw:read_int32(Ip, Device, Start + 2),
	R3 = mbgw:read_int32(Ip, Device, Start + 4),

	?assertEqual(W1, R1),
	?assertEqual(W2, R2),
	?assertEqual(unsigned_to_signed32(W3), R3)
	.

uint32() ->
	[].

uint32(_Config) ->
	Ip = ct:get_config(mbtcp_server_ip),
	Device = ct:get_config(device_id),

	Start = 40020,

	rand:seed(exsplus, os:timestamp()),

	{W1, W2, W3} = {rand:uniform(2147483648), -rand:uniform(2147483648), rand:uniform(4294967296)},

	ct:pal("uint32: W1: ~b, W2: ~b, W3: ~b", [W1, W2, W3]),

	mbgw:write_register32(Ip, Device, Start + 0, W1),
	mbgw:write_register32(Ip, Device, Start + 2, W2),
	mbgw:write_register32(Ip, Device, Start + 4, W3),

	R1 = mbgw:read_uint32(Ip, Device, Start + 0),
	R2 = mbgw:read_uint32(Ip, Device, Start + 2),
	R3 = mbgw:read_uint32(Ip, Device, Start + 4),

	?assertEqual(signed_to_unsigned32(W1), R1),
	?assertEqual(signed_to_unsigned32(W2), R2),
	?assertEqual(signed_to_unsigned32(W3), R3)
	.

read_write_coil() ->
	[].

read_write_coil(_Config) ->
	Ip = ct:get_config(mbtcp_server_ip),
	Device = ct:get_config(coil_device_id),

	Register = 1,

	mbgw:write_coil(Ip, Device, Register, 0),
	?assertEqual(0, mbgw:read_coil(Ip, Device, Register)),

	mbgw:write_coil(Ip, Device, Register, 1),
	?assertEqual(1, mbgw:read_coil(Ip, Device, Register)),

	mbgw:write_coil(Ip, Device, Register, 0),
	?assertEqual(0, mbgw:read_coil(Ip, Device, Register))
	.

signed_to_unsigned16(S) when S >= 0 -> S;
signed_to_unsigned16(S) -> 65536 + S.

signed_to_unsigned32(S) when S >= 0 -> S;
signed_to_unsigned32(S) -> 4294967296 + S.

unsigned_to_signed16(S) when S >= 32768 -> S - 65536 ;
unsigned_to_signed16(S) -> S.

unsigned_to_signed32(S) when S >= 2147483648 -> S - 4294967296 ;
unsigned_to_signed32(S) -> S.


