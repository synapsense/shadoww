-module(esapi).


-export([
	 to/1,
	 null_to/0,
	 to_id/1,
	 object_id/1,
	 value_to/2,
	 alert_type/5,
	 alert_type/7,
	 alert_type_name/1
	]).

-export([event_type_filter/1]).

-export([
	 create_object/1,
	 create_object/2,
	 get_object_types/0,
	 delete_object/1,
	 exists/1,
	 get_all_properties_values/1,
	 get_children/1,
	 get_children/2,
	 get_parents/1,
	 get_parents/2,
	 get_properties_values/2,
	 get_property_value/2,
	 remove_relation/2,
	 set_all_properties_values/2,
	 set_property_value/3,
	 set_relation/2,
	 register_processor/2,
	 deregister_processor/1,
	 get_objects_by_type/1,
	 log_activity/4,
	 raise_alert/4,
	 raise_alert/5,
	 create_alert_type/1,
	 get_alert_type/1,
	 proplist_to_valuetos/1,
	 node_name/0,
	 connected/0
	]).

-export([
	 object_type/1,
	 object_instance/1
	]).


-export_type([
	      to/0,
	      proplist/0,
	      simple_object/0
	     ]).


-define(TIMEOUT, 60 * 1000).  % 60 second timeout

-define(IS_TO_LIST(L), is_list(L), is_record(hd(L), to)).

%% @type to() = #to{id = binary()}.
%% Mirrors the 'com.synapsense.service.impl.dao.to.GenericObjectTO' Java class
-record(to, {
	  id :: binary() | null
	 }).
-type to() :: #to{}.

-type proplist() :: [proplists:property(binary(), any())].
-type simple_object() :: {to(), proplist()}.


%% @type value_to() = #value_to{property_name = binary(),
%%                              value = binary() | integer() | float() | to(),
%%                              timestamp = integer()}.
%% Mirrors the 'com.synapsense.dto.ValueTO' Java class
-record(value_to, {
	  property_name :: binary(),
	  value :: term(),
	  timestamp :: integer()
	 }).
-type value_to() :: #value_to{}.

%% @type collection_to() = #collection_to{object_id = to(), values = [value_to]}.
%% Mirrors the 'com.synapsense.dto.CollectionTO' Java class
-record(collection_to, {
	  object_id :: to(),
	  values :: [value_to()]
	 }).
-type collection_to() :: #collection_to{}.


-type event_type() :: configuration_started_event | configuration_complete_event .

-record(event_type_filter, {event_types = [] :: [event_type()]}).
-type event_type_filter() :: #event_type_filter{}.

-spec event_type_filter([event_type()]) -> event_type_filter().
event_type_filter(Types) -> #event_type_filter{event_types = Types} .


%% @type alert_type() = #alert_type{name = binary(),
%% 				display_name = binary(),
%% 				description = binary(),
%% 				priority = binary(),
%% 				header = binary(),
%% 				body = binary(),
%%				auto_dismiss = boolean()}.
%% Mirrors the 'com.synapsense.dto.AlertType' Java class
-record(alert_type, {
	  name :: binary(),
	  display_name :: binary(),
	  description :: binary(),
	  priority :: binary(),
	  header :: binary() | undefined,
	  body :: binary() | undefined,
	  auto_dismiss :: boolean()
	 }).
-type alert_type() :: #alert_type{}.


-spec to(Bin :: binary()) -> To :: to() .
to(To = #to{}) -> To;
to(Id) when is_binary(Id) -> #to{id = Id}.

-spec null_to() -> to().
null_to() -> #to{id=null}.

-spec to_id(To :: to()) -> Bin :: binary().
to_id(#to{id=Id}) -> Id.

-spec object_id(Object :: simple_object()) -> Id :: to() .
object_id({Id = #to{}, Pl}) when is_list(Pl) -> Id.

-spec object_type(to()) -> binary().
object_type(#to{id = Id}) when is_binary(Id) ->
	[BType,_] = binary:split(Id, <<":">>),
	BType.

-spec object_instance(to()) -> integer().
object_instance(#to{id=Id}) when is_binary(Id) ->
	[_,BInstance] = binary:split(Id, <<":">>),
	list_to_integer(binary_to_list(BInstance)).

-spec alert_type(binary(), binary(), binary(), binary(), binary(), binary(), atom()) -> alert_type().
alert_type(Name, DisplayName, Description, Priority, Header, Body, AutoDismiss) ->
	#alert_type{
	   name = Name,
	   display_name = DisplayName,
	   description = Description,
	   priority = Priority,
	   header = Header,
	   body = Body,
	   auto_dismiss = AutoDismiss
	  }.

-spec alert_type(Name :: binary(), DisplayName :: binary(), Description :: binary(), Priority :: binary(), AutoDismiss :: boolean()) -> alert_type().
alert_type(Name, DisplayName, Description, Priority, AutoDismiss) ->
	#alert_type{
	   name = Name,
	   display_name = DisplayName,
	   description = Description,
	   priority = Priority,
	   auto_dismiss = AutoDismiss
	  }.

% public API

%% @spec log_activity(User, Module, Action, Description) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       User = binary()
%%       Module = binary()
%%       Action = binary()
%%       Description = binary()
%% @doc Maps to:
%% ```void ActivityLogService.log(String userName, ActivityLogService.ActivityLogModule module, ActivityLogService.ActivityAction action, String description)'''
-spec log_activity(User :: binary(), Module :: binary(), Action :: binary(), Description :: binary()) -> ok.
log_activity(User, Module, Action, Description) ->
	maybe_convert(call_es({log_activity, User, Module, Action, Description}), fun passthrough/1).

%% @spec raise_alert(Name, AlertType, AlertTime, Message, HostTo) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       Name = binary()
%%       AlertType = binary()
%%       AlertTime = integer()
%%       Message = binary()
%%       HostTo = to() | []
%% @doc Maps to:
%% ```AlertService.raise(new Alert(String name, String alertType, Date alertTime, String message, TO<?> ruleTO, TO<?> hostTO))'''
-spec raise_alert(Name :: binary(), AlertType :: binary(), AlertTime :: integer(), Message :: binary()) -> ok.
raise_alert(Name, AlertType, AlertTime, Message) ->
	raise_alert(Name, AlertType, AlertTime, Message, null_to()).
-spec raise_alert(Name :: binary(), AlertType :: binary(), AlertTime :: integer(), Message :: binary(), HostTo :: to()) -> ok.
raise_alert(Name, AlertType, AlertTime, Message, HostTo) ->
	maybe_convert(call_es({raise_alert, Name, AlertType, AlertTime, Message, HostTo}), fun passthrough/1).

%% @spec create_alert_type(AlertType) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       AlertType = alert_type()
%% @doc Maps to:
%% ```AlertService.createAlertType(AlertType alertType)'''
-spec create_alert_type(AlertType :: alert_type()) -> ok.
create_alert_type(AlertType) ->
	maybe_convert(call_es({create_alert_type, AlertType}), fun passthrough/1).

%% @spec get_alert_type(Name) -> {ok, alert_type()} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Name = binary()
%% @doc Maps to:
%% ```AlertService.getAlertType(String typeName)'''
-spec get_alert_type(Name :: binary()) -> Type :: alert_type().
get_alert_type(Name) ->
	maybe_convert(call_es({get_alert_type, Name}), fun passthrough/1).

-spec alert_type_name(AlertType :: alert_type()) -> Name :: binary() .
alert_type_name(AlertType) ->
	AlertType#alert_type.name.

%% @spec get_object_types() -> {ok, Types} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Types = list(binary())
%%       ExceptionType = binary()
%%       Details = binary()
%% @doc Maps to:
%% ```String[] getObjectTypes()'''
-spec get_object_types() -> Types :: [TypeName :: binary()].
get_object_types() ->
	maybe_convert(call_es({get_object_types}), fun passthrough/1).

%% @spec get_objects_by_type(TypeName) -> {ok, Objects} | not_connected | {exception, ExceptionType, Details}
%% where
%%       TypeName = binary()
%%       Objects = list(to())
%%       ExceptionType = binary()
%%       Details = binary()
%% @doc Maps to:
%% ```java.util.Collection<TO<?>> getObjectsByType(String typeName)'''
-spec get_objects_by_type(TypeName :: binary()) -> TOs :: [to()].
get_objects_by_type(TypeName) ->
	maybe_convert(call_es({get_objects_by_type, TypeName}), fun passthrough/1).

%% @spec create_object(TypeName) -> {ok, To} | not_connected | {exception, ExceptionType, Details}
%% where
%%       TypeName = binary()
%%       To = to()
%%       ExceptionType = binary()
%%       Details = binary()
%% @doc Maps to:
%% ```TO<?> createObject(java.lang.String typeName)'''
-spec create_object(TypeName :: binary()) -> Id :: to().
create_object(TypeName) ->
	maybe_convert(call_es({create_object_1, TypeName}), fun passthrough/1).

%% @spec create_object(TypeName, PropertyValues) -> {ok, To} | not_connected | {exception, ExceptionType, Details}
%% where
%%       TypeName = string()
%%       PropertyValues = list(value_to())
%%       To = to()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```TO<?> createObject(java.lang.String typeName, ValueTO[] propertyValues)'''
-spec create_object(TypeName :: binary(), Properties :: proplist()) -> Id :: to().
create_object(TypeName, PropertyValues) ->
	maybe_convert(call_es({create_object_2, TypeName, PropertyValues}), fun passthrough/1).

%% @spec delete_object(Object) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```void deleteObject(TO<?> objId)'''
-spec delete_object(Id :: to()) -> ok.
delete_object(Id = #to{}) ->
	maybe_convert(call_es({delete_object, Id}), fun passthrough/1).

%% @spec exists(Object) -> {ok, Exists} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       Exists = bool()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```boolean exists(TO<?> objId)'''
-spec exists(Id :: to()) -> ok.
exists(Id = #to{}) ->
	maybe_convert(call_es({exists, Id}), fun passthrough/1).

%% @spec get_all_properties_values(ObjectSpec) -> Values} | not_connected | {exception, ExceptionType, Details}
%% where
%%       ObjectSpec = list(to()) | to()
%%       Values = list(collection_to()) | list(value_to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```java.util.Collection<CollectionTO> getAllPropertiesValues(java.util.Collection<TO<?>> objIds)'''
%% if ObjectSpec is a list(), otherwise:
%% ```java.util.Collection<ValueTO> getAllPropertiesValues(TO<?> objId)'''
-spec get_all_properties_values(Ids :: to() | [to()]) ->
	Values :: [simple_object()] | proplist().
get_all_properties_values([]) ->
	[];
get_all_properties_values(ObjectSpec) when ?IS_TO_LIST(ObjectSpec) ->
	maybe_convert(call_es({get_all_properties_values, ObjectSpec}), fun collectiontos_to_simpleobjects/1);
get_all_properties_values(ObjectSpec = #to{}) ->
	maybe_convert(call_es({get_all_properties_values, ObjectSpec}), fun valuetos_to_proplist/1).

%% @spec get_children(Objects) -> {ok, Children} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Objects = list(to()) | to()
%%       Children = list(to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```java.util.Collection<CollectionTO> getChildren(java.util.Collection<TO<?>> objIds)'''
-spec get_children(Ids :: to() | [to()]) -> ChildIds :: [to()].
get_children([]) ->
	[];
get_children(Objects) when ?IS_TO_LIST(Objects) ->
	maybe_convert(call_es({get_children_1, Objects}), fun collectiontos_to_proplist/1);
get_children(Object = #to{}) ->
	maybe_convert(call_es({get_children_1, Object}), fun passthrough/1).

%% @spec get_children(Objects, TypeName) -> {ok, Children} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Objects = list(to()) | to()
%%       TypeName = string()
%%       Children = list(to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```java.util.Collection<CollectionTO> getChildren(java.util.Collection<TO<?>> objIds, java.lang.String typeName)'''
%% if Objects is a list(), otherwise:
%% ```java.util.Collection<TO<?>> getChildren(TO<?> objId)'''
-spec get_children(Objects :: to() | [to()], TypeName :: binary()) -> ChildIds :: [to()].
get_children(Objects, TypeName) when ?IS_TO_LIST(Objects), is_binary(TypeName) ->
	maybe_convert(call_es({get_children_2, Objects, TypeName}), fun passthrough/1);
get_children(Object = #to{}, TypeName) when is_binary(TypeName) ->
	maybe_convert(call_es({get_children_2, Object, TypeName}), fun passthrough/1).

%% @spec get_parents(Object) -> {ok, Parents} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       Parents = list(to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```java.util.Collection<TO<?>> getParents(TO<?> objId)'''
-spec get_parents(Object :: to()) -> ParentIds :: [to()].
get_parents(Object = #to{}) ->
	maybe_convert(call_es({get_parents_1, Object}), fun passthrough/1).

%% @spec get_parents(Object, TypeName) -> {ok, Parents} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       TypeName = binary()
%%       Parents = list(to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```java.util.Collection<TO<?>> getParents(TO<?> objId, java.lang.String typeName)'''
-spec get_parents(Object :: to(), TypeName :: binary()) -> ParentIds :: [to()].
get_parents(Object = #to{}, TypeName) when is_binary(TypeName) ->
	maybe_convert(call_es({get_parents_2, Object, TypeName}), fun passthrough/1).

%% @spec get_properties_values(Objects, PropertyNames) -> {ok, Values} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Objects = list(to())
%%       PropertyNames = list(binary())
%%       Values = list(collection_to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```java.util.Collection<CollectionTO> getPropertyValue(java.util.Collection<TO<?>> objIds, java.lang.String[] propNames)'''
-spec get_properties_values(TOs :: [to()], PropertyNames :: [binary()]) -> Objects :: [simple_object()].
get_properties_values(TOs, PropertyNames) when ?IS_TO_LIST(TOs), is_list(PropertyNames), is_binary(hd(PropertyNames)) ->
	maybe_convert(call_es({get_properties_values, TOs, PropertyNames}), fun collectiontos_to_simpleobjects/1).

%% @spec get_property_value(Object, PropertyName) -> {ok, Value} | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       PropertyName = string()
%%       Value = string() | integer() | float() | to()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```<RETURN_TYPE> RETURN_TYPE getPropertyValue(java.lang.String typeName, java.lang.String propName, java.lang.Class<RETURN_TYPE> clazz)'''
-spec get_property_value(Object :: to(), PropertyName :: binary()) -> any().
get_property_value(Object = #to{}, PropertyName) when is_binary(PropertyName) ->
	maybe_convert(call_es({get_property_value, Object, PropertyName}), fun passthrough/1).

%% @spec remove_relation(Parent, Child) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       Parent = to()
%%       Child = to()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```void removeRelation(TO<?> parentId, TO<?> childId)'''
-spec remove_relation(Parent :: to(), Child :: to()) -> ok.
remove_relation(Parent = #to{}, Child = #to{}) ->
	maybe_convert(call_es({remove_relation, Parent, Child}), fun passthrough/1).

%% @spec set_all_properties_values(Object, Values) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       Values = list(value_to())
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```void setAllPropertiesValues(TO<?> objId, ValueTO[] values)'''
-spec set_all_properties_values(Id :: to(), Properties :: proplist()) -> ok.
set_all_properties_values(Id = #to{}, Properties) when is_list(Properties), is_binary(element(1, hd(Properties))) ->
	maybe_convert(call_es({set_all_properties_values, Id, proplist_to_valuetos(Properties)}), fun passthrough/1).

%% @spec set_property_value(Object, PropertyName, Value) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       Object = to()
%%       PropertyName = string()
%%       Value = string() | integer() | float() | to()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```<REQUESTED_TYPE> void setPropertyValue(TO<?> objId, java.lang.String propName, REQUESTED_TYPE value)'''
-spec set_property_value(Id :: to(), PName :: binary(), Value :: any()) -> ok.
set_property_value(Id = #to{}, PropertyName, Value) when is_binary(PropertyName) ->
	maybe_convert(call_es({set_property_value, Id, PropertyName, Value}), fun passthrough/1).

%% @spec set_relation(Parent, Child) -> ok | not_connected | {exception, ExceptionType, Details}
%% where
%%       Parent = to()
%%       Child = to()
%%       ExceptionType = string()
%%       Details = string()
%% @doc Maps to:
%% ```void setRelation(TO<?> parentId, TO<?> childId)'''
-spec set_relation(Parent :: to(), Child :: to()) -> ok.
set_relation(Parent = #to{}, Child = #to{}) ->
	maybe_convert(call_es({set_relation, Parent, Child}), fun passthrough/1).

%% @spec register_processor(pid(), filter) -> ok | not_connected | {exception, ExceptionType, Details}
-spec register_processor(Pid :: pid(), Filter :: event_type_filter()) -> ok.
register_processor(Pid, Filter) when is_pid(Pid) ->
	maybe_convert(call_es({register_processor, Pid, Filter}), fun passthrough/1).

%% @spec deregister_processor(pid()) -> ok | not_connected
-spec deregister_processor(Pid :: pid()) -> ok.
deregister_processor(Pid) when is_pid(Pid) ->
	maybe_convert(call_es({deregister_processor, Pid}), fun passthrough/1).

% must be called by the caller's PID
call_es(Message) ->
	call_es(Message, ?TIMEOUT).

call_es(Message, Timeout) ->
	NodeName = node_name(),
	{ok, MboxName} = application:get_env(esapi, mbox_name),
	lager:debug("send: ~p", [Message]),
	erlang:send({MboxName, NodeName}, Message),
	receive
		{esapi_result, Reply} ->
			lager:debug("receive: ~p", [Reply]),
			Reply
	after
		Timeout ->
			lager:warning("ES API command timed out: ~p", [element(1,Message)]),
			{error, noreply}
	end.

node_name() ->
	{ok, EsHost} = application:get_env(esapi, es_host),
	list_to_atom("esapi@" ++ EsHost).

connected() ->
	lists:member(node_name(), nodes(hidden)).

% internal utility functions
% mostly for conversion

proplist_to_valuetos(PropList) -> [value_to(Name, Value) || {Name, Value} <- PropList].

valuetos_to_proplist(Vtos) -> [{Name,Val} || #value_to{property_name=Name,value=Val} <- Vtos].

% [#collection_to{}] -> [simple_object()]
-spec collectiontos_to_simpleobjects([collection_to()]) -> [simple_object()].
collectiontos_to_simpleobjects(Ctos) -> [simple_object(Cto) || Cto <- Ctos].

simple_object(#collection_to{object_id = TO, values = Values}) ->
	{TO, [{Name, Value} || #value_to{property_name = Name, value = Value} <- Values]}.

-spec collectiontos_to_proplist([collection_to()]) -> [{to(), [to()]}].
collectiontos_to_proplist(Ctos) ->
	[{To, [V || #value_to{value=V} <- Vs]} || #collection_to{object_id=To, values=Vs} <- Ctos].

-spec value_to(binary(), any()) -> value_to() .
value_to(Name, Value) -> #value_to{property_name = Name, value = Value} .


maybe_convert(Return, Convert) ->
	case Return of
		ok -> ok;
		{ok, Value} ->
			Convert(Value);
		Other ->
			throw(Other)
	end.

passthrough(X) -> X.

