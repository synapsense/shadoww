
% Read in a data file containing CRAH information like: {CrahId, {X,Y}, Neighbors}
% Identify CRAH clusters at many different neighborhood sizes
% Write out the neighborhood size and the size of each cluster found with that neighborhood size

-module(cluster_test).

-export([
	 test/1
	]).

-record(tracker, {
          id,
          strategy_status,
          device_status,
          location,
          neighbors,
          stage2
         }).

test(FName) ->
	{ok, [CrahInfo]} = file:consult(FName),
	Trackers = lists:map(fun make_tracker/1, CrahInfo),
	ClusterData = lists:foldl(fun(N, Txt) -> [Txt | print_clusters(Trackers, N) ++ "\n"] end,
				  "",
				  [2,3,4,5]),
	ok = file:write_file("clusters.dat", ClusterData).

print_cluster_sizes(Trackers, NeighborhoodSize) ->
	Clusters = rf_balancer_strategy:neighborhood_clusters(Trackers, NeighborhoodSize),
	ClusterTxt = string:join([io_lib:format("~p", [length(Cluster)]) || Cluster <- Clusters], ","),
	io_lib:format("~p:~s", [NeighborhoodSize, ClusterTxt]).

print_clusters(Trackers, NeighborhoodSize) ->
	Clusters = rf_balancer_strategy:neighborhood_clusters(Trackers, NeighborhoodSize),
	ClusterTxt = string:join([string:join([io_lib:format("~s(~f,~f)", [Crah | xy(Crah, Trackers)]) || Crah <- Cluster], ",") || Cluster <- Clusters], ";"),
	io_lib:format("~p:~s", [NeighborhoodSize, ClusterTxt]).

xy(Crah, Trackers) ->
	#tracker{location={point,X,Y}} = lists:keyfind(Crah, #tracker.id, Trackers),
	[X,Y].

make_tracker({CrahId, {X, Y}, NeighborIds}) ->
	#tracker{
	   id = CrahId,
	   strategy_status = automatic,
	   device_status = online,
	   location = geom:point(X,Y),
	   neighbors = NeighborIds,
	   stage2 = 55.0
	  }.

