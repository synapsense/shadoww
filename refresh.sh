#!/usr/bin/env bash

NAME="${@: -1}"

declare -a app_names=("active_control" "esapi")

usage() {
	echo
	echo "Usage: [-r] $0 <instance_name>"
	echo "Options:"
	echo "-r    call './rebar compile apps=active_control' before looking for diffs"
	echo
	exit 1
}

recompile() {
	./rebar compile apps=active_control,esapi
	if [ $? -ne 0 ]
	then
		echo "build failed, exiting..."
		exit $?
	fi
}

copy_changes() {
	APP="$1"
	SRCDIR=apps/$APP/ebin
	DESTDIR=rel/controller/lib/$APP-*/ebin
	if [ -d $DESTDIR ]; then
		diff -q -r -x '*.swp' $SRCDIR $DESTDIR | sed -n -e "s%Only in $SRCDIR: \(.*\)%$SRCDIR/\1%p" -e "s/Files \(.*\) and .*/\1/p" | xargs -t -J % cp % $DESTDIR
	else
		echo "$DESTDIR doesn't exist"
		exit 1
	fi
}

for x in $@
do
	case "$x" in
		"-r") 
			shift
			recompile
			;;
	esac	
done

for app in "${app_names[@]}"
do
	copy_changes "$app"
done

