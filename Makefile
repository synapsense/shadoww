
.PHONY: compile release test test_reports deps

DEPS_DIR=deps
RELDIR=_build/default/rel
PROFILE?=shadoww

dm_driver=apps/dm_api/priv/lib/dmdriver.jar
glpsol=apps/glpsol/priv/glpsol

REBAR=./rebar3

default: compile

compile: $(dm_driver) $(glpsol)
	$(REBAR) compile

$(dm_driver): dm_driver/build/lib/dmdriver.jar
	mkdir -p apps/dm_api/priv
	cp -R dm_driver/build/* apps/dm_api/priv

dm_driver/build/lib/dmdriver.jar: dm_driver/src/com/synapsense/dmdriver/*.java
	(cd dm_driver; ant)

$(glpsol):
	$(MAKE) -C glpk copy destdir=../apps/glpsol/priv

test: $(dm_driver) $(glpsol)
	-$(REBAR) ct
	cp `find _build -name junit_report.xml | sort -r | head -n 1` .
	./covertool -cover _build/test/cover/ct.coverdata -src apps/active_control/src,apps/cluster_config/src,apps/dm_api/src,apps/dumbtable/src,apps/es_ac_sync/src,apps/esapi/src,apps/glpsol/src/,apps/io_eval/src/,apps/mbgw/src,apps/node_connector/src,apps/shadoww_lib/src

dialyze:
	-$(REBAR) dialyzer

release: compile
	git describe --long > www/build_stamp
	date -R -u >> www/build_stamp
	$(REBAR) release -c profile/$(PROFILE)/relx.config

clean:
	(cd dm_driver; ant clean)
	rm -Rf apps/dm_api/priv
	$(MAKE) -C glpk clean
	rm -f apps/glpsol/priv/glpsol
	$(REBAR) clean
	rm -Rf $(RELDIR)/*
	rm -f coverage.xml junit_report.xml

run:
	$(RELDIR)/$(PROFILE)/bin/$(PROFILE) console

tar: release
	tar -czf $(PROFILE).tar.gz -C $(RELDIR) $(PROFILE)

