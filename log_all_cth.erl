%%% @doc Common Test Example Common Test Hook module.
-module(log_all_cth).

%% Callbacks
-export([id/1]).
-export([init/2]).

-export([pre_init_per_suite/3]).
-export([post_init_per_suite/4]).
-export([pre_end_per_suite/3]).
-export([post_end_per_suite/4]).

-export([pre_init_per_group/3]).
-export([post_init_per_group/4]).
-export([pre_end_per_group/3]).
-export([post_end_per_group/4]).

-export([pre_init_per_testcase/3]).
-export([post_end_per_testcase/4]).

-export([on_tc_fail/3]).
-export([on_tc_skip/3]).

-export([terminate/1]).

-record(state, { file_handle, total, suite_total, ts, tcs, data }).

%% @doc Return a unique id for this CTH.
id(Opts) ->
	log_all_cth.

%% @doc Always called before any other callback function. Use this to initiate
%% any common state.
init(Id, Opts) ->
	{ok, nostate}.

%% @doc Called before init_per_suite is called.
pre_init_per_suite(Suite,Config,State) ->
	ct:pal("~p:pre_init_per_suite(~p)", [?MODULE, Suite]),
	{Config, State}.

%% @doc Called after init_per_suite.
post_init_per_suite(Suite,Config,Return,State) ->
	ct:pal("~p:post_init_per_suite(~p)", [?MODULE, Suite]),
	{Return, State}.

%% @doc Called before end_per_suite.
pre_end_per_suite(Suite,Config,State) ->
	ct:pal("~p:pre_end_per_suite(~p)", [?MODULE, Suite]),
	{Config, State}.

%% @doc Called after end_per_suite.
post_end_per_suite(Suite,Config,Return,State) ->
	ct:pal("~p:post_end_per_suite(~p)", [?MODULE, Suite]),
	{Return, State}.

%% @doc Called before each init_per_group.
pre_init_per_group(Group,Config,State) ->
	ct:pal("~p:pre_init_per_group(~p)", [?MODULE, Group]),
	{Config, State}.

%% @doc Called after each init_per_group.
post_init_per_group(Group,Config,Return,State) ->
	ct:pal("~p:post_init_per_group(~p)", [?MODULE, Group]),
	{Return, State}.

%% @doc Called after each end_per_group.
pre_end_per_group(Group,Config,State) ->
	ct:pal("~p:pre_end_per_group(~p)", [?MODULE, Group]),
	{Config, State}.

%% @doc Called after each end_per_group.
post_end_per_group(Group,Config,Return,State) ->
	ct:pal("~p:post_end_per_group(~p)", [?MODULE, Group]),
	{Return, State}.

%% @doc Called before each test case.
pre_init_per_testcase(TC,Config,State) ->
	ct:pal("~p:pre_init_per_testcase(~p)", [?MODULE, TC]),
	{Config, State}.

%% @doc Called after each test case.
post_end_per_testcase(TC,Config,Return,State) ->
	ct:pal("~p:post_end_per_testcase(~p)", [?MODULE, TC]),
	{Return, State}.

%% @doc Called after post_init_per_suite, post_end_per_suite, post_init_per_group,
%% post_end_per_group and post_end_per_testcase if the suite, group or test case failed.
on_tc_fail(TC, Reason, State) ->
	ct:pal("~p:on_tc_fail(~p)", [?MODULE, TC]),
	State.

%% @doc Called when a test case is skipped by either user action
%% or due to an init function failing.
on_tc_skip(TC, Reason, State) ->
	ct:pal("~p:on_tc_skip(~p)", [?MODULE, TC]),
	State.

%% @doc Called when the scope of the CTH is done
terminate(State) ->
	ct:pal("~p:terminate()", [?MODULE]),
	ok.

