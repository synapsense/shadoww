package com.synapsense.dmdriver;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBoolean;
import com.ericsson.otp.erlang.OtpErlangDecodeException;
import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangExit;
import com.ericsson.otp.erlang.OtpErlangFloat;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangRangeException;
import com.ericsson.otp.erlang.OtpErlangRef;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.ericsson.otp.erlang.OtpMbox;
import com.ericsson.otp.erlang.OtpMsg;
import com.ericsson.otp.erlang.OtpNode;
import com.synapsense.plugin.apinew.StatelessPlugin;

public class PluginAdapter {

	private final static Logger logger = Logger.getLogger(PluginAdapter.class);

	public static OtpErlangAtom ok = new OtpErlangAtom("ok");
	public static OtpErlangAtom error = new OtpErlangAtom("error");
	public static String NO_PLUGIN_ERROR = "no_plugin";
	public static String BAD_ARGS_ERROR = "bad_args";

	private ExecutorService executorService = Executors.newCachedThreadPool();
	private OtpNode node;
	private OtpMbox mbox;

	private Map<String, StatelessPlugin> plugins;

	public PluginAdapter(String nodeName, String mboxName, String cookie) throws IOException {
		logger.info("Starting Erlang node '" + nodeName + "'");
		node = new OtpNode(nodeName);
		logger.info("Setting cookie '" + cookie + "'");
		node.setCookie(cookie);
		logger.info("Creating mailbox '" + mboxName + "'");
		mbox = node.createMbox(mboxName);
		loadPlugins();
	}

	public void waitForLink() throws OtpErlangExit, InterruptedException, OtpErlangDecodeException {

		do {
			logger.debug("Waiting for 'link' message...");
			OtpMsg m = mbox.receiveMsg(60 * 1000); // Wait up to 1 minute for
													// the 'link' message.
													// throws
													// InterruptedException on
													// timeout.
			OtpErlangPid fromPid = m.getSenderPid();
			OtpErlangObject command = m.getMsg();

			logger.debug("Message '" + command.toString() + "' received from '" + fromPid.toString() + "'");

			if ("link".equals(command.toString())) {
				mbox.link(fromPid);
				logger.debug("Mailbox linked to PID " + fromPid.toString());
				break;
			} else {
				logger.error("Unexpected message while waiting for 'link' message: '" + command.toString() + "'");
			}
		} while (true);

	}

	public void startMessaging() throws OtpErlangExit, OtpErlangDecodeException {

		while (true) {
			logger.debug("Waiting for message...");
			OtpMsg m = mbox.receiveMsg();
			OtpErlangPid fromPid = m.getSenderPid();
			OtpErlangTuple callWrapper = (OtpErlangTuple) m.getMsg();
			OtpErlangRef replyRef = (OtpErlangRef) callWrapper.elementAt(0);
			OtpErlangObject command = callWrapper.elementAt(1);

			logger.debug("Message '" + command.toString() + "' received from '" + fromPid.toString() + "'");

			if ("shutdown".equals(command.toString())) {
				break;
			}
			executorService.execute(new HandleCommand(fromPid, replyRef, command));
		}

	}

	public void stop() {
		if (mbox != null) {
			mbox.close();
		}
		if (node != null) {
			node.close();
		}
	}

	private void loadPlugins() {
		plugins = new HashMap<String, StatelessPlugin>();
		ServiceLoader<StatelessPlugin> svcs = ServiceLoader.load(StatelessPlugin.class);
		for (StatelessPlugin plugin : svcs) {

			logger.info("Found plugin for service " + StatelessPlugin.class.toString() + " named "
					+ plugin.getClass().toString() + " with ID " + plugin.getID());
			if (plugins.get(plugin.getID()) != null) {
				logger.error("Key " + plugin.getID() + " is duplicated in "
						+ plugins.get(plugin.getID()).getClass().toString() + " and " + plugin.getClass().toString());
			} else {
				plugins.put(plugin.getID(), plugin);
			}
		}
	}

	public static OtpErlangTuple makeError(String atom) {
		return new OtpErlangTuple(new OtpErlangObject[] { error, new OtpErlangString(atom) });
	}

	public static OtpErlangTuple makeError(Exception e) {
		return new OtpErlangTuple(new OtpErlangObject[] { error, new OtpErlangString(e.getMessage()) });
	}

	private class HandleCommand implements Runnable {

		private OtpErlangPid fromPid;
		private OtpErlangRef replyRef;
		private OtpErlangObject command;

		public HandleCommand(final OtpErlangPid fromPid, final OtpErlangRef replyRef, final OtpErlangObject command2) {
			this.fromPid = fromPid;
			this.replyRef = replyRef;
			this.command = command2;
		}

		@Override
		public void run() {
			OtpErlangObject resp = handleCommand((OtpErlangTuple) command);
			logger.debug("Sending response '" + resp + "' to '" + fromPid + "'");
			OtpErlangTuple taggedReply = new OtpErlangTuple(new OtpErlangObject[] {replyRef, resp});
			mbox.send(fromPid, taggedReply);
		}

		private OtpErlangObject handleCommand(OtpErlangTuple command) {
			logger.debug("elementAt(0) " + ((OtpErlangAtom) command.elementAt(0)).atomValue());
			logger.debug("elementAt(1) " + command.elementAt(1).getClass());
			logger.debug("elementAt(2) " + command.elementAt(2).getClass());
			logger.debug("arity " + command.arity());
			if (command.arity() > 2) {
				OtpErlangObject arg0 = command.elementAt(0);
				OtpErlangObject arg1 = command.elementAt(1);
				OtpErlangObject arg2 = command.elementAt(2);
				if (arg0 instanceof OtpErlangAtom && arg1 instanceof OtpErlangAtom && arg2 instanceof OtpErlangList) {

					String commandValue = ((OtpErlangAtom) arg0).atomValue();
					String pluginId = ((OtpErlangAtom) arg1).atomValue();
					OtpErlangList params = (OtpErlangList) command.elementAt(2);
					StatelessPlugin plugin = plugins.get(pluginId);
					if (plugin == null) {
						logger.error("No Plugin found for pluginId [" + pluginId + "]");
						return makeError("no_plugin");
					}
					if ("get_value".equalsIgnoreCase(commandValue)) {
						try {
							Object result = plugin.getPropertyValue(convertParams(params));
							return convertValToOTP(result);
						} catch (IllegalArgumentException e) {
							logger.error("Incorrect parameters [" + params + "]");
							return makeError(BAD_ARGS_ERROR);

						}

						catch (Exception e) {
							logger.error("Unable to perform get due to exception.");
							logger.debug(e);
							return makeError(e);
						}

					} else if ("set_value".equalsIgnoreCase(commandValue) && command.arity() > 3) {
						OtpErlangObject newValue = command.elementAt(3);
						try {
							plugin.setPropertyValue(convertParams(params), convertValFromOTP(newValue));
							return newValue;
						} catch (Exception e) {
							logger.error("Unable to perform set due to exception.");
							logger.debug(e);
							return makeError(e);
						}
					}
				}
			}
			return makeError("wrong_command");
		}
	}

	private Properties convertParams(OtpErlangList list) throws OtpErlangRangeException {
		Properties params = new Properties();
		for (OtpErlangObject param : list) {
			if (param instanceof OtpErlangTuple) {
				OtpErlangTuple paramTuple = (OtpErlangTuple) param;
				if (paramTuple.arity() == 2) {
					params.put(otpToString(paramTuple.elementAt(0)), otpToString(paramTuple.elementAt(1)));
				}
			}
		}
		return params;
	}

	private String otpToString(Object obj) {
		if (obj instanceof OtpErlangString) {
			return ((OtpErlangString) obj).stringValue();
		}
		if (obj instanceof OtpErlangAtom) {
			return ((OtpErlangAtom) obj).atomValue();
		} else {
			return obj.toString();
		}
	}

	private OtpErlangObject convertValToOTP(Object value) {
		OtpErlangObject valueObj = null;
		if (value instanceof Integer) {
			valueObj = new OtpErlangInt(((Integer) value).intValue());
		} else if (value instanceof Double) {
			valueObj = new OtpErlangDouble(((Double) value).doubleValue());
		} else if (value instanceof Boolean) {
			valueObj = new OtpErlangBoolean(((Boolean) value).booleanValue());
		} else if (value instanceof String) {
			valueObj = new OtpErlangString((String) value);
		}
		return valueObj;
	}

	private Serializable convertValFromOTP(OtpErlangObject value) throws OtpErlangRangeException {
		Serializable valueObj = null;
		if (value instanceof OtpErlangInt) {
			valueObj = new Integer(((OtpErlangInt) value).intValue());
		} else if (value instanceof OtpErlangLong) {
			valueObj = new Integer(((OtpErlangLong) value).intValue());
		} else if (value instanceof OtpErlangFloat) {
			valueObj = new Float(((OtpErlangFloat) value).floatValue());
		} else if (value instanceof OtpErlangDouble) {
			valueObj = new Float(((OtpErlangDouble) value).doubleValue());
		} else if (value instanceof OtpErlangBoolean) {
			valueObj = new Boolean(((OtpErlangBoolean) value).booleanValue());
		} else if (value instanceof OtpErlangAtom) {
			valueObj = new String(((OtpErlangAtom) value).atomValue());
		} else if (value instanceof OtpErlangString) {
			valueObj = ((OtpErlangString) value).stringValue();
		}
		return valueObj;
	}

}
