package com.synapsense.dmdriver;

import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.ericsson.otp.erlang.OtpErlangException;
import com.ericsson.otp.erlang.OtpErlangExit;

public class Launcher {
	private final static Logger logger = Logger.getLogger(Launcher.class);

	private static PluginAdapter adapter = null;
	private final static String FILE_SEPARATOR = System.getProperty("file.separator");

	public static void main(String[] args) {

		PropertyConfigurator.configureAndWatch("conf" + FILE_SEPARATOR + "log4j.properties");
		/*
		 * String newPath = System.getProperty("java.library.path") +
		 * PATH_SEPARATOR +
		 * "c:\\syn\\shadoww\\rel\\controller\\lib\\dm_api-0.1.0\\priv\\lib\\x86"
		 * ;
		 * 
		 * System.setProperty("java.library.path", newPath); Field fieldSysPath;
		 * try { fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
		 * fieldSysPath.setAccessible(true); fieldSysPath.set(null, null); }
		 * catch (SecurityException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); } catch (NoSuchFieldException e1) { // TODO
		 * Auto-generated catch block e1.printStackTrace(); } catch
		 * (IllegalArgumentException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (IllegalAccessException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				adapter.stop();
				logger.info("good bye");
			}
		});

		logger.info("launching Plugin Adapter...");

		if (args.length < 3) {
			logger.error("Expected command line args are: nodeName, mboxName, cookie. Passed were: "
			        + Arrays.toString(args) + ". Port Driver Exiting...");
			System.exit(-1);
		}

		String nodeName = args[0];
		String mboxName = args[1];
		String cookie = args[2];

		try {
			adapter = new PluginAdapter(nodeName, mboxName, cookie);
		} catch (IOException e) {
			logger.error("Exception setting up Erlang node and mailbox: ", e);
			stop(-1);
		}

		try {
			adapter.waitForLink();
		} catch (InterruptedException e) {
			logger.error("Timeout waiting for 'link' message.  Port Driver Exiting...");
			stop(-1);
			logger.error("Port Driver Exiting...", e);
		} catch (OtpErlangException e) {
			logger.info("OtpErlangException: " + e);
			stop(-1);
			logger.error("Port Driver Exiting...", e);
		}
		try {
			adapter.startMessaging();
		} catch (OtpErlangExit e) {
			logger.info("OtpErlangExit: " + e.getMessage());
			stop(-1);
		} catch (Throwable t) {
			logger.error("Exiting from unhandled exception:", t);
			stop(-1);
		}
		// we will get to this point after 'shutdown' command
		stop(0);
	}

	public static void stop(int exitCode) {
		if (adapter != null) {
			adapter.stop();
		}
		logger.error("Plugin Adapter Exiting...");
		System.exit(exitCode);
	}

}
