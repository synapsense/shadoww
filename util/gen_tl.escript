#!escript
-module(gen_tl).

-export([main/1]).

main(Args) ->
    [TraceIn,TraceOut] = Args,

    {ok, F} = file:open(TraceOut, write),
	TC = dbg:trace_client(file, TraceIn, {fun(Trace,File) ->
		case Trace of
			{trace_ts, Pid, Type, MFA, NowTime} ->
                T = wt(NowTime),
                %file:write(File, io_lib:format("~s|~p~n", [T, NowTime]));
				file:write(File, io_lib:format("~s|~p|~10s--~w~n", [T, Pid, Type, MFA]));
            {trace_ts, Pid, send, Msg, To, NowTime} ->
                T = wt(NowTime),
                %file:write(File, io_lib:format("~s|~p~n", [T, NowTime]));
				file:write(File, io_lib:format("~s|~p|~p ! ~w~n", [T, Pid, To, Msg]));
            {trace_ts, Pid, spawn, NPid, Initial, NowTime} ->
                T = wt(NowTime),
                %file:write(File, io_lib:format("~s|~p~n", [T, NowTime]));
				file:write(File, io_lib:format("~s|~p|~p -> ~w~n", [T, Pid, NPid, Initial]));
			Trace -> file:write(File, io_lib:format("~p~n", [Trace]))
		end,
		File
	end, F}),
    MR = monitor(process, TC),
    receive {'DOWN', MR, _, _, _} -> ok end,
    file:close(F)
    .

wt({_, _, Usec} = Now) ->
    {{YY, MM, DD}, {H,M,S}} = calendar:now_to_local_time(Now),
    lists:flatten(io_lib:format("~b-~b-~b ~b:~b:~b.~b", [YY, MM, DD, H, M, S, Usec]))
    .
