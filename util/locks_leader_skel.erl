
-module(module).

-behaviour(locks_leader).

-export([
         start_link/0
        ]).

-export([init/1,
         elected/3,
         surrendered/3,
         handle_DOWN/3,
         handle_leader_call/4,
         handle_leader_cast/3,
         from_leader/3,
         handle_call/4,
         handle_cast/3,
         handle_info/3,
         terminate/2,
         code_change/4]).

-define(SERVER, ?MODULE).
-define(STATE, ?MODULE).

-record(?STATE, {
           am_leader = false
          }).

start_link() ->
    locks_leader:start_link(?SERVER, ?MODULE, {}, []).

init({}) ->
    {ok, #?STATE{}}.

%% @spec elected(State::state(), I::info(), Cand::pid() | undefined) ->
%%   {ok, Broadcast, NState}
%% | {reply, Msg, NState}
%% | {ok, AmLeaderMsg, FromLeaderMsg, NState}
%% | {error, term()}
%%
%%     Broadcast = broadcast()
%%     NState    = state()
%%
%% @doc Called by the leader when it is elected leader, and each time a
%% candidate recognizes the leader.
%%
%% This function is only called in the leader instance, and `Broadcast'
%% will be sent to all candidates (when the leader is first elected),
%% or to the new candidate that has appeared.
%%
%% `Broadcast' might be the same as `NState', but doesn't have to be.
%% This is up to the application.
%%
%% If `Cand == undefined', it is possible to obtain a list of all new
%% candidates that we haven't synced with (in the normal case, this will be
%% all known candidates, but if our instance is re-elected after a netsplit,
%% the 'new' candidates will be the ones that haven't yet recognized us as
%% leaders). This gives us a chance to talk to them before crafting our
%% broadcast message.
%%
%% We can also choose a different message for the new candidates and for
%% the ones that already see us as master. This would be accomplished by
%% returning `{ok, AmLeaderMsg, FromLeaderMsg, NewState}', where
%% `AmLeaderMsg' is sent to the new candidates (and processed in
%% {@link surrendered/3}, and `FromLeaderMsg' is sent to the old
%% (and processed in {@link from_leader/3}).
%%
%% If `Cand == Pid', a new candidate has connected. If this affects our state
%% such that all candidates need to be informed, we can return `{ok, Msg, NSt}'.
%% If, on the other hand, we only need to get the one candidate up to speed,
%% we can return `{reply, Msg, NSt}', and only the candidate will get the
%% message. In either case, the candidate (`Cand') will receive the message
%% in {@link surrendered/3}. In the former case, the other candidates will
%% receive the message in {@link from_leader/3}.
%%
%% @end
%%
elected(#?STATE{} = State, _LdrInfo, undefined) ->
    {ok, {sync, State}, State#?STATE{am_leader = true}};
elected(#?STATE{} = State, _LdrInfo, Pid) when is_pid(Pid) ->
    {reply, {sync, State}, State#?STATE{am_leader = true}}.


%% @spec surrendered(State::state(), Synch::broadcast(), I::info()) ->
%%          {ok, NState}
%%
%%    NState = state()
%%
%% @doc Called by each candidate when it recognizes another instance as
%% leader.
%%
%% Strictly speaking, this function is called when the candidate
%% acknowledges a leader and receives a Synch message in return.
%%
%% @end
surrendered(#?STATE{} = State, {sync, _LeaderState}, _LdrInfo) ->
    {ok, State#?STATE{am_leader = false}}.

%% @spec handle_DOWN(Candidate::pid(), State::state(), I::info()) ->
%%    {ok, NState} | {ok, Broadcast, NState}
%%
%%   Broadcast = broadcast()
%%   NState    = state()
%%
%% @doc Called by the leader when it detects loss of a candidate.
%%
%% If the function returns a `Broadcast' object, this will be sent to all
%% candidates, and they will receive it in the function {@link from_leader/3}.
%% @end
handle_DOWN(_Pid, State, _I) ->
    {ok, State}.

%% @spec handle_leader_call(Msg::term(), From::callerRef(), State::state(),
%%                          I::info()) ->
%%    {reply, Reply, NState} |
%%    {reply, Reply, Broadcast, NState} |
%%    {noreply, state()} |
%%    {stop, Reason, Reply, NState} |
%%    commonReply()
%%
%%   Broadcast = broadcast()
%%   NState    = state()
%%
%% @doc Called by the leader in response to a
%% {@link locks_leader:leader_call/2. leader_call()}.
%%
%% If the return value includes a `Broadcast' object, it will be sent to all
%% candidates, and they will receive it in the function {@link from_leader/3}.
%%
%% In this particular example, `leader_lookup' is not actually supported
%% from the {@link gdict. gdict} module, but would be useful during
%% complex operations, involving a series of updates and lookups. Using
%% `leader_lookup', all dictionary operations are serialized through the
%% leader; normally, lookups are served locally and updates by the leader,
%% which can lead to race conditions.
%% @end
handle_leader_call(_Request, _From, #?STATE{} = State, _LdrInfo) ->
    Broadcast = ok,
    {reply, ok, Broadcast, State#?STATE{}}.


%% @spec handle_leader_cast(Msg::term(), State::term(), I::info()) ->
%%   commonReply()
%%
%% @doc Called by the leader in response to a {@link locks_leader:leader_cast/2.
%% leader_cast()}.
%% @end
handle_leader_cast(_Msg, #?STATE{} = State, _LdrInfo) ->
    {ok, State}.

%% @spec from_leader(Msg::term(), State::state(), I::info()) ->
%%    {ok, NState}
%%
%%   NState = state()
%%
%% @doc Called by each candidate in response to a message from the leader.
%%
%% In this particular module, the leader passes an update function to be
%% applied to the candidate's state.
%% @end
from_leader(_Msg, #?STATE{} = State, _LdrInfo) ->
    {ok, State}.


%% @spec handle_call(Request::term(), From::callerRef(), State::state(),
%%                   I::info()) ->
%%    {reply, Reply, NState}
%%  | {noreply, NState}
%%  | {stop, Reason, Reply, NState}
%%  | commonReply()
%%
%% @doc Equivalent to `Mod:handle_call/3' in a gen_server.
%%
%% Note the difference in allowed return values. `{ok,NState}' and
%% `{noreply,NState}' are synonymous.
%%
%% `{noreply,NState}' is allowed as a return value from `handle_call/3',
%% since it could arguably add some clarity, but mainly because people are
%% used to it from gen_server.
%% @end
%%
handle_call(_Request, _From, #?STATE{} = State, _LdrInfo) ->
    Reply = ok,
    {reply, Reply, State}.

%% @spec handle_cast(Msg::term(), State::state(), I::info()) ->
%%    {noreply, NState}
%%  | commonReply()
%%
%% @doc Equivalent to `Mod:handle_call/3' in a gen_server, except
%% (<b>NOTE</b>) for the possible return values.
%%
handle_cast(_Msg, State, _LdrInfo) ->
    {noreply, State}.

%% @spec handle_info(Msg::term(), State::state(), I::info()) ->
%%     {noreply, NState}
%%   | commonReply()
%%
%% @doc Equivalent to `Mod:handle_info/3' in a gen_server,
%% except (<b>NOTE</b>) for the possible return values.
%%
%% This function will be called in response to any incoming message
%% not recognized as a call, cast, leader_call, leader_cast, from_leader
%% message, internal leader negotiation message or system message.
%% @end
handle_info(_Msg, State, _LdrInfo) ->
    {noreply, State}.

%% @spec code_change(FromVsn::string(), OldState::term(),
%%                   I::info(), Extra::term()) ->
%%       {ok, NState}
%%
%%    NState = state()
%%
%% @doc Similar to `code_change/3' in a gen_server callback module, with
%% the exception of the added argument.
%% @end
code_change(_FromVsn, State, _LdrInfo, _Extra) ->
    {ok, State}.

%% @spec terminate(Reason::term(), State::state()) -> Void
%%
%% @doc Equivalent to `terminate/2' in a gen_server callback
%% module.
%% @end
terminate(_Reason, _S) ->
    ok.

