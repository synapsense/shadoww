
-module(tracer).

-compile(export_all).

trace() ->
	dbg:start(),
	dbg:tracer(port, dbg:trace_port(file, "trace.log")),
	{ok, Tracer} = dbg:get_tracer(),
	seq_trace:set_system_tracer(Tracer),
	dbg:tpl(skeleton, []),
	dbg:tpl(gen_leader, []),
	dbg:p(all, [call, timestamp, return_to]),
	%dbg:p(process_info(Tracer, group_leader), clear), % doesn't work with a trace port
	ok
	.

client() ->
	dbg:trace_client(file, "trace.log", {fun(A,B) ->
		File = case B of
			start -> {ok, F} = file:open("traceout", write), F;
			B -> B
		end,
		case A of
			{trace_ts, Pid, Type, MFA, NowTime} ->
				{_, _, Usec} = NowTime,
				{_, {H,M,S}} = calendar:now_to_local_time(NowTime),
				file:write(File, io_lib:format("~p:~p:~p:~p|~p|~p~n    ~p~n", [H, M, S, Usec, Pid, Type, MFA]));
			A -> file:write(File, io_lib:format("~p~n~n", [A]))
		end,
		File
	end, start})
	.

