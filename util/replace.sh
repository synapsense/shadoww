#!/bin/bash

# The script searches and replaces strings in files under given folder using given regular expressions file
# Regular expressions are read line by line. For more info read manual for "sed" command

DPATH="./apps/active_control/src/*.erl"
PPATH="./patterns"
TFILE="/tmp/out.tmp.$$"

for f in $DPATH
do
    if [ -f $f -a -r $f ]; then
	sed -f $PPATH "$f" > $TFILE && mv $TFILE "$f"
    else
       echo "Error: Cannot read $f"
    fi
done
