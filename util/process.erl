
-module(process).

-export([proc_file/1, proc_dir/1]).


-record(file, {name, calls = []}).
-record(call, {name, type, message, line}).


proc_dir(Dir) ->
	try
		{ok, Files} = file:list_dir(Dir),
		Results = lists:map(fun(File) ->
				case string:str(File, ".erl") of
					0 -> #file{name = "blah", calls = []};
					_Any -> proc_file(string:concat(Dir, File))
				end
			end, Files),
		PivotedResults = pivot(Results, dict:new()),
		showPivot(PivotedResults)
	catch
		E:R ->
			io:format("problem processing dir: ~p,~p~n~p~n", [E, R, erlang:get_stacktrace()])
	end
	.

proc_file(File) ->
	{ok, Form} = epp:parse_file(File, ["src"], []),
	%io:format("~p~n", [Form]),
	try
		Results = rec_proc(Form, []),
		%PivotedResults = pivot(Results, dict:new()),
		%showPivot(PivotedResults),
		#file{name = lists:flatten(File), calls = Results}
	catch
		E:R ->
			io:format("problem processing file: ~p,~p~n~p~n", [E, R, erlang:get_stacktrace()])
	end
	.

pivot([], Dict) ->
	Dict
	;
pivot([#file{name = Name, calls = Calls} | MoreFiles], Dict) ->
	NewDict = pivotCall(Calls, Name, Dict),
	pivot(MoreFiles, NewDict)
	.

pivotCall([], _Name, Dict) ->
	Dict
	;
pivotCall([#call{name = Name, type = Type, message = Msg, line = Line} | Results], FName, Dict) ->

	TypeDict = case dict:find(Type, Dict) of
		{ok, Value} -> Value;
		error -> dict:new()
	end,

	Msgs = case dict:find(Name, TypeDict) of
		{ok, Value2} -> Value2;
		error -> []
	end,

	FullMsg = escaped(io_lib:format("~s, ~s, ~p", [Msg, FName, Line])),

	NewDict = dict:store(Type, dict:store(Name, [FullMsg | Msgs], TypeDict), Dict),

	pivotCall(Results, FName, NewDict)
	.

showPivot(Dict) ->
	%io:format("\\begin{itemize}~n"),
	lists:foreach(fun showType/1, dict:to_list(Dict))
	%io:format("\\end{itemize}~n")
	.

showType({Key, Value}) ->
	io:format("~s~n", [Key]),
	io:format("\\begin{itemize}~n"),
	lists:foreach(fun showName/1, dict:to_list(Value)),
	io:format("\\end{itemize}~n")
	.
showName({Key, Value}) ->
	io:format("\\item ~s~n", [Key]),
	io:format("\\begin{itemize}~n"),
	lists:foreach(fun(E) -> io:format("\\item ~s~n", [E]) end, Value),
	io:format("\\end{itemize}~n")
	.

rec_proc([], State) -> State ;
rec_proc([H | T], State) -> rec_proc(T, proc_form(H, State)) .


proc_form({call, Line, {remote, _, {atom, _, es_syncer}, {atom, _, raise_alert}}, Args}, State) ->
	[NameForm, TypeForm, MsgForm, _MsgArgs, _Id] = Args,
	{string, _, Name} = NameForm,
	{string, _, Type} = TypeForm,
	{string, _, Msg} = MsgForm,
	[#call{name = Name, type = Type, message = Msg, line = Line} | State]
	;

proc_form({function, _, _Fname, _, Forms}, State) -> rec_proc(Forms, State) ;

proc_form({clause, _, _, _, Forms}, State) -> rec_proc(Forms, State) ;

proc_form({'case', _, _, Forms}, State) -> rec_proc(Forms, State) ;

proc_form(_F, State) -> State .


escaped(String) ->
	String1 = re:replace(String,	"~",	"\\\\~{}",	[global, {return, list}]),
	String2 = re:replace(String1,	"\\[",	"{[}",		[global, {return, list}]),
	String3 = re:replace(String2,	"\\]",	"{]}",		[global, {return, list}]),
	String4 = re:replace(String3,	"_",	"\\\\_",		[global, {return, list}]),
	String4
	.



