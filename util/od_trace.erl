%% ex: ft=erlang ts=4 sw=4 et

-module(od_trace).

-compile(export_all).


start_dbg(Pid) ->
    dbg:start(),
    dbg:tracer(port, dbg:trace_port(file, "od.trace")),
    %dbg:n('controller@test-ac1.Synapsense.Int'),
    %dbg:p(all, call),
    %
    dbg:tpl({base_driver, '_', '_'}, []),
    dbg:tpl({drv_handler, '_', '_'}, []),
    dbg:tpl({crah_fsm, '_', '_'}, []),
    dbg:tpl({vfd_fsm, '_', '_'}, []),
    dbg:tpl({generic_modbus_crah, '_', '_'}, []),
    dbg:tpl({ach550_vfd, '_', '_'}, []),
    dbg:tpl({strategy_fsm, '_', '_'}, []),
    dbg:tpl({proportional_trim_respond_strategy, '_', '_'}, []),

    if
        is_list(Pid) ->
            [dbg:p(P, all) || P <- Pid];
        true ->
            dbg:p(Pid, all)
    end
    .

stop_dbg() ->
    %dbg:cn('controller@test-ac1.Synapsense.Int'),
    dbg:stop_clear()
    .

gen_tl(TraceIn, TraceOut) ->
    {ok, F} = file:open(TraceOut, write),
	TC = dbg:trace_client(file, TraceIn, {fun(Trace,File) ->
		case Trace of
			{trace_ts, Pid, Type, MFA, NowTime} ->
                T = wt(NowTime),
                %file:write(File, io_lib:format("~s|~p~n", [T, NowTime]));
				file:write(File, io_lib:format("~s|~p|~10s--~w~n", [T, Pid, Type, MFA]));
            {trace_ts, Pid, send, Msg, To, NowTime} ->
                T = wt(NowTime),
                %file:write(File, io_lib:format("~s|~p~n", [T, NowTime]));
				file:write(File, io_lib:format("~s|~p|~p ! ~w~n", [T, Pid, To, Msg]));
            {trace_ts, Pid, spawn, NPid, Initial, NowTime} ->
                T = wt(NowTime),
                %file:write(File, io_lib:format("~s|~p~n", [T, NowTime]));
				file:write(File, io_lib:format("~s|~p|~p -> ~w~n", [T, Pid, NPid, Initial]));
			Trace -> file:write(File, io_lib:format("~p~n", [Trace]))
		end,
		File
	end, F}),
    MR = monitor(process, TC),
    receive {'DOWN', MR, _, _, _} -> ok end,
    file:close(F)
    .

wt({_, _, Usec} = Now) ->
    {{YY, MM, DD}, {H,M,S}} = calendar:now_to_local_time(Now),
    lists:flatten(io_lib:format("~b-~b-~b ~b:~b:~b.~b", [YY, MM, DD, H, M, S, Usec]))
    .
