
-module(tmax).

-compile(export_all).

test() ->
	N = 100000,
	W = 10,
	case rec_test(N, N, W, [], []) of
		ok -> io:fwrite("all good!~n");
		error -> io:fwrite("error!~n")
	end
	.

time_test() ->
	RandList = [{I, rand:uniform()} || I <- lists:seq(0,100000)],
	Window = 100,
	{TList, ok} = timer:tc(fun() -> r_lmax(RandList, Window, []) end),
	{TSmart, ok} = timer:tc(fun() -> r_wmax(RandList, Window, []) end),
	io:fwrite("list: ~p~nwind: ~p~n", [TList, TSmart])
	.

r_lmax([], _, _) -> ok ;
r_lmax([{_, V} | Tl], W, L) ->
	{_, NL} = listmax(L, W, V),
	r_lmax(Tl, W, NL)
	.

r_wmax([], _, _) -> ok ;
r_wmax([{I, V} | Tl], W, L) ->
	{_, NL} = windowmax(L, I, W, V),
	r_wmax(Tl, W, NL)
	.

rec_test(_Start, 0, _W, _L, _WM) ->
	ok
	;
rec_test(Start, N, W, L, WM) ->
	X = rand:uniform(),
	io:fwrite("~p~n~p~n~p:~p~n~n", [L, WM, Start-N, X]),
	{ListMax, NL} = listmax(L, W, X),
	{WindowMax, NWM} = windowmax(WM, Start-N, W, X),
	case ListMax == WindowMax of
		true -> rec_test(Start, N-1, W, NL, NWM);
		false ->
			io:fwrite("~n~p~n~p~n~p~n~p~n~n", [ListMax, NL, WindowMax, NWM]),
			error
	end
	.

listmax(L, W, X) ->
	{NL, _} = safesplit(W, [X | L]),
	{lists:max(NL), NL}
	.

-record(i, {i,v}).

windowmax([], I, _Window, X) -> {X, [#i{i=I, v=X}]} ;
windowmax(L, I, Window, X) when X >= (hd(L))#i.v ->
	InWindow = lists:takewhile(fun(#i{i=ElemI}) -> ElemI > (I-Window) end, L),
	HeadTrimmed = [#i{i=I,v=X} | lists:dropwhile(fun(#i{v=Val}) -> Val =< X end, InWindow)],
	{(lists:last(HeadTrimmed))#i.v, HeadTrimmed}
	;
windowmax(L, I, Window, X) when X < (hd(L))#i.v ->
	InWindow = [#i{i=I,v=X} | lists:takewhile(fun(#i{i=ElemI}) -> ElemI > (I-Window) end, L)],
	{(lists:last(InWindow))#i.v, InWindow}
	.

safesplit(N, L) when N > length(L) -> {L, []} ;
safesplit(N, L) -> lists:split(N, L) .


% get a new value X
% If X < head, push X, trim to window size, find max of list.  Max must be the tail?
% If X > head, remove all elements less than X from the list
%
% List is always [decreasing trend | xxx]
