#!/usr/bin/env python3.2

import os
import re
import subprocess
import sys
import time

def main():
	basedir = "apps/mbgw/ebin/"
	files = [basedir + f for f in os.listdir(basedir) if re.search("beam$", f)]
	print("files " + str(files))
	subprocs = [i for i in range(len(files))]
	for i in range(len(files)):
		paramlist = files[0:i] + files[i+1:]
		args = ["dialyzer", "--build_plt", "--output_plt", str(i) + ".plt"] + paramlist
		astr = " ".join(args)
		print("starting: " + astr)
		subprocs[i] = (astr, subprocess.Popen(args = args, stdout=subprocess.PIPE))

	while True:
		time.sleep(10)
		sys.stdout.write(".")
		sys.stdout.flush()
		for (a, p) in subprocs:
			if p.poll() is not None:
				print("\ngot one: " + astr)
				subprocs.remove((a,p))
		if len(subprocs) == 0:
			break


if __name__ == "__main__":
	main()

