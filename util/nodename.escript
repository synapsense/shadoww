#!/usr/bin/env escript
%%! -name test

main(_) ->
	NodeName = atom_to_list(node()),
	{match, [HostName]} = re:run(NodeName, "@(?<HOST>.*)", [{capture, ['HOST'], list}]),
	io:format("~s~n", [HostName])
	.

