
-module(statpusher).


-behaviour(gen_fsm).


% api functions
-export([start_link/0, is_configured/0]).

% behaviour callbacks
-export([init/1, handle_event/3, handle_sync_event/4, handle_info/3, terminate/3, code_change/4]).

% state functions
-export([configuring/2, configuring/3, waiting/2, waiting/3]).


-define(SERVER, ?MODULE).
-define(STATE, ?MODULE).
-define(PING_TIME, 60 * 1000). % 60 seconds
-define(UPDATE_TIME, 24 * 60 * 60 * 1000).  % 24 hours

-define(CONTROLLER, "CONTROLLER_STATUS").


-record(?STATE, {to, ping_timer, update_timer}).


start_link() ->
	gen_fsm:start_link({local, ?SERVER}, ?MODULE, [], [])
	.

is_configured() ->
	gen_fsm:sync_send_event(?SERVER, is_configured, infinity)
	.

% behaviour callbacks
init(_Args) ->
	case application:get_env(active) of
		{ok, false} ->
			{ok, waiting, #?STATE{}};
		IsActive when (IsActive == {ok, true}) or (IsActive == undefined) ->
			gen_fsm:send_event_after(0, configure),
			{ok, configuring, #?STATE{}}
	end
	.

configuring(configure, State) ->
	try
		{ok, ControllerTos} = esapi:get_objects_by_type(?CONTROLLER),
		Nodename = nodename(),
		HostName = string:substr(Nodename, string:str(Nodename, "@") + 1),
		PropertyList = [esapi:value_to("name", HostName), esapi:value_to("port", yaws_port()), esapi:value_to("productVersion", ac_version())],
		case object_type_exists(?CONTROLLER) of 
			true ->
				case {find_my_object(ControllerTos, HostName), length(ControllerTos) > 1} of
					{not_found, true}  ->
						lager:info("no controller object configured with my controller name. node name: ~p, configured: ~p. exiting...", [Nodename, ControllerTos]),
						ok = esapi:raise_alert("Controller not configured", "Active Control Critical Alert", 0, lists:flatten(io_lib:format("Controller with host name \"~s\" not configured!", [HostName])), []),
						{stop, not_configured, State};
					{not_found, false} ->
						{ok, To} = esapi:create_object(?CONTROLLER, PropertyList),
						{next_state, waiting, State#?STATE{to = To, ping_timer = gen_fsm:send_event_after(?PING_TIME, ping)}};
					{{ok, To}, _} ->
						ok = esapi:set_all_properties_values(To, PropertyList),
						{next_state, waiting, State#?STATE{to = To, ping_timer = gen_fsm:send_event_after(?PING_TIME, ping)}}
				end;
			_ ->
				gen_fsm:send_event_after(?PING_TIME, configure),
				{next_state, configuring, State}
		end
	catch
		error:{badmatch, {error, noreply}} ->
			lager:warning("ES is not responding.  Retry in ~Bs", [round(?PING_TIME/1000)]),
			gen_fsm:send_event_after(?PING_TIME, configure),
			{next_state, configuring, State};
		E:X ->
			lager:error("error looking up status object: {~p, ~p}", [E, X]),
			gen_fsm:send_event_after(?PING_TIME, configure),
			{next_state, configuring, State}
	end
	.


configuring(is_configured, _From, State) ->
	{reply, false, configuring, State}
	.

%% Not used as os_mon doesn't work for Windows
waiting(update, State) ->
	catch gen_fsm:cancel_timer(State#?STATE.update_timer),

	try
		DiskReports = lists:map(fun({Name, Size, Cap}) -> lists:flatten(io_lib:format("Disk: '~p', Size (MB): '~p', Capacity: '~p'", [Name, trunc(Size/1000), Cap])) end, disksup:get_disk_data()),
		DiskReportString = lists:foldl(fun(Str, Acc) -> Str ++ ";" ++ Acc end, "", DiskReports),
		ok = esapi:log_activity("ControlSystem", "ActiveControl", "SystemControlParameterChange", DiskReportString),

		{MemTotal, MemAllocated, _Worst} = memsup:get_memory_data(),
		MemReportString = lists:flatten(io_lib:format("Total memory (MB): ~p, Allocated memory (MB): ~p", [trunc(MemTotal / 1024 / 1024), trunc(MemAllocated / 1024 / 1024)])),
		ok = esapi:log_activity("ControlSystem", "ActiveControl", "SystemControlParameterChange", MemReportString),

		NewState = State#?STATE{update_timer = gen_fsm:send_event_after(?UPDATE_TIME, update)},
		{next_state, waiting, NewState}
	catch
		E:X ->
			lager:error("error logging status: {~p, ~p}", [E, X]),
			catch gen_fsm:cancel_timer(State#?STATE.ping_timer),
			gen_fsm:send_event_after(?PING_TIME, configure),
			{next_state, configuring, State}
	end
	;

waiting(ping, State) ->
	catch gen_fsm:cancel_timer(State#?STATE.ping_timer),
	To = State#?STATE.to,
	try
		ok = esapi:set_property_value(To, "active", is_active_node()),
		NewState = State#?STATE{ping_timer = gen_fsm:send_event_after(?PING_TIME, ping)},
		{next_state, waiting, NewState}
	catch
		E:X ->
			lager:error("error sending ES heartbeat: {~p, ~p}", [E, X]),
			catch gen_fsm:cancel_timer(State#?STATE.update_timer),
			gen_fsm:send_event_after(?PING_TIME, configure),
			{next_state, configuring, State}
	end
	;

waiting(Ev, State) ->
	lager:error("unexpected event. state: waiting, event: ~p, data: ~p", [Ev, State]),
	{next_state, waiting, State}
	.


waiting(is_configured, _From, State) ->
	{reply, true, waiting, State}
	.


handle_event(Ev, StateName, State) ->
	lager:error("Unexpected handle_event. event: ~p, state: ~p, data: ~p", [Ev, StateName, State]),
	{next_state, StateName, State}
	.


handle_sync_event(Ev, _From, StateName, State) ->
	lager:error("Unexpected handle_sync_event. event: ~p, state: ~p, data: ~p", [Ev, StateName, State]),
	{next_state, StateName, State}
	.


handle_info(Ev, StateName, State) ->
	lager:error("Unexpected handle_info. event: ~p, state: ~p, data: ~p", [Ev, StateName, State]),
	{next_state, StateName, State}
	.


terminate(not_configured, _StateName, _State) ->
	ok
	;

terminate(Reason, StateName, State) ->
	lager:info("terminating. reason: ~p, state: ~p, data: ~p", [Reason, StateName, State])
	.


code_change(_OldVsn, StateName, StateData, _Extra) ->
	{ok, StateName, StateData}
	.

% utility functions

find_my_object([], _HostName) ->
	not_found
	;
find_my_object(Tos, HostName) ->
	{ok, CollectionTos} = esapi:get_properties_values(Tos, ["name"]),
 	case lists:filter(
		fun(Cto) ->
			[{"name", EsHostName}] = esapi:to_proplist(Cto),
			string:to_lower(HostName) == string:to_lower(EsHostName)
		end, CollectionTos) of
		[] -> not_found;
		[MyObject] -> {ok, esapi:object_id(MyObject)}
	end
	.

nodename() ->
	atom_to_list(node())
	.

is_active_node() ->
	case lists:keyfind(active_control, 1, application:which_applications()) of
		{active_control, _Desc, _Vsn} ->
			1
			;
		false ->
			0
	end
	.

ac_version() ->
	case lists:keyfind(active_control, 1, application:loaded_applications()) of
		{active_control, _Desc, Vsn} -> Vsn;
		false -> "0"
	end
	.

yaws_port() ->
	{ok, _ , [[SConf]]} = yaws_server:getconf(),
	element(2, SConf).

object_type_exists(ObjectType) ->
	{ok, CreatedTypes} = esapi:get_object_types(),
	lists:member(ObjectType, CreatedTypes).
	
