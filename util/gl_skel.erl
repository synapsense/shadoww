
-module(adc_mgr).

-behaviour(gen_leader).

%% API
-export([
	start_link/1,
	]).

%% gen_leader callbacks
-export([
		init/1,
		handle_cast/3,
		handle_call/4,
		handle_info/3,
		handle_leader_call/4,
		handle_leader_cast/3,
		handle_DOWN/3,
		elected/3,
		surrendered/3,
		from_leader/3,
		code_change/4,
		terminate/2
	]).

%% internal functions
-export([
		check_running/0
	]).


-record(state, {
}).

%%%===================================================================
%%% API
%%%===================================================================

start_link(Nodes) when is_list(Nodes) ->
	gen_leader:start_link(?MODULE, Nodes, [], ?MODULE, [], [])
	.

%%%===================================================================
%%% gen_leader callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
	State = #state{},
	{ok, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called only in the leader process when it is elected. The Sync
%% term will be broadcasted to all the nodes in the cluster.
%%
%% @spec elected(State, Election, undefined) -> {ok, Sync, State}
%% @end
%%--------------------------------------------------------------------
elected(State = #state{}, Election, undefined) ->
	Sync = [],
	{ok, Sync, State}
	;

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called only in the leader process when a new candidate joins the
%% cluster. The Sync term will be sent to Node.
%%
%% @spec elected(State, Election, Node) -> {ok, Sync, State}
%% @end
%%--------------------------------------------------------------------
elected(State = #state{}, _Election, Node) ->
	Sync = [],
	{reply, Sync, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called in all members of the cluster except the leader. Sync is a
%% term returned by the leader in the elected/3 callback.
%%
%% @spec surrendered(State, Sync, Election) -> {ok, State}
%% @end
%%--------------------------------------------------------------------
surrendered(State = #state{}, Sync, _Election) ->
	{ok, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages. Called in the leader.
%%
%% @spec handle_leader_call(Request, From, State, Election) ->
%%                                            {reply, Reply, Broadcast, State} |
%%                                            {reply, Reply, State} |
%%                                            {noreply, State} |
%%                                            {stop, Reason, Reply, State} |
%%                                            {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_leader_call(_Request, _From, State = #state{}, _Election) ->
	{reply, ok, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages. Called in the leader.
%%
%% @spec handle_leader_cast(Request, State, Election) ->
%%                                            {ok, Broadcast, State} |
%%                                            {noreply, State} |
%%                                            {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_leader_cast(_Request, State = #state{}, _Election) ->
	{noreply, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling messages from leader.
%%
%% @spec from_leader(Request, State, Election) ->
%%                                    {ok, State} |
%%                                    {noreply, State} |
%%                                    {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
from_leader(Sync, State, _Election) ->
	lager:warning("unexpected from_leader(~p, ~p)", [Sync, State]),
	{ok, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling nodes going down. Called in the leader only.
%%
%% @spec handle_DOWN(Node, State, Election) ->
%%                                  {ok, State} |
%%                                  {ok, Broadcast, State}
%% @end
%%--------------------------------------------------------------------
handle_DOWN(Node, State, _El) ->
	lager:warning("DOWN message: ~p", [Node]),
	{ok, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State, Election) ->
%%                                   {reply, Reply, State} |
%%                                   {noreply, State} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(Request, _From, State = #state{}, _Election) ->
	lager:warning("Unexpected handle_call(message: ~p, state: ~p)", [Request, State]),
	{reply, huh, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State, Election) ->
%%                                  {noreply, State} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(Msg, State = #state{}, _Election) ->
	lager:warning("Unexpected handle_cast(message: ~p, state: ~p)", [Msg, State]),
	{noreply, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State, Election) -> {noreply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(Info, State = #state{}, _E) ->
	lager:warning("Unexpected handle_info(info: ~p, state: ~p", [Info, State]),
	{noreply, State}
	.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_leader when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_leader terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
	ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Election, Extra) ->
%%                                          {ok, NewState} |
%%                                          {ok, NewState, NewElection}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Election, _Extra) ->
	{ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================


