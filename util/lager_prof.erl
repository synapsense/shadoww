
-module(lager_prof).

-export([
		prof/1,
		log_lots/0,
		fprof/1
	]).

prof(StrPids) ->
	eprof:start(),
	Pids = [if is_list(P) -> list_to_pid(P); true -> P end || P <- StrPids],
	case eprof:profile(Pids, fun log_lots/0) of
		{ok, ok} ->
			eprof:analyze(total);
		{error, R} ->
			io:fwrite("error: ~p", [R])
	end,
	eprof:stop()
	.

log_lots() ->
	Msgs = 1000,
	[lager:info("log message ~b of ~b for eprof", [M, Msgs]) || M <- lists:seq(0,Msgs)],
	ok
	.

fprof(StrPids) ->
	fprof:start(),
	Pids = [if is_list(P) -> list_to_pid(P); true -> P end || P <- StrPids],
	fprof:apply(fun log_lots/0, [], [{procs, Pids}]),
	fprof:profile(),
	fprof:analyse([{cols, 160}, {dest, ""}]),
	fprof:stop()
	.


