
import fileinput

import re
import sys

pattern = r"\?LOG_(?P<level>(ERROR|WARNING|INFO|DEBUG))\(\[(?P<message>\"[^\"]+\")(, (?P<tuples>.*))?\]\)"
searcher = re.compile(pattern)

def newlog(matchobj):
	d = matchobj.groupdict()
	new_log = "lager:"
	if d["level"] == "ERROR":
		new_log += "error("
	elif d["level"] == "WARNING":
		new_log += "warning("
	elif d["level"] == "INFO":
		new_log += "info("
	elif d["level"] == "DEBUG":
		new_log += "debug("

	if d["tuples"] is None:
		return new_log + d["message"] + ")"
	else:
		return new_log + "[" + d["tuples"] +  "], " + d["message"] + ")"


for line in fileinput.input(inplace=1, backup=".bak"):
	sys.stdout.write(searcher.sub(newlog, line))

